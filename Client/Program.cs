//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Program.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.Common;
using Framework.MainUI;

namespace Framework.Client
{
	static class Program
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			ApplicationLoader.SystemLogon += ApplicationLoader_SystemLogon;
			ApplicationLoader.Startup(new string[] { });
		}

		static void ApplicationLoader_SystemLogon(IUser user)
		{
			ClientContext.SetSessionData();
		}
	}
}
