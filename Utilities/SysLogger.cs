﻿using System;
using NLog;

namespace Infrastructure.Utilities
{
	public static class SysLogger
	{
		public static Logger logger = LogManager.GetLogger("");

		public static void Write(string msg)
		{
			logger.Info(msg);
		}

		public static void Write(string id, string msg)
		{
			logger.Info($"({id}){msg}");
		}

		public static void WriteSql(string id, string sql)
		{
			logger.Debug($"({id}){sql}");
		}

		public static void WriteSql(string id, string account, string sql)
		{
			logger.Debug($"({id})({account}){sql}");
		}

		public static void WriteSql(string sql)
		{
			logger.Debug($"{sql}");
		}

		public static void WriteError(Exception ex)
		{
			logger.Error(ex.Message);
			logger.Error(ex.StackTrace); //记录异常的堆栈包含位置（代码行号）
		}

	}
}
