﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	[AttributeUsage(AttributeTargets.Property)]
	public class NotWritedAttribute : Attribute
	{
	}
}
