//==============================================================
//  版权所有：深圳杰文科技
//  文件名：CodeGenerator.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	public static class CodeGenerator
	{
		static void Test()
		{
			Console.WriteLine(Generate("001"));
			Console.WriteLine(Generate("001001"));
			Console.WriteLine(Generate("001002003"));

			Console.WriteLine(Generate("0010"));
		}

		/// <summary>
		/// 生成树级编码（在原编码的基础上最末尾递增1）。
		/// </summary>
		/// <param name="maxCode">原最大编码</param>
		/// <param name="len">每一级的长度</param>
		/// <param name="paddingChar"></param>
		/// <returns></returns>
		public static string Generate(string maxCode, int len = 3, char paddingChar = '0')
		{
			int max = 1;

			if (string.IsNullOrEmpty(maxCode))
			{
				return max.ToString().PadLeft(len, paddingChar);
			}

			if (maxCode.Length < len)
			{
				throw new Exception(string.Format("maxCode的长度不能小于{0}", len));
			}

			if (maxCode.Length % 3 != 0)
			{
				throw new Exception(string.Format("树级编码的长度必须是固定的 {0}", maxCode));
			}

			string prefix = maxCode.Substring(0, maxCode.Length - len);
			string code = maxCode.Substring(maxCode.Length - len, len);

			int n;
			if (!int.TryParse(code, out n))
			{
				throw new Exception(string.Format("{0} 无法解析为一个数字", code));
			}

			n++;
			if (n.ToString().Length > len)
			{
				throw new Exception("超过了树级编码的长度");
			}

			return prefix + n.ToString().PadLeft(len, paddingChar);
		}
	}
}
