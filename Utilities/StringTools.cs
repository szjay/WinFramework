//==============================================================
//  版权所有：深圳杰文科技
//  文件名：StringTools.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	public static class StringTools
	{
		public static string[] SplitArray(this string s, char separator = ',')
		{
			if (string.IsNullOrEmpty(s))
			{
				return new string[] { };
			}
			return s.Replace(" ", "").Split(separator);
		}

		/// <summary>
		/// 把字符串按分隔符增加双引号，以方便SQL使用In操作。
		/// </summary>
		/// <param name="s"></param>
		/// <param name="separator"></param>
		/// <returns></returns>
		public static string SplitAndQuotationMarks(this string s, char separator = ',')
		{
			string result = "";
			string[] values = s.SplitArray(separator);
			foreach (string value in values)
			{
				if (!string.IsNullOrWhiteSpace(value))
				{
					result += "'" + value.ToString() + "',";
				}
			}
			result = result.TrimEnd(',');
			return result;
		}

		public static string ToQuotationMarks(this IEnumerable<string> list, char separator = ',')
		{
			string result = "";
			foreach (string item in list)
			{
				if (!string.IsNullOrWhiteSpace(item))
				{
					result += "'" + item + "',";
				}
			}
			result = result.TrimEnd(',');
			return result;
		}

		public static string ToQuotationMarks(this IEnumerable<Guid> list, char separator = ',')
		{
			string result = "";
			foreach (Guid item in list)
			{
				if (!string.IsNullOrWhiteSpace(item.ToString()))
				{
					result += "'" + item.ToString() + "',";
				}
			}
			result = result.TrimEnd(',');
			return result;
		}

		public static string Merge(this IEnumerable<string> list, string separator = ",")
		{
			if (list.Count() == 0)
			{
				return "";
			}
			string s = "";
			foreach (string item in list)
			{
				if (item == null || string.IsNullOrEmpty(item.Trim()))
				{
					continue;
				}
				if (s != "")
				{
					s += separator;
				}
				s += item;
			}
			return s;
		}

		public static string Fmt(this string s, params object[] args)
		{
			return string.Format(s, args);
		}

		public static bool IsNullOrEmpty(this string s)
		{
			return string.IsNullOrEmpty(s);
		}

		public static bool IsNullOrWhiteSpace(this string s)
		{
			return string.IsNullOrWhiteSpace(s);
		}

		public static bool NotNullOrEmpty(this string s)
		{
			return !IsNullOrEmpty(s);
		}

		public static bool NotNullOrWhiteSpace(this string s)
		{
			return !string.IsNullOrWhiteSpace(s);
		}

		//Base64编码。
		public static string EncodeBase64(this string code)
		{
			string encode = "";
			byte[] bytes = Encoding.GetEncoding("utf-8").GetBytes(code);
			try
			{
				encode = Convert.ToBase64String(bytes);
			}
			catch
			{
				encode = code;
			}
			return encode;
		}

		//Base64解码。
		public static string DecodeBase64(this string code)
		{
			string decode = "";
			byte[] bytes = Convert.FromBase64String(code);
			try
			{
				decode = Encoding.GetEncoding("utf-8").GetString(bytes);
			}
			catch
			{
				decode = code;
			}
			return decode;
		}

		private static void Test()
		{
			string s = null;
			if (s.IsNullOrEmpty())
			{
				Console.WriteLine("NULL");
			}
			else
			{
				Console.WriteLine("Non Null");
			}
		}

		public static string LastStrting(this string s, int n)
		{
			return s.Remove(0, s.Length - n);
		}

		public static string OK(this string s, int n)
		{
			return "OK";
		}

		public static string ToEmptyString(this string str)
		{
			if (str == null)
			{
				return "";
			}
			return str.ToString();
		}
	}
}
