//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ObjectSerializer.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Infrastructure.Utilities
{
	public class ObjectSerializer
	{
		/// <summary>
		/// XmlSerializer无法序列化List，请改用数组。
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <param name="filename"></param>
		public static void XmlSerialize<T>(T obj, string filename)
		{
			XmlSerializer serializer = XmlSerializer.FromTypes(new Type[] { typeof(T) }).FirstOrDefault();
			TextWriter writer = new StreamWriter(filename);
			serializer.Serialize(writer, obj);
			writer.Close();
		}

		/// <summary>
		/// 如果文件不存在，则返回default(T)。
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static T XmlDeserialize<T>(string filename)
		{
			if (!File.Exists(filename))
			{
				return default(T);
			}
			using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				XmlReader reader = XmlReader.Create(fs);
				XmlSerializer serializer = XmlSerializer.FromTypes(new Type[] { typeof(T) }).FirstOrDefault();
				T obj = (T)serializer.Deserialize(reader);
				fs.Close();
				return obj;
			}
		}

		public static void XmlSerializeList<T>(List<T> list, string filename)
		{
			XmlDocument xd = new XmlDocument();
			using (StringWriter sw = new StringWriter())
			{
				XmlSerializer xz = new XmlSerializer(list.GetType());
				xz.Serialize(sw, list);
				xd.LoadXml(sw.ToString());
				xd.Save(filename);
			}
		}

		public static List<T> XmlDeserializeList<T>(string fileName)
		{
			List<T> list = new List<T>();
			using (XmlReader reader = XmlReader.Create(fileName))
			{
				XmlSerializer xz = new XmlSerializer(list.GetType());
				list = (List<T>)xz.Deserialize(reader);
				return list;
			}
		}

		public static T XmlDeserializeObject<T>(string xml)
		{
			byte[] buffer = Encoding.UTF8.GetBytes(xml);
			using (MemoryStream ms = new MemoryStream(buffer))
			{
				XmlSerializer xz = new XmlSerializer(typeof(T));
				object obj = xz.Deserialize(ms);
				return (T)obj;
			}
		}

		/// <summary>
		/// 将传入的对象obj序列化为byte数组
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static byte[] Serialize(object obj)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				new BinaryFormatter().Serialize(ms, obj);
				return ms.ToArray();
			}
		}

		public static void Serialize(object obj, string fileName)
		{
			byte[] bytes = Serialize(obj);
			File.WriteAllBytes(fileName, bytes);
		}

		public static void Serialize(object obj, string fileName, string key, string iv)
		{
			byte[] bytes = Serialize(obj);
			bytes = Encrypt.EncryptBytes(bytes, key, iv);
			File.WriteAllBytes(fileName, bytes);
		}

		/// <summary>
		/// 将传入的byte数组反序列化为对象
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public static object Deserialize(byte[] bytes)
		{
			using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
			{
				return new BinaryFormatter().Deserialize(ms);
			}
		}

		public static object Deserialize(string fileName)
		{
			byte[] bytes = File.ReadAllBytes(fileName);
			object obj = Deserialize(bytes);
			return obj;
		}

		public static object Deserialize(string fileName, string key, string iv)
		{
			byte[] bytes = File.ReadAllBytes(fileName);
			bytes = Encrypt.DecryptBytes(bytes, key, iv);
			object obj = Deserialize(bytes);
			return obj;
		}
	}
}
