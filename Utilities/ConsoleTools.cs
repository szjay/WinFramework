//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ConsoleTools.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Infrastructure.Utilities
{
	public static class ConsoleTools
	{
		[DllImport("User32.dll", EntryPoint = "FindWindow")]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll", EntryPoint = "FindWindowEx")]   //找子窗体   
		private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

		[DllImport("User32.dll", EntryPoint = "SendMessage")]   //用于发送信息给窗体   
		private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);

		[DllImport("User32.dll", EntryPoint = "ShowWindow")]   //
		private static extern bool ShowWindow(IntPtr hWnd, int type);

		//最小化控制台窗口。
		public static void MinimizeConsole(string title)
		{
			IntPtr ParenthWnd = new IntPtr(0);
			IntPtr et = new IntPtr(0);
			ParenthWnd = FindWindow(null, title);
			ShowWindow(ParenthWnd, 2);//隐藏本dos窗体, 0: 后台执行；1:正常启动；2:最小化到任务栏；3:最大化

		}
	}
}
