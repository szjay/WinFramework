//==============================================================
//  版权所有：深圳杰文科技
//  文件名：NumberGenerator.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	public class NumberGenerator
	{
		public const string Prefix = "yyMMdd";

		public static string Generate()
		{
			return Generate("", 4);
		}

		/// <summary>
		/// 给编号的流水号递增1，默认编号后4位是流水号。
		/// </summary>
		/// <param name="number">要递增流水号的编号。编号可以有字母前缀，可以无限长度。例如PO201512310001、1512310002</param>
		/// <returns></returns>
		public static string Generate(string number)
		{
			return Generate(number, 4);
		}

		/// <summary>
		/// 给编号的流水号递增1。
		/// </summary>
		/// <param name="number">要递增流水号的编号。编号可以有字母前缀，可以无限长度。例如PO201512310001、1512310002</param>
		/// <param name="length">流水号长度</param>
		/// <returns></returns>
		public static string Generate(string number, int length)
		{
			if (string.IsNullOrEmpty(number))
			{
				return DateTime.Now.ToString(Prefix) + "1".PadLeft(length, '0'); //默认为“年月日+流水号”。
			}

			string yyMMdd = number.Substring(0, number.Length - length); //获得前缀和年月日。
			string code = number.Substring(yyMMdd.Length, length); //获得后四位流水号。

			long no;
			if (!long.TryParse(code, out no))
			{
				throw new Exception("生成单据编号错误，无法解析流水号");
			}

			no++;
			code = yyMMdd + no.ToString().PadLeft(length, '0');
			return code;
		}

		private static void test()
		{
			Console.WriteLine(Generate("PO201512310001"));
			Console.WriteLine(Generate("201512311002"));
			Console.WriteLine(Generate("SO1512311003"));
			Console.WriteLine(Generate("1512311004"));
		}
	}
}

