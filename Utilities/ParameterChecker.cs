//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ParameterChecker.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	public static class ParameterChecker
	{
		public static Type ExceptionType = typeof(Exception);

		public static void Check(object parameter)
		{
			Check(null, parameter);
		}

		public static void Check(string name, object parameter)
		{
			if (parameter == null)
			{
				string message = "参数不能为空";
				if (!string.IsNullOrEmpty(name))
				{
					message += "：" + name;
				}
				Exception ex = (Exception)Activator.CreateInstance(ExceptionType, message);
			}
		}
	}
}
