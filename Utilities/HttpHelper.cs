//==============================================================
//  版权所有：深圳杰文科技
//  文件名：HttpHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//
//  修改者：Jay
//  修改时间：2021-9-14
//  修改说明：增加Post(string url, object postbody, string header)
//
//  修改者：Jay
//  修改时间：2021-10-8
//  修改说明：增加request.KeepAlive = false;

//==============================================================

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Infrastructure.Utilities
{
	public static class HttpHelper
	{
		public static string Post(string url, object postbody)
		{
			return Post(url, postbody, "");
		}

		public static string Post(string url, object postbody, string header)
		{
			var data = new JavaScriptSerializer
			{
				MaxJsonLength = 102400000
			}.Serialize(postbody);

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			byte[] bytes = Encoding.UTF8.GetBytes(data);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = bytes.Length;
			request.KeepAlive = false;

			if (!header.IsNullOrEmpty())
			{
				string[] str = header.Split(new char[] { '=' }, 2);
				if (str == null || str.Length != 2)
				{
					throw new Exception("无效参数：header。header必须以name=value形式传递");
				}
				request.Headers.Add(str[0], str[1]);
			}

			var rq = request.GetRequestStream();
			rq.Write(bytes, 0, bytes.Length);
			rq.Close();
			var response = (HttpWebResponse)request.GetResponse();
			var result = new StreamReader(response.GetResponseStream()).ReadToEnd();
			return result;
		}

		public static string PostJson(string url, string jsonStr, string header)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(jsonStr);
			return PostBytes(url, bytes, header);
		}

		public static string PostBytes(string url, byte[] bytes, string header)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.Accept = "*/*";
			request.KeepAlive = true;
			request.ContentLength = bytes.Length;

			if (!header.IsNullOrEmpty())
			{
				string[] str = header.Split('=');
				if (str == null || str.Length != 2)
				{
					throw new Exception("无效参数：header。header必须以name=value形式传递");
				}
				request.Headers.Add(str[0], str[1]);
			}

			var rq = request.GetRequestStream();
			rq.Write(bytes, 0, bytes.Length);
			rq.Close();
			var response = (HttpWebResponse)request.GetResponse();
			var result = new StreamReader(response.GetResponseStream()).ReadToEnd();
			return result;
		}

		public static string Get(string url)
		{
			return Get(url, "");
		}

		public static string Get(string url, string args)
		{
			return Get(url, args, "");
		}

		public static string Get(string url, string args, string header)
		{
			url = args.IsNullOrEmpty() ? url : url + "?" + args;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "GET";
			request.KeepAlive = false;

			if (!header.IsNullOrEmpty())
			{
				string[] str = header.Split(new char[] { '=' }, 2);
				if (str == null || str.Length != 2)
				{
					throw new Exception("无效参数：header。header必须以name=value形式传递");
				}
				request.Headers.Add(str[0], str[1]);
			}

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Stream myResponseStream = response.GetResponseStream();
			StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8);
			string result = myStreamReader.ReadToEnd();
			myStreamReader.Close();
			myResponseStream.Close();
			return result;
		}

	}
}
