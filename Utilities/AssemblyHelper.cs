//==============================================================
//  版权所有：深圳杰文科技
//  文件名：AssemblyHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Configuration;

namespace Infrastructure.Utilities
{
	/// <summary>
	/// 包含一些与程序集处理相关的辅助类，以简化程序集处理。
	/// </summary>
	public static class AssemblyHelper
	{
		/// <summary>
		/// 获得指定路径下的所有程序集。
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static List<Assembly> LoadAll(string path, string nameStrategy)
		{
			List<Assembly> asmList = new List<Assembly>();

			if (string.IsNullOrEmpty(nameStrategy))
			{
				nameStrategy = "*";
			}
			else
			{
				nameStrategy = nameStrategy + ".*";
			}

			//注意，为提高效率，不搜索子目录。
			string[] files1 = Directory.GetFiles(path, nameStrategy + ".dll", SearchOption.TopDirectoryOnly);
			string[] files2 = Directory.GetFiles(path, nameStrategy + ".exe", SearchOption.TopDirectoryOnly);
			foreach (string file in files1.Concat(files2))
			{
				try
				{
					Assembly asm = Assembly.LoadFile(file);
					asmList.Add(asm);
				}
				catch
				{
				}
			}
			return asmList;
		}

	}
}
