//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpdUtil.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Infrastructure.Utilities
{
	public static class UpdUtil
	{
		/// <summary>
		/// UDP局域网广播。
		/// </summary>
		/// <param name="port">UDP广播端口</param>
		/// <param name="msg">广播的消息</param>
		public static void Send(int port, string msg)
		{
			using (UdpClient client = new UdpClient(new IPEndPoint(IPAddress.Any, 0)))
			{
				IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse("255.255.255.255"), port);
				byte[] buf = Encoding.Default.GetBytes(msg);
				client.Send(buf, buf.Length, endpoint);
			}
		}

		/// <summary>
		/// UDP局域网接收广播。
		/// </summary>
		public static string RecvThread(int port)
		{
			UdpClient client = new UdpClient(new IPEndPoint(IPAddress.Any, port));
			IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, 0);
			while (true)
			{
				byte[] buf = client.Receive(ref endpoint);
				string msg = Encoding.Default.GetString(buf);
				Console.WriteLine(msg);
			}
		}
	}
}
