//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DataValidator.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace Infrastructure.Utilities
{
	/// <summary>
	/// 数据校验。
	/// </summary>
	[DebuggerStepThrough]
	public class DataValidator
	{
		private StringBuilder _Content = new StringBuilder();
		private Type _CustomExceptionType = null;

		public DataValidator()
		{
			Pass = true;
		}

		public DataValidator(Type customExceptionType)
			: this()
		{
			_CustomExceptionType = customExceptionType;
		}

		//此方法通常用于比较耗时、耗资源的检查。
		//其思路为：因为本次检查太耗时，如果前面有未通过的检查，那么本次检查就可以跳过了。
		public DataValidator CheckIfBeforePass(bool expression, string msg, params object[] args)
		{
			if (Pass)
			{
				return Check(expression, msg, args);
			}
			return this;
		}

		public DataValidator Check(bool expression, string msg, params object[] args)
		{
			if (!Pass) //只有前面的检查通过了，本次检查才有意义。
			{
				return this;
			}

			Pass = !expression;
			if (!Pass)
			{
				_Content.AppendLine(expression ? string.Format(msg, args) : null);
				ThrowExceptionIfFailed();
			}
			return this;
		}

		public bool End(out string msg)
		{
			msg = Content;
			return Pass;
		}

		public bool Pass
		{
			get;
			private set;
		}

		public string Content
		{
			get
			{
				return _Content.ToString();
			}
		}

		/// <summary>
		/// 如果校验失败，那么显示错误信息，否则不显示任何提示。
		/// </summary>
		public DataValidator ShowMsgIfFailed()
		{
			if (!Pass)
			{
				MessageBox.Show(Content, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return this;
		}

		/// <summary>
		/// 如果检验失败，那么用自定义的方式显示信息，否则不显示任何提示。
		/// </summary>
		/// <param name="showMsgMethod"></param>
		public DataValidator CustomShowMsgIfFailed(Action<string> showMsgMethod)
		{
			if (!Pass && showMsgMethod != null)
			{
				showMsgMethod(Content);
			}
			return this;
		}

		/// <summary>
		/// 如果检验失败，那么抛出异常。
		/// </summary>
		public void ThrowExceptionIfFailed()
		{
			if (!Pass && _CustomExceptionType != null)
			{
				Exception ex = Activator.CreateInstance(_CustomExceptionType, Content) as Exception;
				throw ex;
			}
		}

	}
}
