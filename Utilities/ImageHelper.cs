//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ImageHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Infrastructure.Utilities
{
	public static class ImageHelper
	{
		/// <summary>
		/// 转换为缩略图。
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		public static byte[] ConvertThumbnail(byte[] image)
		{
			using (MemoryStream ms = new MemoryStream(image))
			{
				using (Image img = Image.FromStream(ms))
				{
					if (img.Width == 0 || img.Height == 0)
					{
						return null;
					}

					float w = Math.Min(img.Width, 160f);
					float h = Math.Min(img.Height, 120f);

					if (img.Width / w >= img.Height / h)
					{
						h = img.Height * w / img.Width;
					}
					else
					{
						w = img.Width * h / img.Height;
					}

					using (Bitmap bmp = new Bitmap((int)w, (int)h))
					{
						using (Graphics g = Graphics.FromImage(bmp))
						{
							g.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height));
						}

						return Convert16bppArray(bmp);
					}
				}
			}
		}

		/// <summary>
		/// 转换为256色位图并获取字节数组
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
		private static byte[] Convert16bppArray(Bitmap src)
		{
			//转换为256色
			using (var dst = new Bitmap(src.Width, src.Height, PixelFormat.Format16bppRgb565))
			{
				using (var g = Graphics.FromImage(dst))
				{
					g.DrawImageUnscaled(src, 0, 0);
				}

				using (var ms = new MemoryStream())
				{
					dst.Save(ms, ImageFormat.Bmp);
					return ms.ToArray();
				}
			}
		}
	}
}
