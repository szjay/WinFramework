//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DosHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Infrastructure.Utilities
{
	public static class DosHelper
	{
		public static string Execute(string command)
		{
			return Execute(command, 0);
		}

		public static string Execute(string command, int seconds)
		{
			string output = ""; //输出字符串  
			if (command != null && !command.Equals(""))
			{
				Process process = new Process();//创建进程对象  
				ProcessStartInfo startInfo = new ProcessStartInfo();
				startInfo.FileName = "cmd.exe";//设定需要执行的命令  
				startInfo.Arguments = "/C " + command;//“/C”表示执行完命令后马上退出  
				startInfo.UseShellExecute = false;//不使用系统外壳程序启动  
				startInfo.RedirectStandardInput = false;//不重定向输入  
				startInfo.RedirectStandardOutput = true; //重定向输出  
				startInfo.CreateNoWindow = true;//不创建窗口  
				process.StartInfo = startInfo;
				try
				{
					if (process.Start())//开始进程  
					{
						if (seconds == 0)
						{
							process.WaitForExit();//这里无限等待进程结束  
						}
						else
						{
							process.WaitForExit(seconds); //等待进程结束，等待时间为指定的毫秒  
						}
						output = process.StandardOutput.ReadToEnd();//读取进程的输出  
					}
				}
				catch (Exception ex)
				{
					return ex.Message;
				}
				finally
				{
					if (process != null)
					{
						process.Close();
					}
				}
			}
			return output;
		}
	}
}
