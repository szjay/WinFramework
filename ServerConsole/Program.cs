//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Program.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.HandlerBase;
using Framework.Server;
using Infrastructure.Utilities;

namespace ServerConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Title = "服务控制台";
			Console.WriteLine(string.Format("版本号：{0}", ServiceContext.ServerVersion));
			Console.WriteLine(string.Format("{0} 开始启动服务...", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

			QueryBuilder.Database = QueryBuilder.DatabaseType.SqlServer;

			HandlerHost.Init();
			HandlerHost.RegisterHandler(typeof(BaseHandler).Assembly.GetTypes(), typeof(BaseHandler));

			int cnt = SessionCache.Load();
			Console.WriteLine($"装载缓存Session数：{cnt}");

			Console.WriteLine(string.Format("{0} 服务启动完毕", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
			Console.ReadLine();
		}
	}
}
