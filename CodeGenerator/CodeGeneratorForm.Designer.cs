﻿namespace CodeGenerator
{
	partial class CodeGeneratorForm
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeGeneratorForm));
			this.label3 = new System.Windows.Forms.Label();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.txtFind = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.txtSql = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.txtWhere = new System.Windows.Forms.TextBox();
			this.txtRelationTableNames = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.chkHasSerializable = new System.Windows.Forms.CheckBox();
			this.chkYbs = new System.Windows.Forms.CheckBox();
			this.chkAutoAppendWhere = new System.Windows.Forms.CheckBox();
			this.btnParseTableName = new System.Windows.Forms.Button();
			this.btnAppendWhere = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.btnGenerate = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtMainTable = new System.Windows.Forms.TextBox();
			this.btnRefreshClass = new System.Windows.Forms.Button();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.cmbProject = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.btnRefreshProject = new System.Windows.Forms.Button();
			this.btnNewProject = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel6.SuspendLayout();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(20, 20);
			this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(298, 46);
			this.label3.TabIndex = 8;
			this.label3.Text = "Where条件替换";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// treeView1
			// 
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.FullRowSelect = true;
			this.treeView1.HideSelection = false;
			this.treeView1.Location = new System.Drawing.Point(0, 72);
			this.treeView1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.treeView1.Name = "treeView1";
			this.treeView1.Size = new System.Drawing.Size(564, 912);
			this.treeView1.TabIndex = 11;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// txtFind
			// 
			this.txtFind.Location = new System.Drawing.Point(118, 18);
			this.txtFind.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.txtFind.Name = "txtFind";
			this.txtFind.Size = new System.Drawing.Size(276, 35);
			this.txtFind.TabIndex = 0;
			this.txtFind.WordWrap = false;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(0, 26);
			this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(106, 24);
			this.label4.TabIndex = 14;
			this.label4.Text = "快速定位";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtSql);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(564, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1404, 1124);
			this.panel1.TabIndex = 20;
			// 
			// txtSql
			// 
			this.txtSql.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtSql.Location = new System.Drawing.Point(0, 72);
			this.txtSql.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.txtSql.Multiline = true;
			this.txtSql.Name = "txtSql";
			this.txtSql.Size = new System.Drawing.Size(1044, 912);
			this.txtSql.TabIndex = 20;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(1044, 72);
			this.label1.TabIndex = 19;
			this.label1.Text = "SQL";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label3);
			this.panel3.Controls.Add(this.txtWhere);
			this.panel3.Controls.Add(this.txtRelationTableNames);
			this.panel3.Controls.Add(this.label5);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel3.Location = new System.Drawing.Point(1044, 0);
			this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(360, 984);
			this.panel3.TabIndex = 34;
			// 
			// txtWhere
			// 
			this.txtWhere.Location = new System.Drawing.Point(24, 72);
			this.txtWhere.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.txtWhere.Multiline = true;
			this.txtWhere.Name = "txtWhere";
			this.txtWhere.Size = new System.Drawing.Size(308, 290);
			this.txtWhere.TabIndex = 25;
			this.txtWhere.Text = " and 1=2";
			// 
			// txtRelationTableNames
			// 
			this.txtRelationTableNames.Location = new System.Drawing.Point(24, 528);
			this.txtRelationTableNames.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.txtRelationTableNames.Multiline = true;
			this.txtRelationTableNames.Name = "txtRelationTableNames";
			this.txtRelationTableNames.Size = new System.Drawing.Size(308, 306);
			this.txtRelationTableNames.TabIndex = 29;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(22, 476);
			this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(314, 46);
			this.label5.TabIndex = 30;
			this.label5.Text = "关联表";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.chkHasSerializable);
			this.panel2.Controls.Add(this.chkYbs);
			this.panel2.Controls.Add(this.chkAutoAppendWhere);
			this.panel2.Controls.Add(this.btnParseTableName);
			this.panel2.Controls.Add(this.btnAppendWhere);
			this.panel2.Controls.Add(this.checkBox1);
			this.panel2.Controls.Add(this.btnGenerate);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.txtMainTable);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 984);
			this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1404, 140);
			this.panel2.TabIndex = 33;
			// 
			// chkHasSerializable
			// 
			this.chkHasSerializable.AutoSize = true;
			this.chkHasSerializable.Checked = true;
			this.chkHasSerializable.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkHasSerializable.Location = new System.Drawing.Point(1018, 14);
			this.chkHasSerializable.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.chkHasSerializable.Name = "chkHasSerializable";
			this.chkHasSerializable.Size = new System.Drawing.Size(258, 28);
			this.chkHasSerializable.TabIndex = 33;
			this.chkHasSerializable.Text = "需要[Serializable]";
			this.chkHasSerializable.UseVisualStyleBackColor = true;
			// 
			// chkYbs
			// 
			this.chkYbs.AutoSize = true;
			this.chkYbs.Location = new System.Drawing.Point(764, 88);
			this.chkYbs.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.chkYbs.Name = "chkYbs";
			this.chkYbs.Size = new System.Drawing.Size(150, 28);
			this.chkYbs.TabIndex = 32;
			this.chkYbs.Text = "YBS数据库";
			this.chkYbs.UseVisualStyleBackColor = true;
			// 
			// chkAutoAppendWhere
			// 
			this.chkAutoAppendWhere.AutoSize = true;
			this.chkAutoAppendWhere.Checked = true;
			this.chkAutoAppendWhere.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkAutoAppendWhere.Location = new System.Drawing.Point(764, 14);
			this.chkAutoAppendWhere.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.chkAutoAppendWhere.Name = "chkAutoAppendWhere";
			this.chkAutoAppendWhere.Size = new System.Drawing.Size(198, 28);
			this.chkAutoAppendWhere.TabIndex = 27;
			this.chkAutoAppendWhere.Text = "自动追加Where";
			this.chkAutoAppendWhere.UseVisualStyleBackColor = true;
			// 
			// btnParseTableName
			// 
			this.btnParseTableName.Location = new System.Drawing.Point(328, 64);
			this.btnParseTableName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnParseTableName.Name = "btnParseTableName";
			this.btnParseTableName.Size = new System.Drawing.Size(190, 48);
			this.btnParseTableName.TabIndex = 31;
			this.btnParseTableName.Text = "解析表名";
			// 
			// btnAppendWhere
			// 
			this.btnAppendWhere.Location = new System.Drawing.Point(532, 64);
			this.btnAppendWhere.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnAppendWhere.Name = "btnAppendWhere";
			this.btnAppendWhere.Size = new System.Drawing.Size(190, 48);
			this.btnAppendWhere.TabIndex = 26;
			this.btnAppendWhere.Text = "追加Where";
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(120, 64);
			this.checkBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(186, 28);
			this.checkBox1.TabIndex = 24;
			this.checkBox1.Text = "手动指定主表";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// btnGenerate
			// 
			this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGenerate.Location = new System.Drawing.Point(1208, 44);
			this.btnGenerate.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnGenerate.Name = "btnGenerate";
			this.btnGenerate.Size = new System.Drawing.Size(190, 90);
			this.btnGenerate.TabIndex = 23;
			this.btnGenerate.Text = "生成代码(&G)";
			this.btnGenerate.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 14);
			this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(82, 46);
			this.label2.TabIndex = 21;
			this.label2.Text = "主表";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtMainTable
			// 
			this.txtMainTable.Location = new System.Drawing.Point(120, 10);
			this.txtMainTable.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.txtMainTable.Name = "txtMainTable";
			this.txtMainTable.Size = new System.Drawing.Size(598, 35);
			this.txtMainTable.TabIndex = 22;
			// 
			// btnRefreshClass
			// 
			this.btnRefreshClass.Location = new System.Drawing.Point(410, 18);
			this.btnRefreshClass.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnRefreshClass.Name = "btnRefreshClass";
			this.btnRefreshClass.Size = new System.Drawing.Size(142, 40);
			this.btnRefreshClass.TabIndex = 28;
			this.btnRefreshClass.Text = "刷新Class";
			this.btnRefreshClass.UseVisualStyleBackColor = true;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.treeView1);
			this.panel4.Controls.Add(this.panel5);
			this.panel4.Controls.Add(this.panel6);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(564, 1124);
			this.panel4.TabIndex = 22;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.txtFind);
			this.panel5.Controls.Add(this.label4);
			this.panel5.Controls.Add(this.btnRefreshClass);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel5.Location = new System.Drawing.Point(0, 0);
			this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(564, 72);
			this.panel5.TabIndex = 15;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.cmbProject);
			this.panel6.Controls.Add(this.label6);
			this.panel6.Controls.Add(this.btnRefreshProject);
			this.panel6.Controls.Add(this.btnNewProject);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel6.Location = new System.Drawing.Point(0, 984);
			this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(564, 140);
			this.panel6.TabIndex = 16;
			// 
			// cmbProject
			// 
			this.cmbProject.FormattingEnabled = true;
			this.cmbProject.Location = new System.Drawing.Point(88, 10);
			this.cmbProject.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.cmbProject.MaxDropDownItems = 20;
			this.cmbProject.Name = "cmbProject";
			this.cmbProject.Size = new System.Drawing.Size(442, 32);
			this.cmbProject.Sorted = true;
			this.cmbProject.TabIndex = 1;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(10, 16);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(70, 24);
			this.label6.TabIndex = 0;
			this.label6.Text = " 项目";
			// 
			// btnRefreshProject
			// 
			this.btnRefreshProject.Location = new System.Drawing.Point(262, 68);
			this.btnRefreshProject.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnRefreshProject.Name = "btnRefreshProject";
			this.btnRefreshProject.Size = new System.Drawing.Size(184, 40);
			this.btnRefreshProject.TabIndex = 30;
			this.btnRefreshProject.Text = "刷新项目列表";
			this.btnRefreshProject.UseVisualStyleBackColor = true;
			// 
			// btnNewProject
			// 
			this.btnNewProject.Location = new System.Drawing.Point(88, 68);
			this.btnNewProject.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.btnNewProject.Name = "btnNewProject";
			this.btnNewProject.Size = new System.Drawing.Size(162, 40);
			this.btnNewProject.TabIndex = 29;
			this.btnNewProject.Text = "新建项目";
			this.btnNewProject.UseVisualStyleBackColor = true;
			// 
			// CodeGeneratorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1968, 1124);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel4);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
			this.Name = "CodeGeneratorForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "代码生成器 v1.3.903";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.TextBox txtFind;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox chkYbs;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnParseTableName;
		private System.Windows.Forms.TextBox txtSql;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtRelationTableNames;
		private System.Windows.Forms.TextBox txtMainTable;
		private System.Windows.Forms.Button btnGenerate;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button btnRefreshClass;
		private System.Windows.Forms.TextBox txtWhere;
		private System.Windows.Forms.Button btnAppendWhere;
		private System.Windows.Forms.CheckBox chkAutoAppendWhere;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.ComboBox cmbProject;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox chkHasSerializable;
		private System.Windows.Forms.Button btnNewProject;
		private System.Windows.Forms.Button btnRefreshProject;
	}
}

