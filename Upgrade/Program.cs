//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Program.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Framework.Upgrade
{
	static class Programe
	{
		[STAThread]
		static void Main(string[] args)
		{
			if (args == null || args.Length == 0) //如果没有参数，则更新服务器，否则更新客户端。
			{
				throw new Exception("无效的参数");
			}
			else if (args.Length == 1)
			{
				ServerUpgrade.Update(args);
			}
			else
			{
				ClientUpgrade.Update(args);
			}
		}
	}
}
