//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeConfig.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Framework.Upgrade
{
	/// <summary>
	/// 为让UpgradeClient完全独立，因此这里也定义了一个与MainUI中结构兼容的UpgradeConfig。
	/// </summary>
	[Serializable]
	public class UpgradeConfig
	{
		public UpgradeConfig()
		{
			VersionNo = 0;
		}

		public long VersionNo
		{
			get;
			set;
		}

		public DateTime UpgradeTime
		{
			get;
			set;
		}

		public string[] UpgradeFiles
		{
			get;
			set;
		}

		private static string path = Path.Combine(Application.StartupPath, "Upgrade.Config");

		public void Save()
		{
			XmlSerializer serializer = new XmlSerializer(typeof(UpgradeConfig));
			TextWriter writer = new StreamWriter(path);
			serializer.Serialize(writer, this);
			writer.Close();
		}

		public static UpgradeConfig Load()
		{
			if (!File.Exists(path))
			{
				return new UpgradeConfig();
			}
			FileStream fs = new FileStream(path, FileMode.Open);
			XmlReader reader = XmlReader.Create(fs);
			XmlSerializer serializer = new XmlSerializer(typeof(UpgradeConfig));
			UpgradeConfig obj = (UpgradeConfig)serializer.Deserialize(reader);
			fs.Close();
			return obj;
		}
	}
}
