﻿namespace Infrastructure.Components
{
	partial class ComboxMemoUC
	{
		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 组件设计器生成的代码

		/// <summary> 
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// memoEdit1
			// 
			this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.memoEdit1.Location = new System.Drawing.Point(0, 0);
			this.memoEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Size = new System.Drawing.Size(316, 88);
			this.memoEdit1.TabIndex = 0;
			// 
			// comboBoxEdit1
			// 
			this.comboBoxEdit1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.comboBoxEdit1.Location = new System.Drawing.Point(0, 60);
			this.comboBoxEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.comboBoxEdit1.Name = "comboBoxEdit1";
			this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.comboBoxEdit1.Properties.DropDownRows = 20;
			this.comboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.comboBoxEdit1.Size = new System.Drawing.Size(316, 28);
			this.comboBoxEdit1.TabIndex = 1;
			// 
			// ComboxMemoUC
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.comboBoxEdit1);
			this.Controls.Add(this.memoEdit1);
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "ComboxMemoUC";
			this.Size = new System.Drawing.Size(316, 88);
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.MemoEdit memoEdit1;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
	}
}
