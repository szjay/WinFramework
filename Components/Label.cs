//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Label.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Infrastructure.Components
{
	/// <summary>
	/// 标签控件。
	/// </summary>
	[ToolboxItem(false)]
	public class Label : System.Windows.Forms.Label
	{
		public Label()
		{
			this.AutoSize = false; //去掉自动尺寸。
			this.Height = 20; //默认高度20。
			this.Width = 100; //默认宽度100。
			this.TextAlign = System.Drawing.ContentAlignment.MiddleRight; //右对齐。
		}

		[Description("文本右对齐")]
		public bool AlignRight
		{
			get
			{
				return this.TextAlign == System.Drawing.ContentAlignment.MiddleRight;
			}
			set
			{
				this.TextAlign = value ? System.Drawing.ContentAlignment.MiddleRight : System.Drawing.ContentAlignment.MiddleLeft;
			}
		}

		[Description("文本左对齐")]
		public bool AlignLeft
		{
			get
			{
				return this.TextAlign == System.Drawing.ContentAlignment.MiddleLeft;
			}
			set
			{
				this.TextAlign = value ? System.Drawing.ContentAlignment.MiddleLeft : System.Drawing.ContentAlignment.MiddleRight;
			}
		}

		[Description("居中对象")]
		public bool AlignCenter
		{
			get
			{
				return this.TextAlign == System.Drawing.ContentAlignment.MiddleCenter;
			}
			set
			{
				this.TextAlign = value ? System.Drawing.ContentAlignment.MiddleCenter : System.Drawing.ContentAlignment.MiddleRight;
			}
		}
	}
}
