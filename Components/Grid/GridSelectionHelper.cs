//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridSelectionHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace Infrastructure.Components
{
	public static class GridSelectionHelper
	{
		internal static void Set(GridControl grid)
		{
			foreach (BaseView view in grid.Views)
			{
				GridView gridView = view as GridView;
				if (gridView == null)
				{
					continue;
				}

				gridView.DoubleClick += new System.EventHandler(gridView_DoubleClick);
				gridView.MouseUp += new MouseEventHandler(gridView_MouseUp);

				//GridLayoutController.SaveInitStyle(view.Name, view); //先保存最新的设计时风格。
				//GridLayoutController.RestoreStyle(view.Name, view); //恢复自定义的风格。
			}
		}

		private static void gridView_MouseUp(object sender, MouseEventArgs e)
		{
			GridView gridView = sender as GridView;
			GridColumn col = gridView.Columns.ColumnByFieldName("IsSelected");
			if (col == null)
			{
				return;
			}

			GridHitInfo hitInfo = gridView.CalcHitInfo(e.Location) as GridHitInfo;

			//如果是在列的指示符上点击鼠标，那么就认为是要做批量选择。
			if (hitInfo.HitTest == GridHitTest.RowIndicator || hitInfo.HitTest == GridHitTest.RowEdge)
			{
				ProcessSelection(gridView);
			}
		}

		private static void ProcessSelection(GridView gridView)
		{
			try
			{
				for (int i = 0; i < gridView.RowCount; i++)
				{
					dynamic obj = gridView.GetRow(i);
					if (obj == null)
					{
						break;
					}
					obj.IsSelected = false;
				}

				foreach (int i in gridView.GetSelectedRows())
				{
					dynamic obj = gridView.GetRow(i);
					obj.IsSelected = true;
				}
			}
			finally
			{
				gridView.BeginUpdate();
				gridView.EndUpdate();
			}
		}

		//鼠标双击时自动展开/收缩明细记录。
		private static void gridView_DoubleClick(object sender, System.EventArgs e)
		{
			GridView gv = sender as GridView;

			if (!gv.OptionsView.ShowDetailButtons)
			{
				return;
			}

			if (!gv.IsMasterRowEmpty(gv.FocusedRowHandle) && gv.GetMasterRowExpanded(gv.FocusedRowHandle))
			{
				gv.CollapseMasterRow(gv.FocusedRowHandle);
			}
			else
			{
				gv.ExpandMasterRow(gv.FocusedRowHandle);
			}
		}

		#region 多选、全选、反选、不选

		public static List<T> GetSelectedList<T>(this GridView gridView) where T : class
		{
			List<T> list = new List<T>();
			for (int i = 0; i < gridView.RowCount; i++)
			{
				if (!gridView.IsFilterRow(i))
				{
					dynamic d = gridView.GetRow(i);
					if (d == null)
					{
						continue;
					}
					if (d.IsSelected)
					{
						list.Add(d);
					}
				}
			}
			return list;
		}

		public static void SelectAll(this GridView gridView)
		{
			for (int i = 0; i < gridView.RowCount; i++)
			{
				if (!gridView.IsFilterRow(i))
				{
					dynamic d = gridView.GetRow(i);
					d.IsSelected = true;
				}
			}

			gridView.RefreshData();
		}

		public static void UnSelectAll(this GridView gridView)
		{
			for (int i = 0; i < gridView.RowCount; i++)
			{
				if (!gridView.IsFilterRow(i))
				{
					dynamic d = gridView.GetRow(i);
					d.IsSelected = false;
				}
			}

			gridView.RefreshData();
		}

		public static void InverseSelect(this GridView gridView)
		{
			for (int i = 0; i < gridView.RowCount; i++)
			{
				if (!gridView.IsFilterRow(i))
				{
					dynamic d = gridView.GetRow(i);
					d.IsSelected = !d.IsSelected;
				}
			}

			gridView.RefreshData();
		}

		#endregion
	}
}
