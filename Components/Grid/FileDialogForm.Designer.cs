﻿namespace Infrastructure.Components
{
	partial class FileDialogForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txeFileName = new DevExpress.XtraEditors.TextEdit();
			this.btnOK = new DevExpress.XtraEditors.SimpleButton();
			this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
			this.lbcFileName = new DevExpress.XtraEditors.LabelControl();
			this.ilcFileList = new DevExpress.XtraEditors.ImageListBoxControl();
			this.lbcSuffix = new DevExpress.XtraEditors.LabelControl();
			this.txeSuffix = new DevExpress.XtraEditors.TextEdit();
			((System.ComponentModel.ISupportInitialize)(this.txeFileName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ilcFileList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txeSuffix.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// txeFileName
			// 
			this.txeFileName.Location = new System.Drawing.Point(66, 281);
			this.txeFileName.Name = "txeFileName";
			this.txeFileName.Size = new System.Drawing.Size(436, 20);
			this.txeFileName.TabIndex = 0;
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(355, 312);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(66, 23);
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(436, 312);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(66, 23);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "取消";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lbcFileName
			// 
			this.lbcFileName.Location = new System.Drawing.Point(12, 284);
			this.lbcFileName.Name = "lbcFileName";
			this.lbcFileName.Size = new System.Drawing.Size(48, 14);
			this.lbcFileName.TabIndex = 2;
			this.lbcFileName.Text = "文件名称";
			// 
			// ilcFileList
			// 
			this.ilcFileList.Location = new System.Drawing.Point(12, 12);
			this.ilcFileList.Name = "ilcFileList";
			this.ilcFileList.Size = new System.Drawing.Size(490, 260);
			this.ilcFileList.TabIndex = 4;
			this.ilcFileList.SelectedIndexChanged += new System.EventHandler(this.ilcFileList_SelectedIndexChanged);
			this.ilcFileList.Click += new System.EventHandler(this.ilcFileList_Click);
			this.ilcFileList.DoubleClick += new System.EventHandler(this.ilcFileList_DoubleClick);
			// 
			// lbcSuffix
			// 
			this.lbcSuffix.Location = new System.Drawing.Point(12, 312);
			this.lbcSuffix.Name = "lbcSuffix";
			this.lbcSuffix.Size = new System.Drawing.Size(48, 14);
			this.lbcSuffix.TabIndex = 2;
			this.lbcSuffix.Text = "名称后缀";
			// 
			// txeSuffix
			// 
			this.txeSuffix.Location = new System.Drawing.Point(66, 311);
			this.txeSuffix.Name = "txeSuffix";
			this.txeSuffix.Properties.ReadOnly = true;
			this.txeSuffix.Size = new System.Drawing.Size(240, 20);
			this.txeSuffix.TabIndex = 5;
			// 
			// FileDialogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(514, 347);
			this.Controls.Add(this.txeSuffix);
			this.Controls.Add(this.ilcFileList);
			this.Controls.Add(this.lbcSuffix);
			this.Controls.Add(this.lbcFileName);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.txeFileName);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FileDialogForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "文件名称";
			((System.ComponentModel.ISupportInitialize)(this.txeFileName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ilcFileList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txeSuffix.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.TextEdit txeFileName;
		private DevExpress.XtraEditors.SimpleButton btnOK;
		private DevExpress.XtraEditors.SimpleButton btnCancel;
		private DevExpress.XtraEditors.LabelControl lbcFileName;
		private DevExpress.XtraEditors.ImageListBoxControl ilcFileList;
		private DevExpress.XtraEditors.LabelControl lbcSuffix;
		private DevExpress.XtraEditors.TextEdit txeSuffix;

	}
}