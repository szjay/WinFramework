//==============================================================
//  版权所有：深圳杰文科技
//  文件名：FileDialogForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Infrastructure.Components
{
	public partial class FileDialogForm : Form
	{
		#region properties

		#region DialogType
		public DialogType dialogType;
		/// <summary>
		/// 窗体类型。
		/// </summary>
		public DialogType DialogType
		{
			get
			{
				return this.dialogType;
			}
			set
			{
				this.dialogType = value;
			}
		}
		#endregion

		#region Title
		/// <summary>
		/// 窗体标题。
		/// </summary>
		public string Title
		{
			get
			{
				return this.Text;
			}
			set
			{
				this.Text = value;
			}
		}
		#endregion

		#region FileName
		/// <summary>
		/// 文件名称(不包括文件名后缀)。
		/// </summary>
		public string FileName
		{
			get
			{
				return this.txeFileName.Text.Trim();
			}
			set
			{
				this.txeFileName.Text = value;
			}
		}
		#endregion

		#region SubName
		private string subName;
		/// <summary>
		/// 后缀文件名称(不包括文件名后缀)。
		/// </summary>
		public string SubName
		{
			get
			{
				if (this.subName == null)
					this.subName = string.Empty;
				return this.subName;
			}
			set
			{
				this.subName = value;
				this.txeSuffix.Text = string.Format("{0}{1}", this.SubName, this.Suffix.TrimStart('*'));
			}
		}
		#endregion

		#region Suffix
		private string suffix;
		/// <summary>
		/// 文件名后缀。
		/// </summary>
		public string Suffix
		{
			get
			{
				if (this.suffix == null)
					this.suffix = string.Empty;
				return this.suffix;
			}
			set
			{
				this.suffix = value;
				this.txeSuffix.Text = string.Format("{0}{1}", this.SubName, this.Suffix.TrimStart('*'));
			}
		}
		#endregion

		#region FilePath
		private string filePath;
		/// <summary>
		/// 文件路径(不包括文件名)。
		/// </summary>
		public string FilePath
		{
			get
			{
				if (this.filePath == null)
					this.filePath = string.Empty;
				return filePath;
			}
			set
			{
				filePath = value;
			}
		}
		#endregion

		#region FullName
		/// <summary>
		/// 文件全称。
		/// </summary>
		public string FullName
		{
			get
			{
				return string.Format("{0}\\{1}.{2}", this.FilePath, this.FileName, this.txeSuffix.Text);
			}
		}
		#endregion

		#region GroupName
		private string groupName;
		/// <summary>
		/// LayoutControlGroup名称。
		/// </summary>
		public string GroupName
		{
			get
			{
				if (this.groupName == null)
					this.groupName = string.Empty;
				return this.groupName;
			}
			set
			{
				groupName = value;
			}
		}
		#endregion

		#endregion

		#region construct
		/// <summary>
		/// 构造FileDialogForm。
		/// </summary>
		/// <param name="title">窗体标题。</param>
		/// <param name="btnOKText">OK按钮显示文本。</param>
		/// <param name="suffix">文件后缀名称。</param>
		/// <param name="filePath">文件路径。</param>
		/// <param name="groupName">LayoutControlGroup名称。</param>
		public FileDialogForm(string title, string subName, string suffix, string filePath, string groupName, DialogType dialogType)
		{
			InitializeComponent();

			this.Title = title;
			this.SubName = subName;
			this.Suffix = suffix;
			this.FilePath = filePath;
			this.GroupName = groupName;
			this.DialogType = dialogType;
			this.btnOK.Text = dialogType == DialogType.Open ? "打开" : "保存";
			this.GetFileList();
		}
		#endregion

		#region methods

		#region GetFileList
		/// <summary>
		/// 获取文件列表。
		/// </summary>
		private void GetFileList()
		{
			if (!Directory.Exists(this.FilePath))
				return;

			string[] list = Directory.GetFiles(this.FilePath, string.Format("*{0}{1}", this.SubName, this.Suffix), SearchOption.TopDirectoryOnly);
			foreach (string item in list)
			{
				int index = item.LastIndexOf(@"\") + 1;
				string name = item.Substring(index, item.Length - index);
				this.ilcFileList.Items.Add(name);
			}
		}
		#endregion

		#region CheckFileName
		/// <summary>
		/// 校验文件名称。
		/// </summary>
		/// <returns>校验是否通过。</returns>
		private bool CheckFileName()
		{
			//Char[] chars = Path.GetInvalidPathChars();
			Regex regex = new Regex("[\\*\\\\/:? <> |\"]");
			return !regex.IsMatch(this.txeFileName.Text.Trim());
		}
		#endregion

		#region SetFileName
		/// <summary>
		/// 设置文件名称。
		/// </summary>
		private void SetFileName()
		{
			if (this.ilcFileList.SelectedIndex >= 0)
			{
				string name = this.ilcFileList.SelectedItem.ToString();
				if (name.Contains(this.SubName))
				{
					int index = name.LastIndexOf(this.SubName);
					this.txeFileName.Text = name.Substring(0, index - 1);
					this.txeSuffix.Text = name.Substring(index, name.Length - index);
				}
			}
		}
		#endregion

		#region CheckFile
		/// <summary>
		/// 校验文件。
		/// </summary>
		private void CheckFile()
		{
			if (!this.CheckFileName())
			{
				MessageBox.Show(string.Format("文件名 {0} 输入不合法！", this.txeFileName.Text.Trim()));
				return;
			}
			if (this.DialogType == DialogType.Open && !File.Exists(this.FullName))
			{
				MessageBox.Show(string.Format("文件 {0} 不存在！", this.txeFileName.Text.Trim()));
				return;
			}
			else if (this.DialogType == DialogType.Save && File.Exists(this.FullName))
			{
				DialogResult result = MessageBox.Show(string.Format("已经存在文件 {0} ，是否覆盖？", this.txeFileName.Text.Trim()), "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (result != DialogResult.Yes)
					return;

			}
			this.DialogResult = System.Windows.Forms.DialogResult.OK;
		}
		#endregion

		#endregion

		#region events

		#region ilcFileList_Click
		/// <summary>
		/// 列表单击。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ilcFileList_Click(object sender, EventArgs e)
		{
			this.SetFileName();
		}
		#endregion

		#region ilcFileList_DoubleClick
		/// <summary>
		/// 列表双击。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ilcFileList_DoubleClick(object sender, EventArgs e)
		{
			if (this.ilcFileList.SelectedIndex >= 0)
			{
				this.CheckFile();
			}
		}
		#endregion

		#region ilcFileList_SelectedIndexChanged
		/// <summary>
		/// 列表选中行改变。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ilcFileList_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.SetFileName();
		}
		#endregion

		#region btnOK_Click
		/// <summary>
		/// OK。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOK_Click(object sender, EventArgs e)
		{
			CheckFile();
		}
		#endregion

		#region btnCancel_Click
		/// <summary>
		/// 取消。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}
		#endregion

		#endregion
	}

	/// <summary>
	/// 文件窗体类型。
	/// </summary>
	public enum DialogType
	{
		Open = 0,
		Save = 4
	}
}
