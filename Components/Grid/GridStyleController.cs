//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridStyleController.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Infrastructure.Utilities;

namespace Infrastructure.Components
{
	internal class GridStyleController
	{
		public static void SetDefaultStyle(GridControl grid)
		{
			foreach (BaseView view in grid.Views)
			{
				GridView gridView = view as GridView;
				if (gridView == null)
				{
					continue;
				}

				gridView.OptionsPrint.PrintHorzLines = false;
				gridView.OptionsPrint.PrintVertLines = false;
				gridView.OptionsBehavior.Editable = false;
				gridView.OptionsBehavior.ReadOnly = true;
				gridView.OptionsBehavior.CopyToClipboardWithColumnHeaders = false; //整行复制的时候，不复制标题。
				gridView.OptionsCustomization.AllowQuickHideColumns = false; //不允许隐藏列。
				gridView.OptionsView.ShowDetailButtons = false; //不显示明细表。
																//gridView.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False; //不允许删除列。
																//gridView.OptionsBehavior.AllowAddRows = DefaultBoolean.False; //不允许增加行。
																//gridView.OptionsMenu.EnableColumnMenu = false; //不显示右键菜单，以免用户修改隐藏的列。
				gridView.OptionsView.EnableAppearanceEvenRow = true; //允许隔行换色。
				gridView.OptionsView.EnableAppearanceOddRow = true;

				//Font FocusedRowFont = new Font(FontName, 9F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0))); //焦点行字体加粗。
				//gridView1.Appearance.FocusedRow.Font = font;

				Format(gridView, "F2"); //金额、价格显示2位小数。

				gridView.OptionsBehavior.EditorShowMode = EditorShowMode.MouseDown;
				gridView.OptionsSelection.MultiSelect = true;
				gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
				gridView.FocusRectStyle = DrawFocusRectStyle.RowFullFocus;

				gridView.OptionsSelection.UseIndicatorForSelection = true;

				gridView.IndicatorWidth = GridHelper.IndicatorWidth; //行号列的宽度。
				gridView.CustomDrawRowIndicator += new RowIndicatorCustomDrawEventHandler(gridView_CustomDrawRowIndicator); //行号列自绘。
				gridView.CustomColumnDisplayText += new CustomColumnDisplayTextEventHandler(gridView_CustomColumnDisplayText); //每一列自绘显示的文本。

				gridView.EndSorting += (a, b) => gridView.MoveFirst();
			}
		}

		#region - SetListStyle -

		private const string FontName = "Tahoma"; //通用字体名称。

		/// <summary>
		/// 设置列表风格：
		/// 1.焦点行字体变粗；
		/// 2.表格只读；
		/// 3.斑马线风格；
		/// 4.标题文本居中；
		/// 5.decimal类型显示两位小数；
		/// </summary>
		public static void SetListStyle(GridControl grid)
		{
			foreach (BaseView view in grid.Views)
			{
				GridView gridView = view as GridView;
				if (gridView == null)
				{
					continue;
				}

				//GridMenuController.GeneratePopMenu(gridView);

				gridView.OptionsBehavior.CopyToClipboardWithColumnHeaders = false; //整行复制的时候，不复制标题。
				gridView.OptionsCustomization.AllowQuickHideColumns = false;
				//gridView.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
				//gridView.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
				//gridView.OptionsMenu.EnableColumnMenu = false; //不显示右键菜单，以免用户修改隐藏的列。
				gridView.OptionsView.EnableAppearanceEvenRow = true;
				gridView.OptionsView.EnableAppearanceOddRow = true;

				Font FocusedRowFont = new Font(FontName, 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
				Format(gridView, "N4"); //金额、价格显示4位小数。

				gridView.OptionsSelection.MultiSelect = true;
				gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
				gridView.OptionsBehavior.EditorShowMode = EditorShowMode.MouseDown;

				gridView.OptionsSelection.UseIndicatorForSelection = true;

				gridView.IndicatorWidth = 45; //行号列的宽度。
				gridView.CustomDrawRowIndicator += new RowIndicatorCustomDrawEventHandler(gridView_CustomDrawRowIndicator); //行号列自绘。
				gridView.CustomColumnDisplayText += new CustomColumnDisplayTextEventHandler(gridView_CustomColumnDisplayText); //每一列自绘显示的文本。
			}
		}

		static void gridView_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
			if (e.Value == null)
			{
				return;
			}

			if (e.Value is int)
			{
				if ((int)e.Value == 0)
				{
					e.DisplayText = "";
				}
			}
			else if (e.Value is decimal)
			{
				if ((decimal)e.Value == 0)
				{
					e.DisplayText = "";
				}
			}
			else if (e.Value is long)
			{
				if ((long)e.Value == 0)
				{
					e.DisplayText = "";
				}
			}
			else if (e.Value is double)
			{
				if ((double)e.Value == 0)
				{
					e.DisplayText = "";
				}
			}
			else if (e.Value is DateTime)
			{
				if ((DateTime)e.Value <= DateTimeUtil.DefaultMinDate)
				{
					e.DisplayText = "";
				}
			}
		}

		static void gridView_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
		{
			if (e.Info.IsRowIndicator && e.RowHandle >= 0)
			{
				e.Info.DisplayText = (e.RowHandle + 1).ToString();
			}
		}

		//设置显示的字符串格式。
		private static void Format(GridView view, string format)
		{
			if (view == null)
			{
				return;
			}

			foreach (GridColumn column in view.Columns)
			{
				if (string.IsNullOrEmpty(column.FieldName))
				{
					continue;
				}

				FormateColumn(column, format);
				HandleColumnEdit(view, column);

				if (column.ColumnEdit is RepositoryItemSpinEdit)
				{
					column.ColumnEdit.MouseUp += (a, b) => view.ActiveEditor.SelectAll();
				}
			}
		}

		private static void HandleColumnEdit(GridView view, GridColumn column)
		{
			if (column.ColumnEdit == null)
			{
				return;
			}

			if (column.ColumnEdit is RepositoryItemCheckEdit) //如果是勾选框，那么打勾之后立即有效。
			{
				column.ColumnEdit.MouseUp += (s, e) =>
				{
					view.PostEditor();
					view.CloseEditor();

					//如果有明细表，那么刷新明细表。
					if (!view.IsMasterRowEmpty(view.FocusedRowHandle) && view.GetMasterRowExpanded(view.FocusedRowHandle))
					{
						view.PostEditor();
						view.CollapseMasterRow(view.FocusedRowHandle);
						view.ExpandMasterRow(view.FocusedRowHandle);
					}
				};
			}
		}

		private static void FormateColumn(GridColumn column, string format)
		{
			column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center; //标题居中。

			if (!string.IsNullOrEmpty(column.DisplayFormat.FormatString))
			{
				return;
			}

			string name = column.FieldName.ToLower();
			if (name.Contains("amt") || name.Contains("amount") || name.Contains("price") || name.Contains("money")
				|| name.Contains("金额") || name.Contains("单价")) //如果是和金额有关的字段，
			{
				column.DisplayFormat.FormatType = FormatType.Numeric;
				column.DisplayFormat.FormatString = format;

				//if (column.SummaryItem != null)
				//{
				//	if (column.SummaryItem.SummaryType == DevExpress.Data.SummaryItemType.Sum)
				//	{
				//		column.SummaryItem.DisplayFormat = format;
				//	}
				//}
			}
			else if (column.Caption.Contains("日期"))
			{
				column.DisplayFormat.FormatType = FormatType.DateTime;
				column.DisplayFormat.FormatString = "yyyy-MM-dd";
				//column.Width = 70;
			}
			else if (column.Caption.Contains("时间"))
			{
				column.DisplayFormat.FormatType = FormatType.DateTime;
				column.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm";
				//column.Width = 124;
			}
		}

		#endregion

		#region 给GridView增加序号

		/// <summary>
		/// 给GridView增加序号。
		/// </summary>
		/// <param name="gridView"></param>
		public static void SetGridSequenceNumber(GridView gridView)
		{
			SetGridSequenceNumber(gridView, 50);
		}

		/// <summary>
		/// 给GridView增加序号，并指定序号列的宽度。
		/// </summary>
		/// <param name="gridView"></param>
		/// <param name="indicatorWidth"></param>
		public static void SetGridSequenceNumber(GridView gridView, int indicatorWidth)
		{
			gridView.IndicatorWidth = indicatorWidth;
			gridView.CustomDrawRowIndicator += delegate (object sender, RowIndicatorCustomDrawEventArgs e)
			{
				if (e.Info.IsRowIndicator && e.RowHandle >= 0)
				{
					e.Info.DisplayText = (e.RowHandle + 1).ToString();
				}
			};
		}

		#endregion

		#region 设置Grid的背景

		/// <summary>
		/// 设置Grid的背景
		/// 当参数(GridColumn)的值为参数(value)的时候，返回一个StyleFormatCondition
		/// </summary>
		/// <param name="column">参考列</param>
		/// <param name="value">值</param>
		public static DevExpress.XtraGrid.StyleFormatCondition CreateFormatCondition(GridColumn column, object value)
		{
			return CreateFormatCondition(column, value, Color.LightSeaGreen);
		}

		/// <summary>
		/// 设置Grid的背景
		/// 当参数(GridColumn)的值为参数(value)的时候，返回一个StyleFormatCondition
		/// </summary>
		/// <param name="column">参考列</param>
		/// <param name="value">值</param>
		public static DevExpress.XtraGrid.StyleFormatCondition CreateFormatCondition(GridColumn column, object value, Color backColor)
		{
			DevExpress.XtraGrid.StyleFormatCondition style = new DevExpress.XtraGrid.StyleFormatCondition();
			style.ApplyToRow = true;
			style.Column = column;
			style.Condition = FormatConditionEnum.Equal;
			style.Value1 = value;
			style.Appearance.BackColor = backColor;
			style.Appearance.Options.UseBackColor = true;
			style.Appearance.Options.UseForeColor = true;

			return style;
		}

		#endregion
	}
}
