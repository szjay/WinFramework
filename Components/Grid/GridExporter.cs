//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridExporter.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

namespace Infrastructure.Components
{
	internal class GridExporter
	{
		#region 导出Grid数据为Excel

		public enum ExportDocumentType
		{
			Excel,
			CSV,
			PDF,
			OldExcel
		}

		/// <summary>
		/// 导出Grid数据为Excel
		/// </summary>
		/// <param name="grid">数据源</param>
		public static void ExportGrid(string title, GridControl grid, ExportDocumentType docType)
		{
			using (SaveFileDialog dlg = new SaveFileDialog())
			{
				dlg.AddExtension = true;
				dlg.CheckPathExists = true;
				dlg.OverwritePrompt = true;
				dlg.ValidateNames = true;
				dlg.Title = "导出文件";
				switch (docType)
				{
					case ExportDocumentType.Excel:
						dlg.DefaultExt = ".xls";
						dlg.FileName = title + ".xls";
						dlg.Filter = "Microsoft Excel|*.xls";
						break;

					case ExportDocumentType.CSV:
						dlg.DefaultExt = ".csv";
						dlg.FileName = title + ".csv";
						dlg.Filter = "CSV(逗号分隔)|*.csv";
						break;
					case ExportDocumentType.PDF:
						dlg.DefaultExt = ".pdf";
						dlg.FileName = title + ".pdf";
						dlg.Filter = "PDF|*.pdf";
						break;
				}

				if (dlg.ShowDialog() == DialogResult.OK)
				{
					switch (docType)
					{
						case ExportDocumentType.Excel:
							grid.ExportToXls(dlg.FileName);
							break;
						case ExportDocumentType.OldExcel:
							grid.ExportToExcelOld(dlg.FileName);
							break;
						case ExportDocumentType.CSV:
							grid.ExportToCsv(dlg.FileName);
							break;
						case ExportDocumentType.PDF:
							grid.ExportToPdf(dlg.FileName);
							break;
					}

					if (MessageBox.Show("导出成功！是否打开导出的文件？", "询问", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Process process = new Process();
						process.StartInfo.FileName = dlg.FileName;
						process.StartInfo.Verb = "Open";
						process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
						process.Start();
					}
				}
			}
		}

		/// <summary>
		/// 导出Grid数据为Excel
		/// </summary>
		/// <param name="grid">数据源</param>
		public static void ExportGrid(string title, GridView grid)
		{
			using (SaveFileDialog dlg = new SaveFileDialog())
			{
				dlg.DefaultExt = ".xls";
				dlg.AddExtension = true;
				dlg.CheckPathExists = true;
				dlg.OverwritePrompt = true;
				dlg.ValidateNames = true;
				dlg.Title = "导出文件";
				dlg.FileName = title + ".xls";
				dlg.Filter = "Microsoft Excel|*.xls";

				if (dlg.ShowDialog() == DialogResult.OK)
				{
					grid.ExportToXls(dlg.FileName, new XlsExportOptions(TextExportMode.Text));

					if (MessageBox.Show("导出成功！是否打开导出的文件？", "询问", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Process process = new Process();
						process.StartInfo.FileName = dlg.FileName;
						process.StartInfo.Verb = "Open";
						process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
						process.Start();
					}
				}
			}
		}

		/// <summary>
		///  导出Grid数据为txt文件
		/// </summary>
		/// <param name="title"></param>
		/// <param name="grid">数据源</param>
		public static void ExportGridToText(string title, GridControl grid)
		{
			using (SaveFileDialog dlg = new SaveFileDialog())
			{
				dlg.DefaultExt = ".txt";
				dlg.AddExtension = true;
				dlg.CheckPathExists = true;
				dlg.OverwritePrompt = true;
				dlg.ValidateNames = true;
				dlg.Title = "导出文件";
				dlg.FileName = title + ".txt";
				dlg.Filter = "文本文件|*.txt";

				if (dlg.ShowDialog() == DialogResult.OK)
				{
					grid.ExportToText(dlg.FileName);

					if (MessageBox.Show("导出成功！是否打开导出的文件？", "询问", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Process process = new Process();
						process.StartInfo.FileName = dlg.FileName;
						process.StartInfo.Verb = "Open";
						process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
						process.Start();
					}
				}
			}
		}

		#endregion
	}
}
