//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridMenuController.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using Infrastructure.Components.Properties;

namespace Infrastructure.Components
{
	internal static class GridMenuController
	{
		//生成右键菜单。
		public static void GeneratePopMenu(GridView gridView)
		{
			ContextMenuStrip popMenu = new ContextMenuStrip();

			popMenu.Items.Add(new ToolStripMenuItem("保存表格为默认布局", Resources.save,
				(s, e) => GridLayoutController.SaveStyle(gridView.Name, gridView)));

			popMenu.Items.Add(new ToolStripMenuItem("恢复表格的默认布局", null,
				(s, e) => GridLayoutController.RestoreStyle(gridView.Name, gridView)));

			popMenu.Items.Add(new ToolStripMenuItem("初始化表格布局", Resources.refresh,
				(s, e) => GridLayoutController.RestoreInitStyle(gridView.Name, gridView)));

			popMenu.Items.Add(new ToolStripSeparator());

			popMenu.Items.Add(new ToolStripMenuItem("表格布局另存...", null,
				(s, e) => GridLayoutController.SaveStyleTo(gridView)));

			popMenu.Items.Add(new ToolStripMenuItem("恢复指定布局...", null,
				(s, e) => GridLayoutController.RestoreStyleTo(gridView)));

			popMenu.Size = new Size(161, 48);

			gridView.PopupMenuShowing += (s, e) =>
			{
				switch (e.MenuType)
				{
					case GridMenuType.Column:
						break;
					case GridMenuType.Group:
						break;

					case GridMenuType.Row:
						e.Allow = false;
						popMenu.Show(Cursor.Position);
						break;

					case GridMenuType.Summary:
						break;

					case GridMenuType.User:
						e.Allow = false;
						popMenu.Show(Cursor.Position);
						break;

					default:
						break;
				}
			};
		}

	}
}
