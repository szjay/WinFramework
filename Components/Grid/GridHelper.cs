//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridHelper.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Collections.Generic;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

namespace Infrastructure.Components
{
	public static class GridHelper
	{
		public static int IndicatorWidth = 45; //表格序号宽度

		public enum GridStyle
		{
			Default = 1, //默认风格
			List = 2, //列表风格
		}

		public static void SetStyle(this GridControl grid)
		{
			SetStyle(grid, GridStyle.Default);
		}

		public static void SetStyle(this GridControl grid, GridHelper.GridStyle style)
		{
			if (style == GridStyle.Default)
			{
				GridStyleController.SetDefaultStyle(grid);
			}
			else
			{
				GridStyleController.SetListStyle(grid);
			}
			GridSelectionHelper.Set(grid);

			//GridMenuController.GeneratePopMenu((GridView)grid.MainView);
		}

		public static void Edit(this GridControl grid)
		{
			foreach (BaseView view in grid.Views)
			{
				GridView gridView = view as GridView;
				if (gridView == null)
				{
					continue;
				}

				gridView.OptionsBehavior.Editable = true;
				gridView.OptionsBehavior.ReadOnly = false;
			}
		}

		#region - Edit & ReadOnly -

		public static void ReadOnly(this GridControl grid)
		{
			foreach (BaseView view in grid.Views)
			{
				GridView gridView = view as GridView;
				if (gridView == null)
				{
					continue;
				}

				gridView.OptionsBehavior.Editable = false;
				gridView.OptionsBehavior.ReadOnly = true;
			}
		}

		public static void EditOnly(this GridControl grid, params GridColumn[] editableColumns)
		{
			EditOnly(grid, true, editableColumns);
		}

		public static void ReadOnly(this GridControl grid, params GridColumn[] editableColumns)
		{
			EditOnly(grid, false, editableColumns);
		}

		private static void EditOnly(this GridControl grid, bool editable, params GridColumn[] editableColumns)
		{
			foreach (BaseView view in grid.Views)
			{
				if (view is GridView)
				{
					GridView gridView = view as GridView;
					gridView.OptionsBehavior.Editable = true;
					gridView.OptionsBehavior.ReadOnly = false;

					foreach (GridColumn col in gridView.Columns)
					{
						col.OptionsColumn.AllowEdit = !editable;
						foreach (GridColumn editableCol in editableColumns)
						{
							if (editableCol.Name == col.Name)
							{
								editableCol.OptionsColumn.AllowEdit = editable;
							}
						}
					}
				}
			}
		}

		#endregion

		#region - Export -

		public static void ExportGrid(this GridControl grid, string title)
		{
			GridExporter.ExportGrid(title, grid, GridExporter.ExportDocumentType.Excel);
		}

		public static void ExportGridOldExcel(this GridControl grid, string title)
		{
			GridExporter.ExportGrid(title, grid, GridExporter.ExportDocumentType.OldExcel);
		}

		public static void ExportGrid(this GridView gridView, string title)
		{
			GridExporter.ExportGrid(title, gridView);
		}

		#endregion

		#region 设置GridColumn是否允许编辑

		public static void SetColumnEditable(params GridColumn[] columns)
		{
			SetColumnEditable(true, columns);
		}

		/// <summary>
		/// 设置GridColumn是否允许编辑
		/// </summary>
		public static void SetColumnEditable(bool editable, params GridColumn[] columns)
		{
			if (columns == null)
				return;

			foreach (GridColumn col in columns)
			{
				col.OptionsColumn.AllowEdit = editable;
				col.OptionsColumn.ReadOnly = !editable;
			}
		}

		/// <summary>
		/// 设置GridColumn是否允许编辑
		/// </summary>
		public static void SetColumnEditable(bool editable, GridView view)
		{
			if (view == null)
				return;

			foreach (GridColumn col in view.Columns)
			{
				col.OptionsColumn.AllowEdit = editable;
				col.OptionsColumn.ReadOnly = !editable;
			}
		}

		/// <summary>
		/// 设置只允许某个GridColumn为可编辑
		/// </summary>
		public static void SetColumnEditable(GridView view, params GridColumn[] columns)
		{
			if (view == null || columns == null)
				return;

			//将所有的列设置为不可编辑
			foreach (GridColumn col in view.Columns)
			{
				col.OptionsColumn.ReadOnly = true;
			}

			//设置某些可编辑的列
			foreach (GridColumn col in columns)
			{
				col.OptionsColumn.AllowEdit = true;
				col.OptionsColumn.ReadOnly = false;
			}
		}

		#endregion

		#region 将焦点定位到与指定实体类属性绑定的GridColumn上

		/// <summary>
		/// 聚焦到指定的列
		/// </summary>
		/// <param name="view">GridView对象</param>
		/// <param name="property">实体类的属性，对应GridColumn.FieldName</param>
		public static GridColumn Focus(GridView view, string property)
		{
			foreach (GridColumn col in view.Columns)
			{
				if (object.Equals(col.FieldName, property))
				{
					view.FocusedColumn = col;
					return col;
				}
			}

			return null;
		}

		#endregion

		#region - Print -

		public static void Print(this IPrintable printable, string reportName, string printer, bool landscape = false)
		{
			new GridPrinter(printable, reportName, printer, landscape).Preview();
		}

		#endregion

		public class FormatColumn
		{
			public enum Modes
			{
				Format,
				Summary,
			}

			public string FieldName;
			public string FormatString;
			public Modes Mode;
			public FormatType FormatType;

			public FormatColumn()
			{
			}

			public FormatColumn(string fieldName, Modes mode, FormatType formatType, string formatString)
			{
				this.FieldName = fieldName;
				this.Mode = mode;
				this.FormatType = formatType;
				this.FormatString = formatString;
			}
		}

		public static void Format(this GridControl grid, params FormatColumn[] formatColumns)
		{
			GridView view = grid.DefaultView as GridView;
			foreach (FormatColumn fCol in formatColumns)
			{
				GridColumn col = view.Columns.ColumnByFieldName(fCol.FieldName);
				if (col == null)
				{
					continue;
				}

				switch (fCol.Mode)
				{
					case FormatColumn.Modes.Format:
						{
							col.DisplayFormat.FormatType = fCol.FormatType;
							col.DisplayFormat.FormatString = fCol.FormatString;
						}
						break;

					case FormatColumn.Modes.Summary:
						{
							col.SummaryItem.FieldName = col.FieldName;
							col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
							col.SummaryItem.DisplayFormat = fCol.FormatString;
						}
						break;
				}

			}
		}

		public static void Summary(this GridControl grid, Dictionary<string, string> formatColumns)
		{
			GridView view = grid.DefaultView as GridView;
			foreach (KeyValuePair<string, string> kvp in formatColumns)
			{
				GridColumn col = view.Columns.ColumnByFieldName(kvp.Key);
				if (col == null)
				{
					continue;
				}

				col.SummaryItem.FieldName = col.FieldName;
				col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
				//col.SummaryItem.DisplayFormat = kvp.Value;
			}
		}

		public static T GetCurrent<T>(this GridView gridView)
		{
			return (T)gridView.GetFocusedRow();
		}

		public static void RemoveCurrentRow(this GridView gridView)
		{
			gridView.DeleteRow(gridView.FocusedRowHandle);
		}

		public static T GetActiveEditorValue<T>(this GridView gridView)
		{
			BaseEdit editor = gridView.ActiveEditor;
			if (editor.EditValue == null)
			{
				return default(T);
			}

			return (T)editor.EditValue;
		}

	}
}
