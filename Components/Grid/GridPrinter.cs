//==============================================================
//  版权所有：深圳杰文科技
//  文件名：GridPrinter.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Drawing;
using System.Drawing.Printing;
using DevExpress.XtraPrinting;

namespace Infrastructure.Components
{
	public class GridPrinter
	{
		private PrintingSystem printSystem;
		private string _ReportName;
		private string _Condition;
		private string _Printer;

		public GridPrinter(IPrintable printable, string reportName, string printer)
			: this(printable, reportName, printer, "", PaperKind.A4, false)
		{
		}

		public GridPrinter(IPrintable printable, string reportName, string printer, bool landscape)
			: this(printable, reportName, printer, "", PaperKind.A4, landscape)
		{
		}

		public GridPrinter(IPrintable printable, string reportName, string printer, string condition, PaperKind paperKind, bool landscape)
		{
			_ReportName = reportName;
			_Condition = condition;
			_Printer = printer;

			printSystem = new PrintingSystem();

			PrintableComponentLink pcl = new PrintableComponentLink();
			pcl.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(link_CreateMarginalHeaderArea);
			pcl.CreateMarginalFooterArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(link_CreateMarginalFooterArea);
			pcl.Component = printable;
			pcl.Landscape = landscape;

			printSystem.Links.Add(pcl);
			pcl.CreateDocument();

			PrinterSettingsUsing pst = new PrinterSettingsUsing();
			pst.UseMargins = true;
			pst.UsePaperKind = true;

			printSystem.PageSettings.PaperKind = paperKind;
			printSystem.PageSettings.Landscape = landscape;
			printSystem.PageSettings.LeftMargin = 10;
			printSystem.PageSettings.RightMargin = 10;
			printSystem.PageSettings.TopMargin = 10;
			printSystem.PageSettings.BottomMargin = 10;
			printSystem.PageSettings.AssignDefaultPrinterSettings(pst);
		}

		private void link_CreateMarginalHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
		{
			if (!string.IsNullOrEmpty(_ReportName))
			{
				e.Graph.Font = new Font("宋体", 20, FontStyle.Bold);
				e.Graph.BackColor = Color.Transparent;
				RectangleF r = new RectangleF(0, 20, 0, e.Graph.Font.Height + 20);

				PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, _ReportName, Color.Black, r, BorderSide.None);
				brick.Alignment = BrickAlignment.Center;
				brick.AutoWidth = true;

				//BrickStringFormat sf = new BrickStringFormat(StringFormatFlags.DirectionVertical);
				//brick.StringFormat = sf;

				e.Graph.Font = new Font("宋体", 10);
				e.Graph.BackColor = Color.Transparent;
				RectangleF r2 = new RectangleF(0, 60, 0, e.Graph.Font.Height + 20);

				PageInfoBrick brick2 = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "打印人：" + _Printer, Color.Black, r2, BorderSide.None);
				brick2.Alignment = BrickAlignment.Near;
				brick2.AutoWidth = true;

				PageInfoBrick brick3 = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "打印时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm"), Color.Black, r2, BorderSide.None);
				brick3.Alignment = BrickAlignment.Far;
				brick3.AutoWidth = true;
			}

			//顶部居中。
			//if (!string.IsNullOrEmpty(_Condition))
			//{
			//	e.Graph.Font = new Font("宋体", 10);
			//	e.Graph.BackColor = Color.Transparent;
			//	RectangleF r = new RectangleF(0, 60, 0, e.Graph.Font.Height + 20);
			//	PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, _Condition, Color.Black, r, BorderSide.None);
			//	brick.Alignment = BrickAlignment.Center;
			//	brick.AutoWidth = true;
			//}
		}

		private void link_CreateMarginalFooterArea(object sender, CreateAreaEventArgs e)
		{
			string format = "第{0}页 共{1}页";
			e.Graph.Font = new Font("宋体", 10);
			e.Graph.BackColor = Color.Transparent;
			RectangleF r = new RectangleF(0, 5, 0, e.Graph.Font.Height + 20);

			//底部左。
			if (!string.IsNullOrEmpty(_Condition))
			{
				PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, format, Color.Black, r, BorderSide.None);
				brick.Alignment = BrickAlignment.Far;
				brick.AutoWidth = true;

				brick = e.Graph.DrawPageInfo(PageInfo.DateTime, _Condition, Color.Black, r, BorderSide.None);
				brick.Alignment = BrickAlignment.Near;
				brick.AutoWidth = true;
			}
		}

		/// <summary>       
		/// 直接打印        
		/// </summary>       
		public void Print()
		{
			printSystem.Print();
		}

		/// <summary>        
		/// 预览打印        
		/// </summary>       
		public void Preview()
		{
			printSystem.PreviewFormEx.ShowDialog();
		}

	}
}
