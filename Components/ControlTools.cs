//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ControlTools.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	public static class ControlTools
	{
		/// <summary>
		/// 判断控件是否处于设计模式下。
		/// </summary>
		public static bool IsDesignMode(Control control)
		{
			System.Type t = typeof(System.ComponentModel.Component);
			System.Reflection.PropertyInfo pi = t.GetProperty("DesignMode", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
			do
			{
				if ((bool)pi.GetValue(control, null))
				{
					return true;
				}
				control = control.Parent;
			} while (control != null);

			return false;
		}

		/// <summary>
		/// 设置一个控件（容器）以及它的子控件为只读。
		/// </summary>
		/// <param name="parentControl"></param>
		/// <param name="readOnly"></param>
		public static void SetControls(Control parentControl, bool readOnly)
		{
			foreach (Control c in parentControl.Controls)
			{
				if (c is BaseEdit)
				{
					(c as BaseEdit).Properties.ReadOnly = true;
				}
				else
				{
					SetControls(c, readOnly);
				}
			}
		}
	}
}
