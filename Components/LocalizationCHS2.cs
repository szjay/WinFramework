﻿using DevExpress.XtraBars.Customization;
using DevExpress.XtraBars.Localization;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraLayout.Localization;
using DevExpress.XtraPrinting.Localization;
using DevExpress.XtraReports.Localization;
using DevExpress.XtraTreeList.Localization;

namespace Dxperience.LocalizationCHS
{

	public class DevExpressXtraBarsCustomizationLocalizationCHS : CustomizationControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		///
		/// </summary>
		public DevExpressXtraBarsCustomizationLocalizationCHS()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tpOptions.SuspendLayout();
			this.tpCommands.SuspendLayout();
			this.tpToolbars.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.toolBarsList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbCommands)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbCategories)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptionsShowFullMenus.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_showFullMenusAfterDelay.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_showTips.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_ShowShortcutInTips.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
			this.tabControl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbNBDlgName.Properties)).BeginInit();
			this.pnlNBDlg.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_largeIcons.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_MenuAnimation.Properties)).BeginInit();
			this.SuspendLayout();
			//
			// btClose
			//
			this.btClose.Text = "关闭";
			//
			// tpOptions
			//
			this.tpOptions.Size = new System.Drawing.Size(354, 246);
			this.tpOptions.Text = "选项(&O)";
			//
			// tpCommands
			//
			this.tpCommands.Text = "命令(&C)";
			//
			// tpToolbars
			//
			this.tpToolbars.Text = "工具栏(&B)";
			//
			// btNewBar
			//
			this.btNewBar.Text = "新建(&N)";
			//
			// btDeleteBar
			//
			this.btDeleteBar.Text = "删除(&D)";
			//
			// lbCommands
			//
			this.lbCommands.Appearance.BackColor = System.Drawing.SystemColors.Window;
			this.lbCommands.Appearance.Options.UseBackColor = true;
			//
			// cbOptionsShowFullMenus
			//
			this.cbOptionsShowFullMenus.Properties.Caption = "总是显示所有菜单";
			//
			// cbOptions_showFullMenusAfterDelay
			//
			this.cbOptions_showFullMenusAfterDelay.Properties.Caption = "鼠标指针短暂停留后显示全部菜单";
			//
			// btOptions_Reset
			//
			this.btOptions_Reset.Text = "重置惯用数据(&R)";
			//
			// cbOptions_showTips
			//
			this.cbOptions_showTips.Properties.Caption = "显示工具栏屏幕提示(&T)";
			//
			// cbOptions_ShowShortcutInTips
			//
			this.cbOptions_ShowShortcutInTips.Properties.Caption = "在屏幕提示中显示快捷键(&H)";
			//
			// lbDescCaption
			//
			this.lbDescCaption.Text = "详细说明";
			//
			// lbOptions_Other
			//
			this.lbOptions_Other.Text = "其它";
			//
			// lbOptions_PCaption
			//
			this.lbOptions_PCaption.Text = "个性化菜单和工具栏";
			//
			// lbCategoriesCaption
			//
			this.lbCategoriesCaption.Text = "类别:";
			//
			// lbCommandsCaption
			//
			this.lbCommandsCaption.Text = "命令:";
			//
			// lbToolbarCaption
			//
			this.lbToolbarCaption.Text = "工具栏:";
			//
			// btResetBar
			//
			this.btResetBar.Text = "重置(&R)";
			//
			// tbNBDlgName
			//
			//
			// btRenameBar
			//
			this.btRenameBar.Text = "重命名(&E)";
			//
			// cbOptions_largeIcons
			//
			this.cbOptions_largeIcons.Properties.Caption = "大图标(&L)";
			//
			// lbOptions_MenuAnimation
			//
			this.lbOptions_MenuAnimation.Text = "菜单动画设置(&M):";
			//
			// cbOptions_MenuAnimation
			//
			//
			// DevExpressXtraBarsCustomizationLocalizationCHT
			//
			this.Name = "DevExpressXtraBarsCustomizationLocalizationCHT";
			this.tpOptions.ResumeLayout(false);
			this.tpCommands.ResumeLayout(false);
			this.tpToolbars.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.toolBarsList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbCommands)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbCategories)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptionsShowFullMenus.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_showFullMenusAfterDelay.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_showTips.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_ShowShortcutInTips.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
			this.tabControl.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tbNBDlgName.Properties)).EndInit();
			this.pnlNBDlg.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_largeIcons.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbOptions_MenuAnimation.Properties)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}

	public class DxperienceXtraGridLocalizationCHS : GridLocalizer
	{
		public override string GetLocalizedString(GridStringId id)
		{
			switch (id)
			{
				case GridStringId.FileIsNotFoundError:
					return "文件{0}没有找到";

				case GridStringId.ColumnViewExceptionMessage:
					return "是否确定修改？";

				case GridStringId.CustomizationCaption:
					return "自定义显示字段";

				case GridStringId.CustomizationColumns:
					return "列";

				case GridStringId.CustomizationBands:
					return "分区";

				case GridStringId.PopupFilterAll:
					return "(所有)";

				case GridStringId.PopupFilterCustom:
					return "(自定义)";

				case GridStringId.PopupFilterBlanks:
					return "(空值)";

				case GridStringId.PopupFilterNonBlanks:
					return "(非空值)";

				case GridStringId.CustomFilterDialogFormCaption:
					return "自定义筛选条件";

				case GridStringId.CustomFilterDialogCaption:
					return "条件为:";

				case GridStringId.CustomFilterDialogRadioAnd:
					return "并且";

				case GridStringId.CustomFilterDialogRadioOr:
					return "或者";

				case GridStringId.CustomFilterDialogOkButton:
					return "确定(&O)";

				case GridStringId.CustomFilterDialogClearFilter:
					return "清除筛选条件(&L)";

				case GridStringId.CustomFilterDialog2FieldCheck:
					return "字段";

				case GridStringId.CustomFilterDialogCancelButton:
					return "取消(&C)";

				//case GridStringId.CustomFilterDialogConditionEQU:
				//    return "等于=";

				//case GridStringId.CustomFilterDialogConditionNEQ:
				//    return "不等于<>";

				//case GridStringId.CustomFilterDialogConditionGT:
				//    return "大于>";

				//case GridStringId.CustomFilterDialogConditionGTE:
				//    return "大于或等于>=";

				//case GridStringId.CustomFilterDialogConditionLT:
				//    return "小于<";

				//case GridStringId.CustomFilterDialogConditionLTE:
				//    return "小于或等于<=";

				//case GridStringId.CustomFilterDialogConditionBlanks:
				//    return "空值";

				//case GridStringId.CustomFilterDialogConditionNonBlanks:
				//    return "非空值";

				//case GridStringId.CustomFilterDialogConditionLike:
				//    return "包含";

				//case GridStringId.CustomFilterDialogConditionNotLike:
				//    return "不包含";

				case GridStringId.MenuFooterSum:
					return "合计";

				case GridStringId.MenuFooterMin:
					return "最小";

				case GridStringId.MenuFooterMax:
					return "最大";

				case GridStringId.MenuFooterCount:
					return "计数";

				case GridStringId.MenuFooterAverage:
					return "平均";

				case GridStringId.MenuFooterNone:
					return "空";

				case GridStringId.MenuFooterSumFormat:
					return "合计={0:#.##}";

				case GridStringId.MenuFooterMinFormat:
					return "最小={0}";

				case GridStringId.MenuFooterMaxFormat:
					return "最大={0}";

				case GridStringId.MenuFooterCountFormat:
					return "{0}";

				case GridStringId.MenuFooterAverageFormat:
					return "平均={0:#.##}";

				case GridStringId.MenuColumnSortAscending:
					return "升序排序";

				case GridStringId.MenuColumnSortDescending:
					return "降序排序";

				case GridStringId.MenuColumnGroup:
					return "按此列分组";

				case GridStringId.MenuColumnUnGroup:
					return "取消分组";

				case GridStringId.MenuColumnColumnCustomization:
					return "显示/隐藏字段";

				case GridStringId.MenuColumnBestFit:
					return "自动调整字段宽度";

				case GridStringId.MenuColumnFilter:
					return "筛选";

				case GridStringId.MenuColumnClearFilter:
					return "清除筛选条件";

				case GridStringId.MenuColumnBestFitAllColumns:
					return "自动调整所有字段宽度";

				case GridStringId.MenuGroupPanelFullExpand:
					return "展开全部分组";

				case GridStringId.MenuGroupPanelFullCollapse:
					return "收缩全部分组";

				case GridStringId.MenuGroupPanelClearGrouping:
					return "清除所有分组";

				case GridStringId.PrintDesignerGridView:
					return "打印设置(表格模式)";

				case GridStringId.PrintDesignerCardView:
					return "打印设置(卡片模式)";

				case GridStringId.PrintDesignerBandedView:
					return "打印设置(区域模式)";

				case GridStringId.PrintDesignerBandHeader:
					return "区标题";

				case GridStringId.MenuColumnGroupBox:
					return "显示/隐藏分组区";

				case GridStringId.CardViewNewCard:
					return "新卡片";

				case GridStringId.CardViewQuickCustomizationButton:
					return "自定义格式";

				case GridStringId.CardViewQuickCustomizationButtonFilter:
					return "筛选";

				case GridStringId.CardViewQuickCustomizationButtonSort:
					return "排序:";

				case GridStringId.GridGroupPanelText:
					return "拖动列标题到这进行分组";

				case GridStringId.GridNewRowText:
					return "新增资料";

				case GridStringId.GridOutlookIntervals:
					return "一个月以上;上个月;三周前;两周前;上周;;;;;;;;昨天;今天;明天; ;;;;;;;下周;两周后;三周后;下个月;一个月之后;";

				case GridStringId.PrintDesignerDescription:
					return "为当前视图设置不同的打印选项.";

				case GridStringId.MenuFooterCustomFormat:
					return "自定={0}";

				case GridStringId.MenuFooterCountGroupFormat:
					return "计数={0}";

				case GridStringId.MenuColumnClearSorting:
					return "清除排序";
			}
			return base.GetLocalizedString(id);
		}
	}

	public class DxperienceXtraBarsLocalizationCHS : BarLocalizer
	{
		protected override CustomizationControl CreateCustomizationControl()
		{
			return new DevExpressXtraBarsCustomizationLocalizationCHS();
		}

		public override string GetLocalizedString(BarString id)
		{
			switch (id)
			{
				case BarString.None:
					return "";

				case BarString.AddOrRemove:
					return "新增或删除按钮(&A)";

				case BarString.ResetBar:
					return "是否确实要重置对 '{0}' 工具栏所作的修改？";

				case BarString.ResetBarCaption:
					return "自定义";

				case BarString.ResetButton:
					return "重设工具栏(&R)";

				case BarString.CustomizeButton:
					return "自定义(&C)...";

				case BarString.ToolBarMenu:
					return "重设(&R)$删除(&D)$!命名(&N)$!标准(&L)$总使用文字(&T)$在菜单中只用文字(&O)$图像与文本(&A)$!开始一组(&G)$常用的(&M)";

				case BarString.ToolbarNameCaption:
					return "工具栏名称(&T):";

				case BarString.NewToolbarCaption:
					return "新建工具栏";

				case BarString.NewToolbarCustomNameFormat:
					return "自定义 {0}";

				case BarString.RenameToolbarCaption:
					return "重新命名";

				case BarString.CustomizeWindowCaption:
					return "自定义";

				case BarString.MenuAnimationSystem:
					return "(系统默认)";

				case BarString.MenuAnimationNone:
					return "空";

				case BarString.MenuAnimationSlide:
					return "滚动";

				case BarString.MenuAnimationFade:
					return "减弱";

				case BarString.MenuAnimationUnfold:
					return "展开";

				case BarString.MenuAnimationRandom:
					return "随机";
			}
			return base.GetLocalizedString(id);
		}
	}

	public class DxperienceXtraEditorsLocalizationCHS : Localizer
	{
		public override string GetLocalizedString(StringId id)
		{
			switch (id)
			{
				case StringId.None:
					return "";

				case StringId.CaptionError:
					return "错误";

				case StringId.InvalidValueText:
					return "无效的值";

				case StringId.CheckChecked:
					return "选中";

				case StringId.CheckUnchecked:
					return "未选中";

				case StringId.CheckIndeterminate:
					return "未选择";

				case StringId.DateEditToday:
					return "今天";

				case StringId.DateEditClear:
					return "清除";

				case StringId.OK:
					return "确定(&O)";

				case StringId.Cancel:
					return "取消(&C)";

				case StringId.NavigatorFirstButtonHint:
					return "第一条";

				case StringId.NavigatorPreviousButtonHint:
					return "上一条";

				case StringId.NavigatorPreviousPageButtonHint:
					return "上一页";

				case StringId.NavigatorNextButtonHint:
					return "下一条";

				case StringId.NavigatorNextPageButtonHint:
					return "下一页";

				case StringId.NavigatorLastButtonHint:
					return "最后一条";

				case StringId.NavigatorAppendButtonHint:
					return "添加";

				case StringId.NavigatorRemoveButtonHint:
					return "删除";

				case StringId.NavigatorEditButtonHint:
					return "编辑";

				case StringId.NavigatorEndEditButtonHint:
					return "结束编辑";

				case StringId.NavigatorCancelEditButtonHint:
					return "取消编辑";

				case StringId.NavigatorTextStringFormat:
					return "记录{0}/{1}";

				case StringId.PictureEditMenuCut:
					return "剪贴";

				case StringId.PictureEditMenuCopy:
					return "复制";

				case StringId.PictureEditMenuPaste:
					return "粘贴";

				case StringId.PictureEditMenuDelete:
					return "删除";

				case StringId.PictureEditMenuLoad:
					return "读取";

				case StringId.PictureEditMenuSave:
					return "保存";

				case StringId.PictureEditOpenFileFilter:
					return "Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg;*.jpeg|Icon Files (*.ico)|*.ico|All Picture Files |*.bmp;*.gif;*.jpg;*.jpeg;*.ico;*.png;*.tif|All Files |*.*";

				case StringId.PictureEditSaveFileFilter:
					return "Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG File Interchange Format (*.jpg)|*.jpg";

				case StringId.PictureEditOpenFileTitle:
					return "打开";

				case StringId.PictureEditSaveFileTitle:
					return "另存为";

				case StringId.PictureEditOpenFileError:
					return "错误的图片格式";

				case StringId.PictureEditOpenFileErrorCaption:
					return "打开错误";

				case StringId.LookUpEditValueIsNull:
					return "[无数据]";

				case StringId.LookUpInvalidEditValueType:
					return "无效的数据类型";

				case StringId.LookUpColumnDefaultName:
					return "名称";

				case StringId.MaskBoxValidateError:
					return "输入数值不完整. 是否须要修改?\r\n\r\n是 - 回到编辑模式以修改数值.\r\n否 - 保持原来数值.\r\n取消 - 回复原来数值.\r\n";

				case StringId.UnknownPictureFormat:
					return "不明图片格式";

				case StringId.DataEmpty:
					return "无图像";

				case StringId.NotValidArrayLength:
					return "无效的数组长度.";

				case StringId.ImagePopupEmpty:
					return "(空)";

				case StringId.ImagePopupPicture:
					return "(图片)";

				case StringId.ColorTabCustom:
					return "自定";

				case StringId.ColorTabWeb:
					return "网络";

				case StringId.ColorTabSystem:
					return "系统";

				case StringId.CalcButtonMC:
					return "MC";

				case StringId.CalcButtonMR:
					return "MR";

				case StringId.CalcButtonMS:
					return "MS";

				case StringId.CalcButtonMx:
					return "M+";

				case StringId.CalcButtonSqrt:
					return "sqrt";

				case StringId.CalcButtonBack:
					return "Back";

				case StringId.CalcButtonCE:
					return "CE";

				case StringId.CalcButtonC:
					return "C";

				case StringId.CalcError:
					return "计算错误";

				case StringId.TabHeaderButtonPrev:
					return "向左滚动";

				case StringId.TabHeaderButtonNext:
					return "向右滚动";

				case StringId.TabHeaderButtonClose:
					return "关闭";

				case StringId.XtraMessageBoxOkButtonText:
					return "确定(&O)";

				case StringId.XtraMessageBoxCancelButtonText:
					return "取消(&C)";

				case StringId.XtraMessageBoxYesButtonText:
					return "是(&Y)";

				case StringId.XtraMessageBoxNoButtonText:
					return "否(&N)";

				case StringId.XtraMessageBoxAbortButtonText:
					return "中断(&A)";

				case StringId.XtraMessageBoxRetryButtonText:
					return "重试(&R)";

				case StringId.XtraMessageBoxIgnoreButtonText:
					return "忽略(&I)";

				case StringId.TextEditMenuUndo:
					return "撤消(&U)";

				case StringId.TextEditMenuCut:
					return "剪贴(&T)";

				case StringId.TextEditMenuCopy:
					return "复制(&C)";

				case StringId.TextEditMenuPaste:
					return "粘贴(&P)";

				case StringId.TextEditMenuDelete:
					return "删除(&D)";

				case StringId.TextEditMenuSelectAll:
					return "全选(&A)";
			}
			return base.GetLocalizedString(id);
		}
		public override string Language
		{
			get
			{
				return "简体中文";
			}
		}
	}

	public class DxperienceXtraLayoutLocalizationCHS : LayoutLocalizer
	{
		public override string GetLocalizedString(LayoutStringId id)
		{
			switch (id)
			{
				case LayoutStringId.CustomizationParentName:
					return "自定义";

				case LayoutStringId.DefaultItemText:
					return "项目";

				case LayoutStringId.DefaultActionText:
					return "默认方式";

				case LayoutStringId.DefaultEmptyText:
					return "无";

				case LayoutStringId.LayoutItemDescription:
					return "版面";

				case LayoutStringId.LayoutGroupDescription:
					return "版面分组";

				case LayoutStringId.TabbedGroupDescription:
					return "版面标签组";

				case LayoutStringId.LayoutControlDescription:
					return "版面控件";

				case LayoutStringId.CustomizationFormTitle:
					return "自定义";

				case LayoutStringId.HiddenItemsPageTitle:
					return "隐藏项目";

				case LayoutStringId.TreeViewPageTitle:
					return "版面视图";

				case LayoutStringId.RenameSelected:
					return "重命名";

				case LayoutStringId.HideItemMenutext:
					return "隐藏项目";

				case LayoutStringId.LockItemSizeMenuText:
					return "锁定项目大小";

				case LayoutStringId.UnLockItemSizeMenuText:
					return "解开锁定项目大小";

				case LayoutStringId.GroupItemsMenuText:
					return "分组";

				case LayoutStringId.UnGroupItemsMenuText:
					return "取消分组";

				case LayoutStringId.CreateTabbedGroupMenuText:
					return "创建标签";

				case LayoutStringId.AddTabMenuText:
					return "新增标签";

				case LayoutStringId.UnGroupTabbedGroupMenuText:
					return "移除标签";

				case LayoutStringId.TreeViewRootNodeName:
					return "根目录";

				case LayoutStringId.ShowCustomizationFormMenuText:
					return "显示自定义窗体";

				case LayoutStringId.HideCustomizationFormMenuText:
					return "隐藏自定义窗体";

				case LayoutStringId.EmptySpaceItemDefaultText:
					return "空项";

				case LayoutStringId.ControlGroupDefaultText:
					return "组";

				case LayoutStringId.EmptyRootGroupText:
					return "拖动到这";

				case LayoutStringId.EmptyTabbedGroupText:
					return "拖动分组到标题区";

				case LayoutStringId.ResetLayoutMenuText:
					return "重置版面";

				case LayoutStringId.RenameMenuText:
					return "重命名";

				case LayoutStringId.TextPositionMenuText:
					return "文本位置";

				case LayoutStringId.TextPositionLeftMenuText:
					return "左";

				case LayoutStringId.TextPositionRightMenuText:
					return "右";

				case LayoutStringId.TextPositionTopMenuText:
					return "上";

				case LayoutStringId.TextPositionBottomMenuText:
					return "下";

				case LayoutStringId.ShowTextMenuItem:
					return "显示文本";

				case LayoutStringId.LockSizeMenuItem:
					return "锁定大小";

				case LayoutStringId.LockWidthMenuItem:
					return "锁定宽度";

				case LayoutStringId.LockHeightMenuItem:
					return "锁定高度";
			}
			return base.GetLocalizedString(id);
		}
		public override string Language
		{
			get
			{
				return "简体中文";
			}
		}

	}

	public class DxperienceXtraPrintingLocalizationCHS : PreviewLocalizer
	{
		public override string GetLocalizedString(PreviewStringId id)
		{
			switch (id)
			{
				case PreviewStringId.Button_Cancel:
					return "取消";

				case PreviewStringId.Button_Ok:
					return "确定";

				case PreviewStringId.Button_Help:
					return "帮助";

				case PreviewStringId.Button_Apply:
					return "应用";

				case PreviewStringId.PreviewForm_Caption:
					return "预览";

				case PreviewStringId.TB_TTip_Customize:
					return "自定义";

				case PreviewStringId.TB_TTip_Print:
					return "打印";

				case PreviewStringId.TB_TTip_PrintDirect:
					return "直接打印";

				case PreviewStringId.TB_TTip_PageSetup:
					return "页面设置";

				case PreviewStringId.TB_TTip_Magnifier:
					return "放大/缩小";

				case PreviewStringId.TB_TTip_ZoomIn:
					return "放大";

				case PreviewStringId.TB_TTip_ZoomOut:
					return "缩小";

				case PreviewStringId.TB_TTip_Zoom:
					return "缩放";

				case PreviewStringId.TB_TTip_Search:
					return "搜索";

				case PreviewStringId.TB_TTip_FirstPage:
					return "首页";

				case PreviewStringId.TB_TTip_PreviousPage:
					return "上一页";

				case PreviewStringId.TB_TTip_NextPage:
					return "下一页";

				case PreviewStringId.TB_TTip_LastPage:
					return "尾页";

				case PreviewStringId.TB_TTip_MultiplePages:
					return "多页";

				case PreviewStringId.TB_TTip_Backgr:
					return "背景色";

				case PreviewStringId.TB_TTip_Close:
					return "退出";

				case PreviewStringId.TB_TTip_EditPageHF:
					return "页眉页脚";

				case PreviewStringId.TB_TTip_HandTool:
					return "手掌工具";

				case PreviewStringId.TB_TTip_Export:
					return "导出文件...";

				case PreviewStringId.TB_TTip_Send:
					return "发送e-mail...";

				case PreviewStringId.TB_TTip_Map:
					return "文档视图";

				case PreviewStringId.TB_TTip_Watermark:
					return "水印";

				case PreviewStringId.MenuItem_PdfDocument:
					return "PDF文件";

				case PreviewStringId.MenuItem_PageLayout:
					return "页面布置(&P)";

				case PreviewStringId.MenuItem_TxtDocument:
					return "Text文件";

				case PreviewStringId.MenuItem_GraphicDocument:
					return "图片文件";

				case PreviewStringId.MenuItem_CsvDocument:
					return "CSV文件";

				case PreviewStringId.MenuItem_MhtDocument:
					return "MHT文件";

				case PreviewStringId.MenuItem_XlsDocument:
					return "Excel文件";

				case PreviewStringId.MenuItem_RtfDocument:
					return "Rich Text文件";

				case PreviewStringId.MenuItem_HtmDocument:
					return "HTML文件";

				case PreviewStringId.SaveDlg_FilterBmp:
					return "BMP Bitmap Format";

				case PreviewStringId.SaveDlg_FilterGif:
					return "GIF Graphics Interchange Format";

				case PreviewStringId.SaveDlg_FilterJpeg:
					return "JPEG File Interchange Format";

				case PreviewStringId.SaveDlg_FilterPng:
					return "PNG Portable Network Graphics Format";

				case PreviewStringId.SaveDlg_FilterTiff:
					return "TIFF Tag Image File Format";

				case PreviewStringId.SaveDlg_FilterEmf:
					return "EMF Enhanced Windows Metafile";

				case PreviewStringId.SaveDlg_FilterWmf:
					return "WMF Windows Metafile";

				case PreviewStringId.SB_TotalPageNo:
					return "总页码:";

				case PreviewStringId.SB_CurrentPageNo:
					return "目前页码:";

				case PreviewStringId.SB_ZoomFactor:
					return "缩放系数:";

				case PreviewStringId.SB_PageNone:
					return "无";

				case PreviewStringId.SB_PageInfo:
					return "{0}/{1}";

				case PreviewStringId.MPForm_Lbl_Pages:
					return "页";

				case PreviewStringId.Msg_EmptyDocument:
					return "此文件没有页面.";

				case PreviewStringId.Msg_CreatingDocument:
					return "正在生成文件...";

				case PreviewStringId.Msg_UnavailableNetPrinter:
					return "网络打印机无法使用.";

				case PreviewStringId.Msg_NeedPrinter:
					return "没有安装打印机.";

				case PreviewStringId.Msg_WrongPrinter:
					return "无效的打印机名称.请检查打印机的设置是否正确.";

				case PreviewStringId.Msg_WrongPageSettings:
					return "打印机不支持所选的纸张大小.\n是否继续打印？";

				case PreviewStringId.Msg_CustomDrawWarning:
					return "警告！";

				case PreviewStringId.Msg_PageMarginsWarning:
					return "一个或以上的边界超出了打印范围.是否继续？";

				case PreviewStringId.Msg_IncorrectPageRange:
					return "设置的页边界不正确";

				case PreviewStringId.Msg_FontInvalidNumber:
					return "字体大小不能为0或负数";

				case PreviewStringId.Msg_NotSupportedFont:
					return "这种字体不被支持";

				case PreviewStringId.Msg_IncorrectZoomFactor:
					return "数字必须在 {0} 与 {1} 之间。";

				case PreviewStringId.Msg_InvalidMeasurement:
					return "不规范";

				case PreviewStringId.Margin_Inch:
					return "英寸";

				case PreviewStringId.Margin_Millimeter:
					return "毫米";

				case PreviewStringId.Margin_TopMargin:
					return "上边界";

				case PreviewStringId.Margin_BottomMargin:
					return "下边界";

				case PreviewStringId.Margin_LeftMargin:
					return "左边界";

				case PreviewStringId.Margin_RightMargin:
					return "右边界";

				case PreviewStringId.ScrollingInfo_Page:
					return "页";

				case PreviewStringId.WMForm_PictureDlg_Title:
					return "选择图片";

				case PreviewStringId.WMForm_ImageStretch:
					return "伸展";

				case PreviewStringId.WMForm_ImageClip:
					return "剪辑";

				case PreviewStringId.WMForm_ImageZoom:
					return "缩放";

				case PreviewStringId.WMForm_Watermark_Asap:
					return "ASAP";

				case PreviewStringId.WMForm_Watermark_Confidential:
					return "CONFIDENTIAL";

				case PreviewStringId.WMForm_Watermark_Copy:
					return "COPY";

				case PreviewStringId.WMForm_Watermark_DoNotCopy:
					return "DO NOT COPY";

				case PreviewStringId.WMForm_Watermark_Draft:
					return "DRAFT";

				case PreviewStringId.WMForm_Watermark_Evaluation:
					return "EVALUATION";

				case PreviewStringId.WMForm_Watermark_Original:
					return "ORIGINAL";

				case PreviewStringId.WMForm_Watermark_Personal:
					return "PERSONAL";

				case PreviewStringId.WMForm_Watermark_Sample:
					return "SAMPLE";

				case PreviewStringId.WMForm_Watermark_TopSecret:
					return "TOP SECRET";

				case PreviewStringId.WMForm_Watermark_Urgent:
					return "URGENT";

				case PreviewStringId.WMForm_Direction_Horizontal:
					return "横向";

				case PreviewStringId.WMForm_Direction_Vertical:
					return "纵向";

				case PreviewStringId.WMForm_Direction_BackwardDiagonal:
					return "反向倾斜";

				case PreviewStringId.WMForm_Direction_ForwardDiagonal:
					return "正向倾斜";

				case PreviewStringId.WMForm_VertAlign_Bottom:
					return "底端";

				case PreviewStringId.WMForm_VertAlign_Middle:
					return "中间";

				case PreviewStringId.WMForm_VertAlign_Top:
					return "顶端";

				case PreviewStringId.WMForm_HorzAlign_Left:
					return "靠左";

				case PreviewStringId.WMForm_HorzAlign_Center:
					return "置中";

				case PreviewStringId.WMForm_HorzAlign_Right:
					return "靠右";

				case PreviewStringId.WMForm_ZOrderRgrItem_InFront:
					return "在前面";

				case PreviewStringId.WMForm_ZOrderRgrItem_Behind:
					return "在后面";

				case PreviewStringId.WMForm_PageRangeRgrItem_All:
					return "全部";

				case PreviewStringId.WMForm_PageRangeRgrItem_Pages:
					return "页码";

				case PreviewStringId.SaveDlg_Title:
					return "另存为";

				case PreviewStringId.SaveDlg_FilterPdf:
					return "PDF文件";

				case PreviewStringId.SaveDlg_FilterTxt:
					return "Txt文件";

				case PreviewStringId.SaveDlg_FilterCsv:
					return "CSV文件";

				case PreviewStringId.SaveDlg_FilterMht:
					return "MHT文件";

				case PreviewStringId.SaveDlg_FilterXls:
					return "Excel文件";

				case PreviewStringId.SaveDlg_FilterRtf:
					return "Rtf文件";

				case PreviewStringId.SaveDlg_FilterHtm:
					return "HTML文件";

				case PreviewStringId.MenuItem_File:
					return "文件(&F)";

				case PreviewStringId.MenuItem_View:
					return "视图(&V)";

				case PreviewStringId.MenuItem_Background:
					return "背景(&B)";

				case PreviewStringId.MenuItem_PageSetup:
					return "页面设置(&U)";

				case PreviewStringId.MenuItem_Print:
					return "打印(&P)...";

				case PreviewStringId.MenuItem_PrintDirect:
					return "直接打印(&R)";

				case PreviewStringId.MenuItem_Export:
					return "导出(&E)";

				case PreviewStringId.MenuItem_Send:
					return "发送(&D)";

				case PreviewStringId.MenuItem_Exit:
					return "退出(&X)";

				case PreviewStringId.MenuItem_ViewToolbar:
					return "工具栏(&T)";

				case PreviewStringId.MenuItem_ViewStatusbar:
					return "状态栏(&S)";

				case PreviewStringId.MenuItem_ViewContinuous:
					return "连续";

				case PreviewStringId.MenuItem_ViewFacing:
					return "对页";

				case PreviewStringId.MenuItem_BackgrColor:
					return "颜色(&C)...";

				case PreviewStringId.MenuItem_Watermark:
					return "水印(&W)...";

				case PreviewStringId.MenuItem_ZoomPageWidth:
					return "页宽";

				case PreviewStringId.MenuItem_ZoomTextWidth:
					return "整页";

				case PreviewStringId.PageInfo_PageNumber:
					return "[Page #]";

				case PreviewStringId.PageInfo_PageNumberOfTotal:
					return "[Page # of Pages #]";

				case PreviewStringId.PageInfo_PageDate:
					return "[Date Printed]";

				case PreviewStringId.PageInfo_PageTime:
					return "[Time Printed]";

				case PreviewStringId.PageInfo_PageUserName:
					return "[User Name]";

				case PreviewStringId.EMail_From:
					return "From";

				case PreviewStringId.BarText_Toolbar:
					return "工具栏";

				case PreviewStringId.BarText_MainMenu:
					return "主菜单";

				case PreviewStringId.BarText_StatusBar:
					return "状态栏";
			}
			return base.GetLocalizedString(id);
		}
		public override string Language
		{
			get
			{
				return "简体中文";
			}
		}

	}

	public class DxperienceXtraReportsLocalizationCHS : ReportLocalizer
	{
		public override string GetLocalizedString(ReportStringId id)
		{
			switch (id)
			{
				case ReportStringId.Msg_FileNotFound:
					return "文件没有找到";

				case ReportStringId.Msg_WrongReportClassName:
					return "一个错误发生在并行化期间 - 可能是报表类名错误";

				case ReportStringId.Msg_CreateReportInstance:
					return "您试图打开一个不同类型的报表来编辑。\r\n是否确定建立实例？";

				case ReportStringId.Msg_FileCorrupted:
					return "不能加载报表，文件可能被破坏或者报表组件丢失。";

				case ReportStringId.Msg_FileContentCorrupted:
					return "不能加载报表布局，文件可能损坏或包含错误的信息。";

				case ReportStringId.Msg_IncorrectArgument:
					return "参数值输入不正确";

				case ReportStringId.Msg_InvalidMethodCall:
					return "对象的当前状态下不能调用此方法";

				case ReportStringId.Msg_ScriptError:
					return "在脚本中发现错误：\r\n{0}";

				case ReportStringId.Msg_ScriptExecutionError:
					return "在脚本执行过程中发现错误 {0}:\r\n {1}\r\n过程 {0} 被运行，将不能再被调用。";

				case ReportStringId.Msg_InvalidReportSource:
					return "无法设置原报表";

				case ReportStringId.Msg_IncorrectBandType:
					return "无效的带型";

				case ReportStringId.Msg_InvPropName:
					return "无效的属性名";

				case ReportStringId.Msg_CantFitBarcodeToControlBounds:
					return "条形码控件的边界太小";

				case ReportStringId.Msg_InvalidBarcodeText:
					return "在文本中有无效的字符";

				case ReportStringId.Msg_InvalidBarcodeTextFormat:
					return "无效的文本格式";

				case ReportStringId.Msg_CreateSomeInstance:
					return "在窗体中不能建立两个实例类。";

				case ReportStringId.Msg_DontSupportMulticolumn:
					return "详细报表不支持多字段。";

				case ReportStringId.Msg_FillDataError:
					return "数据加载时发生错误。错误为：";

				case ReportStringId.Msg_CyclicBoormarks:
					return "报表循环书签";

				case ReportStringId.Msg_LargeText:
					return "文本太长";

				case ReportStringId.Msg_ScriptingPermissionErrorMessage:
					return "在此报表不允许运行脚本。\n\n信息:\n\n{0}";

				case ReportStringId.Msg_ScriptingErrorTitle:
					return "脚本错误";

				case ReportStringId.Cmd_InsertDetailReport:
					return "插入详细报表";

				case ReportStringId.Cmd_InsertUnboundDetailReport:
					return "非绑定";

				case ReportStringId.Cmd_ViewCode:
					return "检视代码";

				case ReportStringId.Cmd_BringToFront:
					return "移到最上层";

				case ReportStringId.Cmd_SendToBack:
					return "移到最下层";

				case ReportStringId.Cmd_AlignToGrid:
					return "对齐网格线";

				case ReportStringId.Cmd_TopMargin:
					return "顶端边缘";

				case ReportStringId.Cmd_BottomMargin:
					return "底端边缘";

				case ReportStringId.Cmd_ReportHeader:
					return "报表首";

				case ReportStringId.Cmd_ReportFooter:
					return "报表尾";

				case ReportStringId.Cmd_PageHeader:
					return "页首";

				case ReportStringId.Cmd_PageFooter:
					return "页尾";

				case ReportStringId.Cmd_GroupHeader:
					return "群组首";

				case ReportStringId.Cmd_GroupFooter:
					return "群组尾";

				case ReportStringId.Cmd_Detail:
					return "详细";

				case ReportStringId.Cmd_DetailReport:
					return "详细报表";

				case ReportStringId.Cmd_RtfClear:
					return "清除";

				case ReportStringId.Cmd_RtfLoad:
					return "加载文件...";

				case ReportStringId.Cmd_TableInsert:
					return "插入(&I)";

				case ReportStringId.Cmd_TableInsertRowAbove:
					return "上行(&A)";

				case ReportStringId.Cmd_TableInsertRowBelow:
					return "下行(&B)";

				case ReportStringId.Cmd_TableInsertColumnToLeft:
					return "左列(&L)";

				case ReportStringId.Cmd_TableInsertColumnToRight:
					return "右列(&R)";

				case ReportStringId.Cmd_TableInsertCell:
					return "单元格(&C)";

				case ReportStringId.Cmd_TableDelete:
					return "删除(&L)";

				case ReportStringId.Cmd_TableDeleteRow:
					return "行(&R)";

				case ReportStringId.Cmd_TableDeleteColumn:
					return "列(&C)";

				case ReportStringId.Cmd_TableDeleteCell:
					return "单元格(&L)";

				case ReportStringId.Cmd_Cut:
					return "剪贴";

				case ReportStringId.Cmd_Copy:
					return "复制";

				case ReportStringId.Cmd_Paste:
					return "粘贴";

				case ReportStringId.Cmd_Delete:
					return "删除";

				case ReportStringId.Cmd_Properties:
					return "属性";

				case ReportStringId.Cmd_InsertBand:
					return "插入区段";

				case ReportStringId.CatLayout:
					return "布局";

				case ReportStringId.CatAppearance:
					return "版面";

				case ReportStringId.CatData:
					return "数据";

				case ReportStringId.CatBehavior:
					return "状态";

				case ReportStringId.CatNavigation:
					return "导航";

				case ReportStringId.CatPageSettings:
					return "页面设置";

				case ReportStringId.CatUserDesigner:
					return "用户设计";

				case ReportStringId.BandDsg_QuantityPerPage:
					return "一个页面集合";

				case ReportStringId.BandDsg_QuantityPerReport:
					return "一个报表集合";

				case ReportStringId.UD_ReportDesigner:
					return "XtraReport设计";

				case ReportStringId.UD_Msg_ReportChanged:
					return "报表内容已被修改，是否须要储存？";

				case ReportStringId.UD_TTip_FileOpen:
					return "打开文件";

				case ReportStringId.UD_TTip_FileSave:
					return "保存文件";

				case ReportStringId.UD_TTip_EditCut:
					return "剪贴";

				case ReportStringId.UD_TTip_EditCopy:
					return "复制";

				case ReportStringId.UD_TTip_EditPaste:
					return "粘贴";

				case ReportStringId.UD_TTip_Undo:
					return "撤消";

				case ReportStringId.UD_TTip_Redo:
					return "恢复";

				case ReportStringId.UD_TTip_AlignToGrid:
					return "对齐网格线";

				case ReportStringId.UD_TTip_AlignLeft:
					return "对齐主控项的左缘";

				case ReportStringId.UD_TTip_AlignVerticalCenters:
					return "对齐主控项的水平中央";

				case ReportStringId.UD_TTip_AlignRight:
					return "对齐主控项的右缘";

				case ReportStringId.UD_TTip_AlignTop:
					return "对齐主控项的上缘";

				case ReportStringId.UD_TTip_AlignHorizontalCenters:
					return "对齐主控项的垂直中间";

				case ReportStringId.UD_TTip_AlignBottom:
					return "对齐主控项的下缘";

				case ReportStringId.UD_TTip_SizeToControlWidth:
					return "设置成相同宽度";

				case ReportStringId.UD_TTip_SizeToGrid:
					return "依网格线调整大小";

				case ReportStringId.UD_TTip_SizeToControlHeight:
					return "设置成相同高度";

				case ReportStringId.UD_TTip_SizeToControl:
					return "设置成相同大小";

				case ReportStringId.UD_TTip_HorizSpaceMakeEqual:
					return "将水平间距设成相等";

				case ReportStringId.UD_TTip_HorizSpaceIncrease:
					return "增加水平间距";

				case ReportStringId.UD_TTip_HorizSpaceDecrease:
					return "减少水平间距";

				case ReportStringId.UD_TTip_HorizSpaceConcatenate:
					return "移除水平间距";

				case ReportStringId.UD_TTip_VertSpaceMakeEqual:
					return "将垂直间距设为相等";

				case ReportStringId.UD_TTip_VertSpaceIncrease:
					return "增加垂直间距";

				case ReportStringId.UD_TTip_VertSpaceDecrease:
					return "减少垂直间距";

				case ReportStringId.UD_TTip_VertSpaceConcatenate:
					return "移除垂直间距";

				case ReportStringId.UD_TTip_CenterHorizontally:
					return "水平置中";

				case ReportStringId.UD_TTip_CenterVertically:
					return "垂直置中";

				case ReportStringId.UD_TTip_BringToFront:
					return "移到最上层";

				case ReportStringId.UD_TTip_SendToBack:
					return "移到最下层";

				case ReportStringId.UD_TTip_FormatBold:
					return "粗体";

				case ReportStringId.UD_TTip_FormatItalic:
					return "斜体";

				case ReportStringId.UD_TTip_FormatUnderline:
					return "下划线";

				case ReportStringId.UD_TTip_FormatAlignLeft:
					return "左对齐";

				case ReportStringId.UD_TTip_FormatCenter:
					return "居中";

				case ReportStringId.UD_TTip_FormatAlignRight:
					return "右对齐";

				case ReportStringId.UD_TTip_FormatFontName:
					return "字体";

				case ReportStringId.UD_TTip_FormatFontSize:
					return "大小";

				case ReportStringId.UD_TTip_FormatForeColor:
					return "前景颜色";

				case ReportStringId.UD_TTip_FormatBackColor:
					return "背景颜色";

				case ReportStringId.UD_TTip_FormatJustify:
					return "两端对齐";

				case ReportStringId.UD_FormCaption:
					return "XtraReport设计";

				//case ReportStringId.VS_XtraReportsToolboxCategoryName:
				//    return "Developer Express: 报表";

				case ReportStringId.UD_XtraReportsToolboxCategoryName:
					return "标准控制";

				case ReportStringId.UD_XtraReportsPointerItemCaption:
					return "指针";

				case ReportStringId.Verb_EditBands:
					return "编辑区域...";

				case ReportStringId.Verb_EditGroupFields:
					return "编辑组字段...";

				case ReportStringId.Verb_Import:
					return "导入...";

				case ReportStringId.Verb_Save:
					return "保存...";

				case ReportStringId.Verb_About:
					return "关于...";

				case ReportStringId.Verb_RTFClear:
					return "清除";

				case ReportStringId.Verb_RTFLoad:
					return "加载文件...";

				case ReportStringId.Verb_FormatString:
					return "格式化字符串...";

				case ReportStringId.Verb_SummaryWizard:
					return "总结...";

				case ReportStringId.Verb_ReportWizard:
					return "向导...";

				case ReportStringId.Verb_Insert:
					return "插入...";

				case ReportStringId.Verb_Delete:
					return "删除...";

				case ReportStringId.Verb_Bind:
					return "赋值";

				case ReportStringId.Verb_EditText:
					return "文本编辑";

				//case ReportStringId.FSForm_Lbl_Category:
				//    return "类别";

				//case ReportStringId.FSForm_Lbl_Prefix:
				//    return "上标";

				//case ReportStringId.FSForm_Lbl_Suffix:
				//    return "下标";

				//case ReportStringId.FSForm_Lbl_CustomGeneral:
				//    return "一般格式不包含特殊数字格式";

				//case ReportStringId.FSForm_GrBox_Sample:
				//    return "范例";

				//case ReportStringId.FSForm_Tab_StandardTypes:
				//    return "标准类型";

				//case ReportStringId.FSForm_Tab_Custom:
				//    return "自定义";

				//case ReportStringId.FSForm_Msg_BadSymbol:
				//    return "损坏的符号";

				//case ReportStringId.FSForm_Btn_Delete:
				//    return "删除";

				case ReportStringId.BCForm_Lbl_Property:
					return "属性";

				case ReportStringId.BCForm_Lbl_Binding:
					return "结合";

				case ReportStringId.SSForm_Caption:
					return "式样单编辑";

				case ReportStringId.SSForm_Btn_Close:
					return "关闭";

				case ReportStringId.SSForm_Msg_NoStyleSelected:
					return "没有式样被选中";

				case ReportStringId.SSForm_Msg_MoreThanOneStyle:
					return "你选择了多过一个以上的式样";

				case ReportStringId.SSForm_Msg_SelectedStylesText:
					return "被选中的式样...";

				case ReportStringId.SSForm_Msg_StyleSheetError:
					return "StyleSheet错误";

				case ReportStringId.SSForm_Msg_InvalidFileFormat:
					return "无效的文件格式";

				case ReportStringId.SSForm_Msg_StyleNamePreviewPostfix:
					return " 式样";

				case ReportStringId.SSForm_Msg_FileFilter:
					return "Report StyleSheet files (*.repss)|*.repss|All files (*.*)|*.*";

				case ReportStringId.SSForm_TTip_AddStyle:
					return "添加式样";

				case ReportStringId.SSForm_TTip_RemoveStyle:
					return "移除式样";

				case ReportStringId.SSForm_TTip_ClearStyles:
					return "清除式样";

				case ReportStringId.SSForm_TTip_PurgeStyles:
					return "清除式样";

				case ReportStringId.SSForm_TTip_SaveStyles:
					return "保存式样到文件";

				case ReportStringId.SSForm_TTip_LoadStyles:
					return "从文件中读入式样";

				case ReportStringId.FindForm_Msg_FinishedSearching:
					return "搜索文件完成";

				case ReportStringId.FindForm_Msg_TotalFound:
					return "合计查找:";

				case ReportStringId.RepTabCtl_HtmlView:
					return "HTML视图";

				case ReportStringId.RepTabCtl_Preview:
					return "预览";

				case ReportStringId.RepTabCtl_Designer:
					return "设计";

				case ReportStringId.PanelDesignMsg:
					return "在此可放置不同控件";

				case ReportStringId.MultiColumnDesignMsg1:
					return "重复列之间的空位";

				case ReportStringId.MultiColumnDesignMsg2:
					return "控件位置不正确，将会导致打印错误";

				case ReportStringId.UD_Group_File:
					return "文件(&F)";

				case ReportStringId.UD_Group_Edit:
					return "编辑(&E)";

				case ReportStringId.UD_Group_View:
					return "视图(&V)";

				case ReportStringId.UD_Group_Format:
					return "格式(&R)";

				case ReportStringId.UD_Group_Font:
					return "字体(&F)";

				case ReportStringId.UD_Group_Justify:
					return "两端对齐(&J)";

				case ReportStringId.UD_Group_Align:
					return "对齐(&A)";

				case ReportStringId.UD_Group_MakeSameSize:
					return "设置成相同大小(M)";

				case ReportStringId.UD_Group_HorizontalSpacing:
					return "水平间距(&H)";

				case ReportStringId.UD_Group_VerticalSpacing:
					return "垂直间距(&V)";

				case ReportStringId.UD_Group_CenterInForm:
					return "对齐窗体中央(&C)";

				case ReportStringId.UD_Group_Order:
					return "顺序(&O)";

				case ReportStringId.UD_Group_ToolbarsList:
					return "工具栏(&T)";

				case ReportStringId.UD_Group_DockPanelsList:
					return "窗口(&W)";

				case ReportStringId.UD_Capt_MainMenuName:
					return "主菜单";

				case ReportStringId.UD_Capt_ToolbarName:
					return "工具栏";

				case ReportStringId.UD_Capt_LayoutToolbarName:
					return "布局工具栏";

				case ReportStringId.UD_Capt_FormattingToolbarName:
					return "格式工具栏";

				case ReportStringId.UD_Capt_StatusBarName:
					return "状态栏";

				case ReportStringId.UD_Capt_NewReport:
					return "新增(&N)";

				case ReportStringId.UD_Capt_NewWizardReport:
					return "向导(&W)";

				case ReportStringId.UD_Capt_OpenFile:
					return "开启(&O)";

				case ReportStringId.UD_Capt_SaveFile:
					return "保存(&S)";

				case ReportStringId.UD_Capt_SaveFileAs:
					return "另存为(&A)...";

				case ReportStringId.UD_Capt_Exit:
					return "结束(&X)";

				case ReportStringId.UD_Capt_Cut:
					return "剪切(&T)";

				case ReportStringId.UD_Capt_Copy:
					return "复制(&C)";

				case ReportStringId.UD_Capt_Paste:
					return "粘贴(&P)";

				case ReportStringId.UD_Capt_Delete:
					return "删除(&D)";

				case ReportStringId.UD_Capt_SelectAll:
					return "全选(&A)";

				case ReportStringId.UD_Capt_Undo:
					return "复原(&U)";

				case ReportStringId.UD_Capt_Redo:
					return "重复(&R)";

				case ReportStringId.UD_Capt_ForegroundColor:
					return "前景色(&E)";

				case ReportStringId.UD_Capt_BackGroundColor:
					return "背景色(&K)";

				case ReportStringId.UD_Capt_FontBold:
					return "粗体(&B)";

				case ReportStringId.UD_Capt_FontItalic:
					return "斜体(&I)";

				case ReportStringId.UD_Capt_FontUnderline:
					return "底线(&U)";

				case ReportStringId.UD_Capt_JustifyLeft:
					return "靠左(&L)";

				case ReportStringId.UD_Capt_JustifyCenter:
					return "居中(&C)";

				case ReportStringId.UD_Capt_JustifyRight:
					return "靠右(&R)";

				case ReportStringId.UD_Capt_JustifyJustify:
					return "两端对齐(&J)";

				case ReportStringId.UD_Capt_AlignLefts:
					return "左(&L)";

				case ReportStringId.UD_Capt_AlignCenters:
					return "置中(&C)";

				case ReportStringId.UD_Capt_AlignRights:
					return "右(&R)";

				case ReportStringId.UD_Capt_AlignTops:
					return "上(&T)";

				case ReportStringId.UD_Capt_AlignMiddles:
					return "中间(&M)";

				case ReportStringId.UD_Capt_AlignBottoms:
					return "下(&B)";

				case ReportStringId.UD_Capt_AlignToGrid:
					return "网格线(&G)";

				case ReportStringId.UD_Capt_MakeSameSizeWidth:
					return "宽度(&W)";

				case ReportStringId.UD_Capt_MakeSameSizeSizeToGrid:
					return "依网格线调整大小(&D)";

				case ReportStringId.UD_Capt_MakeSameSizeHeight:
					return "高度(&H)";

				case ReportStringId.UD_Capt_MakeSameSizeBoth:
					return "两者(&B)";

				case ReportStringId.UD_Capt_SpacingMakeEqual:
					return "设成相等(&E)";

				case ReportStringId.UD_Capt_SpacingIncrease:
					return "增加(&I)";

				case ReportStringId.UD_Capt_SpacingDecrease:
					return "减少(&D)";

				case ReportStringId.UD_Capt_SpacingRemove:
					return "移除(&R)";

				case ReportStringId.UD_Capt_CenterInFormHorizontally:
					return "水平(&H)";

				case ReportStringId.UD_Capt_CenterInFormVertically:
					return "垂直(&V)";

				case ReportStringId.UD_Capt_OrderBringToFront:
					return "提到最上层(&B)";

				case ReportStringId.UD_Capt_OrderSendToBack:
					return "移到最下层(&S)";

				case ReportStringId.UD_Hint_NewReport:
					return "创建新报表";

				case ReportStringId.UD_Hint_NewWizardReport:
					return "用向导创建新报表";

				case ReportStringId.UD_Hint_OpenFile:
					return "打开报表";

				case ReportStringId.UD_Hint_SaveFile:
					return "储存报表";

				case ReportStringId.UD_Hint_SaveFileAs:
					return "另起新名储存报表";

				case ReportStringId.UD_Hint_Exit:
					return "关闭设计师";

				case ReportStringId.UD_Hint_Cut:
					return "删除控件和复制到剪贴板";

				case ReportStringId.UD_Hint_Copy:
					return "复制控件到剪贴板";

				case ReportStringId.UD_Hint_Paste:
					return "从剪贴板添加控件";

				case ReportStringId.UD_Hint_Delete:
					return "删除控件";

				case ReportStringId.UD_Hint_SelectAll:
					return "全选";

				case ReportStringId.UD_Hint_Undo:
					return "复原最后操作";

				case ReportStringId.UD_Hint_Redo:
					return "重复最后操作";

				case ReportStringId.UD_Hint_ForegroundColor:
					return "设置前景色";

				case ReportStringId.UD_Hint_BackGroundColor:
					return "设置背景色";

				case ReportStringId.UD_Hint_FontBold:
					return "粗体";

				case ReportStringId.UD_Hint_FontItalic:
					return "斜体";

				case ReportStringId.UD_Hint_FontUnderline:
					return "底线";

				case ReportStringId.UD_Hint_JustifyLeft:
					return "靠左";

				case ReportStringId.UD_Hint_JustifyCenter:
					return "居中";

				case ReportStringId.UD_Hint_JustifyRight:
					return "靠右";

				case ReportStringId.UD_Hint_JustifyJustify:
					return "两端对齐";

				case ReportStringId.UD_Hint_AlignLefts:
					return "被选控件左对齐";

				case ReportStringId.UD_Hint_AlignCenters:
					return "被选纵向控件居中对齐";

				case ReportStringId.UD_Hint_AlignRights:
					return "被选控件右对齐";

				case ReportStringId.UD_Hint_AlignTops:
					return "被选控件上对齐";

				case ReportStringId.UD_Hint_AlignMiddles:
					return "被选横向控件居中对齐";

				case ReportStringId.UD_Hint_AlignBottoms:
					return "被选控件下对齐";

				case ReportStringId.UD_Hint_AlignToGrid:
					return "被选控件依线格对齐";

				case ReportStringId.UD_Hint_MakeSameSizeWidth:
					return "被选控件设置成相同宽度";

				case ReportStringId.UD_Hint_MakeSameSizeSizeToGrid:
					return "被选控件依线格调整大小";

				case ReportStringId.UD_Hint_MakeSameSizeHeight:
					return "被选控件设置成相同高度";

				case ReportStringId.UD_Hint_MakeSameSizeBoth:
					return "被选控件设置成相同大小";

				case ReportStringId.UD_Hint_SpacingMakeEqual:
					return "被选控件间距设成相等";

				case ReportStringId.UD_Hint_SpacingIncrease:
					return "增加被选控件的间距";

				case ReportStringId.UD_Hint_SpacingDecrease:
					return "减少被选控件的间距";

				case ReportStringId.UD_Hint_SpacingRemove:
					return "移除被选控件的间距";

				case ReportStringId.UD_Hint_CenterInFormHorizontally:
					return "被选控件水平对齐窗体中央";

				case ReportStringId.UD_Hint_CenterInFormVertically:
					return "被选控件垂直对齐窗体中央";

				case ReportStringId.UD_Hint_OrderBringToFront:
					return "被选控件提到最上层";

				case ReportStringId.UD_Hint_OrderSendToBack:
					return "被选控件提到最下层";

				case ReportStringId.UD_Hint_ViewBars:
					return "显示/隐藏{0}";

				case ReportStringId.UD_Hint_ViewDockPanels:
					return "显示/隐藏 {0} 窗口";

				case ReportStringId.UD_Hint_ViewTabs:
					return "转到 {0} 标签";

				case ReportStringId.UD_Title_FieldList:
					return "字段清单";

				case ReportStringId.UD_Title_ReportExplorer:
					return "报表资源管理器";

				case ReportStringId.UD_Title_PropertyGrid:
					return "属性";

				case ReportStringId.UD_Title_ToolBox:
					return "工具箱";

				//case ReportStringId.STag_Name_Text:
				//    return "文本";

				case ReportStringId.STag_Name_DataBinding:
					return "数据连接";

				case ReportStringId.STag_Name_FormatString:
					return "字符串格式";

				//case ReportStringId.STag_Name_ForeColor:
				//    return "前景色";

				//case ReportStringId.STag_Name_BackColor:
				//    return "背景色";

				//case ReportStringId.STag_Name_Font:
				//    return "字体";

				//case ReportStringId.STag_Name_LineDirection:
				//    return "线条方向";

				//case ReportStringId.STag_Name_LineStyle:
				//    return "线条样式";

				//case ReportStringId.STag_Name_LineWidth:
				//    return "线条宽度";

				//case ReportStringId.STag_Name_CanGrow:
				//    return "增长";

				//case ReportStringId.STag_Name_CanShrink:
				//    return "收缩";

				//case ReportStringId.STag_Name_Multiline:
				//    return "多线";

				//case ReportStringId.STag_Name_Summary:
				//    return "摘要";

				//case ReportStringId.STag_Name_Symbology:
				//    return "符号";

				//case ReportStringId.STag_Name_Module:
				//    return "模块数";

				//case ReportStringId.STag_Name_ShowText:
				//    return "显示文本";

				//case ReportStringId.STag_Name_SegmentWidth:
				//    return "分段宽度";

				case ReportStringId.STag_Name_Checked:
					return "选中";

				//case ReportStringId.STag_Name_Image:
				//    return "图像";

				//case ReportStringId.STag_Name_ImageUrl:
				//    return "图像位置(URL)";

				//case ReportStringId.STag_Name_ImageSizing:
				//    return "图象尺寸";

				//case ReportStringId.STag_Name_ReportSource:
				//    return "报表来源";

				case ReportStringId.STag_Name_PreviewRowCount:
					return "预览行数";

				//case ReportStringId.STag_Name_ShrinkSubReportIconArea:
				//    return "收缩子报表的图标区域";

				//case ReportStringId.STag_Name_PageInfo:
				//    return "页码信息";

				//case ReportStringId.STag_Name_StartPageNumber:
				//    return "起始页码";

				//case ReportStringId.STag_Name_Format:
				//    return "格式";

				//case ReportStringId.STag_Name_KeepTogether:
				//    return "保持一致";

				case ReportStringId.STag_Name_Bands:
					return "区";

				case ReportStringId.STag_Name_Height:
					return "高度";

				//case ReportStringId.STag_Name_RepeatEveryPage:
				//    return "重复每个页面";

				//case ReportStringId.STag_Name_PrintAtBottom:
				//    return "打印在底端";

				//case ReportStringId.STag_Name_GroupFields:
				//    return "群组区段";

				//case ReportStringId.STag_Name_SortFields:
				//    return "排序字段";

				//case ReportStringId.STag_Name_GroupUnion:
				//    return "群组并集";

				//case ReportStringId.STag_Name_Level:
				//    return "层次";

				case ReportStringId.STag_Name_ColumnMode:
					return "列模式";

				case ReportStringId.STag_Name_ColumnCount:
					return "列计算";

				case ReportStringId.STag_Name_ColumnWidth:
					return "列宽";

				case ReportStringId.STag_Name_ColumnSpacing:
					return "列距";

					//case ReportStringId.STag_Name_Direction:
					//    return "汇总数据位置";

					//case ReportStringId.STag_Name_Watermark:
					//    return "水印";

					//case ReportStringId.STag_Name_ReportUnit:
					//    return "报表单元";

					//case ReportStringId.STag_Name_DataSource:
					//    return "数据来源";

					//case ReportStringId.STag_Name_DataMember:
					//    return "数据项";

					//case ReportStringId.STag_Name_DataAdapter:
					//    return "数据适配器";
			}
			return base.GetLocalizedString(id);
		}

	}

	public class DxperienceXtraTreeListLocalizationCHS : TreeListLocalizer
	{
		public override string GetLocalizedString(TreeListStringId id)
		{
			string text1 = string.Empty;
			switch (id)
			{
				case TreeListStringId.MenuFooterSum:
					return "合计";

				case TreeListStringId.MenuFooterMin:
					return "最小值";

				case TreeListStringId.MenuFooterMax:
					return "最大值";

				case TreeListStringId.MenuFooterCount:
					return "计数";

				case TreeListStringId.MenuFooterAverage:
					return "平均";

				case TreeListStringId.MenuFooterNone:
					return "无";

				case TreeListStringId.MenuFooterAllNodes:
					return "全部节点";

				case TreeListStringId.MenuFooterSumFormat:
					return "合计={0:#.##}";

				case TreeListStringId.MenuFooterMinFormat:
					return "最小值={0}";

				case TreeListStringId.MenuFooterMaxFormat:
					return "最大值={0}";

				case TreeListStringId.MenuFooterCountFormat:
					return "{0}";

				case TreeListStringId.MenuFooterAverageFormat:
					return "平均值={0:#.##}";

				case TreeListStringId.MenuColumnSortAscending:
					return "升序排序";

				case TreeListStringId.MenuColumnSortDescending:
					return "降序排序";

				case TreeListStringId.MenuColumnColumnCustomization:
					return "列选择";

				case TreeListStringId.MenuColumnBestFit:
					return "最佳匹配";

				case TreeListStringId.MenuColumnBestFitAllColumns:
					return "最佳匹配 (所有列)";

				case TreeListStringId.ColumnCustomizationText:
					return "自定显示字段";

				case TreeListStringId.ColumnNamePrefix:
					return "列名首标";

				case TreeListStringId.PrintDesignerHeader:
					return "打印设置";

				case TreeListStringId.PrintDesignerDescription:
					return "为当前的树状列表设置不同的打印选项.";

				case TreeListStringId.InvalidNodeExceptionText:
					return "是否确定要修改？";

				case TreeListStringId.MultiSelectMethodNotSupported:
					return "OptionsBehavior.MultiSelect未激活时，指定方法不能工作.";
			}
			return base.GetLocalizedString(id);
		}

	}

}