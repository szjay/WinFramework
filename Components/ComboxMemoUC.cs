//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ComboxMemoUC.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Utils.Design;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	[ToolboxItem(true)]
	public partial class ComboxMemoUC : XtraUserControl
	{
		public ComboxMemoUC()
		{
			InitializeComponent();
			this.Load += ComboxMemoUC_Load;
		}

		void ComboxMemoUC_Load(object sender, EventArgs e)
		{
			if (ControlUtil.IsDesignMode(this))
			{
				return;
			}

			comboBoxEdit1.Visible = false;

			memoEdit1.Click += memoEdit1_Click;
			memoEdit1.Leave += memoEdit1_Leave;
			comboBoxEdit1.SelectedIndexChanged += comboBoxEdit1_SelectedIndexChanged;
		}

		public override string Text
		{
			get
			{
				return Memo;
			}
			set
			{
				Memo = value;
			}
		}

		[Bindable(true)]
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DXCategory("Appearance")]
		[RefreshProperties(RefreshProperties.All)]
		[SmartTagProperty("Text", "", SmartTagActionType.RefreshBoundsAfterExecute)]
		[SettingsBindable(true)]
		public string Memo
		{
			get
			{
				return memoEdit1.Text;
			}
			set
			{
				memoEdit1.Text = value;
			}
		}

		public void AddItems(IEnumerable<string> strList)
		{
			foreach (string s in strList)
			{
				comboBoxEdit1.Properties.Items.Add(s);
			}
		}

		public void ClearItems()
		{
			comboBoxEdit1.Properties.Items.Clear();
		}

		void memoEdit1_Click(object sender, EventArgs e)
		{
			comboBoxEdit1.Visible = true;
		}

		void memoEdit1_Leave(object sender, EventArgs e)
		{
			comboBoxEdit1.Visible = false;
		}

		void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
		{
			memoEdit1.Text += comboBoxEdit1.Text;
			comboBoxEdit1.Visible = false;
			comboBoxEdit1.Text = "";
			memoEdit1.Focus();
			memoEdit1.SelectionStart = memoEdit1.Text.Length;
		}
	}
}
