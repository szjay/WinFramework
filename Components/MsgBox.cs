//==============================================================
//  版权所有：深圳杰文科技
//  文件名：MsgBox.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;

namespace Infrastructure.Components
{
	public static class MsgBox
	{
		public static string Title = "";

		public static void Show(string msg, params object[] args)
		{
			if (args != null && args.Length > 0)
			{
				msg = string.Format(msg, args);
			}
			MsgHintForm.Show(msg);
		}

		public static bool Confirm(string msg, params object[] args)
		{
			if (args != null && args.Length > 0)
			{
				msg = string.Format(msg, args);
			}
			DialogResult result = MessageBox.Show(msg, "请确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			return result == DialogResult.Yes;
		}

		public static void Warning(string msg, params object[] args)
		{
			if (args != null && args.Length > 0)
			{
				msg = string.Format(msg, args);
			}
			MessageBox.Show(msg, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public static void Wait(Action action, string msg, params object[] args)
		{
			Wait(msg, args);
			try
			{
				action.Invoke();
			}
			finally
			{
				CloseWait();
			}
		}

		private static WaitDialogForm _WaitDialogForm;
		public static void Wait(string msg, params object[] args)
		{
			if (args != null && args.Length > 0)
			{
				msg = string.Format(msg, args);
			}
			if (_WaitDialogForm == null || _WaitDialogForm.IsDisposed || _WaitDialogForm.Disposing)
			{
				_WaitDialogForm = new WaitDialogForm(msg);
			}
			else
			{
				_WaitDialogForm.Text = msg;
				_WaitDialogForm.Show();
			}
		}

		public static void CloseWait()
		{
			if (_WaitDialogForm != null)
			{
				_WaitDialogForm.Close();
			}
		}

		public static void Error(string msg, params object[] args)
		{
			if (args != null && args.Length > 0)
			{
				msg = string.Format(msg, args);
			}
			MessageBox.Show(msg, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}
