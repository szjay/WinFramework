//==============================================================
//  版权所有：深圳杰文科技
//  文件名：TelBitsController.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	public class TelBitsController
	{
		private Control _BitsLabel = null;
		private BaseEdit _Edit = null;
		private bool _withBrackets = false;

		public void Set(BaseEdit edit, Control bitsLabel)
		{
			Set(edit, bitsLabel, true);
		}

		public void Set(BaseEdit edit, Control bitsLabel, bool withBrackets)
		{
			_BitsLabel = bitsLabel;
			_Edit = edit;
			_withBrackets = withBrackets;

			if (_withBrackets)
			{
				_BitsLabel.Text = "(0)";
			}
			else
			{
				_BitsLabel.Text = "0";
			}
			_Edit.TextChanged += _Edit_TextChanged;
		}

		void _Edit_TextChanged(object sender, EventArgs e)
		{
			if (_withBrackets)
			{
				_BitsLabel.Text = string.Format("({0})", _Edit.Text.Length.ToString());
			}
			else
			{
				_BitsLabel.Text = string.Format("{0}", _Edit.Text.Length.ToString());
			}
		}

	}
}
