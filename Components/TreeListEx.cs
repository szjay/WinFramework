//==============================================================
//  版权所有：深圳杰文科技
//  文件名：TreeListEx.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace Infrastructure.Components
{
	public static class TreeListEx
	{
		public static void SetStyle(this TreeList tree)
		{
			tree.OptionsBehavior.Editable = false;
			tree.OptionsView.EnableAppearanceEvenRow = true;
			tree.OptionsView.EnableAppearanceOddRow = true;
			//tree.LookAndFeel.UseDefaultLookAndFeel = false;
			//tree.LookAndFeel.UseWindowsXPTheme = true;
		}

		public static T GetFocusedNode<T>(this TreeList tree)
		{
			if (tree.FocusedNode == null)
			{
				return default(T);
			}
			object obj = tree.GetDataRecordByNode(tree.FocusedNode);
			return (T)obj;
		}

		public static TreeListNode Locate(this TreeList tree, Guid id)
		{
			TreeListNode node = tree.FindNodeByKeyID(id);
			if (node != null)
			{
				tree.FocusedNode = node;
				return node;
			}
			return null;
		}

		public static TreeListNode Locate(this TreeList tree, string text)
		{
			TreeListNode node = tree.FindNode(a => a.GetDisplayText(0) == text);
			if (node != null)
			{
				tree.FocusedNode = node;
				return node;
			}
			return null;
		}

		public static T GetActiveEditorValue<T>(this TreeList tree)
		{
			BaseEdit editor = tree.ActiveEditor;
			if (editor.EditValue == null)
			{
				return default(T);
			}

			return (T)editor.EditValue;
		}

		//设置TreeList能够级联更新状态。
		public static void SetCascadeChange(this TreeList tree)
		{
			tree.OptionsView.ShowCheckBoxes = true;
			tree.OptionsBehavior.AllowIndeterminateCheckState = true;
			tree.BeforeCheckNode += treeList1_BeforeCheckNode;
			tree.AfterCheckNode += treeList1_AfterCheckNode;
		}

		private static void treeList1_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
		{
			((TreeList)sender).BeginUpdate();
			SetCheckedChildNodes(e.Node, e.Node.CheckState);
			SetCheckedParentNodes(e.Node, e.Node.CheckState);
			((TreeList)sender).EndUpdate();
		}

		private static void treeList1_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
		{
			e.State = (e.PrevState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
		}

		//设置子节点的状态。
		private static void SetCheckedChildNodes(DevExpress.XtraTreeList.Nodes.TreeListNode node, CheckState check)
		{
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				node.Nodes[i].CheckState = check;
				SetCheckedChildNodes(node.Nodes[i], check);
			}
		}

		//设置父节点的状态。
		private static void SetCheckedParentNodes(DevExpress.XtraTreeList.Nodes.TreeListNode node, CheckState check)
		{
			if (node.ParentNode != null)
			{
				bool b = false;
				CheckState state;
				for (int i = 0; i < node.ParentNode.Nodes.Count; i++)
				{
					state = (CheckState)node.ParentNode.Nodes[i].CheckState;
					if (!check.Equals(state))
					{
						b = !b;
						break;
					}
				}
				node.ParentNode.CheckState = b ? CheckState.Indeterminate : check;
				SetCheckedParentNodes(node.ParentNode, check);
			}
		}

	}
}
