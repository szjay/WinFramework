//==============================================================
//  版权所有：深圳杰文科技
//  文件名：YearMonthSelector.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	public partial class YearMonthSelector : XtraUserControl
	{
		public YearMonthSelector()
		{
			InitializeComponent();
			this.Load += YearMonthSelector_Load;
		}

		void YearMonthSelector_Load(object sender, EventArgs e)
		{
			if (ControlUtil.IsDesignMode(this))
			{
				return;
			}

			for (int y = DateTime.Now.Year - 10; y < DateTime.Now.Year + 20; y++)
			{
				int idx = cmbYear.Properties.Items.Add(y.ToString() + "年");
				if (y == DateTime.Now.Year)
				{
					cmbYear.SelectedIndex = idx;
				}
			}

			for (int m = 1; m <= 12; m++)
			{
				int idx = cmbMonth.Properties.Items.Add(m.ToString() + "月");
				if (m == DateTime.Now.Month)
				{
					cmbMonth.SelectedIndex = idx;
				}
			}

		}

		public int Year
		{
			get
			{
				int r;
				if (int.TryParse(cmbYear.Text.TrimEnd('年'), out r))
				{
					return r;
				}
				return DateTime.Now.Year;
			}
		}

		public int Month
		{
			get
			{
				int m;
				if (int.TryParse(cmbMonth.Text.TrimEnd('月'), out m))
				{
					return m;
				}
				return DateTime.Now.Month;
			}
		}

		public DateTime StartDate
		{
			get
			{
				return new DateTime(Year, Month, 1);
			}
		}

		public DateTime EndDate
		{
			get
			{
				return StartDate.AddMonths(1).AddSeconds(-1);
			}
		}
	}
}
