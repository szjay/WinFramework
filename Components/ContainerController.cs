//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ContainerController.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace Infrastructure.Components
{
	/// <summary>
	/// 容器控制器。
	/// 用于控制容器中的控件的（只读）状态。
	/// </summary>
	public class ContainerController
	{
		protected List<Control> stateList = new List<Control>();	//记录状态变化的控件。
		protected List<Control> readOnlyList = new List<Control>();	//记录永远只读（状态不变化）的控件。

		public ContainerController()
		{
		}

		public ContainerController(Control container)
			: this()
		{
			Init(container);
		}

		public ContainerController(Control container, bool Readonly)
			: this(container)
		{
			this.ReadOnly = Readonly;
		}

		/// <summary>
		/// 初始化容器。
		/// </summary>
		public void Init(params Control[] containers)
		{
			stateList.Clear();
			readOnlyList.Clear();

			foreach (Control c in containers)
			{
				InitControl(c);
			}
		}

		public void Init(bool readOnly, params Control[] containers)
		{
			Init(containers);
			this.ReadOnly = readOnly;
		}

		private void InitControl(Control container)
		{
			//如果控件的初始状态为只读，那么默认认为此控件的状态是不变化的（永远只读）。
			foreach (Control c in container.Controls)
			{
				if (c is BaseEdit)
				{
					if ((c as BaseEdit).Properties.ReadOnly)
					{
						readOnlyList.Add(c);
					}
					else
					{
						stateList.Add(c);
					}
				}
				else if (c is GridControl)
				{
					GridControl grid = c as GridControl;
					foreach (BaseView view in grid.Views)
					{
						if (view is GridView)
						{
							if ((view as GridView).OptionsBehavior.ReadOnly)
							{
								readOnlyList.Add(c);
							}
							else
							{
								stateList.Add(c);
							}
						}
					}
				}
				else
				{
					InitControl(c);
				}
			}
		}

		private bool readOnly;

		public bool ReadOnly
		{
			get
			{
				return readOnly;
			}
			set
			{
				readOnly = value;
				SetReadOnly();
			}
		}

		private void SetReadOnly()
		{
			foreach (Control c in stateList)
			{
				if (c is BaseEdit)
				{
					(c as BaseEdit).Properties.ReadOnly = readOnly;
					if (c is ButtonEdit)
					{
						ButtonEdit button = c as ButtonEdit;
						if (button.Properties.Buttons.Count > 0)
						{
							button.Properties.Buttons[0].Enabled = !readOnly;
						}
					}
				}
				else if (c is GridControl)
				{
					GridControl grid = c as GridControl;
					foreach (BaseView view in grid.Views)
					{
						if (view is GridView)
						{
							(view as GridView).OptionsBehavior.ReadOnly = readOnly;
							(view as GridView).OptionsBehavior.Editable = !readOnly;
						}
					}
				}
				else
				{
					ControlUtil.SetControls(c, readOnly);
				}
			}
			if (ReadOnlyChanged != null)
			{
				ReadOnlyChanged(readOnly);
			}
		}

		public event Action<bool> ReadOnlyChanged;

	}
}
