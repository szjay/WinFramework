//==============================================================
//  版权所有：深圳杰文科技
//  文件名：KeyNameComplexUC.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	[ToolboxItem(false)]
	public partial class KeyNameComplexUC : DevExpress.XtraEditors.XtraUserControl
	{
		public KeyNameComplexUC()
		{
			InitializeComponent();
			this.SizeChanged += new EventHandler(KeyValueComplexUC_SizeChanged);
		}

		void KeyValueComplexUC_SizeChanged(object sender, EventArgs e)
		{
			txtKey.Width = this.ClientSize.Width / 3;
			txtKey.Height = this.ClientSize.Height;
			txtKey.Left = 0;
			txtKey.Top = 0;

			txtName.Width = this.ClientSize.Width - txtKey.Width;			
			txtName.Height = this.ClientSize.Height - 1;
			txtName.Left = txtKey.Width + 1;
			txtName.Top = 0;
		}

		public string KeyText
		{
			get
			{
				return txtKey.Text;
			}
			set
			{
				txtKey.Text = value;
			}
		}

		public string NameText
		{
			get
			{
				return txtName.Text;
			}
			set
			{
				txtName.Text = value;
			}
		}
	}
}
