//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ScreenUtil.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Infrastructure.Components
{
	//屏幕、分辨率常用方法。
	public static class ScreenUtil
	{
		public static Screen GetSecondScreen()
		{
			foreach (Screen screen in Screen.AllScreens)
			{
				if (!screen.Primary)
				{
					return screen;
				}
			}
			return Screen.PrimaryScreen;
		}

		//如果有第二个屏幕，那么把窗体显示在第二个屏幕上，否则显示在主屏幕上。
		public static void ShowToSecondScreen(this Form form, FormWindowState state = FormWindowState.Maximized, bool isDialog = false)
		{
			Screen screen = GetSecondScreen();
			if (!screen.Primary)
			{
				form.DesktopLocation = screen.Bounds.Location;
				form.StartPosition = FormStartPosition.CenterScreen;
				form.WindowState = state;
			}

			form.MdiParent = null;
			form.WindowState = FormWindowState.Maximized;
			if (isDialog)
			{
				form.ShowDialog();
			}
			else
			{
				form.Show();
			}
		}
	}
}
