﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Infrastructure.Components
{
	public static class FormExt
	{
		public static bool Open<T>(this Form form, Action<T> action, T obj)
		{
			form.Shown += (a, b) =>
			{
				form.Invoke(action, obj);
			};
			return form.ShowDialog() == DialogResult.OK;
		}

		public static bool Open(this Form form, Action action)
		{
			form.Shown += (a, b) =>
			{
				form.Invoke(action);
			};
			return form.ShowDialog() == DialogResult.OK;
		}

	}
}
