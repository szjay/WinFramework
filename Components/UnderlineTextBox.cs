//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UnderlineTextBox.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	public partial class UnderlineTextBox : TextEdit
	{
		public UnderlineTextBox()
		{
			InitializeComponent();
		}

		protected override void OnLoaded()
		{
			base.OnLoaded();
			this.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
			this.Properties.Appearance.Options.UseBackColor = true;
		}

		protected override void OnPaint(PaintEventArgs pe)
		{			
			base.OnPaint(pe);
			pe.Graphics.DrawLine(Pens.Black, new Point(0, this.Height - 1), new Point(this.Width, this.Height - 1));
		}
	}
}
