//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Title.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.ComponentModel;
using System.Drawing;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	[ToolboxItem(false)]
	public class Title : XtraUserControl
	{
		private System.Windows.Forms.Label label1;

		public Title()
		{
			InitializeComponent();
		}

		#region InitializeComponent
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Blue;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(390, 36);
			this.label1.TabIndex = 2;
			this.label1.Text = "标题";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Title
			// 
			this.Controls.Add(this.label1);
			this.Name = "Title";
			this.Size = new System.Drawing.Size(390, 36);
			this.ResumeLayout(false);

		}
		#endregion

		public new Font Font
		{
			get
			{
				return label1 == null ? null : label1.Font;
			}
			set
			{
				if (label1 != null)
				{
					label1.Font = value;
				}
			}
		}

		public string Caption
		{
			get
			{
				return label1 == null ? "" : label1.Text;
			}
			set
			{
				if (label1 != null)
				{
					label1.Text = value;
				}
			}
		}

	}
}
