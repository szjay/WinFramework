﻿namespace Infrastructure.Components
{
    partial class DateExpandForm
    {
        /// <summary>
        /// 必需的设计器变量。



        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。



        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。



        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.label2 = new System.Windows.Forms.Label();
			this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
			this.btnOK = new DevExpress.XtraEditors.SimpleButton();
			this.btnDay = new DevExpress.XtraEditors.SimpleButton();
			this.btnWeek = new DevExpress.XtraEditors.SimpleButton();
			this.btnMonth = new DevExpress.XtraEditors.SimpleButton();
			this.btnRecentHalfYear = new DevExpress.XtraEditors.SimpleButton();
			this.btnRecentMonth3 = new DevExpress.XtraEditors.SimpleButton();
			this.btnAll = new DevExpress.XtraEditors.SimpleButton();
			this.btnPrevDay = new DevExpress.XtraEditors.SimpleButton();
			this.btnPrevDay2 = new DevExpress.XtraEditors.SimpleButton();
			this.btnRecentMonth1 = new DevExpress.XtraEditors.SimpleButton();
			this.cmbMonth = new DevExpress.XtraEditors.ImageComboBoxEdit();
			this.dtpEnd = new Infrastructure.Components.DateTimePickerEx(this.components);
			this.dtpBegin = new Infrastructure.Components.DateTimePickerEx(this.components);
			this.btnCurrentYear = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)(this.cmbMonth.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(302, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(19, 14);
			this.label2.TabIndex = 13;
			this.label2.Text = "至";
			// 
			// btnCancel
			// 
			this.btnCancel.Image = global::Infrastructure.Components.Properties.Resources.Close;
			this.btnCancel.Location = new System.Drawing.Point(531, 1);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(58, 27);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "取消";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Image = global::Infrastructure.Components.Properties.Resources.OK;
			this.btnOK.Location = new System.Drawing.Point(472, 1);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(58, 27);
			this.btnOK.TabIndex = 3;
			this.btnOK.Text = "确定";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnDay
			// 
			this.btnDay.Location = new System.Drawing.Point(3, 28);
			this.btnDay.Name = "btnDay";
			this.btnDay.Size = new System.Drawing.Size(57, 27);
			this.btnDay.TabIndex = 5;
			this.btnDay.Text = "本日";
			this.btnDay.Click += new System.EventHandler(this.btnDay_Click);
			// 
			// btnWeek
			// 
			this.btnWeek.Location = new System.Drawing.Point(180, 28);
			this.btnWeek.Name = "btnWeek";
			this.btnWeek.Size = new System.Drawing.Size(57, 27);
			this.btnWeek.TabIndex = 8;
			this.btnWeek.Text = "本周";
			this.btnWeek.Click += new System.EventHandler(this.btnWeek_Click);
			// 
			// btnMonth
			// 
			this.btnMonth.Location = new System.Drawing.Point(239, 28);
			this.btnMonth.Name = "btnMonth";
			this.btnMonth.Size = new System.Drawing.Size(57, 27);
			this.btnMonth.TabIndex = 9;
			this.btnMonth.Text = "本月";
			this.btnMonth.Click += new System.EventHandler(this.btnMonth_Click);
			// 
			// btnRecentHalfYear
			// 
			this.btnRecentHalfYear.Location = new System.Drawing.Point(416, 28);
			this.btnRecentHalfYear.Name = "btnRecentHalfYear";
			this.btnRecentHalfYear.Size = new System.Drawing.Size(57, 27);
			this.btnRecentHalfYear.TabIndex = 13;
			this.btnRecentHalfYear.Text = "近半年";
			this.btnRecentHalfYear.Click += new System.EventHandler(this.btnRecentHalfYear_Click);
			// 
			// btnRecentMonth3
			// 
			this.btnRecentMonth3.Location = new System.Drawing.Point(357, 28);
			this.btnRecentMonth3.Name = "btnRecentMonth3";
			this.btnRecentMonth3.Size = new System.Drawing.Size(57, 27);
			this.btnRecentMonth3.TabIndex = 12;
			this.btnRecentMonth3.Text = "近三月";
			this.btnRecentMonth3.Click += new System.EventHandler(this.btnRecentMonth3_Click);
			// 
			// btnAll
			// 
			this.btnAll.Location = new System.Drawing.Point(533, 28);
			this.btnAll.Name = "btnAll";
			this.btnAll.Size = new System.Drawing.Size(55, 27);
			this.btnAll.TabIndex = 14;
			this.btnAll.Text = "全部";
			this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
			// 
			// btnPrevDay
			// 
			this.btnPrevDay.Location = new System.Drawing.Point(62, 28);
			this.btnPrevDay.Name = "btnPrevDay";
			this.btnPrevDay.Size = new System.Drawing.Size(57, 27);
			this.btnPrevDay.TabIndex = 6;
			this.btnPrevDay.Text = "昨日";
			this.btnPrevDay.Click += new System.EventHandler(this.btnPrevDay_Click);
			// 
			// btnPrevDay2
			// 
			this.btnPrevDay2.Location = new System.Drawing.Point(121, 28);
			this.btnPrevDay2.Name = "btnPrevDay2";
			this.btnPrevDay2.Size = new System.Drawing.Size(57, 27);
			this.btnPrevDay2.TabIndex = 7;
			this.btnPrevDay2.Text = "前日";
			this.btnPrevDay2.Click += new System.EventHandler(this.btnPrevDay2_Click);
			// 
			// btnRecentMonth1
			// 
			this.btnRecentMonth1.Location = new System.Drawing.Point(298, 28);
			this.btnRecentMonth1.Name = "btnRecentMonth1";
			this.btnRecentMonth1.Size = new System.Drawing.Size(57, 27);
			this.btnRecentMonth1.TabIndex = 10;
			this.btnRecentMonth1.Text = "近一月";
			this.btnRecentMonth1.Click += new System.EventHandler(this.btnRecentMonth1_Click);
			// 
			// cmbMonth
			// 
			this.cmbMonth.Location = new System.Drawing.Point(5, 2);
			this.cmbMonth.Name = "cmbMonth";
			this.cmbMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbMonth.Properties.DropDownRows = 15;
			this.cmbMonth.Size = new System.Drawing.Size(113, 22);
			this.cmbMonth.TabIndex = 0;
			this.cmbMonth.EditValueChanged += new System.EventHandler(this.cmbMonth_EditValueChanged);
			// 
			// dtpEnd
			// 
			this.dtpEnd.Checked = false;
			this.dtpEnd.Location = new System.Drawing.Point(324, 3);
			this.dtpEnd.Name = "dtpEnd";
			this.dtpEnd.ShowCheckBox = true;
			this.dtpEnd.Size = new System.Drawing.Size(145, 22);
			this.dtpEnd.TabIndex = 2;
			this.dtpEnd.CloseUp += new System.EventHandler(this.DateTimePicker_CloseUp);
			this.dtpEnd.DropDown += new System.EventHandler(this.DateTimePicker_DropDown);
			// 
			// dtpBegin
			// 
			this.dtpBegin.Checked = false;
			this.dtpBegin.Location = new System.Drawing.Point(153, 3);
			this.dtpBegin.Name = "dtpBegin";
			this.dtpBegin.ShowCheckBox = true;
			this.dtpBegin.Size = new System.Drawing.Size(145, 22);
			this.dtpBegin.TabIndex = 1;
			this.dtpBegin.CloseUp += new System.EventHandler(this.DateTimePicker_CloseUp);
			this.dtpBegin.DropDown += new System.EventHandler(this.DateTimePicker_DropDown);
			// 
			// btnCurrentYear
			// 
			this.btnCurrentYear.Location = new System.Drawing.Point(475, 28);
			this.btnCurrentYear.Name = "btnCurrentYear";
			this.btnCurrentYear.Size = new System.Drawing.Size(57, 27);
			this.btnCurrentYear.TabIndex = 15;
			this.btnCurrentYear.Text = "本年";
			this.btnCurrentYear.Click += new System.EventHandler(this.btnCurrentYear_Click);
			// 
			// DateExpandForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(591, 57);
			this.ControlBox = false;
			this.Controls.Add(this.btnCurrentYear);
			this.Controls.Add(this.cmbMonth);
			this.Controls.Add(this.btnAll);
			this.Controls.Add(this.btnPrevDay2);
			this.Controls.Add(this.btnPrevDay);
			this.Controls.Add(this.btnDay);
			this.Controls.Add(this.btnWeek);
			this.Controls.Add(this.btnRecentMonth1);
			this.Controls.Add(this.btnMonth);
			this.Controls.Add(this.btnRecentHalfYear);
			this.Controls.Add(this.btnRecentMonth3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.dtpEnd);
			this.Controls.Add(this.dtpBegin);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Location = new System.Drawing.Point(300, 300);
			this.Name = "DateExpandForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "DateExpand";
			this.Deactivate += new System.EventHandler(this.DateExpandForm_Deactivate);
			this.Load += new System.EventHandler(this.DateExpandForm_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.DateExpandForm_Paint);
			this.Leave += new System.EventHandler(this.DateExpandForm_Leave);
			((System.ComponentModel.ISupportInitialize)(this.cmbMonth.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        public DateTimePickerEx dtpBegin;
        public DateTimePickerEx dtpEnd;
        public DevExpress.XtraEditors.SimpleButton btnOK;
        public DevExpress.XtraEditors.SimpleButton btnCancel;
        public System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton btnDay;
        public DevExpress.XtraEditors.SimpleButton btnWeek;
		public DevExpress.XtraEditors.SimpleButton btnMonth;
        public DevExpress.XtraEditors.SimpleButton btnRecentHalfYear;
        public DevExpress.XtraEditors.SimpleButton btnRecentMonth3;
        public DevExpress.XtraEditors.SimpleButton btnAll;
        public DevExpress.XtraEditors.SimpleButton btnPrevDay;
        public DevExpress.XtraEditors.SimpleButton btnPrevDay2;
        public DevExpress.XtraEditors.SimpleButton btnRecentMonth1;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbMonth;
		public DevExpress.XtraEditors.SimpleButton btnCurrentYear;

    }
}