﻿namespace Infrastructure.Components
{
	partial class DateSelector
	{
		/// <summary>
		/// 必需的设计器变量。


		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 清理所有正在使用的资源。


		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 组件设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。


		/// </summary>
		private void InitializeComponent()
		{
			this.cmb = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
			((System.ComponentModel.ISupportInitialize)(this.cmb)).BeginInit();
			this.SuspendLayout();
			// 
			// fProperties
			// 
			this.cmb.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmb.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic;
			this.cmb.Mask.EditMask = "\\d{2}\\d{2}?年([1-9]|10|11|12)月(0?[1-9]|[12]\\d|30|31)日 — \\d{2}\\d{2}?年([1-9]|10|11|1" +
				"2)月(0?[1-9]|[12]\\d|30|31)日";
			this.cmb.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.cmb.Mask.UseMaskAsDisplayFormat = true;
			this.cmb.Name = "cmb";
			this.cmb.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.DateSelector_QueryPopUp);
			// 
			// DateSelector
			// 
			this.Size = new System.Drawing.Size(348, 21);
			this.Validating += new System.ComponentModel.CancelEventHandler(this.DateSelector_Validating);
			this.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.DateSelector_QueryPopUp);
			((System.ComponentModel.ISupportInitialize)(this.cmb)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cmb;

	}
}
