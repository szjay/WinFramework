//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DateTimePickerEx.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace Infrastructure.Components
{
	[ToolboxItem(false)]
	public partial class DateTimePickerEx : DateTimePicker
	{
		public DateTimePickerEx()
		{
			InitializeComponent();
		}

		public DateTimePickerEx(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
		}

		//注意，DateTimePicker控件的OnDoubleClick方法不会起任何作用
		protected override void OnDoubleClick(EventArgs e)
		{
			base.OnDoubleClick(e);
			if (doubleClick != null)
			{
				doubleClick(this, e);
			}
		}

		private event EventHandler doubleClick;

		[Browsable(true)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public new event EventHandler DoubleClick
		{
			add
			{
				doubleClick += value;
			}
			remove
			{
				doubleClick -= value;
			}
		}

	}
}
