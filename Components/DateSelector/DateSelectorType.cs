//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DateSelectorType.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Components
{
	/// <summary>
	/// 日期选择控件的日期范围类型
	/// </summary>
	public enum DateSelectorType
	{
		/// <summary>
		/// 全部
		/// </summary>
		All,

		/// <summary>
		/// 本年
		/// </summary>
		CurrentYear,

		/// <summary>
		/// 上半年
		/// </summary>
		FirstHalfYear,

		/// <summary>
		/// 下半年
		/// </summary>
		NextHalfYear,

		/// <summary>
		/// 本季
		/// </summary>
		CurrentSeason,

		/// <summary>
		/// 本月
		/// </summary>
		CurrentMonth,

		/// <summary>
		/// 近一月
		/// </summary>
		RecentMonth1,

		/// <summary>
		/// 近两月
		/// </summary>
		RecentMonth2,

		/// <summary>
		/// 近三月
		/// </summary>
		RecentMonth3,

		/// <summary>
		/// 近半年
		/// </summary>
		RecentHalfYear,

		/// <summary>
		/// 本旬
		/// </summary>
		CurrentTenDays,

		/// <summary>
		/// 本周
		/// </summary>
		CurrentWeek,

		/// <summary>
		/// 本日
		/// </summary>
		Today,

		/// <summary>
		/// 用户自定义输入的
		/// </summary>
		Custom,

		/// <summary>
		/// 昨天
		/// </summary>
		Yesterday,

		/// <summary>
		/// 前天
		/// </summary>
		BeforeYesterday,

		/// <summary>
		/// 近10天
		/// </summary>
		LastTenDays,

		/// <summary>
		/// 最近两天
		/// </summary>
		LastTwoDays,

		/// <summary>
		/// 三天（昨天、今天、明天）
		/// </summary>
		ThreeDays,

		/// <summary>
		/// 明天
		/// </summary>
		Tomorrow,

		/// <summary>
		/// 后天
		/// </summary>
		AfterTomorrow,

		/// <summary>
		/// 上个月
		/// </summary>
		LastMonth,
	}
}
