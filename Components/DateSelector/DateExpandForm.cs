//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DateExpandForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Infrastructure.Utilities;

namespace Infrastructure.Components
{
	public delegate void DateChangedDelegate(object sender, DateSelectorType dateSelectorType, DateTime beginDate, DateTime endDate);
	public delegate void CancelSelectDelegate(object sender);

	public partial class DateExpandForm : XtraForm
	{
		private bool isSelecting = false;

		public DateExpandForm()
		{
			InitializeComponent();
		}

		private void DateExpandForm_Load(object sender, EventArgs e)
		{
			dtpBegin.MinDate = DateTimeUtil.DefaultMinDate;
			dtpEnd.MaxDate = DateTimeUtil.DefaultMaxDate;

			dtpBegin.DoubleClick += new EventHandler(dtpBegin_DoubleClick);
			dtpEnd.DoubleClick += new EventHandler(dtpEnd_DoubleClick);

			//月份下拉框
			this.PrepareMonthCombo();
		}

		/// <summary>
		/// 填充月份下拉框
		/// </summary>
		void PrepareMonthCombo()
		{
			this.cmbMonth.Properties.Items.Clear();

			//首项
			ImageComboBoxItem item0 = new ImageComboBoxItem();
			item0.Description = "月份";
			item0.Value = item0.Description;
			this.cmbMonth.Properties.Items.Add(item0);

			//前溯24个月
			DateTime now = DateTime.Now.Date.AddMonths(1);
			now = new DateTime(now.Year, now.Month, 1);
			for (int i = 0; i <= 24; i++)
			{
				ImageComboBoxItem item = new ImageComboBoxItem();

				DateTime time = now.AddMonths(-i);
				item.Value = time;
				item.Description = time.ToString("yyyy年M月");
				this.cmbMonth.Properties.Items.Add(item);
			}

			this.cmbMonth.SelectedIndex = 0;
		}

		private void DateExpandForm_Paint(object sender, PaintEventArgs e)
		{
			DrawControlBorder(e.Graphics, e.ClipRectangle);
		}

		private void DrawControlBorder(Graphics g, Rectangle rect)
		{
			Pen pen = new Pen(Color.Blue);
			rect.Width = rect.Width - 1;
			rect.Height = rect.Height - 1;
			g.DrawRectangle(pen, rect);
		}

		private void DateExpandForm_Leave(object sender, EventArgs e)
		{
			if (!isSelecting)
			{
				this.Hide();
			}
		}

		private void DateExpandForm_Deactivate(object sender, EventArgs e)
		{
			if (!isSelecting)
			{
				this.Hide();
			}
		}

		private void dtpEnd_DoubleClick(object sender, EventArgs e)
		{
			isSelecting = true;
		}

		private void dtpBegin_DoubleClick(object sender, EventArgs e)
		{
			isSelecting = true;
		}

		public event DateChangedDelegate DateChanged;

		/// <summary>
		/// 激发日期选择事件
		/// </summary>
		private void RaiseDateChanged(DateSelectorType dateSelectorType, DateTime beginDate, DateTime endDate)
		{
			DateType = dateSelectorType;

			if (DateChanged != null)
			{
				DateChanged(this, dateSelectorType, beginDate, endDate);
			}
			this.Hide();
		}

		public DateSelectorType DateType = DateSelectorType.All;

		public event CancelSelectDelegate CancelSelect;

		private void RaiseCancelSelect()
		{
			this.Hide();
			if (CancelSelect != null)
			{
				CancelSelect(this);
			}
		}

		#region 按钮点击事件

		public void btnAll_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.All, DateTimeUtil.DefaultMinDate, DateTimeUtil.DefaultMaxDate);
		}

		public void btnRecentMonth3_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.RecentMonth3, dtpBegin.Value, dtpEnd.Value);
		}

		public void btnRecentHalfYear_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.RecentHalfYear, dtpBegin.Value, dtpEnd.Value);
		}

		public void btnMonth_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.CurrentMonth, dtpBegin.Value, dtpEnd.Value);
		}

		public void btnTenDays_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.CurrentTenDays, dtpBegin.Value, dtpEnd.Value);
		}

		public void btnWeek_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.CurrentWeek, dtpBegin.Value, dtpEnd.Value);
		}

		public void btnDay_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			this.RaiseDateChanged(DateSelectorType.Today, dtpBegin.Value, dtpEnd.Value);
		}

		#endregion

		private void DateTimePicker_DropDown(object sender, EventArgs e)
		{
			isSelecting = true;
		}

		private void DateTimePicker_CloseUp(object sender, EventArgs e)
		{
			isSelecting = false;
		}

		public void btnOK_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = true;
			dtpEnd.Checked = true;
			RaiseDateChanged(DateSelectorType.Custom, dtpBegin.Value.Date, dtpEnd.Value.Date);
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			RaiseCancelSelect();
		}

		/// <summary>
		/// 昨日
		/// </summary>
		private void btnPrevDay_Click(object sender, EventArgs e)
		{
			DateTime date = DateTime.Now.AddDays(-1);
			this.RaiseDateChanged(DateSelectorType.Yesterday, date, date);
		}

		/// <summary>
		/// 前日
		/// </summary>
		private void btnPrevDay2_Click(object sender, EventArgs e)
		{
			DateTime date = DateTime.Now.AddDays(-2);
			this.RaiseDateChanged(DateSelectorType.BeforeYesterday, date, date);
		}

		/// <summary>
		/// 近一月
		/// </summary>
		public void btnRecentMonth1_Click(object sender, EventArgs e)
		{
			DateTime beginDate = DateTime.Now.AddMonths(-1);
			DateTime endDate = DateTime.Now;
			RaiseDateChanged(DateSelectorType.RecentMonth1, beginDate, endDate);
		}

		/// <summary>
		/// 选择月份
		/// </summary>
		private void cmbMonth_EditValueChanged(object sender, EventArgs e)
		{
			object value = this.cmbMonth.EditValue;
			if (value.GetType() == typeof(DateTime))
			{
				DateTime time = (DateTime)value;
				DateTime d0 = new DateTime(time.Year, time.Month, 1);
				DateTime d1 = d0.AddMonths(1).AddSeconds(-1);
				this.RaiseDateChanged(DateSelectorType.Custom, d0, d1);
			}
		}

		//本年。
		private void btnCurrentYear_Click(object sender, EventArgs e)
		{
			dtpBegin.Checked = false;
			dtpEnd.Checked = false;
			RaiseDateChanged(DateSelectorType.CurrentYear, dtpBegin.Value, dtpEnd.Value);
		}
	}
}