﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraLayout;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;

namespace Infrastructure.Components
{
	public static class LayoutHelper
	{
		public static void SetStyle(this Form form)
		{
			foreach (Component component in form.Controls)
			{
				SetStyle(component);
			}
		}

		public static void SetStyle(Component component)
		{
			if (component is Control)
			{
				SetStyle(component as Control);
			}
		}

		public static void SetStyle(Control control)
		{
			if (control is GridControl)
			{
				(control as GridControl).SetStyle();
				return;
			}
			else if (control is TreeList)
			{
				(control as TreeList).SetStyle();
				return;
			}
			//else if (control is GroupControl)
			//{
			//}
			//else if (control is PanelControl)
			//{
			//}
			else if (control is XtraTabControl)
			{
				XtraTabControl tab = control as XtraTabControl;
				if (tab.TabPageWidth == 0)
				{
					tab.TabPageWidth = 100;
					tab.AppearancePage.Header.TextOptions.HAlignment = HorzAlignment.Center;
				}
			}
			else if (control is XtraTabPage)
			{
			}
			else if (control is LayoutControl)
			{
				if ((control as LayoutControl).Root is LayoutControlGroup)
				{
					LayoutControlGroup layoutGroup = (control as LayoutControl).Root as LayoutControlGroup;
					layoutGroup.AppearanceItemCaption.TextOptions.HAlignment = HorzAlignment.Far;
					return;
				}
			}
			else
			{
				return;
			}

			foreach (Control childControl in control.Controls)
			{
				SetStyle(childControl);
			}
		}

		/***************************************************************/

		public static void LoadStyle(this Form form)
		{
			string formName = form.GetType().FullName;

			foreach (Control control in form.Controls)
			{
				LoadStyle(control, formName);
			}
		}

		private static void LoadStyle(Control control, string formName)
		{
			if (control is GridControl)
			{
				string layoutSettingFileName = GetLayoutPath(formName + "_" + control.Name + ".xml");
				if (File.Exists(layoutSettingFileName))
				{
					GridControl g = control as GridControl;
					g.MainView.RestoreLayoutFromXml(layoutSettingFileName);
				}
				return;
			}
			else if (control is TreeList)
			{
				string layoutSettingFileName = GetLayoutPath(formName + "_" + control.Name + ".xml");
				if (File.Exists(layoutSettingFileName))
				{
					TreeList tree = control as TreeList;
					bool allowIndeterminateCheckState = tree.OptionsBehavior.AllowIndeterminateCheckState;
					bool showCheckBoxes = tree.OptionsView.ShowCheckBoxes;

					tree.RestoreLayoutFromXml(layoutSettingFileName, OptionsLayoutBase.FullLayout);

					tree.OptionsBehavior.AllowIndeterminateCheckState = allowIndeterminateCheckState;
					tree.OptionsView.ShowCheckBoxes = showCheckBoxes;
				}
				return;
			}

			foreach (Control childControl in control.Controls)
			{
				LoadStyle(childControl, formName);
			}
		}

		public static void ClearStyle(this Form form)
		{
			string formName = form.GetType().FullName;
			string path = GetLayoutPath("");
			foreach (string f in Directory.GetFiles(path, formName + "*"))
			{
				File.Delete(f);
			}
		}

		/***************************************************************/

		public static void SaveStyle(this Form form)
		{
			string formName = form.GetType().FullName;
			foreach (Control control in form.Controls)
			{
				SaveStyle(control, formName);
			}
		}

		public static void SaveStyle(Control control, string formName)
		{
			string layoutSettingFileName = Path.Combine(GetLayoutPath(formName + "_" + control.Name + ".xml"));
			if (control is GridControl)
			{
				GridControl g = control as GridControl;
				g.MainView.SaveLayoutToXml(layoutSettingFileName);
				return;
			}
			else if (control is TreeList)
			{
				TreeList tree = control as TreeList;
				tree.SaveLayoutToXml(layoutSettingFileName);
				return;
			}

			foreach (Control childControl in control.Controls)
			{
				SaveStyle(childControl, formName);
			}
		}

		private static string GetLayoutPath(string layoutSettingFileName)
		{
			string path = Path.Combine(Application.StartupPath, "LayoutSetting");
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			path = Path.Combine(path, layoutSettingFileName);
			return path;
		}

	}
}
