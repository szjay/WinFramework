//==============================================================
//  版权所有：深圳杰文科技
//  文件名：LocalizationCHS.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Localization;

namespace Infrastructure.Components
{
	/// <summary>
	/// 本地化XtraGrid控件
	/// </summary>
	public class LocalizationGridCHS : GridLocalizer
	{
		public override string GetLocalizedString(GridStringId id)
		{
            switch (id)
            {
                case GridStringId.CardViewNewCard:
                    break;
                case GridStringId.CardViewQuickCustomizationButton:

                    break;
                case GridStringId.CardViewQuickCustomizationButtonFilter:
                    break;
                case GridStringId.CardViewQuickCustomizationButtonSort:
                    break;
                case GridStringId.ColumnViewExceptionMessage:
                    return "是否更正该数据?";
                case GridStringId.CustomFilterDialog2FieldCheck:
                    break;
                case GridStringId.CustomFilterDialogCancelButton:
                    break;
                case GridStringId.CustomFilterDialogCaption:
                    break;
                case GridStringId.CustomFilterDialogClearFilter:
                    break;
                case GridStringId.CustomFilterDialogFormCaption:
                    break;
                case GridStringId.CustomFilterDialogOkButton:
                    break;
                case GridStringId.CustomFilterDialogRadioAnd:
                    break;
                case GridStringId.CustomFilterDialogRadioOr:
                    break;
                case GridStringId.CustomizationBands:
                    return "带区";
                case GridStringId.CustomizationCaption:
                    return "自定义";
                case GridStringId.CustomizationColumns:
                    return "列";
                case GridStringId.FileIsNotFoundError:
                    return " 没有找到文件 {0}";
                case GridStringId.GridGroupPanelText:
                    return "请拖动一个列头到这里进行分组";
                case GridStringId.GridNewRowText:
                    return "在这里增加新行，【向下键】确认录入内容";
                case GridStringId.GridOutlookIntervals:
                    break;
                case GridStringId.MenuColumnBestFit:
                    return "自适应宽度";
                case GridStringId.MenuColumnBestFitAllColumns:
                    return "所有列自适应宽度";
                case GridStringId.MenuColumnClearFilter:
                    return "取消过滤";
                case GridStringId.MenuColumnColumnCustomization:
                    return "自定义显示列";
                case GridStringId.MenuColumnFilter:
                    return "过滤";
                case GridStringId.MenuColumnGroup:
                    return "分组";
                case GridStringId.MenuColumnGroupBox:
                    return "显示分组框";
                case GridStringId.MenuColumnSortAscending:
                    return "升序排列";
                case GridStringId.MenuColumnSortDescending:
                    return "降序排列";
                case GridStringId.MenuColumnUnGroup:
                    return "隐藏分组框";
                case GridStringId.MenuFooterAverage:
                    break;
                case GridStringId.MenuFooterAverageFormat:
                    break;
                case GridStringId.MenuFooterCount:
                    break;
                case GridStringId.MenuFooterCountFormat:
                    break;
                case GridStringId.MenuFooterCountGroupFormat:
                    break;
                case GridStringId.MenuFooterCustomFormat:
                    break;
                case GridStringId.MenuFooterMax:
                    break;
                case GridStringId.MenuFooterMaxFormat:
                    break;
                case GridStringId.MenuFooterMin:
                    break;
                case GridStringId.MenuFooterMinFormat:
                    break;
                case GridStringId.MenuFooterNone:
                    break;
                case GridStringId.MenuFooterSum:
                    break;
                case GridStringId.MenuFooterSumFormat:
                    break;
                case GridStringId.MenuGroupPanelClearGrouping:
                    return "取消所有分组";
                case GridStringId.MenuGroupPanelFullCollapse:
                    return "完全收缩";
                case GridStringId.MenuGroupPanelFullExpand:
                    return "完全展开";
                case GridStringId.PopupFilterAll:
                    return "(全部)";
                case GridStringId.PopupFilterBlanks:
                    return "(空)";
                case GridStringId.PopupFilterCustom:
                    return "(自定义)";
                case GridStringId.PopupFilterNonBlanks:
                    return "(非空)";
                case GridStringId.PrintDesignerBandHeader:
                    break;
                case GridStringId.PrintDesignerBandedView:
                    break;
                case GridStringId.PrintDesignerCardView:
                    break;
                case GridStringId.PrintDesignerDescription:
                    break;
                case GridStringId.PrintDesignerGridView:
                    break;
                case GridStringId.WindowErrorCaption:
                    break;
                default:
                    break;
            }

            return base.GetLocalizedString(id);

		}
	}

	/// <summary>
	/// 本地化DevExpress控件
	/// </summary>
	public class LocalizationEditorCHS : Localizer
	{
		public override string GetLocalizedString(StringId id)
		{
			switch (id)
			{
				case StringId.None:
					return "";

				case StringId.CaptionError:
					return "错误";

				case StringId.InvalidValueText:
					return "无效的值";

				case StringId.CheckChecked:
					return "选中";

				case StringId.CheckUnchecked:
					return "未选中";

				case StringId.CheckIndeterminate:
					return "未选择";

				case StringId.DateEditToday:
					return "今天";

				case StringId.DateEditClear:
					return "清除";

				case StringId.OK:
					return "确定(&O)";

				case StringId.Cancel:
					return "取消(&C)";

				case StringId.NavigatorFirstButtonHint:
					return "第一条";

				case StringId.NavigatorPreviousButtonHint:
					return "上一条";

				case StringId.NavigatorPreviousPageButtonHint:
					return "上一页";

				case StringId.NavigatorNextButtonHint:
					return "下一条";

				case StringId.NavigatorNextPageButtonHint:
					return "下一页";

				case StringId.NavigatorLastButtonHint:
					return "最后一条";

				case StringId.NavigatorAppendButtonHint:
					return "添加";

				case StringId.NavigatorRemoveButtonHint:
					return "删除";

				case StringId.NavigatorEditButtonHint:
					return "编辑";

				case StringId.NavigatorEndEditButtonHint:
					return "结束编辑";

				case StringId.NavigatorCancelEditButtonHint:
					return "取消编辑";

				case StringId.NavigatorTextStringFormat:
					return "记录{0}/{1}";

				case StringId.PictureEditMenuCut:
					return "剪贴";

				case StringId.PictureEditMenuCopy:
					return "复制";

				case StringId.PictureEditMenuPaste:
					return "粘贴";

				case StringId.PictureEditMenuDelete:
					return "删除";

				case StringId.PictureEditMenuLoad:
					return "读取";

				case StringId.PictureEditMenuSave:
					return "保存";

				case StringId.PictureEditOpenFileFilter:
					return "Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg;*.jpeg|Icon Files (*.ico)|*.ico|All Picture Files |*.bmp;*.gif;*.jpg;*.jpeg;*.ico;*.png;*.tif|All Files |*.*";

				case StringId.PictureEditSaveFileFilter:
					return "Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG File Interchange Format (*.jpg)|*.jpg";

				case StringId.PictureEditOpenFileTitle:
					return "打开";

				case StringId.PictureEditSaveFileTitle:
					return "另存为";

				case StringId.PictureEditOpenFileError:
					return "错误的图片格式";

				case StringId.PictureEditOpenFileErrorCaption:
					return "打开错误";

				case StringId.LookUpEditValueIsNull:
					return "[无数据]";

				case StringId.LookUpInvalidEditValueType:
					return "无效的数据类型";

				case StringId.LookUpColumnDefaultName:
					return "名称";

				case StringId.MaskBoxValidateError:
					return "输入数值不完整. 是否须要修改?\r\n\r\n是 - 回到编辑模式以修改数值.\r\n否 - 保持原来数值.\r\n取消 - 回复原来数值.\r\n";

				case StringId.UnknownPictureFormat:
					return "不明图片格式";

				case StringId.DataEmpty:
					return "无图像";

				case StringId.NotValidArrayLength:
					return "无效的数组长度.";

				case StringId.ImagePopupEmpty:
					return "(空)";

				case StringId.ImagePopupPicture:
					return "(图片)";

				case StringId.ColorTabCustom:
					return "自定";

				case StringId.ColorTabWeb:
					return "网络";

				case StringId.ColorTabSystem:
					return "系统";

				case StringId.CalcButtonMC:
					return "MC";

				case StringId.CalcButtonMR:
					return "MR";

				case StringId.CalcButtonMS:
					return "MS";

				case StringId.CalcButtonMx:
					return "M+";

				case StringId.CalcButtonSqrt:
					return "sqrt";

				case StringId.CalcButtonBack:
					return "Back";

				case StringId.CalcButtonCE:
					return "CE";

				case StringId.CalcButtonC:
					return "C";

				case StringId.CalcError:
					return "计算错误";

				case StringId.TabHeaderButtonPrev:
					return "向左滚动";

				case StringId.TabHeaderButtonNext:
					return "向右滚动";

				case StringId.TabHeaderButtonClose:
					return "关闭";

				case StringId.XtraMessageBoxOkButtonText:
					return "确定(&O)";

				case StringId.XtraMessageBoxCancelButtonText:
					return "取消(&C)";

				case StringId.XtraMessageBoxYesButtonText:
					return "是(&Y)";

				case StringId.XtraMessageBoxNoButtonText:
					return "否(&N)";

				case StringId.XtraMessageBoxAbortButtonText:
					return "中断(&A)";

				case StringId.XtraMessageBoxRetryButtonText:
					return "重试(&R)";

				case StringId.XtraMessageBoxIgnoreButtonText:
					return "忽略(&I)";

				case StringId.TextEditMenuUndo:
					return "撤消(&U)";

				case StringId.TextEditMenuCut:
					return "剪贴(&T)";

				case StringId.TextEditMenuCopy:
					return "复制(&C)";

				case StringId.TextEditMenuPaste:
					return "粘贴(&P)";

				case StringId.TextEditMenuDelete:
					return "删除(&D)";

				case StringId.TextEditMenuSelectAll:
					return "全选(&A)";
			}

			return base.GetLocalizedString(id);
		}

		public override string Language
		{
			get
			{
				return "简体中文";
			}
		}

	}
}
