//==============================================================
//  版权所有：深圳杰文科技
//  文件名：MsgHintForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;

namespace Infrastructure.Components
{
	internal partial class MsgHintForm : Form
	{
		public MsgHintForm()
		{
			InitializeComponent();
			title1.Caption = MsgBox.Title;
		}

		public static void Show(string msg, int interval = 20)
		{
			Show(msg);
		}

		private static readonly MsgHintForm form = new MsgHintForm();

		public static void Show(string msg)
		{
			form.label1.Text = msg;
			form.Open();
		}

		public void Open()
		{
			this.d = 1;
			this.Opacity = 100;

			this.timer1.Enabled = false; //先将上一轮的timer停止
			this.timer1.Interval = 100;
			this.timer1.Enabled = true;

			this.timer2.Enabled = false; //先将上一轮的timer停止
			this.timer2.Interval = 2500;
			this.timer2.Enabled = true;

			this.Show();
			this.BringToFront();
		}

		private void simpleButton1_Click(object sender, EventArgs e)
		{
			this.timer1.Enabled = false;
			this.timer2.Enabled = false;
			this.Hide();
		}

		double d = 1;
		private void timer1_Tick(object sender, EventArgs e)
		{
			if (d > 0.03)
			{
				d -= 0.02;
			}
			this.Opacity = d;
		}

		private void timer2_Tick(object sender, EventArgs e)
		{
			if (this.Disposing || this.IsDisposed)
			{
				return;
			}

			this.timer1.Enabled = false;
			this.timer2.Enabled = false;

			this.Hide();
		}

	}

}
