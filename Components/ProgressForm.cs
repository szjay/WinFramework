//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ProgressForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Infrastructure.Components
{
	/// <summary>
	/// 调用示例：
	/// ProgressForm form = new ProgressForm();
	/// form.Shown += delegate
	/// {
	/// 	new Thread(delegate()
	/// 	{
	/// 		CallMethod();
	/// 		form.DialogResult = DialogResult.OK;
	/// 	}).Start();
	/// };
	/// form.ShowDialog();
	/// </summary>
	public partial class ProgressForm : Form
	{
		public ProgressForm()
		{
			InitializeComponent();
		}

		public static T Show<T>(Func<Action, T> f, Action action)
		{
			T obj = default(T);
			ProgressForm form = new ProgressForm();
			form.Shown += delegate
			{
				new Thread(delegate()
				{
					obj = f.Invoke(action);
					form.DialogResult = DialogResult.OK;
				}).Start();
			};
			form.ShowDialog();
			return obj;
		}

		public static void Show(string title, string msg, Action action)
		{
			ProgressForm form = new ProgressForm();
			form.Title = title;
			form.Message = msg;
			form.Shown += delegate
			{
				new Thread(delegate()
				{
					action.Invoke();
					form.DialogResult = DialogResult.OK;
				}).Start();
			};
			form.ShowDialog();
		}

		public string Title
		{
			get
			{
				return this.Text;
			}
			set
			{
				this.Text = value;
			}
		}

		public string Message
		{
			get
			{
				return label1.Text;
			}
			set
			{
				label1.Text = value;
			}
		}

	}
}
