//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ViewObjectBindingParser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Infrastructure.Components
{
	internal class ViewObjectBindingParser
	{
		private static Dictionary<string, List<ViewObjectProperty>> typeDict = new Dictionary<string, List<ViewObjectProperty>>();

		public static readonly ViewObjectBindingParser Instance = new ViewObjectBindingParser();

		public List<ViewObjectProperty> Parser(Type type)
		{
			if (typeDict.ContainsKey(type.FullName)) //如果已经解析过了，那么不重复解析。
			{
				return typeDict[type.FullName];
			}

			object[] attributes = type.GetCustomAttributes(typeof(ViewObjectBindingAttribute), true);
			if (attributes.Length == 0) //如果VO没有标记ViewObjectBindingAttribute，那么不解析。
			{
				return new List<ViewObjectProperty>();
			}

			return Parse(type);
		}

		private List<ViewObjectProperty> Parse(Type type)
		{
			List<ViewObjectProperty> list = new List<ViewObjectProperty>();

			foreach (PropertyInfo pi in type.GetProperties())
			{
				list.Add(Parse(pi));
			}
			return list;
		}

		private ViewObjectProperty Parse(PropertyInfo pi)
		{
			object[] attributes = pi.GetCustomAttributes(typeof(PropertyBindingAttribute), true);
			if (attributes.Length == 0)
			{
				return null;
			}

			return new ViewObjectProperty()
			{
				Attribute = (PropertyBindingAttribute)attributes[0],
				FieldName = pi.Name,
				//FieldType = pi.PropertyType
			};
		}

	}
}
