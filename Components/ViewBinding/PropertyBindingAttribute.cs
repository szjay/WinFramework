//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PropertyBindingAttribute.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Components
{
	//标记属性要做绑定。因为GridView只能绑定属性，因此必须标记到属性上。
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class PropertyBindingAttribute : Attribute
	{
		//要显示的标题文本。
		public string Caption
		{
			get;
			set;
		}

		//属性显示的格式。日期默认为yyyy-MM-dd；时间默认为yyyy-MM-dd HH:mm:ss；浮点数默认为N2。
		public string Formatter
		{
			get;
			set;
		}

	}
}
