//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ViewBinder.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid.Views.Grid;
using System.ComponentModel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;

namespace Infrastructure.Components
{
	public static class ViewBinder
	{
		public static void Bind<T>(GridControl gridControl, BindingList<T> datasource)
		{
			gridControl.DataSource = datasource;

			foreach (BaseView gridView in gridControl.Views)
			{
				if (gridView is GridView)
				{
					Bind<T>(gridView as GridView, datasource);
				}
			}
		}

		private static void Bind<T>(GridView gridView, BindingList<T> datasource)
		{
			gridView.Columns.Clear();

			List<ViewObjectProperty> propertylist = ViewObjectBindingParser.Instance.Parser(typeof(T));
			foreach (ViewObjectProperty property in propertylist)
			{
				GridColumn col = gridView.Columns.AddField(property.FieldName);
				
			}
		}
	}
}
