//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SymbolSeparateTextBox.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Infrastructure.Components
{
	/// <summary>
	/// 用符号（逗号、分号、冒号等）分隔多个文本，以便于in条件查询。
	/// </summary>
	public partial class SymbolSeparateTextBox : MemoEdit
	{
		public SymbolSeparateTextBox()
		{
			InitializeComponent();
		}

		protected override void OnLoaded()
		{
			base.OnLoaded();
			this.Height = 20;
			this.Properties.WordWrap = false;
			this.Properties.ScrollBars = ScrollBars.None;
			this.Symbol = ","; //默认使用逗号分隔。
			this.Properties.NullValuePromptShowForEmptyValue = true;
			this.Properties.NullValuePrompt = "多项输入请用逗号分隔";
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			this.Height = 20;
		}

		//要分隔的符号。
		private string _Symbol = "";
		public string Symbol
		{
			get
			{
				return _Symbol;
			}
			set
			{
				_Symbol = value == null ? "" : value;
			}
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Enter)
			{
				if (!string.IsNullOrWhiteSpace(this.Text.Trim()))
				{
					e.KeyChar = Convert.ToChar(Symbol); //把回车转为分隔符。
				}
			}
			if (e.KeyChar == (char)Keys.Space)
			{
				if (!string.IsNullOrWhiteSpace(this.Text.Trim()))
				{
					e.KeyChar = Convert.ToChar(Symbol); //把空格转为分隔符。
				}
			}
		}

		protected override void OnTextChanged(EventArgs e)
		{
			if (string.IsNullOrEmpty(this.Text))
			{
				return;
			}

			string[] str = EditValue.ToString().TrimStart().TrimEnd().TrimStart(new char[] { '\r', '\n' }).Replace("\r\n", Symbol).Split(new string[] { Symbol }, StringSplitOptions.None);
			string temp = "";

			for (int i = 0; i < str.Length; i++)
			{
				if (str.Length - i == 1)
				{
					temp = temp + str[i];
				}
				else
				{
					temp = temp + str[i] + Symbol;
				}
			}

			this.EditValue = temp;
			this.Refresh();
		}
	}
}
