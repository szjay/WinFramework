//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Rpc.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Configuration;

namespace Framework.RPC
{
	//[DebuggerStepThrough]
	public class Rpc
	{
		private static readonly string ProxyType = ConfigurationManager.AppSettings["ProxyType"];

		private static object objectLock = new object();

		private static IProxy proxy = null;

		private static string _Url;
		public static string Url
		{
			get
			{
				return _Url;
			}
			set
			{
				_Url = value;
				proxy = null;
				ProxyContainer.Clear();
			}
		}

		public static IProxy Instance
		{
			get
			{
				if (proxy == null)
				{
					lock (objectLock)
					{
						if (proxy == null)
						{
							proxy = GetProxy();
							//如果Url为空字符串，那么请检查程序中是否有未登录就访问服务的地方。
							//例如静态类的构造函数中有访问服务。
							proxy.Init(Url);
						}
					}
				}
				return proxy;
			}
		}

		private static Dictionary<string, object> ProxyContainer = new Dictionary<string, object>();

		public static T Create<T>() where T : new()
		{
			T proxyObject = Instance.Create<T>();
			if (proxyObject == null)
			{
				throw new Exception("代理创建失败：" + typeof(T).FullName);
			}
			return proxyObject;
		}

		private static IProxy GetProxy()
		{
			if (string.IsNullOrEmpty(ProxyType)) //如果没有指定代理类，那么默认为Remoting代理。
			{
				return new RemotingProxy();
			}
			else
			{
				Type t = Type.GetType(ProxyType, false);
				return Activator.CreateInstance(t) as IProxy;
			}
		}

	}
}
