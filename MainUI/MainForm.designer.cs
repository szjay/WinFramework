﻿namespace Framework.MainUI
{
	partial class MainForm
	{
		/// <summary>
		/// 必需的设计器变量。


		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。


		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。


		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.tabNav = new DevExpress.XtraTab.XtraTabControl();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.tabPage1 = new DevExpress.XtraTab.XtraTabPage();
			this.tabPage2 = new DevExpress.XtraTab.XtraTabPage();
			this.BarManager = new DevExpress.XtraBars.BarManager(this.components);
			this.bar3 = new DevExpress.XtraBars.Bar();
			this.lblUser = new DevExpress.XtraBars.BarStaticItem();
			this.lblComp = new DevExpress.XtraBars.BarStaticItem();
			this.lblDept = new DevExpress.XtraBars.BarStaticItem();
			this.txtServer = new DevExpress.XtraBars.BarStaticItem();
			this.btnNotify = new DevExpress.XtraBars.BarButtonItem();
			this.lblTime = new DevExpress.XtraBars.BarStaticItem();
			this.btnClose = new DevExpress.XtraBars.BarButtonItem();
			this.bar1 = new DevExpress.XtraBars.Bar();
			this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.timer2 = new System.Windows.Forms.Timer(this.components);
			this.txtDbName = new DevExpress.XtraBars.BarStaticItem();
			((System.ComponentModel.ISupportInitialize)(this.tabNav)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BarManager)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
			this.SuspendLayout();
			// 
			// tabNav
			// 
			this.tabNav.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabNav.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
			this.tabNav.AppearancePage.HeaderActive.Options.UseFont = true;
			this.tabNav.AppearancePage.HeaderActive.Options.UseForeColor = true;
			this.tabNav.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabNav.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
			this.tabNav.Location = new System.Drawing.Point(0, 642);
			this.tabNav.Name = "tabNav";
			this.tabNav.Size = new System.Drawing.Size(1028, 27);
			this.tabNav.TabIndex = 3;
			this.tabNav.TabStop = false;
			// 
			// barDockControlTop
			// 
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(1028, 22);
			// 
			// barDockControlBottom
			// 
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 669);
			this.barDockControlBottom.Size = new System.Drawing.Size(1028, 30);
			// 
			// barDockControlLeft
			// 
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 647);
			// 
			// barDockControlRight
			// 
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(1028, 22);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 647);
			// 
			// tabPage1
			// 
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(784, 0);
			this.tabPage1.Text = "tabPage1";
			// 
			// tabPage2
			// 
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(784, 0);
			this.tabPage2.Text = "tabPage2";
			// 
			// BarManager
			// 
			this.BarManager.AllowCustomization = false;
			this.BarManager.AllowMoveBarOnToolbar = false;
			this.BarManager.AllowQuickCustomization = false;
			this.BarManager.AllowShowToolbarsPopup = false;
			this.BarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3,
            this.bar1});
			this.BarManager.DockControls.Add(this.barDockControlTop);
			this.BarManager.DockControls.Add(this.barDockControlBottom);
			this.BarManager.DockControls.Add(this.barDockControlLeft);
			this.BarManager.DockControls.Add(this.barDockControlRight);
			this.BarManager.Form = this;
			this.BarManager.HideBarsWhenMerging = false;
			this.BarManager.Images = this.imageCollection1;
			this.BarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.lblUser,
            this.lblDept,
            this.lblComp,
            this.btnClose,
            this.lblTime,
            this.btnNotify,
            this.txtServer,
            this.txtDbName});
			this.BarManager.MainMenu = this.bar1;
			this.BarManager.MaxItemId = 63;
			this.BarManager.StatusBar = this.bar3;
			// 
			// bar3
			// 
			this.bar3.BarName = "系统状态栏";
			this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
			this.bar3.DockCol = 0;
			this.bar3.DockRow = 0;
			this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
			this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.lblUser, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblComp, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblDept, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.txtServer, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.txtDbName),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNotify, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblTime),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnClose, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
			this.bar3.OptionsBar.AllowQuickCustomization = false;
			this.bar3.OptionsBar.DisableClose = true;
			this.bar3.OptionsBar.DrawDragBorder = false;
			this.bar3.OptionsBar.UseWholeRow = true;
			this.bar3.Text = "系统状态栏";
			// 
			// lblUser
			// 
			this.lblUser.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.lblUser.Caption = "用户";
			this.lblUser.Glyph = ((System.Drawing.Image)(resources.GetObject("lblUser.Glyph")));
			this.lblUser.Id = 9;
			this.lblUser.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblUser.LargeGlyph")));
			this.lblUser.LeftIndent = 10;
			this.lblUser.Name = "lblUser";
			this.lblUser.RightIndent = 10;
			this.lblUser.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// lblComp
			// 
			this.lblComp.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.lblComp.Caption = "公司";
			this.lblComp.Glyph = ((System.Drawing.Image)(resources.GetObject("lblComp.Glyph")));
			this.lblComp.Id = 37;
			this.lblComp.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblComp.LargeGlyph")));
			this.lblComp.Name = "lblComp";
			this.lblComp.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.lblComp.RightIndent = 10;
			this.lblComp.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// lblDept
			// 
			this.lblDept.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.lblDept.Caption = "部门";
			this.lblDept.Glyph = ((System.Drawing.Image)(resources.GetObject("lblDept.Glyph")));
			this.lblDept.Id = 10;
			this.lblDept.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblDept.LargeGlyph")));
			this.lblDept.Name = "lblDept";
			this.lblDept.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.lblDept.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// txtServer
			// 
			this.txtServer.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.txtServer.Caption = "Server";
			this.txtServer.Glyph = ((System.Drawing.Image)(resources.GetObject("txtServer.Glyph")));
			this.txtServer.Id = 60;
			this.txtServer.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("txtServer.LargeGlyph")));
			this.txtServer.Name = "txtServer";
			this.txtServer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.txtServer.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// btnNotify
			// 
			this.btnNotify.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
			this.btnNotify.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNotify.Glyph")));
			this.btnNotify.Id = 59;
			this.btnNotify.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnNotify.LargeGlyph")));
			this.btnNotify.Name = "btnNotify";
			this.btnNotify.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
			// 
			// lblTime
			// 
			this.lblTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
			this.lblTime.Caption = "2012年4月16日 15:00:00 星期一";
			this.lblTime.Glyph = ((System.Drawing.Image)(resources.GetObject("lblTime.Glyph")));
			this.lblTime.Id = 50;
			this.lblTime.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblTime.LargeGlyph")));
			this.lblTime.Name = "lblTime";
			this.lblTime.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.lblTime.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// btnClose
			// 
			this.btnClose.Caption = "退出";
			this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
			this.btnClose.Id = 48;
			this.btnClose.Name = "btnClose";
			this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
			// 
			// bar1
			// 
			this.bar1.BarName = "系统菜单";
			this.bar1.DockCol = 0;
			this.bar1.DockRow = 0;
			this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.bar1.HideWhenMerging = DevExpress.Utils.DefaultBoolean.False;
			this.bar1.OptionsBar.AllowQuickCustomization = false;
			this.bar1.OptionsBar.DisableClose = true;
			this.bar1.OptionsBar.DisableCustomization = true;
			this.bar1.OptionsBar.DrawDragBorder = false;
			this.bar1.OptionsBar.MultiLine = true;
			this.bar1.OptionsBar.UseWholeRow = true;
			this.bar1.Text = "系统菜单";
			// 
			// imageCollection1
			// 
			this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
			this.imageCollection1.InsertGalleryImage("feature", "images/support/feature_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/feature_16x16.png"), 1);
			this.imageCollection1.Images.SetKeyName(1, "feature");
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// timer2
			// 
			this.timer2.Interval = 1000;
			// 
			// txtDbName
			// 
			this.txtDbName.Caption = "DbName";
			this.txtDbName.Glyph = ((System.Drawing.Image)(resources.GetObject("txtDbName.Glyph")));
			this.txtDbName.Id = 62;
			this.txtDbName.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("txtDbName.LargeGlyph")));
			this.txtDbName.Name = "txtDbName";
			this.txtDbName.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.txtDbName.TextAlignment = System.Drawing.StringAlignment.Near;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1028, 699);
			this.Controls.Add(this.tabNav);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.IsMdiContainer = true;
			this.Name = "MainForm";
			this.ShowInTaskbar = true;
			this.Text = "MainForm";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			((System.ComponentModel.ISupportInitialize)(this.tabNav)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BarManager)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraTab.XtraTabControl tabNav;
		private DevExpress.XtraTab.XtraTabPage tabPage1;
        private DevExpress.XtraTab.XtraTabPage tabPage2;
		private DevExpress.XtraBars.BarManager BarManager;
		private DevExpress.XtraBars.Bar bar3;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarStaticItem lblUser;
		private DevExpress.XtraBars.BarStaticItem lblDept;
		private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem lblComp;
		private DevExpress.Utils.ImageCollection imageCollection1;
		private DevExpress.XtraBars.BarButtonItem btnClose;
		private DevExpress.XtraBars.BarStaticItem lblTime;
		private System.Windows.Forms.Timer timer1;
		private DevExpress.XtraBars.BarButtonItem btnNotify;
		private DevExpress.XtraBars.BarStaticItem txtServer;
		private System.Windows.Forms.Timer timer2;
		private DevExpress.XtraBars.BarStaticItem txtDbName;
	}
}

