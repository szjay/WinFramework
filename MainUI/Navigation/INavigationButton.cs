//==============================================================
//  版权所有：深圳杰文科技
//  文件名：INavigationButton.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Framework.ViewPortal
{
	public interface INavigationButton
	{
		string Name
		{
			get;
			set;
		}

		bool IsBizButton
		{
			get;
			set;
		}

		string Module
		{
			get;
			set;
		}

		string Class
		{
			get;
			set;
		}

		string Action
		{
			get;
			set;
		}

		event EventHandler Click;

		event MouseEventHandler MouseDown;

		event MouseEventHandler MouseUp;
	}
}
