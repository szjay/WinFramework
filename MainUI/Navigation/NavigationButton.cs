//==============================================================
//  版权所有：深圳杰文科技
//  文件名：NavigationButton.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace Framework.ViewPortal
{
	[ToolboxItem(false)]
	public class NavigationButton : Button, INavigationButton
	{

		public NavigationButton()
		{
			IsBizButton = true;
			IsFavorite = false;
		}

		public bool IsBizButton
		{
			get;
			set;
		}

		public string Module
		{
			get;
			set;
		}

		public string Class
		{
			get;
			set;
		}

		public string Action
		{
			get;
			set;
		}

		public bool IsFavorite
		{
			get;
			set;
		}
	}
}
