//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysLocalConfig.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Framework.BaseUI;

namespace Framework.MainUI
{
	[Serializable]
	public class SysLocalConfig
	{
		public static void Save(SysLocalConfig config)
		{
			string path = Path.Combine(ClientContext.ApplicationPath, "SysLocal.config");
			Infrastructure.Utilities.ObjectSerializer.XmlSerialize<SysLocalConfig>(config, path);
		}

		public static SysLocalConfig Load()
		{
			string path = Path.Combine(ClientContext.ApplicationPath, "SysLocal.config");
			if (File.Exists(path))
			{
				SysLocalConfig config = Infrastructure.Utilities.ObjectSerializer.XmlDeserialize<SysLocalConfig>(path);
				return config;
			}
			return null;
		}

		//本机编号。
		public string LocalHostNo
		{
			get;
			set;
		}

		//自动登录账号。
		public string LoginAccount
		{
			get;
			set;
		}

		//自动登录密码。
		public string Password
		{
			get;
			set;
		}

		//自启动模块。
		public string StartupModule
		{
			get;
			set;
		}

		public string StartupModule2
		{
			get;
			set;
		}

		public string StartupModule3
		{
			get;
			set;
		}
	}
}
