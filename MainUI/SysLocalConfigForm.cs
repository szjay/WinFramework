//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysLocalConfigForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Linq;
using Framework.BaseUI;
using Infrastructure.Components;
using Infrastructure.Utilities;

namespace Framework.MainUI
{
	[Module(Name = "本机配置", RootModule = "基础资料", Order = 3, Pop = true)]
	public partial class SysLocalConfigForm : ToolbarBaseForm
	{
		public SysLocalConfigForm()
		{
			InitializeComponent();
			this.Load += LocalSettingForm_Load;
		}

		void LocalSettingForm_Load(object sender, EventArgs e)
		{
			foreach (ModuleInfo moduleInfo in ModuleParser.Instance.ModuleInfoList.OrderBy(a => a.Attribute.Name))
			{
				cmbStartup.Properties.Items.Add(moduleInfo.Attribute.Name);
				cmbStartup2.Properties.Items.Add(moduleInfo.Attribute.Name);
				cmbStartup3.Properties.Items.Add(moduleInfo.Attribute.Name);
			}

			SysLocalConfig config = SysLocalConfig.Load();
			if (config == null)
			{
				config = new SysLocalConfig();
			}
			bindingSource1.DataSource = config;
		}

		[Action(Name = "保存")]
		private void Save()
		{
			this.EndEdit();
			SysLocalConfig config = bindingSource1.GetCurrent<SysLocalConfig>();
			config.Password = Encrypt.DesEncrype("12345678", "87654321", txtPassword.Text);
			SysLocalConfig.Save(config);
			this.Close();
		}
	}
}
