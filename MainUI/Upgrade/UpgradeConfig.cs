//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeConfig.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;
using System.Xml;

namespace Framework.MainUI
{
	//注意！！！为保证Upgrade的独立性，在Upgrade中另有一个结构兼容的UpgradeConfig类。
	[Serializable]
	public class UpgradeConfig
	{
		public UpgradeConfig()
		{
			VersionNo = 0;
		}

		public long VersionNo
		{
			get;
			set;
		}

		public DateTime UpgradeTime
		{
			get;
			set;
		}

		public bool HaveRead
		{
			get;
			set;
		}

		public string Log
		{
			get;
			set;
		}

		public string[] UpgradeFiles
		{
			get;
			set;
		}

		private static string path = Path.Combine(System.Windows.Forms.Application.StartupPath, "Upgrade.Config");

		public void Save()
		{
			XmlSerializer serializer = XmlSerializer.FromTypes(new Type[] { typeof(UpgradeConfig) }).FirstOrDefault();
			TextWriter writer = new StreamWriter(path);
			serializer.Serialize(writer, this);
			writer.Close();
		}

		public static UpgradeConfig Load()
		{
			if (!File.Exists(path))
			{
				return new UpgradeConfig();
			}
			FileStream fs = new FileStream(path, FileMode.Open);
			XmlReader reader = XmlReader.Create(fs);

			XmlSerializer serializer = XmlSerializer.FromTypes(new Type[] { typeof(UpgradeConfig) }).FirstOrDefault();
			UpgradeConfig obj = (UpgradeConfig)serializer.Deserialize(reader);
			fs.Close();
			return obj;
		}
	}
}
