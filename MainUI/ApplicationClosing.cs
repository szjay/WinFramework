//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ApplicationClosing.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.BaseUI;
using Infrastructure.Components;

namespace Framework.MainUI
{
	[Module(Name = "系统管理.关闭系统", Fix = true, Order = 122, UI = false)]
	public class ApplicationClosing
	{
		public ApplicationClosing()
		{
			if (MsgBox.Confirm("您确定要关闭系统？"))
			{
				ModuleLoader.Instance.MainForm.Close();
			}
		}
	}
}
