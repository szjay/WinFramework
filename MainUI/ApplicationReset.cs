//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ApplicationReset.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Framework.BaseUI;

namespace Framework.MainUI
{
	[Module(Name = "系统管理.切换用户", Fix = true, Order = 121, HR = true, UI = false)]
	public class ApplicationReset
	{
		public ApplicationReset()
		{
			ModuleLoader.Instance.MainForm.Close();
			Application.Exit();

			Process.Start(Application.ExecutablePath, "noautologin");
			Process.GetCurrentProcess().Kill();
		}
	}
}
