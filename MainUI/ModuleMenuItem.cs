//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ModuleMenuItem.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.Utils;
using Framework.BaseUI;

namespace Framework.MainUI
{
	public class ModuleBarItem : BarSubItem
	{
		public string DllName; //DLL文件名
		public string ClassName; //Form的类名
		public string NodeName; //Form的节点名

		public ModuleBarItem()
		{
		}

		public ModuleBarItem(string text)
			: this()
		{
			this.Caption = text;
		}

		private string tooltip;

		public string Tooltip
		{
			get
			{
				return tooltip;
			}
			set
			{
				tooltip = value;

				ToolTipItem toolTipItem1 = new ToolTipItem();
				toolTipItem1.Text = tooltip;

				SuperToolTip superToolTip1 = new SuperToolTip();
				superToolTip1.Items.Add(toolTipItem1);

				this.SuperTip = superToolTip1;
			}
		}

	}

	public class BarButtonMenu : BarButtonItem
	{
		public string DllName; //DLL文件名
		public string ClassName; //Form的类名
		public string NodeName; //Form的节点名
		public ModuleInfo ModuleInfo;

		public BarButtonMenu()
		{
		}

		public BarButtonMenu(string text)
			: this()
		{
			this.Caption = text;
		}

		private string tooltip;

		public string Tooltip
		{
			get
			{
				return tooltip;
			}
			set
			{
				tooltip = value;

				ToolTipItem toolTipItem1 = new ToolTipItem();
				toolTipItem1.Text = tooltip;

				SuperToolTip superToolTip1 = new SuperToolTip();
				superToolTip1.Items.Add(toolTipItem1);

				this.SuperTip = superToolTip1;
			}
		}

	}
}
