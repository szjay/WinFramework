//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeLogForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.HandlerBase;
using Framework.RPC;

namespace Framework.MainUI
{
	public partial class UpgradeLogForm : ToolbarBaseForm
	{
		public static void Open()
		{
			UpgradeConfig upgradeConfig = UpgradeConfig.Load();
			if (upgradeConfig.HaveRead) //如果已经阅读过，那么不再弹出显示。
			{
				return;
			}

			upgradeConfig.Log = Rpc.Create<UpgradeHandler>().GetUpgradeLog();
			if (string.IsNullOrEmpty(upgradeConfig.Log))
			{
				return;
			}

			UpgradeLogForm form = new UpgradeLogForm();
			form.Shown += (a, b) => form.Open(upgradeConfig);
			if (form.ShowDialog() == DialogResult.OK)
			{
				upgradeConfig.HaveRead = true;
				upgradeConfig.Save();
			}
		}

		public UpgradeLogForm()
		{
			InitializeComponent();
		}

		public void Open(UpgradeConfig upgradeConfig)
		{
			memoEdit1.Text = upgradeConfig.Log;
		}

		[Action(Name = "已阅")]
		private void OK()
		{
			this.DialogResult = DialogResult.OK;
		}

	}
}
