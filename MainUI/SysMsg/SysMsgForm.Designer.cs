﻿namespace Framework.MainUI
{
	partial class SysMsgForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// memoEdit1
			// 
			this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.memoEdit1.Location = new System.Drawing.Point(0, 31);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Properties.ReadOnly = true;
			this.memoEdit1.Size = new System.Drawing.Size(384, 231);
			this.memoEdit1.TabIndex = 0;
			this.memoEdit1.UseOptimizedRendering = true;
			// 
			// SysMsgForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 262);
			this.Controls.Add(this.memoEdit1);
			this.Name = "SysMsgForm";
			this.Text = "系统消息";
			this.Controls.SetChildIndex(this.memoEdit1, 0);
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.MemoEdit memoEdit1;
	}
}