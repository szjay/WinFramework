//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysMsgForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.IO;
using System.Windows.Forms;
using Framework.BaseUI;

namespace Framework.MainUI
{
	//[Module(Name = "系统消息", Pop = true)]
	public partial class SysMsgForm : ToolbarBaseForm
	{
		public SysMsgForm()
		{
			InitializeComponent();
			this.Load += Form1_Load;
		}

		void Form1_Load(object sender, EventArgs e)
		{
		}

		public void Open(string msg)
		{
			memoEdit1.Text = msg;
		}

		[Action(Name = "保存消息")]
		private void Save()
		{
			if (memoEdit1.Text == "")
			{
				return;
			}

			SaveFileDialog dialog = new SaveFileDialog();
			dialog.Filter = "文本文件|*.txt|所有文件|*.*";
			if (dialog.ShowDialog() != DialogResult.OK)
			{
				File.WriteAllText(dialog.FileName, memoEdit1.Text);
			}
		}
	}
}
