﻿namespace Framework.MainUI
{
	partial class SysLocalConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
			this.txtPassword = new DevExpress.XtraEditors.TextEdit();
			this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.cmbStartup3 = new DevExpress.XtraEditors.ComboBoxEdit();
			this.cmbStartup2 = new DevExpress.XtraEditors.ComboBoxEdit();
			this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.cmbStartup = new DevExpress.XtraEditors.ComboBoxEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Location = new System.Drawing.Point(294, 103);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(8, 8);
			this.layoutControl1.TabIndex = 9;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(20, 20);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControl2
			// 
			this.layoutControl2.Controls.Add(this.txtPassword);
			this.layoutControl2.Controls.Add(this.textEdit2);
			this.layoutControl2.Controls.Add(this.cmbStartup3);
			this.layoutControl2.Controls.Add(this.cmbStartup2);
			this.layoutControl2.Controls.Add(this.textEdit1);
			this.layoutControl2.Controls.Add(this.cmbStartup);
			this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl2.Location = new System.Drawing.Point(0, 31);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(451, 331);
			this.layoutControl2.TabIndex = 10;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(82, 140);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Properties.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(345, 22);
			this.txtPassword.StyleController = this.layoutControl2;
			this.txtPassword.TabIndex = 13;
			// 
			// textEdit2
			// 
			this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource1, "LoginAccount", true));
			this.textEdit2.Location = new System.Drawing.Point(82, 114);
			this.textEdit2.Name = "textEdit2";
			this.textEdit2.Size = new System.Drawing.Size(345, 22);
			this.textEdit2.StyleController = this.layoutControl2;
			this.textEdit2.TabIndex = 12;
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.MainUI.SysLocalConfig);
			// 
			// cmbStartup3
			// 
			this.cmbStartup3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource1, "StartupModule3", true));
			this.cmbStartup3.Location = new System.Drawing.Point(82, 262);
			this.cmbStartup3.Name = "cmbStartup3";
			this.cmbStartup3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbStartup3.Properties.DropDownRows = 20;
			this.cmbStartup3.Size = new System.Drawing.Size(345, 22);
			this.cmbStartup3.StyleController = this.layoutControl2;
			this.cmbStartup3.TabIndex = 11;
			// 
			// cmbStartup2
			// 
			this.cmbStartup2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource1, "StartupModule2", true));
			this.cmbStartup2.Location = new System.Drawing.Point(82, 236);
			this.cmbStartup2.Name = "cmbStartup2";
			this.cmbStartup2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbStartup2.Properties.DropDownRows = 20;
			this.cmbStartup2.Size = new System.Drawing.Size(345, 22);
			this.cmbStartup2.StyleController = this.layoutControl2;
			this.cmbStartup2.TabIndex = 6;
			// 
			// textEdit1
			// 
			this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource1, "LocalHostNo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.textEdit1.Location = new System.Drawing.Point(82, 44);
			this.textEdit1.Name = "textEdit1";
			this.textEdit1.Size = new System.Drawing.Size(345, 22);
			this.textEdit1.StyleController = this.layoutControl2;
			this.textEdit1.TabIndex = 5;
			// 
			// cmbStartup
			// 
			this.cmbStartup.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource1, "StartupModule", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.cmbStartup.Location = new System.Drawing.Point(82, 210);
			this.cmbStartup.Name = "cmbStartup";
			this.cmbStartup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbStartup.Properties.DropDownRows = 20;
			this.cmbStartup.Size = new System.Drawing.Size(345, 22);
			this.cmbStartup.StyleController = this.layoutControl2;
			this.cmbStartup.TabIndex = 4;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Size = new System.Drawing.Size(451, 331);
			this.layoutControlGroup2.Text = "layoutControlGroup2";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlGroup3
			// 
			this.layoutControlGroup3.CustomizationFormText = "自动启动";
			this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4});
			this.layoutControlGroup3.Location = new System.Drawing.Point(0, 166);
			this.layoutControlGroup3.Name = "layoutControlGroup3";
			this.layoutControlGroup3.Size = new System.Drawing.Size(431, 145);
			this.layoutControlGroup3.Text = "自动启动";
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.cmbStartup;
			this.layoutControlItem1.CustomizationFormText = "自动启动";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(407, 26);
			this.layoutControlItem1.Text = "自动启动1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(55, 14);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.cmbStartup2;
			this.layoutControlItem3.CustomizationFormText = "自动启动2";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(407, 26);
			this.layoutControlItem3.Text = "自动启动2";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(55, 14);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.cmbStartup3;
			this.layoutControlItem4.CustomizationFormText = "自动启动3";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(407, 49);
			this.layoutControlItem4.Text = "自动启动3";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(55, 14);
			// 
			// layoutControlGroup4
			// 
			this.layoutControlGroup4.CustomizationFormText = "本机";
			this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
			this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup4.Name = "layoutControlGroup4";
			this.layoutControlGroup4.Size = new System.Drawing.Size(431, 70);
			this.layoutControlGroup4.Text = "本机";
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.textEdit1;
			this.layoutControlItem2.CustomizationFormText = "本机编号";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(407, 26);
			this.layoutControlItem2.Text = "本机编号";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(55, 14);
			// 
			// layoutControlGroup5
			// 
			this.layoutControlGroup5.CustomizationFormText = "自动登录";
			this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6});
			this.layoutControlGroup5.Location = new System.Drawing.Point(0, 70);
			this.layoutControlGroup5.Name = "layoutControlGroup5";
			this.layoutControlGroup5.Size = new System.Drawing.Size(431, 96);
			this.layoutControlGroup5.Text = "自动登录";
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.textEdit2;
			this.layoutControlItem5.CustomizationFormText = "登录账号";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(407, 26);
			this.layoutControlItem5.Text = "登录账号";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(55, 14);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.txtPassword;
			this.layoutControlItem6.CustomizationFormText = "登录密码";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(407, 26);
			this.layoutControlItem6.Text = "登录密码";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(55, 14);
			// 
			// SysLocalConfigForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(451, 362);
			this.Controls.Add(this.layoutControl2);
			this.Controls.Add(this.layoutControl1);
			this.Name = "SysLocalConfigForm";
			this.Text = "本机配置";
			this.Controls.SetChildIndex(this.layoutControl1, 0);
			this.Controls.SetChildIndex(this.layoutControl2, 0);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbStartup.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraEditors.ComboBoxEdit cmbStartup;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraEditors.TextEdit textEdit1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraEditors.ComboBoxEdit cmbStartup2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
		private DevExpress.XtraEditors.ComboBoxEdit cmbStartup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraEditors.TextEdit txtPassword;
		private DevExpress.XtraEditors.TextEdit textEdit2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
	}
}