﻿namespace Framework.SystemUI
{
	partial class ChangePasswordForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.txtNewPwd2 = new DevExpress.XtraEditors.TextEdit();
			this.txtNewPwd1 = new DevExpress.XtraEditors.TextEdit();
			this.txtOldPwd = new DevExpress.XtraEditors.TextEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtNewPwd2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewPwd1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldPwd.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.txtNewPwd2);
			this.layoutControl1.Controls.Add(this.txtNewPwd1);
			this.layoutControl1.Controls.Add(this.txtOldPwd);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 31);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(350, 220);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// txtNewPwd2
			// 
			this.txtNewPwd2.Location = new System.Drawing.Point(75, 64);
			this.txtNewPwd2.Name = "txtNewPwd2";
			this.txtNewPwd2.Size = new System.Drawing.Size(263, 22);
			this.txtNewPwd2.StyleController = this.layoutControl1;
			this.txtNewPwd2.TabIndex = 2;
			// 
			// txtNewPwd1
			// 
			this.txtNewPwd1.Location = new System.Drawing.Point(75, 38);
			this.txtNewPwd1.Name = "txtNewPwd1";
			this.txtNewPwd1.Size = new System.Drawing.Size(263, 22);
			this.txtNewPwd1.StyleController = this.layoutControl1;
			this.txtNewPwd1.TabIndex = 1;
			// 
			// txtOldPwd
			// 
			this.txtOldPwd.Location = new System.Drawing.Point(75, 12);
			this.txtOldPwd.Name = "txtOldPwd";
			this.txtOldPwd.Size = new System.Drawing.Size(263, 22);
			this.txtOldPwd.StyleController = this.layoutControl1;
			this.txtOldPwd.TabIndex = 0;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(350, 220);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.txtOldPwd;
			this.layoutControlItem1.CustomizationFormText = "原密码";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(330, 26);
			this.layoutControlItem1.Text = "原密码";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 14);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.txtNewPwd1;
			this.layoutControlItem2.CustomizationFormText = "新密码";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(330, 26);
			this.layoutControlItem2.Text = "新密码";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.txtNewPwd2;
			this.layoutControlItem3.CustomizationFormText = "确认新密码";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(330, 148);
			this.layoutControlItem3.Text = "确认新密码";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
			// 
			// ChangePasswordForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 251);
			this.Controls.Add(this.layoutControl1);
			this.Name = "ChangePasswordForm";
			this.Text = "修改密码";
			this.Controls.SetChildIndex(this.layoutControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtNewPwd2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewPwd1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldPwd.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.TextEdit txtNewPwd2;
		private DevExpress.XtraEditors.TextEdit txtNewPwd1;
		private DevExpress.XtraEditors.TextEdit txtOldPwd;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
	}
}