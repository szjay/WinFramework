﻿//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionRoleEditForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：Jay
//  修改时间：2018-3-20
//  修改说明：增加角色分类；完善数据权限。
//==============================================================

using System;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	public partial class PermissionRoleEditForm : ToolbarBaseForm
	{
		private ContainerController cc = new ContainerController();

		public PermissionRoleEditForm()
		{
			InitializeComponent();
			this.Load += PermissionRoleEditForm_Load;
		}

		void PermissionRoleEditForm_Load(object sender, EventArgs e)
		{
			cc.ReadOnlyChanged += r =>
			{
				if (r)
				{
					Toolbar.HideButton(Save);
				}
				else
				{
					Toolbar.ShowButton(Save);
				}
			};
			cc.Init(true, this);
		}

		public PermissionRole Current
		{
			get
			{
				return bindingSource1.GetCurrent<PermissionRole>();
			}
		}

		public void Add()
		{
			PermissionRole role = new PermissionRole()
			{
				RoleId = Guid.NewGuid()
			};
			bindingSource1.DataSource = role;
			cc.ReadOnly = false;
		}

		public void Modify(PermissionRole role)
		{
			bindingSource1.DataSource = Rpc.Create<PermissionHandler>().GetRole(role.RoleId);
			cc.ReadOnly = false;
		}

		public void Browse(PermissionRole role)
		{
			bindingSource1.DataSource = Rpc.Create<PermissionHandler>().GetRole(role.RoleId);
			cc.ReadOnly = true;
		}

		[Action(Name = "保存")]
		private void Save()
		{
			this.EndEdit();
			PermissionRole role = bindingSource1.GetCurrent<PermissionRole>();
			if (string.IsNullOrEmpty(role.RoleName))
			{
				MsgBox.Show("请填写角色名称");
				return;
			}
			Rpc.Create<PermissionHandler>().SaveRole(role);
			this.DialogResult = DialogResult.OK;
		}
	}
}
