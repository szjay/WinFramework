﻿namespace Framework.SystemUI
{
	partial class SkinStyleForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.fontEdit1 = new DevExpress.XtraEditors.FontEdit();
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
			this.groupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
			this.groupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// simpleButton11
			// 
			this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton11.Appearance.Options.UseFont = true;
			this.simpleButton11.Location = new System.Drawing.Point(199, 112);
			this.simpleButton11.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
			this.simpleButton11.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton11.Name = "simpleButton11";
			this.simpleButton11.Size = new System.Drawing.Size(173, 33);
			this.simpleButton11.TabIndex = 29;
			this.simpleButton11.Tag = "Visual Studio 2013 Blue";
			this.simpleButton11.Text = "蓝色";
			this.simpleButton11.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton10
			// 
			this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton10.Appearance.Options.UseFont = true;
			this.simpleButton10.Location = new System.Drawing.Point(199, 73);
			this.simpleButton10.LookAndFeel.SkinName = "Office 2013 Light Gray";
			this.simpleButton10.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton10.Name = "simpleButton10";
			this.simpleButton10.Size = new System.Drawing.Size(173, 33);
			this.simpleButton10.TabIndex = 28;
			this.simpleButton10.Tag = "Office 2013 Light Gray";
			this.simpleButton10.Text = "明灰";
			this.simpleButton10.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton9
			// 
			this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton9.Appearance.Options.UseFont = true;
			this.simpleButton9.Location = new System.Drawing.Point(199, 34);
			this.simpleButton9.LookAndFeel.SkinName = "Office 2013 Dark Gray";
			this.simpleButton9.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton9.Name = "simpleButton9";
			this.simpleButton9.Size = new System.Drawing.Size(173, 33);
			this.simpleButton9.TabIndex = 27;
			this.simpleButton9.Tag = "Office 2013 Dark Gray";
			this.simpleButton9.Text = "暗灰";
			this.simpleButton9.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton8
			// 
			this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton8.Appearance.Options.UseFont = true;
			this.simpleButton8.Location = new System.Drawing.Point(378, 112);
			this.simpleButton8.LookAndFeel.SkinName = "Office 2010 Silver";
			this.simpleButton8.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton8.Name = "simpleButton8";
			this.simpleButton8.Size = new System.Drawing.Size(173, 33);
			this.simpleButton8.TabIndex = 26;
			this.simpleButton8.Tag = "Office 2010 Silver";
			this.simpleButton8.Text = "银色";
			this.simpleButton8.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton7
			// 
			this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton7.Appearance.Options.UseFont = true;
			this.simpleButton7.Location = new System.Drawing.Point(378, 73);
			this.simpleButton7.LookAndFeel.SkinName = "Office 2010 Black";
			this.simpleButton7.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton7.Name = "simpleButton7";
			this.simpleButton7.Size = new System.Drawing.Size(173, 33);
			this.simpleButton7.TabIndex = 25;
			this.simpleButton7.Tag = "Office 2010 Black";
			this.simpleButton7.Text = "黑色";
			this.simpleButton7.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton6
			// 
			this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton6.Appearance.Options.UseFont = true;
			this.simpleButton6.Location = new System.Drawing.Point(378, 34);
			this.simpleButton6.LookAndFeel.SkinName = "Office 2010 Blue";
			this.simpleButton6.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton6.Name = "simpleButton6";
			this.simpleButton6.Size = new System.Drawing.Size(173, 33);
			this.simpleButton6.TabIndex = 24;
			this.simpleButton6.Tag = "Office 2010 Blue";
			this.simpleButton6.Text = "浅蓝";
			this.simpleButton6.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton5
			// 
			this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton5.Appearance.Options.UseFont = true;
			this.simpleButton5.Location = new System.Drawing.Point(199, 151);
			this.simpleButton5.LookAndFeel.SkinName = "Seven Classic";
			this.simpleButton5.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton5.Name = "simpleButton5";
			this.simpleButton5.Size = new System.Drawing.Size(173, 33);
			this.simpleButton5.TabIndex = 23;
			this.simpleButton5.Tag = "Seven Classic";
			this.simpleButton5.Text = "经典";
			this.simpleButton5.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton4
			// 
			this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton4.Appearance.Options.UseFont = true;
			this.simpleButton4.Location = new System.Drawing.Point(20, 151);
			this.simpleButton4.LookAndFeel.SkinName = "VS2010";
			this.simpleButton4.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton4.Name = "simpleButton4";
			this.simpleButton4.Size = new System.Drawing.Size(173, 33);
			this.simpleButton4.TabIndex = 22;
			this.simpleButton4.Tag = "VS2010";
			this.simpleButton4.Text = "灰色";
			this.simpleButton4.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton3
			// 
			this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton3.Appearance.Options.UseFont = true;
			this.simpleButton3.Location = new System.Drawing.Point(20, 112);
			this.simpleButton3.LookAndFeel.SkinName = "Office 2013";
			this.simpleButton3.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton3.Name = "simpleButton3";
			this.simpleButton3.Size = new System.Drawing.Size(173, 33);
			this.simpleButton3.TabIndex = 21;
			this.simpleButton3.Tag = "Office 2013";
			this.simpleButton3.Text = "Office 2013";
			this.simpleButton3.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton2
			// 
			this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton2.Appearance.Options.UseFont = true;
			this.simpleButton2.Location = new System.Drawing.Point(20, 34);
			this.simpleButton2.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton2.Name = "simpleButton2";
			this.simpleButton2.Size = new System.Drawing.Size(173, 33);
			this.simpleButton2.TabIndex = 20;
			this.simpleButton2.Tag = "DevExpress Style";
			this.simpleButton2.Text = "默认";
			this.simpleButton2.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// simpleButton1
			// 
			this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simpleButton1.Appearance.Options.UseFont = true;
			this.simpleButton1.Location = new System.Drawing.Point(20, 73);
			this.simpleButton1.LookAndFeel.SkinName = "DevExpress Dark Style";
			this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
			this.simpleButton1.Name = "simpleButton1";
			this.simpleButton1.Size = new System.Drawing.Size(173, 33);
			this.simpleButton1.TabIndex = 19;
			this.simpleButton1.Tag = "DevExpress Dark Style";
			this.simpleButton1.Text = "暗色";
			this.simpleButton1.Click += new System.EventHandler(this.ChangeStyle);
			// 
			// groupControl1
			// 
			this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupControl1.AppearanceCaption.Options.UseFont = true;
			this.groupControl1.Controls.Add(this.simpleButton2);
			this.groupControl1.Controls.Add(this.simpleButton11);
			this.groupControl1.Controls.Add(this.simpleButton1);
			this.groupControl1.Controls.Add(this.simpleButton10);
			this.groupControl1.Controls.Add(this.simpleButton3);
			this.groupControl1.Controls.Add(this.simpleButton9);
			this.groupControl1.Controls.Add(this.simpleButton4);
			this.groupControl1.Controls.Add(this.simpleButton8);
			this.groupControl1.Controls.Add(this.simpleButton5);
			this.groupControl1.Controls.Add(this.simpleButton7);
			this.groupControl1.Controls.Add(this.simpleButton6);
			this.groupControl1.Location = new System.Drawing.Point(12, 37);
			this.groupControl1.Name = "groupControl1";
			this.groupControl1.Size = new System.Drawing.Size(568, 202);
			this.groupControl1.TabIndex = 30;
			this.groupControl1.Text = "界面风格";
			// 
			// groupControl2
			// 
			this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupControl2.AppearanceCaption.Options.UseFont = true;
			this.groupControl2.Controls.Add(this.labelControl2);
			this.groupControl2.Controls.Add(this.fontEdit1);
			this.groupControl2.Controls.Add(this.labelControl1);
			this.groupControl2.Controls.Add(this.spinEdit1);
			this.groupControl2.Location = new System.Drawing.Point(12, 246);
			this.groupControl2.Name = "groupControl2";
			this.groupControl2.Size = new System.Drawing.Size(568, 189);
			this.groupControl2.TabIndex = 31;
			this.groupControl2.Text = "界面字体";
			// 
			// labelControl2
			// 
			this.labelControl2.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControl2.Location = new System.Drawing.Point(20, 48);
			this.labelControl2.Name = "labelControl2";
			this.labelControl2.Size = new System.Drawing.Size(61, 22);
			this.labelControl2.TabIndex = 4;
			this.labelControl2.Text = "字体";
			// 
			// fontEdit1
			// 
			this.fontEdit1.Location = new System.Drawing.Point(81, 48);
			this.fontEdit1.Name = "fontEdit1";
			this.fontEdit1.Properties.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.fontEdit1.Properties.Appearance.Options.UseFont = true;
			this.fontEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.fontEdit1.Size = new System.Drawing.Size(96, 26);
			this.fontEdit1.TabIndex = 3;
			// 
			// labelControl1
			// 
			this.labelControl1.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControl1.Location = new System.Drawing.Point(213, 48);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(61, 22);
			this.labelControl1.TabIndex = 2;
			this.labelControl1.Text = "大小";
			// 
			// spinEdit1
			// 
			this.spinEdit1.EditValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
			this.spinEdit1.Location = new System.Drawing.Point(274, 48);
			this.spinEdit1.Name = "spinEdit1";
			this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.spinEdit1.Properties.Appearance.Options.UseFont = true;
			this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.spinEdit1.Size = new System.Drawing.Size(98, 26);
			this.spinEdit1.TabIndex = 1;
			// 
			// SkinStyleForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(592, 447);
			this.Controls.Add(this.groupControl2);
			this.Controls.Add(this.groupControl1);
			this.Name = "SkinStyleForm";
			this.Text = "风格设置";
			this.Controls.SetChildIndex(this.groupControl1, 0);
			this.Controls.SetChildIndex(this.groupControl2, 0);
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
			this.groupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
			this.groupControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.SimpleButton simpleButton11;
		private DevExpress.XtraEditors.SimpleButton simpleButton10;
		private DevExpress.XtraEditors.SimpleButton simpleButton9;
		private DevExpress.XtraEditors.SimpleButton simpleButton8;
		private DevExpress.XtraEditors.SimpleButton simpleButton7;
		private DevExpress.XtraEditors.SimpleButton simpleButton6;
		private DevExpress.XtraEditors.SimpleButton simpleButton5;
		private DevExpress.XtraEditors.SimpleButton simpleButton4;
		private DevExpress.XtraEditors.SimpleButton simpleButton3;
		private DevExpress.XtraEditors.SimpleButton simpleButton2;
		private DevExpress.XtraEditors.SimpleButton simpleButton1;
		private DevExpress.XtraEditors.GroupControl groupControl1;
		private DevExpress.XtraEditors.GroupControl groupControl2;
		private DevExpress.XtraEditors.SpinEdit spinEdit1;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private DevExpress.XtraEditors.FontEdit fontEdit1;
	}
}