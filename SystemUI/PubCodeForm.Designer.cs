﻿namespace Framework.BaseDataUI
{
	partial class PubCodeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colSeqNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colCodeName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDelete = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			this.SuspendLayout();
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bindingSource1;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(265, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDelete});
			this.gridControl1.Size = new System.Drawing.Size(509, 339);
			this.gridControl1.TabIndex = 9;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.DomainBase.PubCode);
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSeqNo,
            this.colCodeName,
            this.colRemark,
            this.colDelete});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.NewItemRowText = "点击这里增加记录";
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSeqNo, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colSeqNo
			// 
			this.colSeqNo.Caption = "序号";
			this.colSeqNo.FieldName = "SeqNo";
			this.colSeqNo.Name = "colSeqNo";
			this.colSeqNo.OptionsColumn.FixedWidth = true;
			this.colSeqNo.Visible = true;
			this.colSeqNo.VisibleIndex = 0;
			this.colSeqNo.Width = 70;
			// 
			// colCodeName
			// 
			this.colCodeName.Caption = "代码";
			this.colCodeName.FieldName = "CodeName";
			this.colCodeName.Name = "colCodeName";
			this.colCodeName.Visible = true;
			this.colCodeName.VisibleIndex = 1;
			this.colCodeName.Width = 187;
			// 
			// colRemark
			// 
			this.colRemark.Caption = "备注";
			this.colRemark.FieldName = "Remark";
			this.colRemark.Name = "colRemark";
			this.colRemark.Visible = true;
			this.colRemark.VisibleIndex = 2;
			this.colRemark.Width = 358;
			// 
			// colDelete
			// 
			this.colDelete.Caption = "删除";
			this.colDelete.ColumnEdit = this.btnDelete;
			this.colDelete.Name = "colDelete";
			this.colDelete.OptionsColumn.FixedWidth = true;
			this.colDelete.Visible = true;
			this.colDelete.VisibleIndex = 3;
			this.colDelete.Width = 50;
			// 
			// btnDelete
			// 
			this.btnDelete.AutoHeight = false;
			this.btnDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// splitterControl1
			// 
			this.splitterControl1.Location = new System.Drawing.Point(260, 31);
			this.splitterControl1.Name = "splitterControl1";
			this.splitterControl1.Size = new System.Drawing.Size(5, 339);
			this.splitterControl1.TabIndex = 11;
			this.splitterControl1.TabStop = false;
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Left;
			this.treeList1.Location = new System.Drawing.Point(0, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.OptionsView.ShowRoot = false;
			this.treeList1.Size = new System.Drawing.Size(260, 339);
			this.treeList1.TabIndex = 12;
			// 
			// treeListColumn1
			// 
			this.treeListColumn1.Caption = "代码类型";
			this.treeListColumn1.FieldName = "代码类型";
			this.treeListColumn1.Name = "treeListColumn1";
			this.treeListColumn1.Visible = true;
			this.treeListColumn1.VisibleIndex = 0;
			// 
			// PubCodeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(774, 370);
			this.Controls.Add(this.gridControl1);
			this.Controls.Add(this.splitterControl1);
			this.Controls.Add(this.treeList1);
			this.Name = "PubCodeForm";
			this.Text = "公共代码";
			this.Controls.SetChildIndex(this.treeList1, 0);
			this.Controls.SetChildIndex(this.splitterControl1, 0);
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraGrid.Columns.GridColumn colCodeName;
		private DevExpress.XtraGrid.Columns.GridColumn colRemark;
		private DevExpress.XtraGrid.Columns.GridColumn colSeqNo;
		private DevExpress.XtraEditors.SplitterControl splitterControl1;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraGrid.Columns.GridColumn colDelete;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDelete;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
	}
}