//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ComboBoxEx.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;

namespace Framework.SystemUI
{
	public static class ComboBoxEx
	{
		public static void LoadCode(this ComboBoxEdit cmb, string codeType)
		{
			foreach (PubCode code in Rpc.Create<PubCodeHandler>().QueryByType(codeType))
			{
				cmb.Properties.Items.Add(code.CodeName);
			}
		}
	}
}
