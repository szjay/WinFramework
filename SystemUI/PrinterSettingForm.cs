//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PrinterSettingForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Reflection;
using Framework.BaseUI;
using Infrastructure.Components;
using Infrastructure.Utilities;

namespace Framework.SystemUI
{
	[Module(Name = "打印机设置", RootModule = "系统管理", Pop = true)]
	public partial class PrinterSettingForm : ToolbarBaseForm
	{
		public PrinterSettingForm()
		{
			InitializeComponent();
			this.Load += PrinterSettingForm_Load;
		}

		void PrinterSettingForm_Load(object sender, EventArgs e)
		{
			this.SetStyle();
			gridControl1.Edit();

			LoadSetting();
			LoadPrinter();
		}

		private void LoadPrinter()
		{
			foreach (string printer in PrinterSettings.InstalledPrinters)
			{
				cmbPrinter.Items.Add(printer);
			}
		}

		private void LoadSetting()
		{
			List<PrinterSetting> settingList = new List<PrinterSetting>();
			foreach (Assembly asm in AssemblyHelper.LoadAll(ClientContext.LocalPath, ""))
			{
				foreach (ReportAttribute attr in asm.GetAllAttribute<ReportAttribute>())
				{
					PrinterSetting setting = Convert(attr);
					if (settingList.Any(a => a.ReportName == setting.ReportName))
					{
						continue;
					}
					settingList.Add(setting);
				}
			}

			foreach (PrinterSetting item in ClientContext.PrinterSettingList)
			{
				PrinterSetting setting = settingList.FirstOrDefault(a => a.ReportName == item.ReportName);
				if (setting != null)
				{
					setting.Printer = item.Printer;
					setting.Preview = item.Preview;
					setting.MarginsLeft = item.MarginsLeft;
					setting.MarginsRight = item.MarginsRight;
					setting.MarginsTop = item.MarginsTop;
					setting.MarginsBottom = item.MarginsBottom;
				}
			}

			bsPrinterSetting.DataSource = settingList;
		}

		private PrinterSetting Convert(ReportAttribute attr)
		{
			PrinterSetting setting = new PrinterSetting()
			{
				ReportName = attr.Name,
				Printer = "",
				Preview = true
			};
			return setting;
		}

		[Action(Name = "保存")]
		private void Save()
		{
			this.EndEdit();
			List<PrinterSetting> list = bsPrinterSetting.AsList<PrinterSetting>();
			PrinterSetting.Save(list);
			MsgBox.Show("保存成功");
			this.Close();
		}
	}

}
