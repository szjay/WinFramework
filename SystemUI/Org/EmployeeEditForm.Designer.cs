﻿namespace Framework.SystemUI
{
	partial class EmployeeEditForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
			this.bsUser = new System.Windows.Forms.BindingSource(this.components);
			this.cmbPosition = new DevExpress.XtraEditors.CheckedComboBoxEdit();
			this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
			this.bsEmp = new System.Windows.Forms.BindingSource(this.components);
			this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
			this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
			this.edtDept = new DevExpress.XtraEditors.ButtonEdit();
			this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.cmbSex = new DevExpress.XtraEditors.ImageComboBoxEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbPosition.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtDept.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.textEdit6);
			this.layoutControl1.Controls.Add(this.cmbPosition);
			this.layoutControl1.Controls.Add(this.checkEdit2);
			this.layoutControl1.Controls.Add(this.checkEdit1);
			this.layoutControl1.Controls.Add(this.textEdit3);
			this.layoutControl1.Controls.Add(this.memoEdit1);
			this.layoutControl1.Controls.Add(this.textEdit5);
			this.layoutControl1.Controls.Add(this.textEdit4);
			this.layoutControl1.Controls.Add(this.edtDept);
			this.layoutControl1.Controls.Add(this.textEdit2);
			this.layoutControl1.Controls.Add(this.textEdit1);
			this.layoutControl1.Controls.Add(this.cmbSex);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 31);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(828, 352);
			this.layoutControl1.TabIndex = 4;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// textEdit6
			// 
			this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsUser, "UserCardNo", true));
			this.textEdit6.Location = new System.Drawing.Point(467, 90);
			this.textEdit6.Name = "textEdit6";
			this.textEdit6.Size = new System.Drawing.Size(349, 22);
			this.textEdit6.StyleController = this.layoutControl1;
			this.textEdit6.TabIndex = 17;
			// 
			// bsUser
			// 
			this.bsUser.DataSource = typeof(Framework.DomainBase.SysUser);
			// 
			// cmbPosition
			// 
			this.cmbPosition.EditValue = "";
			this.cmbPosition.Location = new System.Drawing.Point(63, 90);
			this.cmbPosition.Name = "cmbPosition";
			this.cmbPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbPosition.Properties.DropDownRows = 20;
			this.cmbPosition.Size = new System.Drawing.Size(349, 22);
			this.cmbPosition.StyleController = this.layoutControl1;
			this.cmbPosition.TabIndex = 16;
			// 
			// checkEdit2
			// 
			this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "OnJob", true));
			this.checkEdit2.Location = new System.Drawing.Point(668, 64);
			this.checkEdit2.Name = "checkEdit2";
			this.checkEdit2.Properties.Caption = "";
			this.checkEdit2.Size = new System.Drawing.Size(148, 19);
			this.checkEdit2.StyleController = this.layoutControl1;
			this.checkEdit2.TabIndex = 15;
			// 
			// bsEmp
			// 
			this.bsEmp.DataSource = typeof(Framework.DomainBase.Employee);
			// 
			// checkEdit1
			// 
			this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "AllowLogin", true));
			this.checkEdit1.Location = new System.Drawing.Point(466, 64);
			this.checkEdit1.Name = "checkEdit1";
			this.checkEdit1.Properties.Caption = "";
			this.checkEdit1.Size = new System.Drawing.Size(147, 19);
			this.checkEdit1.StyleController = this.layoutControl1;
			this.checkEdit1.TabIndex = 14;
			// 
			// textEdit3
			// 
			this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "IdCard", true));
			this.textEdit3.Location = new System.Drawing.Point(63, 64);
			this.textEdit3.Name = "textEdit3";
			this.textEdit3.Size = new System.Drawing.Size(348, 22);
			this.textEdit3.StyleController = this.layoutControl1;
			this.textEdit3.TabIndex = 13;
			// 
			// memoEdit1
			// 
			this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "Remark", true));
			this.memoEdit1.Location = new System.Drawing.Point(63, 116);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Size = new System.Drawing.Size(753, 224);
			this.memoEdit1.StyleController = this.layoutControl1;
			this.memoEdit1.TabIndex = 11;
			this.memoEdit1.UseOptimizedRendering = true;
			// 
			// textEdit5
			// 
			this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "Address", true));
			this.textEdit5.Location = new System.Drawing.Point(466, 38);
			this.textEdit5.Name = "textEdit5";
			this.textEdit5.Size = new System.Drawing.Size(350, 22);
			this.textEdit5.StyleController = this.layoutControl1;
			this.textEdit5.TabIndex = 10;
			// 
			// textEdit4
			// 
			this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "Tel", true));
			this.textEdit4.Location = new System.Drawing.Point(63, 38);
			this.textEdit4.Name = "textEdit4";
			this.textEdit4.Size = new System.Drawing.Size(348, 22);
			this.textEdit4.StyleController = this.layoutControl1;
			this.textEdit4.TabIndex = 9;
			// 
			// edtDept
			// 
			this.edtDept.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "DeptName", true));
			this.edtDept.Location = new System.Drawing.Point(668, 12);
			this.edtDept.Name = "edtDept";
			this.edtDept.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.edtDept.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.edtDept.Size = new System.Drawing.Size(148, 22);
			this.edtDept.StyleController = this.layoutControl1;
			this.edtDept.TabIndex = 7;
			// 
			// textEdit2
			// 
			this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "EmpName", true));
			this.textEdit2.Location = new System.Drawing.Point(63, 12);
			this.textEdit2.Name = "textEdit2";
			this.textEdit2.Size = new System.Drawing.Size(147, 22);
			this.textEdit2.StyleController = this.layoutControl1;
			this.textEdit2.TabIndex = 5;
			// 
			// textEdit1
			// 
			this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "EmpNo", true));
			this.textEdit1.Location = new System.Drawing.Point(265, 12);
			this.textEdit1.Name = "textEdit1";
			this.textEdit1.Size = new System.Drawing.Size(146, 22);
			this.textEdit1.StyleController = this.layoutControl1;
			this.textEdit1.TabIndex = 4;
			// 
			// cmbSex
			// 
			this.cmbSex.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEmp, "Sex", true));
			this.cmbSex.Location = new System.Drawing.Point(466, 12);
			this.cmbSex.Name = "cmbSex";
			this.cmbSex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbSex.Size = new System.Drawing.Size(147, 22);
			this.cmbSex.StyleController = this.layoutControl1;
			this.cmbSex.TabIndex = 6;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlGroup1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem5,
            this.layoutControlItem9});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(828, 352);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.textEdit1;
			this.layoutControlItem1.CustomizationFormText = "工号";
			this.layoutControlItem1.Location = new System.Drawing.Point(202, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(201, 26);
			this.layoutControlItem1.Text = "工号";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.cmbSex;
			this.layoutControlItem3.CustomizationFormText = "性别";
			this.layoutControlItem3.Location = new System.Drawing.Point(403, 0);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(202, 26);
			this.layoutControlItem3.Text = "性别";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.textEdit2;
			this.layoutControlItem2.CustomizationFormText = "姓名";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(202, 26);
			this.layoutControlItem2.Text = "姓名";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.edtDept;
			this.layoutControlItem4.CustomizationFormText = "部门";
			this.layoutControlItem4.Location = new System.Drawing.Point(605, 0);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(203, 26);
			this.layoutControlItem4.Text = "部门";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.textEdit4;
			this.layoutControlItem6.CustomizationFormText = "电话";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(403, 26);
			this.layoutControlItem6.Text = "电话";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.textEdit5;
			this.layoutControlItem7.CustomizationFormText = "住址";
			this.layoutControlItem7.Location = new System.Drawing.Point(403, 26);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(405, 26);
			this.layoutControlItem7.Text = "住址";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.memoEdit1;
			this.layoutControlItem8.CustomizationFormText = "备注";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 104);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(808, 228);
			this.layoutControlItem8.Text = "备注";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.textEdit3;
			this.layoutControlItem10.CustomizationFormText = "身份证号";
			this.layoutControlItem10.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(403, 26);
			this.layoutControlItem10.Text = "身份证号";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.checkEdit1;
			this.layoutControlItem11.CustomizationFormText = "允许登录系统";
			this.layoutControlItem11.Location = new System.Drawing.Point(403, 52);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(202, 26);
			this.layoutControlItem11.Text = "允许登录";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.checkEdit2;
			this.layoutControlItem12.CustomizationFormText = "在职";
			this.layoutControlItem12.Location = new System.Drawing.Point(605, 52);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(203, 26);
			this.layoutControlItem12.Text = "在职";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.cmbPosition;
			this.layoutControlItem5.CustomizationFormText = "岗位";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 78);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(404, 26);
			this.layoutControlItem5.Text = "岗位";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.textEdit6;
			this.layoutControlItem9.CustomizationFormText = "内部卡号";
			this.layoutControlItem9.Location = new System.Drawing.Point(404, 78);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(404, 26);
			this.layoutControlItem9.Text = "内部卡号";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(48, 14);
			this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			// 
			// EmployeeEditForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(828, 383);
			this.Controls.Add(this.layoutControl1);
			this.Name = "EmployeeEditForm";
			this.Text = "员工资料";
			this.Controls.SetChildIndex(this.layoutControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbPosition.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtDept.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.ButtonEdit edtDept;
		private DevExpress.XtraEditors.TextEdit textEdit2;
		private DevExpress.XtraEditors.TextEdit textEdit1;
		private DevExpress.XtraEditors.ImageComboBoxEdit cmbSex;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraEditors.MemoEdit memoEdit1;
		private DevExpress.XtraEditors.TextEdit textEdit5;
		private DevExpress.XtraEditors.TextEdit textEdit4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private System.Windows.Forms.BindingSource bsEmp;
		private DevExpress.XtraEditors.TextEdit textEdit3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraEditors.CheckEdit checkEdit1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraEditors.CheckEdit checkEdit2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraEditors.CheckedComboBoxEdit cmbPosition;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraEditors.TextEdit textEdit6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private System.Windows.Forms.BindingSource bsUser;
	}
}