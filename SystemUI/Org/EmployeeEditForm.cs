//==============================================================
//  版权所有：深圳杰文科技
//  文件名：EmployeeEditForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using Framework.BaseUI;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;
using Infrastructure.Utilities;

namespace Framework.SystemUI
{
	public partial class EmployeeEditForm : ToolbarBaseForm, IEmployeeEditForm
	{
		private ContainerController cc = new ContainerController();

		public EmployeeEditForm()
		{
			InitializeComponent();
			this.Load += EmployeeEditForm_Load;
		}

		void EmployeeEditForm_Load(object sender, EventArgs e)
		{
			cmbSex.Add("男", "男");
			cmbSex.Add("女", "女");

			cc.Init(true, this);
			cc.ReadOnlyChanged += r =>
			{
				if (r)
				{
					Toolbar.HideButton(Save);
				}
			};

			edtDept.ButtonClick += edtDept_ButtonClick;

			foreach (Position p in Rpc.Create<OrgHandler>().QueryPosition())
			{
				cmbPosition.Properties.Items.Add(p.PositionId, p.PositionName, CheckState.Unchecked, true);
			}
		}

		void edtDept_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
		{
			Dept dept = SelectDeptForm.Get();
			if (dept != null)
			{
				Employee emp = bsEmp.GetCurrent<Employee>();
				emp.DeptId = dept.DeptId;
				emp.DeptNo = dept.DeptNo;
				emp.DeptName = dept.DeptName;
				bsEmp.ResetCurrentItem();
			}
		}

		public Employee Employee
		{
			get
			{
				return bsEmp.GetCurrent<Employee>();
			}
		}

		public void Browse(Employee emp)
		{
			cc.ReadOnly = true;
			LoadData(emp.EmpId);
		}

		public void Add(Dept dept)
		{
			Employee emp = new Employee()
			{
				EmpId = Guid.NewGuid(),
				AllowLogin = true,
				OnJob = true
			};

			if (dept != null)
			{
				emp.DeptId = dept.DeptId;
				emp.DeptNo = dept.DeptNo;
				emp.DeptName = dept.DeptName;
			}

			SysUser user = AddUser(emp);

			bsEmp.DataSource = emp;
			bsUser.DataSource = user;
			cc.ReadOnly = false;
		}

		public void Modify(Employee emp)
		{
			cc.ReadOnly = false;
			LoadData(emp.EmpId);
		}

		private void LoadData(Guid empId)
		{
			Employee emp = Rpc.Create<OrgHandler>().GetEmployeeWithPosition(empId);
			bsEmp.DataSource = emp;

			SysUser user = Rpc.Create<OrgHandler>().GetUser(emp.EmpId);
			if (user == null)
			{
				user = AddUser(emp);
			}
			bsUser.DataSource = user;

			foreach (CheckedListBoxItem item in cmbPosition.Properties.Items)
			{
				foreach (EmployeePosition p in emp.Positions)
				{
					if (p.PositionId == (Guid)item.Value)
					{
						item.CheckState = CheckState.Checked;
						break;
					}
				}
			}
		}

		private SysUser AddUser(Employee emp)
		{
			SysUser user = new SysUser
			{
				UserId = emp.EmpId,
				Name = emp.EmpName,
				Account = emp.EmpNo,
				Password = "123",
				Enable = true,
				UserCardNo = "",
				Mobile = emp.Tel
			};
			return user;
		}

		[Action(Name = "重置密码", Image = Toolbar.refreshdata)]
		private void ResetPassword()
		{
			if (!MsgBox.Confirm("您确定要重置此员工的登录密码为初始密码123？"))
			{
				return;
			}

			Rpc.Create<LoginHandler>().ResetPassword(Employee.EmpId);
			MsgBox.Show("密码重置成功");
			this.DialogResult = DialogResult.OK;
		}

		[Action(Name = "保存")]
		private void Save()
		{
			this.EndEdit();

			Employee emp = bsEmp.GetCurrent<Employee>();

			if (!new DataValidator()
				.Check(string.IsNullOrEmpty(emp.EmpNo), "请填写工号")
				.Check(string.IsNullOrEmpty(emp.EmpName), "请填写姓名")
				.Check(emp.DeptId == Guid.Empty, "请选择所属部门")
				.ShowMsgIfFailed()
				.Pass)
			{
				return;
			}

			emp.Positions = new List<EmployeePosition>();
			foreach (CheckedListBoxItem item in cmbPosition.Properties.Items)
			{
				if (item.CheckState == CheckState.Checked)
				{
					EmployeePosition p = new EmployeePosition()
					{
						Id = Guid.NewGuid(),
						EmpId = emp.EmpId,
						PositionId = (Guid)item.Value,
					};
					emp.Positions.Add(p);
				}
			}

			SysUser user = bsUser.GetCurrent<SysUser>();
			Rpc.Create<OrgHandler>().Save(emp, user);
			MsgBox.Show("保存成功");
			this.DialogResult = DialogResult.OK;
		}
	}
}
