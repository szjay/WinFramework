﻿namespace Framework.SystemUI
{
	partial class OrgForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bsOrg = new System.Windows.Forms.BindingSource(this.components);
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.colNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
			this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
			this.tabDept = new DevExpress.XtraTab.XtraTabPage();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.btnTopDept = new DevExpress.XtraEditors.SimpleButton();
			this.cmbDeptType = new DevExpress.XtraEditors.ImageComboBoxEdit();
			this.bsDept = new System.Windows.Forms.BindingSource(this.components);
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.txtDeptParent = new DevExpress.XtraEditors.ButtonEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
			this.tabGroup = new DevExpress.XtraTab.XtraTabPage();
			this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
			this.gridControl2 = new DevExpress.XtraGrid.GridControl();
			this.bsGroupEmployee = new System.Windows.Forms.BindingSource(this.components);
			this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colEmpNo1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeleteGroupEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnDeleteGroupEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
			this.bsGroup = new System.Windows.Forms.BindingSource(this.components);
			this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
			this.edtDept = new DevExpress.XtraEditors.ButtonEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
			this.tabCompany = new DevExpress.XtraTab.XtraTabPage();
			this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
			this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
			this.bsCompany = new System.Windows.Forms.BindingSource(this.components);
			this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
			this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
			this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
			this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.tabEmployee = new DevExpress.XtraTab.XtraTabPage();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.bsEmployee = new System.Windows.Forms.BindingSource(this.components);
			this.gvEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colEmpNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colSex = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnModifyEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.btnDeleteEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.chkAllowLogin = new DevExpress.XtraEditors.CheckEdit();
			this.chkOnJob = new DevExpress.XtraEditors.CheckEdit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
			this.xtraTabControl1.SuspendLayout();
			this.tabDept.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmbDeptType.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsDept)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptParent.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
			this.tabGroup.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsGroupEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteGroupEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtDept.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
			this.tabCompany.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
			this.layoutControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsCompany)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			this.tabEmployee.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gvEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnModifyEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
			this.panelControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAllowLogin.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOnJob.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// bsOrg
			// 
			this.bsOrg.DataSource = typeof(Framework.DomainBase.Org);
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colNo,
            this.colName});
			this.treeList1.DataSource = this.bsOrg;
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Left;
			this.treeList1.KeyFieldName = "Id";
			this.treeList1.Location = new System.Drawing.Point(0, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.ParentFieldName = "ParentId";
			this.treeList1.Size = new System.Drawing.Size(258, 451);
			this.treeList1.TabIndex = 4;
			// 
			// colNo
			// 
			this.colNo.FieldName = "No";
			this.colNo.Name = "colNo";
			this.colNo.SortOrder = System.Windows.Forms.SortOrder.Ascending;
			this.colNo.Width = 48;
			// 
			// colName
			// 
			this.colName.Caption = " ";
			this.colName.FieldName = "Name";
			this.colName.Name = "colName";
			this.colName.Visible = true;
			this.colName.VisibleIndex = 0;
			this.colName.Width = 48;
			// 
			// splitterControl1
			// 
			this.splitterControl1.Location = new System.Drawing.Point(258, 31);
			this.splitterControl1.Name = "splitterControl1";
			this.splitterControl1.Size = new System.Drawing.Size(5, 451);
			this.splitterControl1.TabIndex = 5;
			this.splitterControl1.TabStop = false;
			// 
			// xtraTabControl1
			// 
			this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtraTabControl1.Location = new System.Drawing.Point(263, 31);
			this.xtraTabControl1.Name = "xtraTabControl1";
			this.xtraTabControl1.SelectedTabPage = this.tabDept;
			this.xtraTabControl1.Size = new System.Drawing.Size(711, 451);
			this.xtraTabControl1.TabIndex = 6;
			this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabDept,
            this.tabGroup,
            this.tabCompany,
            this.tabEmployee});
			// 
			// tabDept
			// 
			this.tabDept.Controls.Add(this.layoutControl1);
			this.tabDept.Name = "tabDept";
			this.tabDept.Size = new System.Drawing.Size(705, 422);
			this.tabDept.Text = "部门资料";
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.btnTopDept);
			this.layoutControl1.Controls.Add(this.cmbDeptType);
			this.layoutControl1.Controls.Add(this.memoEdit1);
			this.layoutControl1.Controls.Add(this.textEdit2);
			this.layoutControl1.Controls.Add(this.textEdit1);
			this.layoutControl1.Controls.Add(this.txtDeptParent);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(705, 422);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// btnTopDept
			// 
			this.btnTopDept.Location = new System.Drawing.Point(577, 12);
			this.btnTopDept.Name = "btnTopDept";
			this.btnTopDept.Size = new System.Drawing.Size(116, 22);
			this.btnTopDept.StyleController = this.layoutControl1;
			this.btnTopDept.TabIndex = 7;
			this.btnTopDept.Text = "最顶级部门";
			// 
			// cmbDeptType
			// 
			this.cmbDeptType.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsDept, "DeptTypeId", true));
			this.cmbDeptType.Location = new System.Drawing.Point(63, 90);
			this.cmbDeptType.Name = "cmbDeptType";
			this.cmbDeptType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbDeptType.Size = new System.Drawing.Size(630, 22);
			this.cmbDeptType.StyleController = this.layoutControl1;
			this.cmbDeptType.TabIndex = 5;
			// 
			// bsDept
			// 
			this.bsDept.DataSource = typeof(Framework.DomainBase.Dept);
			// 
			// memoEdit1
			// 
			this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsDept, "Remark", true));
			this.memoEdit1.Location = new System.Drawing.Point(63, 116);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Size = new System.Drawing.Size(630, 294);
			this.memoEdit1.StyleController = this.layoutControl1;
			this.memoEdit1.TabIndex = 4;
			this.memoEdit1.UseOptimizedRendering = true;
			// 
			// textEdit2
			// 
			this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsDept, "DeptNo", true));
			this.textEdit2.Location = new System.Drawing.Point(63, 38);
			this.textEdit2.Name = "textEdit2";
			this.textEdit2.Size = new System.Drawing.Size(630, 22);
			this.textEdit2.StyleController = this.layoutControl1;
			this.textEdit2.TabIndex = 0;
			// 
			// textEdit1
			// 
			this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsDept, "DeptName", true));
			this.textEdit1.Location = new System.Drawing.Point(63, 64);
			this.textEdit1.Name = "textEdit1";
			this.textEdit1.Size = new System.Drawing.Size(630, 22);
			this.textEdit1.StyleController = this.layoutControl1;
			this.textEdit1.TabIndex = 1;
			// 
			// txtDeptParent
			// 
			this.txtDeptParent.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsDept, "ParentName", true));
			this.txtDeptParent.Location = new System.Drawing.Point(63, 12);
			this.txtDeptParent.Name = "txtDeptParent";
			this.txtDeptParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.txtDeptParent.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.txtDeptParent.Size = new System.Drawing.Size(510, 22);
			this.txtDeptParent.StyleController = this.layoutControl1;
			this.txtDeptParent.TabIndex = 6;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(705, 422);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.textEdit1;
			this.layoutControlItem1.CustomizationFormText = "部门名称";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem1.Text = "部门名称";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.textEdit2;
			this.layoutControlItem2.CustomizationFormText = "部门编号";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem2.Text = "部门编号";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.memoEdit1;
			this.layoutControlItem3.CustomizationFormText = "备注";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 104);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(685, 298);
			this.layoutControlItem3.Text = "备注";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem16
			// 
			this.layoutControlItem16.Control = this.cmbDeptType;
			this.layoutControlItem16.CustomizationFormText = "部门类型";
			this.layoutControlItem16.Location = new System.Drawing.Point(0, 78);
			this.layoutControlItem16.Name = "layoutControlItem16";
			this.layoutControlItem16.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem16.Text = "部门类型";
			this.layoutControlItem16.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem17
			// 
			this.layoutControlItem17.Control = this.txtDeptParent;
			this.layoutControlItem17.CustomizationFormText = "上级";
			this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem17.Name = "layoutControlItem17";
			this.layoutControlItem17.Size = new System.Drawing.Size(565, 26);
			this.layoutControlItem17.Text = "上级";
			this.layoutControlItem17.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem18
			// 
			this.layoutControlItem18.Control = this.btnTopDept;
			this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
			this.layoutControlItem18.Location = new System.Drawing.Point(565, 0);
			this.layoutControlItem18.Name = "layoutControlItem18";
			this.layoutControlItem18.Size = new System.Drawing.Size(120, 26);
			this.layoutControlItem18.Text = "layoutControlItem18";
			this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem18.TextToControlDistance = 0;
			this.layoutControlItem18.TextVisible = false;
			// 
			// tabGroup
			// 
			this.tabGroup.Controls.Add(this.layoutControl2);
			this.tabGroup.Name = "tabGroup";
			this.tabGroup.Size = new System.Drawing.Size(705, 422);
			this.tabGroup.Text = "小组资料";
			// 
			// layoutControl2
			// 
			this.layoutControl2.Controls.Add(this.gridControl2);
			this.layoutControl2.Controls.Add(this.memoEdit2);
			this.layoutControl2.Controls.Add(this.textEdit4);
			this.layoutControl2.Controls.Add(this.edtDept);
			this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl2.Location = new System.Drawing.Point(0, 0);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(705, 422);
			this.layoutControl2.TabIndex = 1;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// gridControl2
			// 
			this.gridControl2.DataSource = this.bsGroupEmployee;
			this.gridControl2.Location = new System.Drawing.Point(12, 152);
			this.gridControl2.MainView = this.gridView2;
			this.gridControl2.Name = "gridControl2";
			this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteGroupEmployee});
			this.gridControl2.Size = new System.Drawing.Size(681, 258);
			this.gridControl2.TabIndex = 5;
			this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
			// 
			// bsGroupEmployee
			// 
			this.bsGroupEmployee.DataSource = typeof(Framework.DomainBase.DeptGroupEmployee);
			// 
			// gridView2
			// 
			this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo1,
            this.colEmpName1,
            this.colDeleteGroupEmployee});
			this.gridView2.GridControl = this.gridControl2;
			this.gridView2.Name = "gridView2";
			this.gridView2.OptionsView.ShowDetailButtons = false;
			this.gridView2.OptionsView.ShowViewCaption = true;
			this.gridView2.ViewCaption = "小组成员";
			// 
			// colEmpNo1
			// 
			this.colEmpNo1.Caption = "工号";
			this.colEmpNo1.FieldName = "EmpNo";
			this.colEmpNo1.Name = "colEmpNo1";
			this.colEmpNo1.Visible = true;
			this.colEmpNo1.VisibleIndex = 0;
			this.colEmpNo1.Width = 99;
			// 
			// colEmpName1
			// 
			this.colEmpName1.Caption = "员工姓名";
			this.colEmpName1.FieldName = "EmpName";
			this.colEmpName1.Name = "colEmpName1";
			this.colEmpName1.Visible = true;
			this.colEmpName1.VisibleIndex = 1;
			this.colEmpName1.Width = 564;
			// 
			// colDeleteGroupEmployee
			// 
			this.colDeleteGroupEmployee.Caption = "删除";
			this.colDeleteGroupEmployee.ColumnEdit = this.btnDeleteGroupEmployee;
			this.colDeleteGroupEmployee.Name = "colDeleteGroupEmployee";
			this.colDeleteGroupEmployee.Visible = true;
			this.colDeleteGroupEmployee.VisibleIndex = 2;
			this.colDeleteGroupEmployee.Width = 48;
			// 
			// btnDeleteGroupEmployee
			// 
			this.btnDeleteGroupEmployee.AutoHeight = false;
			this.btnDeleteGroupEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.btnDeleteGroupEmployee.Name = "btnDeleteGroupEmployee";
			this.btnDeleteGroupEmployee.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// memoEdit2
			// 
			this.memoEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsGroup, "Remark", true));
			this.memoEdit2.Location = new System.Drawing.Point(63, 64);
			this.memoEdit2.Name = "memoEdit2";
			this.memoEdit2.Size = new System.Drawing.Size(630, 84);
			this.memoEdit2.StyleController = this.layoutControl2;
			this.memoEdit2.TabIndex = 4;
			this.memoEdit2.UseOptimizedRendering = true;
			// 
			// bsGroup
			// 
			this.bsGroup.DataSource = typeof(Framework.DomainBase.DeptGroup);
			// 
			// textEdit4
			// 
			this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsGroup, "GroupName", true));
			this.textEdit4.Location = new System.Drawing.Point(63, 38);
			this.textEdit4.Name = "textEdit4";
			this.textEdit4.Size = new System.Drawing.Size(630, 22);
			this.textEdit4.StyleController = this.layoutControl2;
			this.textEdit4.TabIndex = 1;
			// 
			// edtDept
			// 
			this.edtDept.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsGroup, "DeptName", true));
			this.edtDept.Location = new System.Drawing.Point(63, 12);
			this.edtDept.Name = "edtDept";
			this.edtDept.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.edtDept.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.edtDept.Size = new System.Drawing.Size(630, 22);
			this.edtDept.StyleController = this.layoutControl2;
			this.edtDept.TabIndex = 0;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem15});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup1";
			this.layoutControlGroup2.Size = new System.Drawing.Size(705, 422);
			this.layoutControlGroup2.Text = "layoutControlGroup1";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.textEdit4;
			this.layoutControlItem4.CustomizationFormText = "部门名称";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem4.Name = "layoutControlItem1";
			this.layoutControlItem4.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem4.Text = "小组名称";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.edtDept;
			this.layoutControlItem5.CustomizationFormText = "部门编号";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem5.Name = "layoutControlItem2";
			this.layoutControlItem5.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem5.Text = "所属部门";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.memoEdit2;
			this.layoutControlItem6.CustomizationFormText = "备注";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem6.Name = "layoutControlItem3";
			this.layoutControlItem6.Size = new System.Drawing.Size(685, 88);
			this.layoutControlItem6.Text = "备注";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem15
			// 
			this.layoutControlItem15.Control = this.gridControl2;
			this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
			this.layoutControlItem15.Location = new System.Drawing.Point(0, 140);
			this.layoutControlItem15.Name = "layoutControlItem15";
			this.layoutControlItem15.Size = new System.Drawing.Size(685, 262);
			this.layoutControlItem15.Text = "layoutControlItem15";
			this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem15.TextToControlDistance = 0;
			this.layoutControlItem15.TextVisible = false;
			// 
			// tabCompany
			// 
			this.tabCompany.Controls.Add(this.layoutControl3);
			this.tabCompany.Name = "tabCompany";
			this.tabCompany.Size = new System.Drawing.Size(705, 422);
			this.tabCompany.Text = "单位资料";
			// 
			// layoutControl3
			// 
			this.layoutControl3.Controls.Add(this.textEdit11);
			this.layoutControl3.Controls.Add(this.textEdit10);
			this.layoutControl3.Controls.Add(this.textEdit9);
			this.layoutControl3.Controls.Add(this.textEdit8);
			this.layoutControl3.Controls.Add(this.textEdit7);
			this.layoutControl3.Controls.Add(this.memoEdit3);
			this.layoutControl3.Controls.Add(this.textEdit5);
			this.layoutControl3.Controls.Add(this.textEdit6);
			this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl3.Location = new System.Drawing.Point(0, 0);
			this.layoutControl3.Name = "layoutControl3";
			this.layoutControl3.Root = this.layoutControlGroup3;
			this.layoutControl3.Size = new System.Drawing.Size(705, 422);
			this.layoutControl3.TabIndex = 2;
			this.layoutControl3.Text = "layoutControl3";
			// 
			// textEdit11
			// 
			this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "PostCode", true));
			this.textEdit11.Location = new System.Drawing.Point(46, 116);
			this.textEdit11.Name = "textEdit11";
			this.textEdit11.Size = new System.Drawing.Size(647, 22);
			this.textEdit11.StyleController = this.layoutControl3;
			this.textEdit11.TabIndex = 9;
			// 
			// bsCompany
			// 
			this.bsCompany.DataSource = typeof(Framework.DomainBase.Company);
			// 
			// textEdit10
			// 
			this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Address", true));
			this.textEdit10.Location = new System.Drawing.Point(46, 90);
			this.textEdit10.Name = "textEdit10";
			this.textEdit10.Size = new System.Drawing.Size(647, 22);
			this.textEdit10.StyleController = this.layoutControl3;
			this.textEdit10.TabIndex = 8;
			// 
			// textEdit9
			// 
			this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Fax", true));
			this.textEdit9.Location = new System.Drawing.Point(388, 64);
			this.textEdit9.Name = "textEdit9";
			this.textEdit9.Size = new System.Drawing.Size(305, 22);
			this.textEdit9.StyleController = this.layoutControl3;
			this.textEdit9.TabIndex = 7;
			// 
			// textEdit8
			// 
			this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Tel3", true));
			this.textEdit8.Location = new System.Drawing.Point(46, 64);
			this.textEdit8.Name = "textEdit8";
			this.textEdit8.Size = new System.Drawing.Size(304, 22);
			this.textEdit8.StyleController = this.layoutControl3;
			this.textEdit8.TabIndex = 6;
			// 
			// textEdit7
			// 
			this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Tel2", true));
			this.textEdit7.Location = new System.Drawing.Point(388, 38);
			this.textEdit7.Name = "textEdit7";
			this.textEdit7.Size = new System.Drawing.Size(305, 22);
			this.textEdit7.StyleController = this.layoutControl3;
			this.textEdit7.TabIndex = 5;
			// 
			// memoEdit3
			// 
			this.memoEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Remark", true));
			this.memoEdit3.Location = new System.Drawing.Point(46, 142);
			this.memoEdit3.Name = "memoEdit3";
			this.memoEdit3.Size = new System.Drawing.Size(647, 268);
			this.memoEdit3.StyleController = this.layoutControl3;
			this.memoEdit3.TabIndex = 4;
			this.memoEdit3.UseOptimizedRendering = true;
			// 
			// textEdit5
			// 
			this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "CompanyName", true));
			this.textEdit5.Location = new System.Drawing.Point(46, 12);
			this.textEdit5.Name = "textEdit5";
			this.textEdit5.Size = new System.Drawing.Size(647, 22);
			this.textEdit5.StyleController = this.layoutControl3;
			this.textEdit5.TabIndex = 0;
			// 
			// textEdit6
			// 
			this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsCompany, "Tel1", true));
			this.textEdit6.Location = new System.Drawing.Point(46, 38);
			this.textEdit6.Name = "textEdit6";
			this.textEdit6.Size = new System.Drawing.Size(304, 22);
			this.textEdit6.StyleController = this.layoutControl3;
			this.textEdit6.TabIndex = 1;
			// 
			// layoutControlGroup3
			// 
			this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup3.GroupBordersVisible = false;
			this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem10,
            this.layoutControlItem12});
			this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup3.Name = "layoutControlGroup1";
			this.layoutControlGroup3.Size = new System.Drawing.Size(705, 422);
			this.layoutControlGroup3.Text = "layoutControlGroup1";
			this.layoutControlGroup3.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.textEdit6;
			this.layoutControlItem7.CustomizationFormText = "部门名称";
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem7.Name = "layoutControlItem1";
			this.layoutControlItem7.Size = new System.Drawing.Size(342, 26);
			this.layoutControlItem7.Text = "电话1";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.textEdit5;
			this.layoutControlItem8.CustomizationFormText = "部门编号";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem8.Name = "layoutControlItem2";
			this.layoutControlItem8.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem8.Text = "名称";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.memoEdit3;
			this.layoutControlItem9.CustomizationFormText = "备注";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 130);
			this.layoutControlItem9.Name = "layoutControlItem3";
			this.layoutControlItem9.Size = new System.Drawing.Size(685, 272);
			this.layoutControlItem9.Text = "备注";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.textEdit8;
			this.layoutControlItem11.CustomizationFormText = "电话3";
			this.layoutControlItem11.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(342, 26);
			this.layoutControlItem11.Text = "电话3";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.Control = this.textEdit10;
			this.layoutControlItem13.CustomizationFormText = "地址";
			this.layoutControlItem13.Location = new System.Drawing.Point(0, 78);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem13.Text = "地址";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.Control = this.textEdit11;
			this.layoutControlItem14.CustomizationFormText = "邮编";
			this.layoutControlItem14.Location = new System.Drawing.Point(0, 104);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(685, 26);
			this.layoutControlItem14.Text = "邮编";
			this.layoutControlItem14.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.textEdit7;
			this.layoutControlItem10.CustomizationFormText = "电话2";
			this.layoutControlItem10.Location = new System.Drawing.Point(342, 26);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(343, 26);
			this.layoutControlItem10.Text = "电话2";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(31, 14);
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.textEdit9;
			this.layoutControlItem12.CustomizationFormText = "传真";
			this.layoutControlItem12.Location = new System.Drawing.Point(342, 52);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(343, 26);
			this.layoutControlItem12.Text = "传真";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(31, 14);
			// 
			// tabEmployee
			// 
			this.tabEmployee.Controls.Add(this.gridControl1);
			this.tabEmployee.Controls.Add(this.panelControl1);
			this.tabEmployee.Name = "tabEmployee";
			this.tabEmployee.Size = new System.Drawing.Size(705, 422);
			this.tabEmployee.Text = "员工资料";
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bsEmployee;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 0);
			this.gridControl1.MainView = this.gvEmployee;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnModifyEmployee,
            this.btnDeleteEmployee});
			this.gridControl1.Size = new System.Drawing.Size(705, 394);
			this.gridControl1.TabIndex = 0;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEmployee});
			// 
			// bsEmployee
			// 
			this.bsEmployee.DataSource = typeof(Framework.DomainBase.Employee);
			// 
			// gvEmployee
			// 
			this.gvEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo,
            this.colEmpName,
            this.colSex,
            this.colPosition,
            this.colRemark});
			this.gvEmployee.GridControl = this.gridControl1;
			this.gvEmployee.Name = "gvEmployee";
			this.gvEmployee.OptionsView.ShowDetailButtons = false;
			this.gvEmployee.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmpNo, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colEmpNo
			// 
			this.colEmpNo.Caption = "工号";
			this.colEmpNo.FieldName = "EmpNo";
			this.colEmpNo.Name = "colEmpNo";
			this.colEmpNo.OptionsColumn.ReadOnly = true;
			this.colEmpNo.Visible = true;
			this.colEmpNo.VisibleIndex = 0;
			this.colEmpNo.Width = 58;
			// 
			// colEmpName
			// 
			this.colEmpName.Caption = "姓名";
			this.colEmpName.FieldName = "EmpName";
			this.colEmpName.Name = "colEmpName";
			this.colEmpName.OptionsColumn.ReadOnly = true;
			this.colEmpName.Visible = true;
			this.colEmpName.VisibleIndex = 1;
			this.colEmpName.Width = 66;
			// 
			// colSex
			// 
			this.colSex.Caption = "性别";
			this.colSex.FieldName = "Sex";
			this.colSex.Name = "colSex";
			this.colSex.Visible = true;
			this.colSex.VisibleIndex = 2;
			this.colSex.Width = 45;
			// 
			// colPosition
			// 
			this.colPosition.Caption = "部门";
			this.colPosition.FieldName = "DeptName";
			this.colPosition.Name = "colPosition";
			this.colPosition.OptionsColumn.ReadOnly = true;
			this.colPosition.Visible = true;
			this.colPosition.VisibleIndex = 3;
			this.colPosition.Width = 61;
			// 
			// colRemark
			// 
			this.colRemark.Caption = "备注";
			this.colRemark.FieldName = "Remark";
			this.colRemark.Name = "colRemark";
			this.colRemark.OptionsColumn.ReadOnly = true;
			this.colRemark.Visible = true;
			this.colRemark.VisibleIndex = 4;
			this.colRemark.Width = 111;
			// 
			// btnModifyEmployee
			// 
			this.btnModifyEmployee.AutoHeight = false;
			this.btnModifyEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.btnModifyEmployee.Name = "btnModifyEmployee";
			this.btnModifyEmployee.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// btnDeleteEmployee
			// 
			this.btnDeleteEmployee.AutoHeight = false;
			this.btnDeleteEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.btnDeleteEmployee.Name = "btnDeleteEmployee";
			this.btnDeleteEmployee.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// panelControl1
			// 
			this.panelControl1.Controls.Add(this.chkAllowLogin);
			this.panelControl1.Controls.Add(this.chkOnJob);
			this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelControl1.Location = new System.Drawing.Point(0, 394);
			this.panelControl1.Name = "panelControl1";
			this.panelControl1.Size = new System.Drawing.Size(705, 28);
			this.panelControl1.TabIndex = 3;
			// 
			// chkAllowLogin
			// 
			this.chkAllowLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkAllowLogin.EditValue = true;
			this.chkAllowLogin.Location = new System.Drawing.Point(402, 6);
			this.chkAllowLogin.Name = "chkAllowLogin";
			this.chkAllowLogin.Properties.Caption = "不显示不允许登录的员工";
			this.chkAllowLogin.Size = new System.Drawing.Size(158, 19);
			this.chkAllowLogin.TabIndex = 3;
			// 
			// chkOnJob
			// 
			this.chkOnJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkOnJob.EditValue = true;
			this.chkOnJob.Location = new System.Drawing.Point(581, 5);
			this.chkOnJob.Name = "chkOnJob";
			this.chkOnJob.Properties.Caption = "不显示离职员工";
			this.chkOnJob.Size = new System.Drawing.Size(117, 19);
			this.chkOnJob.TabIndex = 2;
			// 
			// OrgForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(974, 482);
			this.Controls.Add(this.xtraTabControl1);
			this.Controls.Add(this.splitterControl1);
			this.Controls.Add(this.treeList1);
			this.Name = "OrgForm";
			this.Text = "组织结构";
			this.Controls.SetChildIndex(this.treeList1, 0);
			this.Controls.SetChildIndex(this.splitterControl1, 0);
			this.Controls.SetChildIndex(this.xtraTabControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
			this.xtraTabControl1.ResumeLayout(false);
			this.tabDept.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmbDeptType.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsDept)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptParent.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
			this.tabGroup.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsGroupEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteGroupEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtDept.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
			this.tabCompany.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
			this.layoutControl3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsCompany)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			this.tabEmployee.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gvEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnModifyEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
			this.panelControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAllowLogin.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOnJob.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bsOrg;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colNo;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
		private DevExpress.XtraEditors.SplitterControl splitterControl1;
		private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
		private DevExpress.XtraTab.XtraTabPage tabDept;
		private DevExpress.XtraTab.XtraTabPage tabGroup;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.TextEdit textEdit1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraEditors.TextEdit textEdit2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraEditors.MemoEdit memoEdit1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraEditors.MemoEdit memoEdit2;
		private DevExpress.XtraEditors.TextEdit textEdit4;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private System.Windows.Forms.BindingSource bsDept;
		private System.Windows.Forms.BindingSource bsGroup;
		private DevExpress.XtraTab.XtraTabPage tabCompany;
		private DevExpress.XtraLayout.LayoutControl layoutControl3;
		private DevExpress.XtraEditors.TextEdit textEdit8;
		private DevExpress.XtraEditors.TextEdit textEdit7;
		private DevExpress.XtraEditors.MemoEdit memoEdit3;
		private DevExpress.XtraEditors.TextEdit textEdit5;
		private DevExpress.XtraEditors.TextEdit textEdit6;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraEditors.TextEdit textEdit9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraEditors.TextEdit textEdit11;
		private DevExpress.XtraEditors.TextEdit textEdit10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
		private System.Windows.Forms.BindingSource bsCompany;
		private DevExpress.XtraTab.XtraTabPage tabEmployee;
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gvEmployee;
		private System.Windows.Forms.BindingSource bsEmployee;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName;
		private DevExpress.XtraGrid.Columns.GridColumn colPosition;
		private DevExpress.XtraGrid.Columns.GridColumn colRemark;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnModifyEmployee;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteEmployee;
		private DevExpress.XtraGrid.GridControl gridControl2;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
		private DevExpress.XtraGrid.Columns.GridColumn colSex;
		private DevExpress.XtraEditors.ButtonEdit edtDept;
		private System.Windows.Forms.BindingSource bsGroupEmployee;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo1;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName1;
		private DevExpress.XtraGrid.Columns.GridColumn colDeleteGroupEmployee;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteGroupEmployee;
		private DevExpress.XtraEditors.ImageComboBoxEdit cmbDeptType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
		private DevExpress.XtraEditors.ButtonEdit txtDeptParent;
		private DevExpress.XtraEditors.SimpleButton btnTopDept;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
		private DevExpress.XtraEditors.CheckEdit chkOnJob;
		private DevExpress.XtraEditors.PanelControl panelControl1;
		private DevExpress.XtraEditors.CheckEdit chkAllowLogin;

	}
}