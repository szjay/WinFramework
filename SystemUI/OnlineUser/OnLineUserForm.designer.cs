﻿namespace Framework.SystemUI
{
	partial class OnLineUserForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colLoginAccount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colIP = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colFirstActiveTime = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colLastActiveTime = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colLastCallMethod = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colLoginCount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colLoginTime = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.HandlerBase.OnlineUser);
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bindingSource1;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(836, 447);
			this.gridControl1.TabIndex = 9;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUserName,
            this.colLoginAccount,
            this.colIP,
            this.colFirstActiveTime,
            this.colLastActiveTime,
            this.colLastCallMethod,
            this.colLoginCount,
            this.colLoginTime});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsBehavior.Editable = false;
			this.gridView1.OptionsBehavior.ReadOnly = true;
			this.gridView1.OptionsView.ShowFooter = true;
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLastActiveTime, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colUserName
			// 
			this.colUserName.Caption = "姓名";
			this.colUserName.FieldName = "UserName";
			this.colUserName.Name = "colUserName";
			this.colUserName.OptionsColumn.ReadOnly = true;
			this.colUserName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "UserName", "共{0}名在线用户")});
			this.colUserName.Visible = true;
			this.colUserName.VisibleIndex = 0;
			// 
			// colLoginAccount
			// 
			this.colLoginAccount.Caption = "账号";
			this.colLoginAccount.FieldName = "LoginAccount";
			this.colLoginAccount.Name = "colLoginAccount";
			this.colLoginAccount.OptionsColumn.ReadOnly = true;
			this.colLoginAccount.Visible = true;
			this.colLoginAccount.VisibleIndex = 1;
			// 
			// colIP
			// 
			this.colIP.Caption = "IP";
			this.colIP.FieldName = "IP";
			this.colIP.Name = "colIP";
			this.colIP.OptionsColumn.ReadOnly = true;
			this.colIP.Visible = true;
			this.colIP.VisibleIndex = 2;
			// 
			// colFirstActiveTime
			// 
			this.colFirstActiveTime.Caption = "初始活动时间";
			this.colFirstActiveTime.FieldName = "FirstActiveTime";
			this.colFirstActiveTime.Name = "colFirstActiveTime";
			this.colFirstActiveTime.OptionsColumn.ReadOnly = true;
			this.colFirstActiveTime.Visible = true;
			this.colFirstActiveTime.VisibleIndex = 3;
			// 
			// colLastActiveTime
			// 
			this.colLastActiveTime.Caption = "最后活动时间";
			this.colLastActiveTime.FieldName = "LastCallTime";
			this.colLastActiveTime.Name = "colLastActiveTime";
			this.colLastActiveTime.OptionsColumn.ReadOnly = true;
			this.colLastActiveTime.Visible = true;
			this.colLastActiveTime.VisibleIndex = 4;
			// 
			// colLastCallMethod
			// 
			this.colLastCallMethod.Caption = "最后调用方法";
			this.colLastCallMethod.FieldName = "LastCallMethod";
			this.colLastCallMethod.Name = "colLastCallMethod";
			this.colLastCallMethod.Visible = true;
			this.colLastCallMethod.VisibleIndex = 5;
			// 
			// colLoginCount
			// 
			this.colLoginCount.Caption = "登录次数";
			this.colLoginCount.FieldName = "LoginCount";
			this.colLoginCount.Name = "colLoginCount";
			this.colLoginCount.Visible = true;
			this.colLoginCount.VisibleIndex = 6;
			// 
			// colLoginTime
			// 
			this.colLoginTime.Caption = "登录时间";
			this.colLoginTime.FieldName = "LoginTime";
			this.colLoginTime.Name = "colLoginTime";
			this.colLoginTime.Visible = true;
			this.colLoginTime.VisibleIndex = 7;
			// 
			// OnLineUserForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(836, 478);
			this.Controls.Add(this.gridControl1);
			this.Name = "OnLineUserForm";
			this.Text = "在线用户列表";
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn colUserName;
		private DevExpress.XtraGrid.Columns.GridColumn colLoginAccount;
		private DevExpress.XtraGrid.Columns.GridColumn colIP;
		private DevExpress.XtraGrid.Columns.GridColumn colFirstActiveTime;
		private DevExpress.XtraGrid.Columns.GridColumn colLastActiveTime;
		private DevExpress.XtraGrid.Columns.GridColumn colLoginCount;
		private DevExpress.XtraGrid.Columns.GridColumn colLoginTime;
		private DevExpress.XtraGrid.Columns.GridColumn colLastCallMethod;
	}
}