//==============================================================
//  版权所有：深圳杰文科技
//  文件名：OnLineUserForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.BaseUI;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	[Module(Name = "在线用户", Order = 4, SystemModule = true)]
	public partial class OnLineUserForm : ToolbarBaseForm
	{
		public OnLineUserForm()
		{
			InitializeComponent();
			this.Load += new EventHandler(OnLineUserForm_Load);
		}

		void OnLineUserForm_Load(object sender, EventArgs e)
		{
			gridControl1.SetStyle(GridHelper.GridStyle.List);
			RefreshData();
		}

		[Action(Name = "刷新")]
		private void RefreshData()
		{
			bindingSource1.DataSource = Rpc.Create<SessionHandler>().GetOnlineUserList();
			gridView1.BestFitColumns();
		}

		[Action(Name = "Kick Out", Image = Toolbar.delete)]
		private void Kickout()
		{
			OnlineUser user = bindingSource1.Current as OnlineUser;
			Rpc.Create<SessionHandler>().KickoutOnlineUser(user.LoginAccount);
			bindingSource1.RemoveCurrent();
			bindingSource1.ResetBindings(false);
		}

		//[Action(Name = "Kick All", Image = Toolbar.delete)]
		private void KickAll()
		{
			if (MsgBox.Confirm("您确定要踢掉所有用户吗？"))
			{
				//Rpc.Create<SessionHandler>().KickoutAll();
				bindingSource1.Clear();
			}
		}

		[Action(Name = "导出", BeginGroup = true)]
		private void Export()
		{
			gridControl1.ExportGrid(this.Text);
		}
	}
}
