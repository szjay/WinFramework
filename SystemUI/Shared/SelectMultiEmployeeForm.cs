//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectMultiEmployeeForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.RPC;
using Infrastructure.Components;
using Framework.DomainBase;
using Framework.HandlerBase;

namespace Framework.SystemUI
{
	public partial class SelectMultiEmployeeForm : ToolbarBaseForm
	{
		public static List<Employee> Get()
		{
			return Get(null, false);
		}

		//只选择SysUser，不选择Employee。
		public static List<Employee> GetUser()
		{
			return Get(null, true);
		}

		//excludeEmployees：排除不选的人。
		public static List<Employee> Get(IEnumerable<Guid> excludeEmployees)
		{
			return Get(excludeEmployees, false);
		}

		private static List<Employee> Get(IEnumerable<Guid> excludeEmployees, bool onlyUser)
		{
			SelectMultiEmployeeForm form = new SelectMultiEmployeeForm(excludeEmployees);
			form.onlyUser = onlyUser;
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.Employees;
			}
			return new List<Employee>();
		}

		/**********************************************************************************************************/

		private IEnumerable<Guid> _ExcludeEmployees = null;
		private bool onlyUser = false;

		public SelectMultiEmployeeForm(IEnumerable<Guid> excludeEmployees)
		{
			InitializeComponent();
			_ExcludeEmployees = excludeEmployees;
			this.Load += SelectMultiEmployeeForm_Load;
		}

		void SelectMultiEmployeeForm_Load(object sender, EventArgs e)
		{
			this.SetStyle();

			gridView1.OptionsSelection.MultiSelect = true;
			gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;

			this.Shown += SelectMultiEmployeeForm_Shown;
			treeList1.FocusedNodeChanged += treeList1_FocusedNodeChanged;
			gridView1.DoubleClick += gridView1_DoubleClick;
			gridView2.DoubleClick += gridView2_DoubleClick;
			btnSelect.Click += btnSelect_Click;
			btnUnselect.Click += btnUnselect_Click;

			bsOrg.DataSource = Rpc.Create<OrgHandler>().GetOrgTree();
			bsSelected.DataSource = new List<Employee>();
		}

		void SelectMultiEmployeeForm_Shown(object sender, EventArgs e)
		{
			treeList1.ExpandAll();
		}

		public List<Employee> Employees
		{
			get
			{
				return bsSelected.DataSource as List<Employee>;
			}
		}

		void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
		{
			Org org = treeList1.GetDataRecordByNode(e.Node) as Org;
			if (org == null)
			{
				return;
			}

			List<Employee> empList = new List<Employee>();
			switch (org.NodeType)
			{
				case Org.NodeTypes.Company:
					if (onlyUser)
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployeeForUser();
					}
					else
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployee();
					}
					break;

				case Org.NodeTypes.Dept:
					if (onlyUser)
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployeeByDeptForUser(org.Id);
					}
					else
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployeeByDept(org.Id);
					}
					break;

				case Org.NodeTypes.Group:
					if (onlyUser)
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployeeByGroupForUser(org.Id);
					}
					else
					{
						empList = Rpc.Create<OrgHandler>().QueryEmployeeByGroup(org.Id);
					}
					break;
			}

			if (_ExcludeEmployees != null)
			{
				foreach (Guid empId in _ExcludeEmployees)
				{
					Employee emp = empList.FirstOrDefault(a => a.EmpId == empId);
					if (emp != null)
					{
						empList.Remove(emp);
					}
				}
			}

			bsEmployee.DataSource = empList;
		}

		void btnUnselect_Click(object sender, EventArgs e)
		{
			gridView2.DeleteSelectedRows();
		}

		void btnSelect_Click(object sender, EventArgs e)
		{
			Employee emp = gridView1.GetFocusedRow() as Employee;
			if (emp == null)
			{
				return;
			}

			var list = bsSelected.DataSource as List<Employee>;
			if (!list.Exists(a => a.EmpId == emp.EmpId))
			{
				bsSelected.Add(emp);
			}
		}

		[Action(Name = "确定")]
		private void OK()
		{
			this.EndEdit();
			this.DialogResult = DialogResult.OK;
		}

		void gridView1_DoubleClick(object sender, EventArgs e)
		{
			btnSelect.PerformClick();
		}

		void gridView2_DoubleClick(object sender, EventArgs e)
		{
			btnUnselect.PerformClick();
		}
	}
}
