//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectDeptForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	public partial class SelectDeptForm : ToolbarBaseForm
	{
		public static Dept Get()
		{
			SelectDeptForm form = new SelectDeptForm();
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.Dept;
			}
			return null;
		}

		/*************************************************************/

		public SelectDeptForm()
		{
			InitializeComponent();
			this.Load += SelectDeptForm_Load;
		}

		void SelectDeptForm_Load(object sender, EventArgs e)
		{
			List<Org> orgList = Rpc.Create<OrgHandler>().GetOrgTreeWithoutGroup();
			bsOrg.DataSource = orgList;
			this.SetStyle();

			treeList1.DoubleClick += treeList1_DoubleClick;
			treeList1.ExpandAll();
		}

		public Dept Dept
		{
			get
			{
				Org org = bsOrg.GetCurrent<Org>();
				if (org.NodeType != Org.NodeTypes.Dept)
				{
					return null;
				}
				Dept dept = Rpc.Create<OrgHandler>().GetDept(org.Id);
				return dept;
			}
		}

		[Action(Name = "确定")]
		private void OK()
		{
			if (this.Dept == null)
			{
				MsgBox.Show("请选择部门");
				return;
			}
			this.DialogResult = DialogResult.OK;
		}

		void treeList1_DoubleClick(object sender, EventArgs e)
		{
			OK();
		}
	}
}
