//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectEmployeeForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	public partial class SelectEmployeeForm : ToolbarBaseForm
	{
		public static Employee Get()
		{
			SelectEmployeeForm form = new SelectEmployeeForm();
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.Employee;
			}
			return null;
		}

		/*********************************************************************/

		public SelectEmployeeForm()
		{
			InitializeComponent();
			this.Load += SelectEmployeeForm_Load;
		}

		void SelectEmployeeForm_Load(object sender, EventArgs e)
		{
			this.SetStyle();

			treeList1.FocusedNodeChanged += treeList1_FocusedNodeChanged;
			gridView1.DoubleClick += gridView1_DoubleClick;

			bsOrg.DataSource = Rpc.Create<OrgHandler>().GetOrgTree();
		}

		public Employee Employee
		{
			get
			{
				return bsEmployee.GetCurrent<Employee>();
			}
		}

		void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
		{
			Org org = treeList1.GetDataRecordByNode(e.Node) as Org;
			if (org == null)
			{
				return;
			}

			switch (org.NodeType)
			{
				case Org.NodeTypes.Company:
					bsEmployee.DataSource = Rpc.Create<OrgHandler>().QueryEmployee();
					break;

				case Org.NodeTypes.Dept:
					bsEmployee.DataSource = Rpc.Create<OrgHandler>().QueryEmployeeByDept(org.Id);
					break;

				case Org.NodeTypes.Group:
					bsEmployee.DataSource = Rpc.Create<OrgHandler>().QueryEmployeeByGroup(org.Id);
					break;
			}
		}

		void gridView1_DoubleClick(object sender, EventArgs e)
		{
			OK();
		}

		[Action(Name = "确定")]
		private void OK()
		{
			if (this.Employee == null)
			{
				MsgBox.Show("请选择员工");
				return;
			}
			this.DialogResult = DialogResult.OK;
		}
	}
}
