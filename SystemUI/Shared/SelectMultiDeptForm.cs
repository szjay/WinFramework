//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectMultiDeptForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.BaseUI;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	public partial class SelectMultiDeptForm : ToolbarBaseForm
	{
		public static List<Dept> Get(bool allowEmpty)
		{
			SelectMultiDeptForm form = new SelectMultiDeptForm();
			form._AllowEmpty = allowEmpty;
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.DeptList;
			}
			return new List<Dept>();
		}

		public static List<Dept> Get(IEnumerable<Guid> selectedDeptIdList)
		{
			SelectMultiDeptForm form = new SelectMultiDeptForm();
			form._AllowEmpty = true;
			form._SelectedDeptIdList = selectedDeptIdList;
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.DeptList;
			}
			return new List<Dept>();
		}

		/*******************************************************************/

		private bool _AllowEmpty = false;
		private IEnumerable<Guid> _SelectedDeptIdList = null;

		public SelectMultiDeptForm()
		{
			InitializeComponent();
			this.Load += SelectDeptForm_Load;
		}

		void SelectDeptForm_Load(object sender, EventArgs e)
		{
			this.SetStyle();
			
			List<Dept> deptList = Rpc.Create<OrgHandler>().QueryDept();

			if (_SelectedDeptIdList != null)
			{
				foreach (Dept dept in deptList)
				{
					if (_SelectedDeptIdList.Any(a => a == dept.DeptId))
					{
						dept.IsSelected = true;
					}
				}
			}
			bindingSource1.DataSource = deptList;
		}

		public List<Dept> DeptList
		{
			get
			{
				List<Dept> list = bindingSource1.AsList<Dept>();
				return list.Where(a => a.IsSelected).ToList();
			}
		}

		[Action(Name = "确定")]
		private void OK()
		{
			this.EndEdit();

			if (!_AllowEmpty && this.DeptList.Count == 0)
			{
				MsgBox.Show("请选择部门");
				return;
			}
			this.DialogResult = DialogResult.OK;
		}

	}
}
