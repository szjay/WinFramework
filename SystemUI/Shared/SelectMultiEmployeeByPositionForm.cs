//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectMultiEmployeeByPositionForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectMultiEmployeeByPositionForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 15:59
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SelectMultiEmployeeByPositionForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 15:54
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.BaseUI;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	public partial class SelectMultiEmployeeByPositionForm : ToolbarBaseForm
	{
		public static List<Employee> Get(Position.PositionType positionType)
		{
			return Get(positionType, null);
		}

		public static List<Employee> Get(Position.PositionType positionType, IEnumerable<Employee> excludeEmployees)
		{
			SelectMultiEmployeeByPositionForm form = new SelectMultiEmployeeByPositionForm(positionType, excludeEmployees);
			if (form.ShowDialog() == DialogResult.OK)
			{
				return form.Employees;
			}
			return new List<Employee>();
		}

		/**************************************************************************************************************************/

		private Position.PositionType _PositionType = Position.PositionType.无;
		private IEnumerable<Employee> _ExcludeEmployees = null;
		private List<Employee> _EmpList = null;

		public SelectMultiEmployeeByPositionForm(Position.PositionType positionType, IEnumerable<Employee> excludeEmployees)
		{
			InitializeComponent();
			_PositionType = positionType;
			_ExcludeEmployees = excludeEmployees == null ? new List<Employee>() : excludeEmployees;
			this.Load += SelectMultiEmployeeForm_Load;
		}

		void SelectMultiEmployeeForm_Load(object sender, EventArgs e)
		{
			treeList1.SetTreeStyle();
			gridControl1.SetViewStyle();
			gridControl2.SetViewStyle();

			gridView1.OptionsSelection.MultiSelect = true;
			gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;

			this.Shown += SelectMultiEmployeeForm_Shown;
			treeList1.FocusedNodeChanged += treeList1_FocusedNodeChanged;
			gridView1.DoubleClick += gridView1_DoubleClick;
			btnSelect.Click += btnSelect_Click;
			btnUnselect.Click += btnUnselect_Click;

			bsOrg.DataSource = Rpc.Create<OrgHandler>().GetOrgTreeWithoutGroup();
			_EmpList = Rpc.Create<OrgHandler>().QueryEmployeeByPosition(_PositionType);
		}

		void SelectMultiEmployeeForm_Shown(object sender, EventArgs e)
		{
			treeList1.ExpandAll();
		}

		public List<Employee> Employees
		{
			get
			{
				return bsSelected.DataSource as List<Employee>;
			}
		}

		void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
		{
			Org org = treeList1.GetDataRecordByNode(e.Node) as Org;
			if (org == null)
			{
				return;
			}

			List<Employee> list = null;
			switch (org.NodeType)
			{
				case Org.NodeTypes.Company:
					list = _EmpList.ToList();
					break;

				case Org.NodeTypes.Dept:
					list = _EmpList.Where(a => a.DeptId == org.Id).ToList();
					break;
			}

			foreach (Employee excludeEmp in _ExcludeEmployees)
			{
				Employee emp = list.FirstOrDefault(a => a.EmpId == excludeEmp.EmpId);
				if (emp != null)
				{
					list.Remove(emp);
				}
			}

			bsEmployee.DataSource = list;
		}

		void btnUnselect_Click(object sender, EventArgs e)
		{
			gridView2.DeleteSelectedRows();
		}

		void btnSelect_Click(object sender, EventArgs e)
		{
			Employee emp = gridView1.GetFocusedRow() as Employee;
			if (emp == null)
			{
				return;
			}

			var list = bsSelected.DataSource as List<Employee>;
			if (!list.Exists(a => a.EmpId == emp.EmpId))
			{
				bsSelected.Add(emp);
			}
		}

		[Action(Name = "确定")]
		private void OK()
		{
			this.EndEdit();
			this.DialogResult = DialogResult.OK;
		}

		void gridView1_DoubleClick(object sender, EventArgs e)
		{
			btnSelect.PerformClick();
		}
	}
}
