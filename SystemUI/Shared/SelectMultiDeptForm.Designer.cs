﻿namespace Framework.SystemUI
{
	partial class SelectMultiDeptForm
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.bindingSource1 = new System.Windows.Forms.BindingSource();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colIsSelected = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeptNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeptName = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bindingSource1;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(428, 332);
			this.gridControl1.TabIndex = 4;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.DomainBase.Dept);
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIsSelected,
            this.colDeptNo,
            this.colDeptName});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDeptNo, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colIsSelected
			// 
			this.colIsSelected.Caption = "选择";
			this.colIsSelected.FieldName = "IsSelected";
			this.colIsSelected.Name = "colIsSelected";
			this.colIsSelected.Visible = true;
			this.colIsSelected.VisibleIndex = 0;
			// 
			// colDeptNo
			// 
			this.colDeptNo.Caption = "编号";
			this.colDeptNo.FieldName = "DeptNo";
			this.colDeptNo.Name = "colDeptNo";
			this.colDeptNo.OptionsColumn.AllowEdit = false;
			this.colDeptNo.OptionsColumn.ReadOnly = true;
			this.colDeptNo.Visible = true;
			this.colDeptNo.VisibleIndex = 1;
			this.colDeptNo.Width = 128;
			// 
			// colDeptName
			// 
			this.colDeptName.Caption = "部门名称";
			this.colDeptName.FieldName = "DeptName";
			this.colDeptName.Name = "colDeptName";
			this.colDeptName.OptionsColumn.AllowEdit = false;
			this.colDeptName.OptionsColumn.ReadOnly = true;
			this.colDeptName.Visible = true;
			this.colDeptName.VisibleIndex = 2;
			this.colDeptName.Width = 282;
			// 
			// SelectMultiDeptForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(428, 363);
			this.Controls.Add(this.gridControl1);
			this.Name = "SelectMultiDeptForm";
			this.Text = "选择部门";
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraGrid.Columns.GridColumn colDeptNo;
		private DevExpress.XtraGrid.Columns.GridColumn colDeptName;
		private DevExpress.XtraGrid.Columns.GridColumn colIsSelected;
	}
}

