﻿namespace Framework.SystemUI
{
	partial class SelectEmployeeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.bsEmployee = new System.Windows.Forms.BindingSource();
			this.colEmpNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colSex = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colTel = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeptName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
			this.bsOrg = new System.Windows.Forms.BindingSource();
			this.colNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).BeginInit();
			this.SuspendLayout();
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bsEmployee;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(201, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(632, 469);
			this.gridControl1.TabIndex = 9;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo,
            this.colEmpName,
            this.colDeptName,
            this.colSex,
            this.colPosition,
            this.colTel,
            this.colAddress,
            this.colRemark});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsView.ShowDetailButtons = false;
			// 
			// bsEmployee
			// 
			this.bsEmployee.DataSource = typeof(Framework.DomainBase.Employee);
			// 
			// colEmpNo
			// 
			this.colEmpNo.Caption = "工号";
			this.colEmpNo.FieldName = "EmpNo";
			this.colEmpNo.Name = "colEmpNo";
			this.colEmpNo.Visible = true;
			this.colEmpNo.VisibleIndex = 0;
			// 
			// colEmpName
			// 
			this.colEmpName.Caption = "员工姓名";
			this.colEmpName.FieldName = "EmpName";
			this.colEmpName.Name = "colEmpName";
			this.colEmpName.Visible = true;
			this.colEmpName.VisibleIndex = 1;
			// 
			// colSex
			// 
			this.colSex.Caption = "性别";
			this.colSex.FieldName = "Sex";
			this.colSex.Name = "colSex";
			this.colSex.Visible = true;
			this.colSex.VisibleIndex = 3;
			// 
			// colPosition
			// 
			this.colPosition.Caption = "岗位";
			this.colPosition.FieldName = "Position";
			this.colPosition.Name = "colPosition";
			this.colPosition.Visible = true;
			this.colPosition.VisibleIndex = 4;
			// 
			// colTel
			// 
			this.colTel.Caption = "联系电话";
			this.colTel.FieldName = "Tel";
			this.colTel.Name = "colTel";
			this.colTel.Visible = true;
			this.colTel.VisibleIndex = 5;
			// 
			// colAddress
			// 
			this.colAddress.Caption = "地址";
			this.colAddress.FieldName = "Address";
			this.colAddress.Name = "colAddress";
			this.colAddress.Visible = true;
			this.colAddress.VisibleIndex = 6;
			// 
			// colRemark
			// 
			this.colRemark.Caption = "备注";
			this.colRemark.FieldName = "Remark";
			this.colRemark.Name = "colRemark";
			this.colRemark.Visible = true;
			this.colRemark.VisibleIndex = 7;
			// 
			// colDeptName
			// 
			this.colDeptName.Caption = "部门";
			this.colDeptName.FieldName = "DeptName";
			this.colDeptName.Name = "colDeptName";
			this.colDeptName.Visible = true;
			this.colDeptName.VisibleIndex = 2;
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colNo,
            this.colName});
			this.treeList1.DataSource = this.bsOrg;
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Left;
			this.treeList1.Location = new System.Drawing.Point(0, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.Size = new System.Drawing.Size(196, 469);
			this.treeList1.TabIndex = 10;
			// 
			// splitterControl1
			// 
			this.splitterControl1.Location = new System.Drawing.Point(196, 31);
			this.splitterControl1.Name = "splitterControl1";
			this.splitterControl1.Size = new System.Drawing.Size(5, 469);
			this.splitterControl1.TabIndex = 11;
			this.splitterControl1.TabStop = false;
			// 
			// bsOrg
			// 
			this.bsOrg.DataSource = typeof(Framework.DomainBase.Org);
			// 
			// colNo
			// 
			this.colNo.FieldName = "No";
			this.colNo.Name = "colNo";
			this.colNo.SortOrder = System.Windows.Forms.SortOrder.Ascending;
			this.colNo.Width = 36;
			// 
			// colName
			// 
			this.colName.Caption = "部门";
			this.colName.FieldName = "Name";
			this.colName.Name = "colName";
			this.colName.Visible = true;
			this.colName.VisibleIndex = 0;
			this.colName.Width = 36;
			// 
			// SelectEmployeeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(833, 500);
			this.Controls.Add(this.gridControl1);
			this.Controls.Add(this.splitterControl1);
			this.Controls.Add(this.treeList1);
			this.Name = "SelectEmployeeForm";
			this.Text = "请选择员工";
			this.Controls.SetChildIndex(this.treeList1, 0);
			this.Controls.SetChildIndex(this.splitterControl1, 0);
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private System.Windows.Forms.BindingSource bsEmployee;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName;
		private DevExpress.XtraGrid.Columns.GridColumn colSex;
		private DevExpress.XtraGrid.Columns.GridColumn colPosition;
		private DevExpress.XtraGrid.Columns.GridColumn colTel;
		private DevExpress.XtraGrid.Columns.GridColumn colAddress;
		private DevExpress.XtraGrid.Columns.GridColumn colRemark;
		private DevExpress.XtraGrid.Columns.GridColumn colDeptName;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraEditors.SplitterControl splitterControl1;
		private System.Windows.Forms.BindingSource bsOrg;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colNo;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colName;

	}
}