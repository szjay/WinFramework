﻿namespace Framework.SystemUI
{
	partial class SelectMultiEmployeeByPositionForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectMultiEmployeeForm));
			this.bsEmployee = new System.Windows.Forms.BindingSource();
			this.bsOrg = new System.Windows.Forms.BindingSource();
			this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
			this.bsSelected = new System.Windows.Forms.BindingSource();
			this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.btnUnselect = new DevExpress.XtraEditors.SimpleButton();
			this.btnSelect = new DevExpress.XtraEditors.SimpleButton();
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colEmpNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colSex = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeptName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridControl2 = new DevExpress.XtraGrid.GridControl();
			this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colEmpNo1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colTel = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsSelected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
			this.panelControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
			this.SuspendLayout();
			// 
			// bsEmployee
			// 
			this.bsEmployee.DataSource = typeof(Framework.DomainBase.Employee);
			// 
			// bsOrg
			// 
			this.bsOrg.DataSource = typeof(Framework.DomainBase.Org);
			// 
			// splitterControl1
			// 
			this.splitterControl1.Location = new System.Drawing.Point(189, 31);
			this.splitterControl1.Name = "splitterControl1";
			this.splitterControl1.Size = new System.Drawing.Size(5, 469);
			this.splitterControl1.TabIndex = 11;
			this.splitterControl1.TabStop = false;
			// 
			// bsSelected
			// 
			this.bsSelected.DataSource = typeof(Framework.DomainBase.Employee);
			// 
			// panelControl1
			// 
			this.panelControl1.Controls.Add(this.btnUnselect);
			this.panelControl1.Controls.Add(this.btnSelect);
			this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelControl1.Location = new System.Drawing.Point(670, 31);
			this.panelControl1.Name = "panelControl1";
			this.panelControl1.Size = new System.Drawing.Size(57, 469);
			this.panelControl1.TabIndex = 13;
			// 
			// btnUnselect
			// 
			this.btnUnselect.Image = ((System.Drawing.Image)(resources.GetObject("btnUnselect.Image")));
			this.btnUnselect.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
			this.btnUnselect.Location = new System.Drawing.Point(6, 137);
			this.btnUnselect.Name = "btnUnselect";
			this.btnUnselect.Size = new System.Drawing.Size(41, 68);
			this.btnUnselect.TabIndex = 1;
			this.btnUnselect.Text = "取消";
			// 
			// btnSelect
			// 
			this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
			this.btnSelect.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
			this.btnSelect.Location = new System.Drawing.Point(6, 53);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(41, 68);
			this.btnSelect.TabIndex = 0;
			this.btnSelect.Text = "选中";
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName});
			this.treeList1.DataSource = this.bsOrg;
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Left;
			this.treeList1.KeyFieldName = "Id";
			this.treeList1.Location = new System.Drawing.Point(0, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.ParentFieldName = "ParentId";
			this.treeList1.Size = new System.Drawing.Size(189, 469);
			this.treeList1.TabIndex = 14;
			// 
			// colName
			// 
			this.colName.Caption = "部门";
			this.colName.FieldName = "Name";
			this.colName.Name = "colName";
			this.colName.Visible = true;
			this.colName.VisibleIndex = 0;
			this.colName.Width = 35;
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bsEmployee;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(194, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(476, 469);
			this.gridControl1.TabIndex = 15;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo,
            this.colEmpName,
            this.colSex,
            this.colPosition,
            this.colDeptName,
            this.colTel});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmpNo, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colEmpNo
			// 
			this.colEmpNo.Caption = "工号";
			this.colEmpNo.FieldName = "EmpNo";
			this.colEmpNo.Name = "colEmpNo";
			this.colEmpNo.Visible = true;
			this.colEmpNo.VisibleIndex = 0;
			this.colEmpNo.Width = 54;
			// 
			// colEmpName
			// 
			this.colEmpName.Caption = "姓名";
			this.colEmpName.FieldName = "EmpName";
			this.colEmpName.Name = "colEmpName";
			this.colEmpName.Visible = true;
			this.colEmpName.VisibleIndex = 1;
			this.colEmpName.Width = 80;
			// 
			// colSex
			// 
			this.colSex.Caption = "性别";
			this.colSex.FieldName = "Sex";
			this.colSex.Name = "colSex";
			this.colSex.Visible = true;
			this.colSex.VisibleIndex = 2;
			this.colSex.Width = 36;
			// 
			// colPosition
			// 
			this.colPosition.Caption = "岗位";
			this.colPosition.FieldName = "PositionNames";
			this.colPosition.Name = "colPosition";
			this.colPosition.Visible = true;
			this.colPosition.VisibleIndex = 4;
			this.colPosition.Width = 106;
			// 
			// colDeptName
			// 
			this.colDeptName.Caption = "部门";
			this.colDeptName.FieldName = "DeptName";
			this.colDeptName.Name = "colDeptName";
			this.colDeptName.Visible = true;
			this.colDeptName.VisibleIndex = 3;
			this.colDeptName.Width = 67;
			// 
			// gridControl2
			// 
			this.gridControl2.DataSource = this.bsSelected;
			this.gridControl2.Dock = System.Windows.Forms.DockStyle.Right;
			this.gridControl2.Location = new System.Drawing.Point(727, 31);
			this.gridControl2.MainView = this.gridView2;
			this.gridControl2.Name = "gridControl2";
			this.gridControl2.Size = new System.Drawing.Size(184, 469);
			this.gridControl2.TabIndex = 16;
			this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
			// 
			// gridView2
			// 
			this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo1,
            this.colEmpName1});
			this.gridView2.GridControl = this.gridControl2;
			this.gridView2.Name = "gridView2";
			this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmpNo1, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colEmpNo1
			// 
			this.colEmpNo1.Caption = "工号";
			this.colEmpNo1.FieldName = "EmpNo";
			this.colEmpNo1.Name = "colEmpNo1";
			this.colEmpNo1.Visible = true;
			this.colEmpNo1.VisibleIndex = 0;
			// 
			// colEmpName1
			// 
			this.colEmpName1.Caption = "姓名";
			this.colEmpName1.FieldName = "EmpName";
			this.colEmpName1.Name = "colEmpName1";
			this.colEmpName1.Visible = true;
			this.colEmpName1.VisibleIndex = 1;
			// 
			// colTel
			// 
			this.colTel.Caption = "联系电话";
			this.colTel.FieldName = "Tel";
			this.colTel.Name = "colTel";
			this.colTel.Visible = true;
			this.colTel.VisibleIndex = 5;
			this.colTel.Width = 115;
			// 
			// SelectMultiEmployeeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(911, 500);
			this.Controls.Add(this.gridControl1);
			this.Controls.Add(this.splitterControl1);
			this.Controls.Add(this.panelControl1);
			this.Controls.Add(this.treeList1);
			this.Controls.Add(this.gridControl2);
			this.Name = "SelectMultiEmployeeForm";
			this.Text = "请选择员工";
			this.Controls.SetChildIndex(this.gridControl2, 0);
			this.Controls.SetChildIndex(this.treeList1, 0);
			this.Controls.SetChildIndex(this.panelControl1, 0);
			this.Controls.SetChildIndex(this.splitterControl1, 0);
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.bsEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsSelected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
			this.panelControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bsEmployee;
		private DevExpress.XtraEditors.SplitterControl splitterControl1;
		private System.Windows.Forms.BindingSource bsOrg;
		private DevExpress.XtraEditors.PanelControl panelControl1;
		private DevExpress.XtraEditors.SimpleButton btnUnselect;
		private DevExpress.XtraEditors.SimpleButton btnSelect;
		private System.Windows.Forms.BindingSource bsSelected;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.GridControl gridControl2;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName;
		private DevExpress.XtraGrid.Columns.GridColumn colSex;
		private DevExpress.XtraGrid.Columns.GridColumn colPosition;
		private DevExpress.XtraGrid.Columns.GridColumn colDeptName;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo1;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName1;
		private DevExpress.XtraGrid.Columns.GridColumn colTel;

	}
}