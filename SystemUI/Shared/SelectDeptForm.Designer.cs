﻿namespace Framework.SystemUI
{
	partial class SelectDeptForm
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bsOrg = new System.Windows.Forms.BindingSource(this.components);
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.colNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			this.SuspendLayout();
			// 
			// bsOrg
			// 
			this.bsOrg.DataSource = typeof(Framework.DomainBase.Org);
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colNo,
            this.colName});
			this.treeList1.DataSource = this.bsOrg;
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeList1.KeyFieldName = "Id";
			this.treeList1.Location = new System.Drawing.Point(0, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.ParentFieldName = "ParentId";
			this.treeList1.Size = new System.Drawing.Size(257, 332);
			this.treeList1.TabIndex = 9;
			// 
			// colNo
			// 
			this.colNo.FieldName = "No";
			this.colNo.Name = "colNo";
			this.colNo.SortOrder = System.Windows.Forms.SortOrder.Ascending;
			this.colNo.Width = 48;
			// 
			// colName
			// 
			this.colName.Caption = " ";
			this.colName.FieldName = "Name";
			this.colName.Name = "colName";
			this.colName.Visible = true;
			this.colName.VisibleIndex = 0;
			this.colName.Width = 48;
			// 
			// SelectDeptForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(257, 363);
			this.Controls.Add(this.treeList1);
			this.Name = "SelectDeptForm";
			this.Text = "选择部门";
			this.Controls.SetChildIndex(this.treeList1, 0);
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bsOrg;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colNo;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colName;

	}
}

