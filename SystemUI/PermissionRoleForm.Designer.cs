﻿namespace Framework.SystemUI
{
	partial class PermissionRoleForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.bsRole = new System.Windows.Forms.BindingSource(this.components);
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colRoleName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRoleType = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDataPermissionTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
			this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.colAction = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.bsAction = new System.Windows.Forms.BindingSource(this.components);
			this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
			this.tabAction = new DevExpress.XtraTab.XtraTabPage();
			this.tabData = new DevExpress.XtraTab.XtraTabPage();
			this.treOrg = new DevExpress.XtraTreeList.TreeList();
			this.colNo = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.bsOrg = new System.Windows.Forms.BindingSource(this.components);
			this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.chkAll = new DevExpress.XtraEditors.CheckEdit();
			this.chkDepts = new DevExpress.XtraEditors.CheckEdit();
			this.chkOnlySelf = new DevExpress.XtraEditors.CheckEdit();
			this.tabUser = new DevExpress.XtraTab.XtraTabPage();
			this.gridControl2 = new DevExpress.XtraGrid.GridControl();
			this.bsUser = new System.Windows.Forms.BindingSource(this.components);
			this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colEmpNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colEmpName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colDeleteUser = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnDeleteUser = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsRole)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsAction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
			this.xtraTabControl1.SuspendLayout();
			this.tabAction.SuspendLayout();
			this.tabData.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.treOrg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
			this.groupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAll.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDepts.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOnlySelf.Properties)).BeginInit();
			this.tabUser.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bsUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteUser)).BeginInit();
			this.SuspendLayout();
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bsRole;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Left;
			this.gridControl1.Location = new System.Drawing.Point(0, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(496, 417);
			this.gridControl1.TabIndex = 4;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// bsRole
			// 
			this.bsRole.DataSource = typeof(Framework.DomainBase.PermissionRole);
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRoleName,
            this.colRoleType,
            this.colDataPermissionTypeName,
            this.colRemark});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsView.ShowDetailButtons = false;
			// 
			// colRoleName
			// 
			this.colRoleName.Caption = "角色名称";
			this.colRoleName.FieldName = "RoleName";
			this.colRoleName.Name = "colRoleName";
			this.colRoleName.Visible = true;
			this.colRoleName.VisibleIndex = 0;
			this.colRoleName.Width = 168;
			// 
			// colRoleType
			// 
			this.colRoleType.Caption = "角色分类";
			this.colRoleType.FieldName = "RoleType";
			this.colRoleType.Name = "colRoleType";
			this.colRoleType.Visible = true;
			this.colRoleType.VisibleIndex = 1;
			this.colRoleType.Width = 106;
			// 
			// colDataPermissionTypeName
			// 
			this.colDataPermissionTypeName.Caption = "数据权限类型";
			this.colDataPermissionTypeName.FieldName = "DataPermissionTypeName";
			this.colDataPermissionTypeName.Name = "colDataPermissionTypeName";
			this.colDataPermissionTypeName.Visible = true;
			this.colDataPermissionTypeName.VisibleIndex = 2;
			this.colDataPermissionTypeName.Width = 90;
			// 
			// colRemark
			// 
			this.colRemark.Caption = "备注";
			this.colRemark.FieldName = "Remark";
			this.colRemark.Name = "colRemark";
			this.colRemark.Visible = true;
			this.colRemark.VisibleIndex = 3;
			this.colRemark.Width = 114;
			// 
			// splitterControl1
			// 
			this.splitterControl1.Location = new System.Drawing.Point(496, 31);
			this.splitterControl1.Name = "splitterControl1";
			this.splitterControl1.Size = new System.Drawing.Size(5, 417);
			this.splitterControl1.TabIndex = 5;
			this.splitterControl1.TabStop = false;
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colAction});
			this.treeList1.DataSource = this.bsAction;
			this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeList1.KeyFieldName = "Id";
			this.treeList1.Location = new System.Drawing.Point(0, 0);
			this.treeList1.Name = "treeList1";
			this.treeList1.OptionsBehavior.AllowRecursiveNodeChecking = true;
			this.treeList1.OptionsView.ShowCheckBoxes = true;
			this.treeList1.ParentFieldName = "ParentId";
			this.treeList1.Size = new System.Drawing.Size(434, 388);
			this.treeList1.TabIndex = 6;
			// 
			// colAction
			// 
			this.colAction.Caption = "权限名称";
			this.colAction.FieldName = "Name";
			this.colAction.MinWidth = 32;
			this.colAction.Name = "colAction";
			this.colAction.Visible = true;
			this.colAction.VisibleIndex = 0;
			this.colAction.Width = 321;
			// 
			// bsAction
			// 
			this.bsAction.DataSource = typeof(Framework.DomainBase.PermissionActionNode);
			// 
			// xtraTabControl1
			// 
			this.xtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
			this.xtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtraTabControl1.Location = new System.Drawing.Point(501, 31);
			this.xtraTabControl1.Name = "xtraTabControl1";
			this.xtraTabControl1.SelectedTabPage = this.tabAction;
			this.xtraTabControl1.Size = new System.Drawing.Size(440, 417);
			this.xtraTabControl1.TabIndex = 9;
			this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabAction,
            this.tabUser,
            this.tabData});
			this.xtraTabControl1.TabPageWidth = 100;
			// 
			// tabAction
			// 
			this.tabAction.Controls.Add(this.treeList1);
			this.tabAction.Name = "tabAction";
			this.tabAction.Size = new System.Drawing.Size(434, 388);
			this.tabAction.Text = "操作权限";
			// 
			// tabData
			// 
			this.tabData.Controls.Add(this.treOrg);
			this.tabData.Controls.Add(this.groupControl1);
			this.tabData.Name = "tabData";
			this.tabData.PageVisible = false;
			this.tabData.Size = new System.Drawing.Size(434, 388);
			this.tabData.Text = "数据权限";
			// 
			// treOrg
			// 
			this.treOrg.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colNo,
            this.colName});
			this.treOrg.DataSource = this.bsOrg;
			this.treOrg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treOrg.KeyFieldName = "Id";
			this.treOrg.Location = new System.Drawing.Point(0, 51);
			this.treOrg.Name = "treOrg";
			this.treOrg.OptionsView.ShowCheckBoxes = true;
			this.treOrg.ParentFieldName = "ParentId";
			this.treOrg.Size = new System.Drawing.Size(434, 337);
			this.treOrg.TabIndex = 5;
			// 
			// colNo
			// 
			this.colNo.FieldName = "No";
			this.colNo.Name = "colNo";
			this.colNo.SortOrder = System.Windows.Forms.SortOrder.Ascending;
			this.colNo.Width = 48;
			// 
			// colName
			// 
			this.colName.Caption = " ";
			this.colName.FieldName = "Name";
			this.colName.MinWidth = 32;
			this.colName.Name = "colName";
			this.colName.Visible = true;
			this.colName.VisibleIndex = 0;
			this.colName.Width = 48;
			// 
			// bsOrg
			// 
			this.bsOrg.DataSource = typeof(Framework.DomainBase.Org);
			// 
			// groupControl1
			// 
			this.groupControl1.Controls.Add(this.chkAll);
			this.groupControl1.Controls.Add(this.chkDepts);
			this.groupControl1.Controls.Add(this.chkOnlySelf);
			this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupControl1.Location = new System.Drawing.Point(0, 0);
			this.groupControl1.Name = "groupControl1";
			this.groupControl1.Size = new System.Drawing.Size(434, 51);
			this.groupControl1.TabIndex = 6;
			this.groupControl1.Text = "数据权限分类";
			// 
			// chkAll
			// 
			this.chkAll.Location = new System.Drawing.Point(269, 25);
			this.chkAll.Name = "chkAll";
			this.chkAll.Properties.Caption = "查询所有部门";
			this.chkAll.Properties.RadioGroupIndex = 1;
			this.chkAll.Size = new System.Drawing.Size(99, 19);
			this.chkAll.TabIndex = 2;
			this.chkAll.TabStop = false;
			// 
			// chkDepts
			// 
			this.chkDepts.Location = new System.Drawing.Point(136, 25);
			this.chkDepts.Name = "chkDepts";
			this.chkDepts.Properties.Caption = "查询指定部门";
			this.chkDepts.Properties.RadioGroupIndex = 1;
			this.chkDepts.Size = new System.Drawing.Size(99, 19);
			this.chkDepts.TabIndex = 1;
			this.chkDepts.TabStop = false;
			// 
			// chkOnlySelf
			// 
			this.chkOnlySelf.Location = new System.Drawing.Point(18, 25);
			this.chkOnlySelf.Name = "chkOnlySelf";
			this.chkOnlySelf.Properties.Caption = "只查自己";
			this.chkOnlySelf.Properties.RadioGroupIndex = 1;
			this.chkOnlySelf.Size = new System.Drawing.Size(99, 19);
			this.chkOnlySelf.TabIndex = 0;
			this.chkOnlySelf.TabStop = false;
			// 
			// tabUser
			// 
			this.tabUser.Controls.Add(this.gridControl2);
			this.tabUser.Name = "tabUser";
			this.tabUser.Size = new System.Drawing.Size(434, 388);
			this.tabUser.Text = "用户";
			// 
			// gridControl2
			// 
			this.gridControl2.DataSource = this.bsUser;
			this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl2.Location = new System.Drawing.Point(0, 0);
			this.gridControl2.MainView = this.gridView2;
			this.gridControl2.Name = "gridControl2";
			this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteUser});
			this.gridControl2.Size = new System.Drawing.Size(434, 388);
			this.gridControl2.TabIndex = 0;
			this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
			// 
			// bsUser
			// 
			this.bsUser.DataSource = typeof(Framework.DomainBase.PermissionRoleUser);
			// 
			// gridView2
			// 
			this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmpNo,
            this.colEmpName,
            this.colDeleteUser});
			this.gridView2.GridControl = this.gridControl2;
			this.gridView2.Name = "gridView2";
			// 
			// colEmpNo
			// 
			this.colEmpNo.Caption = "工号";
			this.colEmpNo.FieldName = "EmpNo";
			this.colEmpNo.Name = "colEmpNo";
			this.colEmpNo.Visible = true;
			this.colEmpNo.VisibleIndex = 0;
			this.colEmpNo.Width = 127;
			// 
			// colEmpName
			// 
			this.colEmpName.Caption = "姓名";
			this.colEmpName.FieldName = "EmpName";
			this.colEmpName.Name = "colEmpName";
			this.colEmpName.Visible = true;
			this.colEmpName.VisibleIndex = 1;
			this.colEmpName.Width = 208;
			// 
			// colDeleteUser
			// 
			this.colDeleteUser.Caption = "删除";
			this.colDeleteUser.ColumnEdit = this.btnDeleteUser;
			this.colDeleteUser.Name = "colDeleteUser";
			this.colDeleteUser.Visible = true;
			this.colDeleteUser.VisibleIndex = 2;
			this.colDeleteUser.Width = 47;
			// 
			// btnDeleteUser
			// 
			this.btnDeleteUser.AutoHeight = false;
			this.btnDeleteUser.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.btnDeleteUser.Name = "btnDeleteUser";
			this.btnDeleteUser.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// PermissionRoleForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(941, 448);
			this.Controls.Add(this.xtraTabControl1);
			this.Controls.Add(this.splitterControl1);
			this.Controls.Add(this.gridControl1);
			this.Name = "PermissionRoleForm";
			this.Text = "系统权限";
			this.Controls.SetChildIndex(this.gridControl1, 0);
			this.Controls.SetChildIndex(this.splitterControl1, 0);
			this.Controls.SetChildIndex(this.xtraTabControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsRole)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsAction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
			this.xtraTabControl1.ResumeLayout(false);
			this.tabAction.ResumeLayout(false);
			this.tabData.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.treOrg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsOrg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
			this.groupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAll.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDepts.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOnlySelf.Properties)).EndInit();
			this.tabUser.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bsUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDeleteUser)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraEditors.SplitterControl splitterControl1;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private System.Windows.Forms.BindingSource bsRole;
		private System.Windows.Forms.BindingSource bsAction;
		private DevExpress.XtraGrid.Columns.GridColumn colRoleName;
		private DevExpress.XtraGrid.Columns.GridColumn colRemark;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colAction;
		private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
		private DevExpress.XtraTab.XtraTabPage tabAction;
		private DevExpress.XtraTab.XtraTabPage tabUser;
		private DevExpress.XtraGrid.GridControl gridControl2;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
		private System.Windows.Forms.BindingSource bsUser;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpNo;
		private DevExpress.XtraGrid.Columns.GridColumn colEmpName;
		private DevExpress.XtraGrid.Columns.GridColumn colDeleteUser;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteUser;
		private DevExpress.XtraTab.XtraTabPage tabData;
		private System.Windows.Forms.BindingSource bsOrg;
		private DevExpress.XtraTreeList.TreeList treOrg;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colNo;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
		private DevExpress.XtraEditors.GroupControl groupControl1;
		private DevExpress.XtraEditors.CheckEdit chkAll;
		private DevExpress.XtraEditors.CheckEdit chkDepts;
		private DevExpress.XtraEditors.CheckEdit chkOnlySelf;
		private DevExpress.XtraGrid.Columns.GridColumn colRoleType;
		private DevExpress.XtraGrid.Columns.GridColumn colDataPermissionTypeName;
	}
}