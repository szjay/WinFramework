//==============================================================
//  版权所有：深圳杰文科技
//  文件名：AssemblyInfo.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]
[assembly: Guid("ea5bdbb3-97db-4440-b62f-769439caa6d1")]

[assembly: Framework.BaseUI.RootModule(Name = "系统管理", AnalyseModule = true, Order = 1)]
[assembly: Framework.BaseUI.RootModule(Name = "基础资料", AnalyseModule = true, Order = 2)]