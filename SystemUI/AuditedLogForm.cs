//==============================================================
//  版权所有：深圳杰文科技
//  文件名：AuditedLogForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using Framework.BaseUI;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;
using Infrastructure.Utilities;

namespace Framework.SystemUI
{
	//[Module(Name = "数据日志", RootModule = "系统管理")]
	public partial class AuditedLogForm : ToolbarBaseForm
	{
		public AuditedLogForm()
		{
			InitializeComponent();
			this.Load += AuditedLogForm_Load;
		}

		void AuditedLogForm_Load(object sender, EventArgs e)
		{
			this.SetStyle();
			dateSelector1.DateSelectorType = DateSelectorType.CurrentTenDays;
		}

		[Action(Name = "查询")]
		private void Query()
		{
			string where = new QueryBuilder()
				.Between(AuditedLog.F_CreatedOn, dateSelector1.StartDate, dateSelector1.EndDate)
				.Like(AuditedLog.F_CreatedBy, textEdit2.Text)
				.Like(AuditedLog.F_TableName, textEdit1.Text)
				.Like(AuditedLog.F_FieldName, textEdit3.Text)
				.Like(AuditedLog.F_Caption, textEdit4.Text)
				.ToString();
			List<AuditedLog> list = Rpc.Create<AuditedLogHandler>().Query(where);
			bindingSource1.DataSource = list;
			gridView1.BestFitColumns();
		}

		[Action(Name = "导出")]
		private void Export()
		{
			gridControl1.ExportGrid(this.Text);
		}
	}
}
