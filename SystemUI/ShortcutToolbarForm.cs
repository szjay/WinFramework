//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ShortcutToolbarForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Linq;
using System.Collections.Generic;
using Framework.DomainBase;
using Framework.HandlerBase;
using Framework.BaseUI;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	//[Module(Name = "快捷工具栏", RootModule = "系统管理", Pop = true, Fix = true)]
	public partial class ShortcutToolbarForm : ToolbarBaseForm
	{
		public ShortcutToolbarForm()
		{
			InitializeComponent();
			this.Load += ShortcutToolbarForm_Load;
		}

		void ShortcutToolbarForm_Load(object sender, EventArgs e)
		{
			gridControl1.SetStyle();
			gridControl1.Edit();

			RefreshData();

			btnUp.ButtonClick += btnUp_ButtonClick;
			btnDown.ButtonClick += btnDown_ButtonClick;
			btnDelete.ButtonClick += btnDelete_ButtonClick;
		}

		[Action(Name = "刷新", Permission = false)]
		private void RefreshData()
		{
			List<ShortcutToolbar> list = Rpc.Create<ShortcutToolbarHandler>().QueryByUser(ClientContext.User.Id);
			bindingSource1.DataSource = list;
		}

		[Action(Name = "保存", Permission = false)]
		private void Save()
		{
			this.EndEdit();
			RefreshSeqNo();

			List<ShortcutToolbar> list = bindingSource1.DataSource as List<ShortcutToolbar>;
			Rpc.Create<ShortcutToolbarHandler>().Save(list);
			MsgBox.Show("保存成功！\r\n重新登录之后工具栏生效");
		}

		private void RefreshSeqNo()
		{
			int n = 0;
			List<ShortcutToolbar> list = bindingSource1.DataSource as List<ShortcutToolbar>;
			foreach (var item in list.OrderBy(a => a.SeqNo))
			{
				item.SeqNo = ++n;
			}
		}

		void btnUp_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
		{
			ShortcutToolbar selected = (ShortcutToolbar)gridView1.GetFocusedRow();
			if (selected.SeqNo <= 1)
			{
				return;
			}

			selected.SeqNo -= 1;
			foreach (ShortcutToolbar item in bindingSource1.DataSource as List<ShortcutToolbar>)
			{
				if (selected.SeqNo == item.SeqNo && selected.Id != item.Id)
				{
					item.SeqNo += 1;
					break;
				}
			}

			gridControl1.RefreshDataSource();
		}

		void btnDown_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
		{
			ShortcutToolbar selected = (ShortcutToolbar)gridView1.GetFocusedRow();
			if (selected.SeqNo >= gridView1.RowCount)
			{
				return;
			}

			selected.SeqNo += 1;
			foreach (ShortcutToolbar item in bindingSource1.DataSource as List<ShortcutToolbar>)
			{
				if (selected.SeqNo == item.SeqNo && selected.Id != item.Id)
				{
					item.SeqNo -= 1;
					break;
				}
			}

			gridControl1.RefreshDataSource();
		}

		void btnDelete_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
		{
			gridView1.DeleteRow(gridView1.FocusedRowHandle);
			RefreshSeqNo();
		}

	}
}
