﻿namespace Framework.SystemUI
{
	partial class PrinterSettingForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bsPrinterSetting = new System.Windows.Forms.BindingSource(this.components);
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colReportName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colPrinter = new DevExpress.XtraGrid.Columns.GridColumn();
			this.cmbPrinter = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
			this.colPreview = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colMarginsBottom = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colMarginsLeft = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colMarginsRight = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colMarginsTop = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.bsPrinterSetting)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbPrinter)).BeginInit();
			this.SuspendLayout();
			// 
			// bsPrinterSetting
			// 
			this.bsPrinterSetting.DataSource = typeof(Framework.BaseUI.PrinterSetting);
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bsPrinterSetting;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cmbPrinter});
			this.gridControl1.Size = new System.Drawing.Size(730, 505);
			this.gridControl1.TabIndex = 9;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportName,
            this.colPrinter,
            this.colPreview,
            this.colMarginsTop,
            this.colMarginsBottom,
            this.colMarginsLeft,
            this.colMarginsRight});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsView.ShowGroupPanel = false;
			// 
			// colReportName
			// 
			this.colReportName.Caption = "报表名称";
			this.colReportName.FieldName = "ReportName";
			this.colReportName.Name = "colReportName";
			this.colReportName.OptionsColumn.ReadOnly = true;
			this.colReportName.Visible = true;
			this.colReportName.VisibleIndex = 0;
			this.colReportName.Width = 237;
			// 
			// colPrinter
			// 
			this.colPrinter.Caption = "打印机";
			this.colPrinter.ColumnEdit = this.cmbPrinter;
			this.colPrinter.FieldName = "Printer";
			this.colPrinter.Name = "colPrinter";
			this.colPrinter.Visible = true;
			this.colPrinter.VisibleIndex = 1;
			this.colPrinter.Width = 323;
			// 
			// cmbPrinter
			// 
			this.cmbPrinter.AutoHeight = false;
			this.cmbPrinter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.cmbPrinter.Name = "cmbPrinter";
			// 
			// colPreview
			// 
			this.colPreview.Caption = "报表预览";
			this.colPreview.FieldName = "Preview";
			this.colPreview.Name = "colPreview";
			this.colPreview.Visible = true;
			this.colPreview.VisibleIndex = 2;
			this.colPreview.Width = 152;
			// 
			// colMarginsBottom
			// 
			this.colMarginsBottom.Caption = "下边距";
			this.colMarginsBottom.FieldName = "MarginsBottom";
			this.colMarginsBottom.Name = "colMarginsBottom";
			this.colMarginsBottom.Visible = true;
			this.colMarginsBottom.VisibleIndex = 4;
			// 
			// colMarginsLeft
			// 
			this.colMarginsLeft.Caption = "左边距";
			this.colMarginsLeft.FieldName = "MarginsLeft";
			this.colMarginsLeft.Name = "colMarginsLeft";
			this.colMarginsLeft.Visible = true;
			this.colMarginsLeft.VisibleIndex = 5;
			// 
			// colMarginsRight
			// 
			this.colMarginsRight.Caption = "右边距";
			this.colMarginsRight.FieldName = "MarginsRight";
			this.colMarginsRight.Name = "colMarginsRight";
			this.colMarginsRight.Visible = true;
			this.colMarginsRight.VisibleIndex = 6;
			// 
			// colMarginsTop
			// 
			this.colMarginsTop.Caption = "上边距";
			this.colMarginsTop.FieldName = "MarginsTop";
			this.colMarginsTop.Name = "colMarginsTop";
			this.colMarginsTop.Visible = true;
			this.colMarginsTop.VisibleIndex = 3;
			// 
			// PrinterSettingForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(730, 536);
			this.Controls.Add(this.gridControl1);
			this.Name = "PrinterSettingForm";
			this.Text = "打印机设置";
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.bsPrinterSetting)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbPrinter)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bsPrinterSetting;
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn colReportName;
		private DevExpress.XtraGrid.Columns.GridColumn colPrinter;
		private DevExpress.XtraGrid.Columns.GridColumn colPreview;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cmbPrinter;
		private DevExpress.XtraGrid.Columns.GridColumn colMarginsTop;
		private DevExpress.XtraGrid.Columns.GridColumn colMarginsBottom;
		private DevExpress.XtraGrid.Columns.GridColumn colMarginsLeft;
		private DevExpress.XtraGrid.Columns.GridColumn colMarginsRight;
	}
}