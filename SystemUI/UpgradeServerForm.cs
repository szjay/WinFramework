//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeServerForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	[Module(Name = "服务器升级", RootModule = "系统管理", SystemModule = true, Pop = true)]
	public partial class UpgradeServerForm : ToolbarBaseForm
	{
		public UpgradeServerForm()
		{
			InitializeComponent();
			this.Load += UpgradeServerForm_Load;
		}

		void UpgradeServerForm_Load(object sender, EventArgs e)
		{
		}

		[Action(Name = "选择文件", Image = Toolbar.open)]
		private void SelectFiles()
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "*.*|*.*";
			dialog.Multiselect = true;
			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			foreach (string file in dialog.FileNames)
			{
				listBoxControl1.Items.Add(file);
			}
		}

		[Action(Name = "提交文件", Image = "Save")]
		private void Upgrade()
		{
			if (listBoxControl1.Items.Count == 0)
			{
				MsgBox.Show("请选择升级文件");
				return;
			}

			List<UpgradeFile> upgradeFileList = new List<UpgradeFile>();

			foreach (string fileName in listBoxControl1.Items)
			{
				UpgradeFile file = new UpgradeFile()
				{
					FileName = Path.GetFileName(fileName),
					FullPath = fileName,
					Content = File.ReadAllBytes(fileName),
					IsLoaded = true
				};
				upgradeFileList.Add(file);
			}

			UpgradeData data = new UpgradeData()
			{
				VersionNo = long.Parse(ClientContext.Now.ToString("yyyyMMddHHmmss")),
				UpgradeFileList = upgradeFileList,
				//UpgradeLog = txtLog.Text.Trim()
			};

			Rpc.Create<SessionHandler>().Test();

			Rpc.Create<UpgradeHandler>().UploadServer(data);
			MsgBox.Show("提交成功，系统服务将重启...");

			this.DialogResult = DialogResult.OK;
		}

		[Action("保存Session", Toolbar.save)]
		private void SaveSeesion()
		{
			int sessionCnt = Rpc.Create<SessionHandler>().Test();
			MsgBox.Show($"保存Session成功，Session数量 {sessionCnt}");
		}
	}
}
