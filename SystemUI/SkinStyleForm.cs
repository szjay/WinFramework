//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SkinStyleForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using Framework.BaseUI;
using Framework.HandlerBase;
using Framework.RPC;

namespace Framework.SystemUI
{
	[Module(Name = "系统风格", RootModule = "系统管理", Pop = true)]
	public partial class SkinStyleForm : ToolbarBaseForm
	{
		public SkinStyleForm()
		{
			InitializeComponent();
			this.Load += SkinStyleForm_Load;
		}

		void SkinStyleForm_Load(object sender, EventArgs e)
		{
			fontEdit1.Text = ClientContext.FontName;
			spinEdit1.Value = ClientContext.FontSize;
		}

		[Action(Name = "还原默认", Image = "RefreshData")]
		private void Reset()
		{
			simpleButton2.PerformClick();
			fontEdit1.Text = DomainBase.SkinStyle.FONT_NAME;
			spinEdit1.Value = DomainBase.SkinStyle.FONT_SIZE;
			OK();
		}

		private void ChangeStyle(object sender, EventArgs e)
		{
			SimpleButton btn = sender as SimpleButton;
			UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;
			UserLookAndFeel.Default.SkinName = btn.Tag.ToString();
			ClientContext.SkinName = btn.Tag.ToString();

			Rpc.Create<SkinStyleHandler>().ChangeSkin(ClientContext.SkinName);
		}

		private void ChangeFont()
		{
			this.EndEdit();
			AppearanceObject.DefaultFont = new Font(fontEdit1.Text, (float)spinEdit1.Value);
			Rpc.Create<SkinStyleHandler>().ChangeFont(AppearanceObject.DefaultFont.Name, (int)AppearanceObject.DefaultFont.Size);
		}

		[Action(Name = "确定")]
		public void OK()
		{
			this.EndEdit();
			ChangeFont();
			this.DialogResult = DialogResult.OK;
		}

	}
}
