//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeClientForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Framework.BaseUI;
using Framework.HandlerBase;
using Framework.RPC;
using Infrastructure.Components;

namespace Framework.SystemUI
{
	[Module(Name = "客户端升级", RootModule = "系统管理", SystemModule = true, Pop = true)]
	public partial class UpgradeClientForm : ToolbarBaseForm
	{
		public UpgradeClientForm()
		{
			InitializeComponent();
			this.Load += UpgradeForm_Load;
		}

		void UpgradeForm_Load(object sender, EventArgs e)
		{
			txtLog.Text = Rpc.Create<UpgradeHandler>().GetUpgradeLog();
		}

		[Action(Name = "选择文件", Image = Toolbar.open)]
		private void SelectFiles()
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "*.*|*.*";
			dialog.Multiselect = true;
			if (dialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}

			foreach (string file in dialog.FileNames)
			{
				listBoxControl1.Items.Add(file);
			}
		}

		//[Action(Name = "选择文件夹", Image = Toolbar.open)]
		//private void SelectDirectory()
		//{
		// 文件夹建议手工到服务器上升级，按照指定格式创建升级文件夹，把所有文件和子文件夹放到升级文件夹中即可。
		//}

		[Action(Name = "提交文件", Image = "Save")]
		private void Upgrade()
		{
			if (listBoxControl1.Items.Count == 0)
			{
				MsgBox.Show("请选择升级文件");
				return;
			}

			List<UpgradeFile> upgradeFileList = new List<UpgradeFile>();

			foreach (string fileName in listBoxControl1.Items)
			{
				UpgradeFile file = new UpgradeFile()
				{
					FileName = Path.GetFileName(fileName),
					FullPath = fileName,
					Content = File.ReadAllBytes(fileName),
					IsLoaded = true
				};
				upgradeFileList.Add(file);
			}

			UpgradeData data = new UpgradeData()
			{
				VersionNo = long.Parse(ClientContext.Now.ToString("yyyyMMddHHmmss")),
				UpgradeFileList = upgradeFileList,
				UpgradeLog = txtLog.Text.Trim()
			};

			Rpc.Create<UpgradeHandler>().UploadClient(data);
			MsgBox.Show("提交成功，系统重新登录将会自动升级");

			this.DialogResult = DialogResult.OK;
		}

	}
}
