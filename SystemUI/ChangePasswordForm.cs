//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ChangePasswordForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using Framework.HandlerBase;
using Framework.BaseUI;
using Framework.RPC;
using Infrastructure.Components;
using Infrastructure.Utilities;

namespace Framework.SystemUI
{
	[Module(Name = "修改密码", RootModule = "系统管理", Fix = true, Pop = true)]
	public partial class ChangePasswordForm : ToolbarBaseForm
	{
		public ChangePasswordForm()
		{
			InitializeComponent();
		}

		[Action(Name = "确定", Permission = false)]
		private void OK()
		{
			Rpc.Create<LoginHandler>().Open();

			if (!new DataValidator()
				.Check(string.IsNullOrEmpty(txtOldPwd.Text), "请填写原密码")
				.Check(string.IsNullOrEmpty(txtNewPwd1.Text), "请填写新密码")
				.Check(string.IsNullOrEmpty(txtNewPwd2.Text), "请确认新密码")
				.ShowMsgIfFailed()
				.Pass)
			{
				return;
			}

			Rpc.Create<LoginHandler>().ChangePassword(txtOldPwd.Text, txtNewPwd1.Text, txtNewPwd2.Text);
			MsgBox.Show("密码修改成功，下次登录请使用新密码");
			this.DialogResult = DialogResult.OK;
		}
	}
}
