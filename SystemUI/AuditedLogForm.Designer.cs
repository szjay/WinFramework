﻿using Framework.DomainBase;
namespace Framework.SystemUI
{
	partial class AuditedLogForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.dateSelector1 = new Infrastructure.Components.DateSelector(this.components);
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colCaption = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colOldValue = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colNewValue = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colCreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colHostName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colStateName = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
			this.groupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dateSelector1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// groupControl1
			// 
			this.groupControl1.Controls.Add(this.layoutControl1);
			this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupControl1.Location = new System.Drawing.Point(0, 31);
			this.groupControl1.Name = "groupControl1";
			this.groupControl1.Size = new System.Drawing.Size(844, 98);
			this.groupControl1.TabIndex = 9;
			this.groupControl1.Text = "查询条件";
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.textEdit4);
			this.layoutControl1.Controls.Add(this.textEdit3);
			this.layoutControl1.Controls.Add(this.textEdit2);
			this.layoutControl1.Controls.Add(this.textEdit1);
			this.layoutControl1.Controls.Add(this.dateSelector1);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(2, 22);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(840, 74);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// textEdit4
			// 
			this.textEdit4.Location = new System.Drawing.Point(63, 38);
			this.textEdit4.Name = "textEdit4";
			this.textEdit4.Size = new System.Drawing.Size(354, 22);
			this.textEdit4.StyleController = this.layoutControl1;
			this.textEdit4.TabIndex = 8;
			// 
			// textEdit3
			// 
			this.textEdit3.Location = new System.Drawing.Point(677, 12);
			this.textEdit3.Name = "textEdit3";
			this.textEdit3.Size = new System.Drawing.Size(151, 22);
			this.textEdit3.StyleController = this.layoutControl1;
			this.textEdit3.TabIndex = 7;
			// 
			// textEdit2
			// 
			this.textEdit2.Location = new System.Drawing.Point(268, 12);
			this.textEdit2.Name = "textEdit2";
			this.textEdit2.Size = new System.Drawing.Size(150, 22);
			this.textEdit2.StyleController = this.layoutControl1;
			this.textEdit2.TabIndex = 6;
			// 
			// textEdit1
			// 
			this.textEdit1.Location = new System.Drawing.Point(473, 12);
			this.textEdit1.Name = "textEdit1";
			this.textEdit1.Size = new System.Drawing.Size(149, 22);
			this.textEdit1.StyleController = this.layoutControl1;
			this.textEdit1.TabIndex = 5;
			// 
			// dateSelector1
			// 
			this.dateSelector1.DateSelectorType = Infrastructure.Components.DateSelectorType.All;
			this.dateSelector1.EditValue = "";
			this.dateSelector1.Location = new System.Drawing.Point(63, 12);
			this.dateSelector1.Name = "dateSelector1";
			this.dateSelector1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.dateSelector1.Size = new System.Drawing.Size(150, 22);
			this.dateSelector1.StyleController = this.layoutControl1;
			this.dateSelector1.TabIndex = 4;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(840, 74);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.dateSelector1;
			this.layoutControlItem1.CustomizationFormText = "操作时间";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(205, 26);
			this.layoutControlItem1.Text = "操作时间";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.textEdit1;
			this.layoutControlItem2.CustomizationFormText = "表名";
			this.layoutControlItem2.Location = new System.Drawing.Point(410, 0);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(204, 26);
			this.layoutControlItem2.Text = "表名";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.textEdit2;
			this.layoutControlItem3.CustomizationFormText = "操作人";
			this.layoutControlItem3.Location = new System.Drawing.Point(205, 0);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(205, 26);
			this.layoutControlItem3.Text = "操作人";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.textEdit3;
			this.layoutControlItem4.CustomizationFormText = "字段名";
			this.layoutControlItem4.Location = new System.Drawing.Point(614, 0);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(206, 26);
			this.layoutControlItem4.Text = "字段名";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.textEdit4;
			this.layoutControlItem5.CustomizationFormText = "更新类型";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(409, 28);
			this.layoutControlItem5.Text = "标题";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(48, 14);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.AllowHotTrack = false;
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(409, 26);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(411, 28);
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.DomainBase.AuditedLog);
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bindingSource1;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 129);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(844, 293);
			this.gridControl1.TabIndex = 10;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTableName,
            this.colCaption,
            this.colFieldName,
            this.colOldValue,
            this.colNewValue,
            this.colCreatedBy,
            this.colCreatedOn,
            this.colHostName,
            this.colStateName});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			// 
			// colTableName
			// 
			this.colTableName.Caption = "表名";
			this.colTableName.FieldName = "TableName";
			this.colTableName.Name = "colTableName";
			this.colTableName.Visible = true;
			this.colTableName.VisibleIndex = 1;
			// 
			// colCaption
			// 
			this.colCaption.Caption = "标题";
			this.colCaption.FieldName = "Caption";
			this.colCaption.Name = "colCaption";
			this.colCaption.Visible = true;
			this.colCaption.VisibleIndex = 0;
			// 
			// colFieldName
			// 
			this.colFieldName.Caption = "字段名";
			this.colFieldName.FieldName = "FieldName";
			this.colFieldName.Name = "colFieldName";
			this.colFieldName.Visible = true;
			this.colFieldName.VisibleIndex = 2;
			// 
			// colOldValue
			// 
			this.colOldValue.Caption = "原值";
			this.colOldValue.FieldName = "OldValue";
			this.colOldValue.Name = "colOldValue";
			this.colOldValue.Visible = true;
			this.colOldValue.VisibleIndex = 3;
			// 
			// colNewValue
			// 
			this.colNewValue.Caption = "新值";
			this.colNewValue.FieldName = "NewValue";
			this.colNewValue.Name = "colNewValue";
			this.colNewValue.Visible = true;
			this.colNewValue.VisibleIndex = 4;
			// 
			// colCreatedBy
			// 
			this.colCreatedBy.Caption = "操作人";
			this.colCreatedBy.FieldName = "CreatedBy";
			this.colCreatedBy.Name = "colCreatedBy";
			this.colCreatedBy.Visible = true;
			this.colCreatedBy.VisibleIndex = 5;
			// 
			// colCreatedOn
			// 
			this.colCreatedOn.Caption = "操作时间";
			this.colCreatedOn.FieldName = "CreatedOn";
			this.colCreatedOn.Name = "colCreatedOn";
			this.colCreatedOn.Visible = true;
			this.colCreatedOn.VisibleIndex = 6;
			// 
			// colHostName
			// 
			this.colHostName.Caption = "操作主机";
			this.colHostName.FieldName = "HostName";
			this.colHostName.Name = "colHostName";
			this.colHostName.Visible = true;
			this.colHostName.VisibleIndex = 7;
			// 
			// colStateName
			// 
			this.colStateName.Caption = "更新类型";
			this.colStateName.FieldName = "StateName";
			this.colStateName.Name = "colStateName";
			this.colStateName.OptionsColumn.ReadOnly = true;
			this.colStateName.Visible = true;
			this.colStateName.VisibleIndex = 8;
			// 
			// AuditedLogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(844, 422);
			this.Controls.Add(this.gridControl1);
			this.Controls.Add(this.groupControl1);
			this.Name = "AuditedLogForm";
			this.Text = "数据日志";
			this.Controls.SetChildIndex(this.groupControl1, 0);
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
			this.groupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dateSelector1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraEditors.GroupControl groupControl1;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.TextEdit textEdit3;
		private DevExpress.XtraEditors.TextEdit textEdit2;
		private DevExpress.XtraEditors.TextEdit textEdit1;
		private Infrastructure.Components.DateSelector dateSelector1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraEditors.TextEdit textEdit4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn colTableName;
		private DevExpress.XtraGrid.Columns.GridColumn colCaption;
		private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
		private DevExpress.XtraGrid.Columns.GridColumn colOldValue;
		private DevExpress.XtraGrid.Columns.GridColumn colNewValue;
		private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
		private DevExpress.XtraGrid.Columns.GridColumn colCreatedOn;
		private DevExpress.XtraGrid.Columns.GridColumn colHostName;
		private DevExpress.XtraGrid.Columns.GridColumn colStateName;
	}
}