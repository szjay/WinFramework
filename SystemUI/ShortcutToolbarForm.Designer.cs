﻿namespace Framework.SystemUI
{
	partial class ShortcutToolbarForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.colSeqNo = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colModule = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colUp = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnUp = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.colDown = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnDown = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.colDelete = new DevExpress.XtraGrid.Columns.GridColumn();
			this.btnDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// gridControl1
			// 
			this.gridControl1.DataSource = this.bindingSource1;
			this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 31);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnUp,
            this.btnDown,
            this.btnDelete});
			this.gridControl1.Size = new System.Drawing.Size(384, 331);
			this.gridControl1.TabIndex = 9;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// bindingSource1
			// 
			this.bindingSource1.DataSource = typeof(Framework.DomainBase.ShortcutToolbar);
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSeqNo,
            this.colModule,
            this.colUp,
            this.colDown,
            this.colDelete});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSeqNo, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// colSeqNo
			// 
			this.colSeqNo.FieldName = "SeqNo";
			this.colSeqNo.Name = "colSeqNo";
			// 
			// colModule
			// 
			this.colModule.Caption = "模块";
			this.colModule.FieldName = "Module";
			this.colModule.Name = "colModule";
			this.colModule.OptionsColumn.AllowEdit = false;
			this.colModule.OptionsColumn.ReadOnly = true;
			this.colModule.Visible = true;
			this.colModule.VisibleIndex = 0;
			this.colModule.Width = 237;
			// 
			// colUp
			// 
			this.colUp.Caption = "向上";
			this.colUp.ColumnEdit = this.btnUp;
			this.colUp.Name = "colUp";
			this.colUp.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
			this.colUp.Visible = true;
			this.colUp.VisibleIndex = 1;
			this.colUp.Width = 44;
			// 
			// btnUp
			// 
			this.btnUp.AutoHeight = false;
			this.btnUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up)});
			this.btnUp.Name = "btnUp";
			this.btnUp.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// colDown
			// 
			this.colDown.Caption = "向下";
			this.colDown.ColumnEdit = this.btnDown;
			this.colDown.Name = "colDown";
			this.colDown.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
			this.colDown.Visible = true;
			this.colDown.VisibleIndex = 2;
			this.colDown.Width = 47;
			// 
			// btnDown
			// 
			this.btnDown.AutoHeight = false;
			this.btnDown.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
			this.btnDown.Name = "btnDown";
			this.btnDown.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// colDelete
			// 
			this.colDelete.Caption = "删除";
			this.colDelete.ColumnEdit = this.btnDelete;
			this.colDelete.Name = "colDelete";
			this.colDelete.Visible = true;
			this.colDelete.VisibleIndex = 3;
			this.colDelete.Width = 38;
			// 
			// btnDelete
			// 
			this.btnDelete.AutoHeight = false;
			this.btnDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			// 
			// ShortcutToolbarForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 362);
			this.Controls.Add(this.gridControl1);
			this.Name = "ShortcutToolbarForm";
			this.Text = "快捷工具栏";
			this.Controls.SetChildIndex(this.gridControl1, 0);
			((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private System.Windows.Forms.BindingSource bindingSource1;
		private DevExpress.XtraGrid.Columns.GridColumn colSeqNo;
		private DevExpress.XtraGrid.Columns.GridColumn colModule;
		private DevExpress.XtraGrid.Columns.GridColumn colUp;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnUp;
		private DevExpress.XtraGrid.Columns.GridColumn colDown;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDown;
		private DevExpress.XtraGrid.Columns.GridColumn colDelete;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDelete;
	}
}