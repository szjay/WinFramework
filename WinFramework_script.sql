USE [Platform]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [uniqueidentifier] NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[Tel1] [nvarchar](50) NULL,
	[Tel2] [nvarchar](50) NULL,
	[Tel3] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PostCode] [nvarchar](50) NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dept]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dept](
	[DeptId] [uniqueidentifier] NOT NULL,
	[CompanyId] [uniqueidentifier] NOT NULL,
	[DeptNo] [nvarchar](50) NOT NULL,
	[DeptName] [nvarchar](50) NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[DeptTypeId] [int] NULL,
	[Tel] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_Dept] PRIMARY KEY CLUSTERED 
(
	[DeptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeptGroup]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeptGroup](
	[GroupId] [uniqueidentifier] NOT NULL,
	[DeptId] [uniqueidentifier] NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_DeptGroup] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeptGroupEmployee]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeptGroupEmployee](
	[Id] [uniqueidentifier] NOT NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[EmpId] [uniqueidentifier] NOT NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_DeptGroupEmployee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmpId] [uniqueidentifier] NOT NULL,
	[EmpNo] [nvarchar](50) NOT NULL,
	[EmpName] [nvarchar](50) NOT NULL,
	[Sex] [nvarchar](50) NULL,
	[DeptId] [uniqueidentifier] NOT NULL,
	[Tel] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[IdCard] [nvarchar](50) NULL,
	[Headship] [nvarchar](50) NULL,
	[AllowLogin] [bit] NOT NULL,
	[OnJob] [bit] NOT NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeePosition]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeePosition](
	[Id] [uniqueidentifier] NOT NULL,
	[EmpId] [uniqueidentifier] NOT NULL,
	[PositionId] [uniqueidentifier] NOT NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_EmployeePosition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionAction]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionAction](
	[ActionId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_PermissionAction] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionData]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionData](
	[DataId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[Data] [nvarchar](50) NOT NULL,
	[DataType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PermissionData] PRIMARY KEY CLUSTERED 
(
	[DataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionRole]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRole](
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[RoleType] [nvarchar](50) NOT NULL,
	[DataPermissionTypeId] [int] NOT NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_PermissionRole] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionRoleUser]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRoleUser](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_PermissionRoleUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Position]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[PositionId] [uniqueidentifier] NOT NULL,
	[PositionNo] [int] NOT NULL,
	[PositionName] [nvarchar](50) NOT NULL,
	[Enable] [bit] NOT NULL,
	[Remark] [nvarchar](500) NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PubCode]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PubCode](
	[CodeId] [uniqueidentifier] NOT NULL,
	[CodeType] [nvarchar](50) NOT NULL,
	[SeqNo] [int] NULL,
	[CodeName] [nvarchar](50) NOT NULL,
	[Remark] [nvarchar](50) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_SystemCode] PRIMARY KEY CLUSTERED 
(
	[CodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShortcutToolbar]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShortcutToolbar](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[SeqNo] [int] NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_ShortcutToolbar] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SkinStyle]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SkinStyle](
	[StyleId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[StyleName] [nvarchar](50) NULL,
	[FontName] [nvarchar](50) NULL,
	[FontSize] [int] NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_SkinStyle] PRIMARY KEY CLUSTERED 
(
	[StyleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SysNumber]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysNumber](
	[NumberId] [uniqueidentifier] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Number] [int] NOT NULL,
 CONSTRAINT [PK_SysNumber] PRIMARY KEY CLUSTERED 
(
	[NumberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SysUser]    Script Date: 2019-5-14 19:41:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUser](
	[UserId] [uniqueidentifier] NOT NULL,
	[UserCardNo] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Enable] [bit] NOT NULL,
	[Account] [nvarchar](50) NULL,
	[Password] [nvarchar](500) NULL,
	[LoginCount] [int] NOT NULL,
	[LastLoginTime] [datetime] NULL,
	[Remark] [nvarchar](500) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [Tel1], [Tel2], [Tel3], [Fax], [Address], [PostCode], [Remark], [Version]) VALUES (N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'杰文科技', NULL, NULL, NULL, NULL, NULL, NULL, N'xxxxxxxxxxxxx', CAST(0x00000000000007D2 AS DateTime))
INSERT [dbo].[Dept] ([DeptId], [CompanyId], [DeptNo], [DeptName], [ParentId], [DeptTypeId], [Tel], [Fax], [Remark], [Version]) VALUES (N'f7e41912-836c-4194-a685-7f0e593d97c8', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'0101', N'子部门0101', N'a3844373-c3fa-4c89-ab85-b6f59153664d', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dept] ([DeptId], [CompanyId], [DeptNo], [DeptName], [ParentId], [DeptTypeId], [Tel], [Fax], [Remark], [Version]) VALUES (N'68ab3fb3-7fd3-4b55-971c-92e61e6c47d2', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'0102', N'子部门02', N'a3844373-c3fa-4c89-ab85-b6f59153664d', 1, NULL, NULL, N'sfsf', NULL)
INSERT [dbo].[Dept] ([DeptId], [CompanyId], [DeptNo], [DeptName], [ParentId], [DeptTypeId], [Tel], [Fax], [Remark], [Version]) VALUES (N'a3844373-c3fa-4c89-ab85-b6f59153664d', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'01', N'总部', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dept] ([DeptId], [CompanyId], [DeptNo], [DeptName], [ParentId], [DeptTypeId], [Tel], [Fax], [Remark], [Version]) VALUES (N'bd90c21d-2e1b-435b-97ee-c404768306f1', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'1', N'2', N'65f28f95-1e3f-4197-9aea-cdad7b1084cc', 1, NULL, NULL, N'3', NULL)
INSERT [dbo].[Dept] ([DeptId], [CompanyId], [DeptNo], [DeptName], [ParentId], [DeptTypeId], [Tel], [Fax], [Remark], [Version]) VALUES (N'65f28f95-1e3f-4197-9aea-cdad7b1084cc', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'02', N'分厂一', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[DeptGroup] ([GroupId], [DeptId], [GroupName], [Remark], [Version]) VALUES (N'97af672f-0485-460c-b7df-0944c1007ade', N'f7e41912-836c-4194-a685-7f0e593d97c8', N'vvvvvvvvvvvvv', N'sssssssssssssddssssssssss', NULL)
INSERT [dbo].[DeptGroup] ([GroupId], [DeptId], [GroupName], [Remark], [Version]) VALUES (N'dad8ddd6-d99c-41f4-ba5e-daef9d1d8837', N'f7e41912-836c-4194-a685-7f0e593d97c8', N'cccccccccccc', NULL, NULL)
INSERT [dbo].[DeptGroup] ([GroupId], [DeptId], [GroupName], [Remark], [Version]) VALUES (N'e8aae9ac-1ea5-4406-b1ef-e6ccfcad5634', N'f7e41912-836c-4194-a685-7f0e593d97c8', N'xxxxxxxxxx', NULL, NULL)
INSERT [dbo].[Employee] ([EmpId], [EmpNo], [EmpName], [Sex], [DeptId], [Tel], [Address], [IdCard], [Headship], [AllowLogin], [OnJob], [Remark], [Version]) VALUES (N'0642e134-01dc-4ab3-8d66-41c8b9d11487', N'aacc', N'aa', NULL, N'a3844373-c3fa-4c89-ab85-b6f59153664d', NULL, NULL, NULL, NULL, 1, 1, NULL, CAST(0x0000A8040179F557 AS DateTime))
INSERT [dbo].[Employee] ([EmpId], [EmpNo], [EmpName], [Sex], [DeptId], [Tel], [Address], [IdCard], [Headship], [AllowLogin], [OnJob], [Remark], [Version]) VALUES (N'e78af70c-4359-4b90-b178-9b041bb075f6', N'34', N'a', N'女', N'f7e41912-836c-4194-a685-7f0e593d97c8', NULL, NULL, NULL, NULL, 0, 1, N'1', CAST(0x0000A8040179F557 AS DateTime))
INSERT [dbo].[Employee] ([EmpId], [EmpNo], [EmpName], [Sex], [DeptId], [Tel], [Address], [IdCard], [Headship], [AllowLogin], [OnJob], [Remark], [Version]) VALUES (N'140ab1f9-2051-4c24-8c58-a0f7603c404d', N'b', N'b', N'男', N'f7e41912-836c-4194-a685-7f0e593d97c8', NULL, NULL, NULL, NULL, 1, 1, N'55555555555', CAST(0x0000A8040179F557 AS DateTime))
INSERT [dbo].[Employee] ([EmpId], [EmpNo], [EmpName], [Sex], [DeptId], [Tel], [Address], [IdCard], [Headship], [AllowLogin], [OnJob], [Remark], [Version]) VALUES (N'79346735-8410-47fc-9d74-a900b4a0cbcf', N'22', N'22', N'男', N'68ab3fb3-7fd3-4b55-971c-92e61e6c47d2', N'aaaaaa', N'bbbbbbbb', N'ccccccccc', NULL, 0, 1, N'1', CAST(0x0000A8040179F557 AS DateTime))
INSERT [dbo].[Employee] ([EmpId], [EmpNo], [EmpName], [Sex], [DeptId], [Tel], [Address], [IdCard], [Headship], [AllowLogin], [OnJob], [Remark], [Version]) VALUES (N'54bc153f-50f8-4af8-8fcb-e511faf997a8', N'a', N'a', N'男', N'a3844373-c3fa-4c89-ab85-b6f59153664d', NULL, NULL, NULL, NULL, 1, 1, N'1', CAST(0x0000A8040179F557 AS DateTime))
INSERT [dbo].[EmployeePosition] ([Id], [EmpId], [PositionId], [Version]) VALUES (N'97ceb055-92c8-429d-9c9f-1972570053a2', N'79346735-8410-47fc-9d74-a900b4a0cbcf', N'caeac100-b136-4cfd-b41b-33f0dc3bde93', NULL)
INSERT [dbo].[EmployeePosition] ([Id], [EmpId], [PositionId], [Version]) VALUES (N'a734bc32-c617-4c64-806b-1b8aa0f5b5e0', N'79346735-8410-47fc-9d74-a900b4a0cbcf', N'9405485e-421e-494d-8737-bf41b001eb48', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'fd05c47a-aceb-4e30-8ca5-012cf966ed0f', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.在线用户.Kick Out', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'6462f509-342d-4752-8742-01771f8a9729', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.数据日志.查询', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'cb91f8fa-a223-49cb-95a0-112f86f5e418', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.浏览员工', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'c3cdc37e-fab8-4af5-ac5b-193b252fe615', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.在线用户.刷新', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'a3a0faf0-2928-4cde-8e3e-1a28f2c224a0', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.修改小组', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'e2a8edf5-fc92-401d-b970-1b020221d67d', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统权限.新增角色', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'37ccd5b3-faf4-4aa9-90a8-1ee4d8a7f321', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.客户端升级.选择文件', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'ef8469e4-6928-4c10-a067-22292af409a4', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.在线用户.Kick All', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'ef709274-871f-4439-9654-237c45b86da8', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.在线用户.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'019c2730-4c6d-41aa-842a-3749d4623749', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.修改员工', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'8e754bc1-abc6-4ae3-a13b-3e02f66d6808', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.服务器升级.提交文件', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'd70cd6b1-cfa8-4576-b026-4315cfada45c', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.新增小组', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'a5f64378-b231-4f69-9d42-4a2cdccd62ab', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.打印机设置.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'db064180-722f-4bc3-bd45-4aa3a2481bf3', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.客户端升级.提交文件', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'aa4320ad-6653-459c-8a56-6b9da401b602', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.客户端升级.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'e4847465-9909-4098-865a-6bb753c429ca', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统风格.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'aa9adf4f-5f46-4994-b395-6eaeded9c9b6', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.本机配置.保存', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'67005ac6-2139-4cb3-ab23-7a1cf0ecd9ab', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.修改部门', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'80bb2137-de8d-4990-a52a-831b397eb49d', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.新增部门', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'70e4622d-f404-4e7e-a747-8d28591e737f', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.服务器升级.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'21dc4b77-f79f-4f97-864b-8e38430d9c88', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.在线用户.导出', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'43c23092-bcd0-4e6c-b78f-929ae5081b9f', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.打印员工卡', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'915e4897-be5e-4471-ad84-a977a7e73981', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.数据日志.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'84b06cb8-c789-4962-917c-a99ac9c96264', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.修改单位', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'c894c006-0633-4f5e-8e0e-ac669cc6af98', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.数据日志.导出', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'e459b1ab-542b-49d4-b763-b4adea5c28b6', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.删除小组', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'f7bc8a0a-d110-44f4-962b-b4f637458b2a', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统权限.修改角色', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'6e52094d-3710-4526-bc77-b7b85e3eadca', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统风格.确定', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'16e9aa93-08fd-4b37-815a-b7df3c23ec23', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'401ce753-47e3-40e0-9ec9-bc951c225d14', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统风格.还原默认', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'5b5bafd1-9d8b-4032-9720-c5fa7b133bae', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.新增员工', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'525ce012-4171-4af8-8bd1-cfaa67a5927b', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.删除部门', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'b716f642-f6a7-4982-bc96-db8e051e5dd7', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.本机配置.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'7f877bed-8927-40f1-b4f5-e96ba5b6e080', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.系统权限.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'fd972bc3-50b4-4c53-aaa8-ee53110a9035', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.公共代码.运行', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'9049d6ff-6baf-4e67-8be6-f5d6619df771', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.服务器升级.选择文件', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'0a283f39-744e-41a0-938a-f64337624844', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.打印机设置.保存', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'1d9176e0-e143-4ce3-8858-faa9fc28f295', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'基础资料.组织结构.导入员工', NULL)
INSERT [dbo].[PermissionAction] ([ActionId], [RoleId], [Action], [Version]) VALUES (N'798951aa-7bb9-4e45-9b35-faca99140e8f', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'系统管理.服务器升级.测试Session', NULL)
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'627d54e7-67e8-4f20-8e74-054de6b8dab0', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'bd90c21d-2e1b-435b-97ee-c404768306f1', N'部门')
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'8beb7b88-bfc9-42a8-8f64-20e3242e5ae7', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'6155fc96-3526-4af6-a15a-0ffbf6bce706', N'部门')
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'ef350e3e-04f4-48ab-8ebe-4cf0d3c70836', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'65f28f95-1e3f-4197-9aea-cdad7b1084cc', N'部门')
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'71736c0b-08ab-4641-a2fe-55ca83965aea', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'68ab3fb3-7fd3-4b55-971c-92e61e6c47d2', N'部门')
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'8ee55947-9bbf-4488-bf07-587ecac1fe0c', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'f7e41912-836c-4194-a685-7f0e593d97c8', N'部门')
INSERT [dbo].[PermissionData] ([DataId], [RoleId], [Data], [DataType]) VALUES (N'77a68122-62ed-4573-a04c-a8daca95da5f', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'a3844373-c3fa-4c89-ab85-b6f59153664d', N'部门')
INSERT [dbo].[PermissionRole] ([RoleId], [RoleName], [RoleType], [DataPermissionTypeId], [Remark], [Version]) VALUES (N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'x', N'0', 0, NULL, NULL)
INSERT [dbo].[PermissionRoleUser] ([Id], [RoleId], [UserId], [Version]) VALUES (N'4c096179-e882-47b9-b108-a6f93e0530e7', N'74e24a6b-de64-4dc5-8490-a9a724b1afa0', N'140ab1f9-2051-4c24-8c58-a0f7603c404d', NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'4e6d8f33-2a63-46f7-a5e0-0692948dd2d3', 5, N'出纳', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'8435ea78-4ec5-4307-b893-2941cf8feb19', 7, N'司机', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'caeac100-b136-4cfd-b41b-33f0dc3bde93', 1, N'业务员', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'bde928e6-8cca-4f14-99ba-3f870852f73c', 9, N'总监', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'c58a6340-682b-4bc3-b35f-67f4713bc397', 3, N'仓管', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'cad2a7d1-5f75-4e22-bf29-8534b347cae1', 4, N'采购员', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'd082da99-ea80-49a0-b619-8d614ce7624c', 8, N'经理', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'9405485e-421e-494d-8737-bf41b001eb48', 2, N'收款员', 1, NULL)
INSERT [dbo].[Position] ([PositionId], [PositionNo], [PositionName], [Enable], [Remark]) VALUES (N'b965b51e-22ca-4925-8553-c251ea2d37cb', 6, N'会计', 1, NULL)
INSERT [dbo].[PubCode] ([CodeId], [CodeType], [SeqNo], [CodeName], [Remark], [Version]) VALUES (N'505fe774-81cd-460f-9f4c-9db4889a7a7c', N'TEST', 2, N'测试', N'备注2', NULL)
INSERT [dbo].[SkinStyle] ([StyleId], [UserId], [StyleName], [FontName], [FontSize], [Version]) VALUES (N'42300bc8-fb32-4239-9bc8-eb2aa756451b', N'a7222b59-25ef-4810-bc39-ca9c3013fd0c', N'DevExpress Style', N'Tahoma', 12, NULL)
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'0642e134-01dc-4ab3-8d66-41c8b9d11487', N'', N'aa', 1, N'aacc', N'202CB962AC59075B964B07152D234B70', 0, NULL, NULL, NULL)
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'e78af70c-4359-4b90-b178-9b041bb075f6', NULL, N'a', 0, N'34', N'202CB962AC59075B964B07152D234B70', 0, NULL, NULL, NULL)
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'140ab1f9-2051-4c24-8c58-a0f7603c404d', N'', N'b', 1, N'b', N'202CB962AC59075B964B07152D234B70', 5, CAST(0x0000A7E200C4B524 AS DateTime), NULL, NULL)
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'79346735-8410-47fc-9d74-a900b4a0cbcf', N'eeeeeeee', N'22', 0, N'22', N'202CB962AC59075B964B07152D234B70', 0, NULL, NULL, NULL)
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'a7222b59-25ef-4810-bc39-ca9c3013fd0c', N'admin', N'管理员', 1, N'admin', N'202CB962AC59075B964B07152D234B70', 148, CAST(0x0000AA4900F9A7EE AS DateTime), NULL, CAST(0x00000000000007D1 AS DateTime))
INSERT [dbo].[SysUser] ([UserId], [UserCardNo], [Name], [Enable], [Account], [Password], [LoginCount], [LastLoginTime], [Remark], [Version]) VALUES (N'54bc153f-50f8-4af8-8fcb-e511faf997a8', N'', N'a', 1, N'a', N'202CB962AC59075B964B07152D234B70', 0, NULL, NULL, NULL)
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_LoginCount]  DEFAULT ((0)) FOR [LoginCount]
GO
ALTER TABLE [dbo].[Dept]  WITH CHECK ADD  CONSTRAINT [FK_Dept_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Dept] CHECK CONSTRAINT [FK_Dept_Company]
GO
ALTER TABLE [dbo].[DeptGroup]  WITH CHECK ADD  CONSTRAINT [FK_DeptGroup_Dept] FOREIGN KEY([DeptId])
REFERENCES [dbo].[Dept] ([DeptId])
GO
ALTER TABLE [dbo].[DeptGroup] CHECK CONSTRAINT [FK_DeptGroup_Dept]
GO
ALTER TABLE [dbo].[DeptGroupEmployee]  WITH CHECK ADD  CONSTRAINT [FK_DeptGroupEmployee_DeptGroup] FOREIGN KEY([GroupId])
REFERENCES [dbo].[DeptGroup] ([GroupId])
GO
ALTER TABLE [dbo].[DeptGroupEmployee] CHECK CONSTRAINT [FK_DeptGroupEmployee_DeptGroup]
GO
ALTER TABLE [dbo].[DeptGroupEmployee]  WITH CHECK ADD  CONSTRAINT [FK_DeptGroupEmployee_Employee] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([EmpId])
GO
ALTER TABLE [dbo].[DeptGroupEmployee] CHECK CONSTRAINT [FK_DeptGroupEmployee_Employee]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Dept] FOREIGN KEY([DeptId])
REFERENCES [dbo].[Dept] ([DeptId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Dept]
GO
ALTER TABLE [dbo].[EmployeePosition]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePosition_Employee] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([EmpId])
GO
ALTER TABLE [dbo].[EmployeePosition] CHECK CONSTRAINT [FK_EmployeePosition_Employee]
GO
ALTER TABLE [dbo].[PermissionAction]  WITH CHECK ADD  CONSTRAINT [FK_PermissionAction_PermissionRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[PermissionRole] ([RoleId])
GO
ALTER TABLE [dbo].[PermissionAction] CHECK CONSTRAINT [FK_PermissionAction_PermissionRole]
GO
ALTER TABLE [dbo].[PermissionRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoleUser_PermissionRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[PermissionRole] ([RoleId])
GO
ALTER TABLE [dbo].[PermissionRoleUser] CHECK CONSTRAINT [FK_PermissionRoleUser_PermissionRole]
GO
ALTER TABLE [dbo].[PermissionRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoleUser_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[SysUser] ([UserId])
GO
ALTER TABLE [dbo].[PermissionRoleUser] CHECK CONSTRAINT [FK_PermissionRoleUser_Users]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'CompanyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'CompanyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司电话1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Tel1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司电话2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Tel2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司电话3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Tel3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Fax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮编' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'PostCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'DeptId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'CompanyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'DeptNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'DeptName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上级部门ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'DeptTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'Tel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'Fax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dept'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小组ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroup', @level2type=N'COLUMN',@level2name=N'GroupId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroup', @level2type=N'COLUMN',@level2name=N'DeptId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小组名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroup', @level2type=N'COLUMN',@level2name=N'GroupName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroup', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroup', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroupEmployee', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroupEmployee', @level2type=N'COLUMN',@level2name=N'GroupId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroupEmployee', @level2type=N'COLUMN',@level2name=N'EmpId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeptGroupEmployee', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EmpId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EmpNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EmpName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Sex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'DeptId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Tel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'住址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'身份证号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'IdCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'职务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Headship'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许登录系统' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'AllowLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'在职' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'OnJob'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'岗位ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeePosition', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeePosition', @level2type=N'COLUMN',@level2name=N'EmpId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'岗位ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeePosition', @level2type=N'COLUMN',@level2name=N'PositionId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeePosition', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工岗位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeePosition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'功能ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionAction', @level2type=N'COLUMN',@level2name=N'ActionId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionAction', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'功能' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionAction', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionAction', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRole', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRole', @level2type=N'COLUMN',@level2name=N'RoleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRole', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRole', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRoleUser', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRoleUser', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRoleUser', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PermissionRoleUser', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代码ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'CodeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代码类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'CodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'项次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代码名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'CodeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公共代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PubCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar', @level2type=N'COLUMN',@level2name=N'Module'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快捷工具栏' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortcutToolbar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'UserCardNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'停用?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'Enable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUser', @level2type=N'COLUMN',@level2name=N'Version'
GO
