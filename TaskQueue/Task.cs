//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Task.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.TaskQueue
{
	[Serializable]
    internal class Task
    {
		public Guid Id
		{
			get;
			set;
		}

		public object Data
		{
			get;
			set;
		}

		public TaskStatus Status
		{
			get;
			set;
		}

		public TaskPriority Priority
		{
			get;
			set;
		}

		//创建的时间。
		public DateTime CreatedOn
		{
			get;
			set;
		}

		//创建的序号。注意，序号不是全局唯一的，只是用于当两个Task的创建时间一样时能够排序。
		public int Number
		{
			get;
			set;
		}

	}
}
