//==============================================================
//  版权所有：深圳杰文科技
//  文件名：TaskStatus.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.TaskQueue
{
	/// <summary>
	/// 任务的状态。
	/// </summary>
	public enum TaskStatus
	{
		/// <summary>
		/// 未执行。
		/// </summary>
		Unexecuted = 0,

		/// <summary>
		/// 已执行。
		/// </summary>
		Executed = 1,

		/// <summary>
		/// 已取消。
		/// </summary>
		Cancelled = 2,

		/// <summary>
		/// 已执行，但发生错误。
		/// </summary>
		Error
	}
}
