//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Test.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Infrastructure.TaskQueue
{
	class Test
	{
		void Test1()
		{
			TaskManager tm = new TaskManager((obj) =>
			{
				Console.WriteLine("{0} {1}", DateTime.Now.ToString("HH:mm:ss.fff"), obj);
			}, "test");

			tm.TaskExecuted += (guid) =>
			{
				Console.WriteLine("{0} {1}", DateTime.Now.ToString("HH:mm:ss.fff"), guid);
			};

			for (int i = 0; i < 10; i++)
			{
				tm.Append(i);
			}

			Console.WriteLine("{0} end", DateTime.Now.ToString("HH:mm:ss.fff"));

			//Thread.Sleep(5000);

			tm.Dispose();

			Console.WriteLine("{0} end2", DateTime.Now.ToString("HH:mm:ss.fff"));
		}

	}
}
