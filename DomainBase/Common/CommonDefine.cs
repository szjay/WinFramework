//==============================================================
//  版权所有：深圳杰文科技
//  文件名：CommonDefine.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	public class CommonDefine
	{
		/// <summary>
		/// 日期格式：yyyy-MM-dd
		/// </summary>
		public const string DateFormatter = "yyyy-MM-dd";

		/// <summary>
		/// 月日格式：MM-dd
		/// </summary>
		public const string MonthDayFormatter = "MM-dd";

		/// <summary>
		/// 时间格式：yyyy-MM-dd HH:mm
		/// </summary>
		public const string DateTimeFormatter = "yyyy-MM-dd HH:mm";

		/// <summary>
		/// 完整时间格式：yyyy-MM-dd HH:mm:ss
		/// </summary>
		public const string FullDateTimeFormatter = "yyyy-MM-dd HH:mm:ss";

	}
}
