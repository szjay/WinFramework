//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysNumber.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	public partial class SysNumber
	{
		public const string SQL = @"select * from SysNumber where 1=1 {0}";

		public enum DeptTypes
		{
			无 = 0,
			总部 = 1,
			分部 = 2
		}

		[Key]
		public Guid NumberId
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public int Year
		{
			get;
			set;
		}

		public int Month
		{
			get;
			set;
		}

		public int Day
		{
			get;
			set;
		}

		public int Number
		{
			get;
			set;
		}

	}
}
