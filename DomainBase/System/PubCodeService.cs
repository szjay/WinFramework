//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PubCodeService.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	public class PubCodeService
	{
		private BaseDB db = null;

		public void Save(PubCode code)
		{
			db.PubCode.Update(code, a => a.CodeId);
		}

		public void Delete(PubCode code)
		{
			PubCode po = db.PubCode.Find(code.CodeId);
			db.PubCode.Remove(po);
		}

		public void Append(string codeType, string codeName)
		{
			if (string.IsNullOrEmpty(codeType) || string.IsNullOrEmpty(codeName))
			{
				return;
			}

			PubCode pubCode = db.PubCode.FirstOrDefault(a => a.CodeType == codeType && a.CodeName == codeName);
			if (pubCode != null)
			{
				return;
			}

			int maxSeqNo = db.PubCode.Where(a => a.CodeType == codeType).Max(a => a.SeqNo).GetValueOrDefault() + 1;
			PubCode code = new PubCode()
			{
				CodeId = Guid.NewGuid(),
				CodeName = codeName,
				CodeType = codeType,
				SeqNo = maxSeqNo
			};
			db.PubCode.Add(code);
		}
	}
}
