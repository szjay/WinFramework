//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SkinStyleService.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	internal class SkinStyleService
	{
		private BaseDB db = null;

		public SkinStyle CreateDefault()
		{
			SkinStyle style = new SkinStyle()
			{
				StyleName = SkinStyle.SKIN_NAME,
				FontName = SkinStyle.FONT_NAME,
				FontSize = SkinStyle.FONT_SIZE
			};
			return style;
		}

		public void ChangeSkin(string styleName)
		{
			SkinStyle po = db.SkinStyle.FirstOrDefault(a => a.UserId == db.CurrentUser.Id);
			if (po == null)
			{
				po = new SkinStyle()
				{
					StyleId = Guid.NewGuid(),
					UserId = db.CurrentUser.Id,
					StyleName = styleName,
				};
				db.SkinStyle.Add(po);
			}
			else
			{
				po.StyleName = styleName;
			}
		}

		public void ChangeFont(string fontName, int fontSize)
		{
			SkinStyle po = db.SkinStyle.FirstOrDefault(a => a.UserId == db.CurrentUser.Id);
			if (po == null)
			{
				po = new SkinStyle()
				{
					StyleId = Guid.NewGuid(),
					UserId = db.CurrentUser.Id,
					FontName = fontName,
					FontSize = fontSize
				};
				db.SkinStyle.Add(po);
			}
			else
			{
				po.FontName = fontName;
				po.FontSize = fontSize;
			}
		}
	}
}
