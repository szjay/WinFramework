//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ShortcutToolbarService.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	internal class ShortcutToolbarService
	{
		private BaseDB db = null;

		public void Add(string module)
		{
			var list = db.ShortcutToolbar.Where(a => a.UserId == db.CurrentUser.Id && a.Module == module);
			if (list.Count(a => a.UserId == db.CurrentUser.Id && a.Module == module) > 0)
			{
				return;
			}

			int seqNo = 0;
			list = db.ShortcutToolbar.Where(a => a.UserId == db.CurrentUser.Id);
			if (list.Count() > 0)
			{
				seqNo = list.Select(a => a.SeqNo).Max();
			}
			ShortcutToolbar toolbar = new ShortcutToolbar()
			{
				Id = Guid.NewGuid(),
				Module = module,
				UserId = db.CurrentUser.Id,
				SeqNo = seqNo + 1
			};
			db.ShortcutToolbar.Add(toolbar);
		}

		public void Remove(string module)
		{
			var po = db.ShortcutToolbar.SingleOrDefault(a => a.UserId == db.CurrentUser.Id && a.Module == module);
			if (po != null)
			{
				db.ShortcutToolbar.Remove(po);
			}
		}

		public void Save(List<ShortcutToolbar> voList)
		{
			db.ShortcutToolbar.Update(voList, db.ShortcutToolbar, a => a.Id);
		}
	}
}
