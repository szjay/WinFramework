//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PubCode.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

	[Serializable]
    [Table("PubCode")]
    public partial class PubCode
    {
		public const string SQL = @"select * from PubCode where 1=1 {0} order by SeqNo";

		[Key]
		public Guid CodeId
		{
			get;
			set;
		}
		
		public String CodeType {get;set;}

		public Int32? SeqNo {get;set;}

		public String CodeName {get;set;}

		public String Remark {get;set;}

	}
}	
