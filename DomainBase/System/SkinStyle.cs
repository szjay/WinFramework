//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SkinStyle.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	[Table("SkinStyle")]
	public partial class SkinStyle
	{
		public const string SQL = @"
select * from SkinStyle
where   1=1 {0}";

		public const string SKIN_NAME = "Office 2010 Blue";
		public const string FONT_NAME = "Tahoma";
		public const int FONT_SIZE = 9;

		[Key]
		public Guid StyleId
		{
			get;
			set;
		}

		public Guid UserId
		{
			get;
			set;
		}

		public String StyleName
		{
			get;
			set;
		}

		public String FontName
		{
			get;
			set;
		}

		public int? FontSize
		{
			get;
			set;
		}

		//[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed), Column(TypeName = "timestamp")]
		public DateTime? Version
		{
			get;
			set;
		}

	}
}
