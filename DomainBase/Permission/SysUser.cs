//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysUser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable, Table("SysUser")]
	public partial class SysUser
	{
		public const string SQL = @"select SysUser.*, Employee.DeptId, Dept.DeptName
from SysUser
left join Employee on Employee.EmpId = SysUser.UserId
left join Dept on Dept.DeptId = Employee.DeptId 
where 1=1 {0}";

		[Key]
		public Guid UserId
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public bool Enable
		{
			get;
			set;
		}

		public string Account
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public string UserCardNo
		{
			get;
			set;
		}

		public string Mobile
		{
			get;
			set;
		}

		public int LoginCount
		{
			get;
			set;
		}

		public DateTime? LastLoginTime
		{
			get;
			set;
		}

		//[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}

		public const string F_UserId = "UserId";
		public const string F_Name = "Name";
		public const string F_Enable = "Enable";
		public const string F_Account = "Account";
		public const string F_Password = "Password";
		public const string F_Mobile = "Mobile";
		public const string F_Version = "Version";

	}
}
