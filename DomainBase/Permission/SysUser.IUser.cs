//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SysUser.IUser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Remoting.Messaging;
using Framework.Common;

namespace Framework.DomainBase
{
	public partial class SysUser : IUser
	{
		[NotMapped]
		public string SessionId
		{
			get;
			set;
		}

		[NotMapped]
		public Guid Id
		{
			get
			{
				return UserId;
			}
			set
			{
				UserId = value;
			}
		}

		[NotMapped]
		public bool IsAdmin
		{
			get;
			set;
		}

		[NotMapped]
		public string IP
		{
			get;
			set;
		}

		[NotMapped]
		public Guid DeptId
		{
			get;
			set;
		}

		[NotMapped]
		public string DeptName
		{
			get;
			set;
		}

		[NotMapped]
		public DataPermissionTypes DataPermissionType
		{
			get;
			set;
		}
	}
}