//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionService.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.Common;
using Infrastructure.Utilities;

namespace Framework.DomainBase
{
	public class PermissionService
	{
		private BaseDB db = null;

		internal void SaveRole(PermissionRole vo, bool withItem)
		{
			PermissionRole po = db.PermissionRole.Find(vo.RoleId);
			if (po == null)
			{
				po = new PermissionRole()
				{
					RoleId = Guid.NewGuid()
				};
				db.PermissionRole.Add(po);
			}

			ObjectMapper.Map<PermissionRole>(vo, po);

			if (withItem)
			{
				List<PermissionAction> actionList = db.PermissionAction.Where(a => a.RoleId == vo.RoleId).ToList();
				foreach (PermissionAction action in actionList)
				{
					if (!vo.Actions.Any(a => a.ActionId == action.ActionId))
					{
						db.PermissionAction.Remove(action);
					}
				}

				foreach (PermissionAction actionVO in vo.Actions)
				{
					if (!actionList.Any(a => a.ActionId == actionVO.ActionId))
					{
						db.PermissionAction.Add(actionVO);
					}
				}

				//var data = db.PermissionData.Where(a => a.RoleId == vo.RoleId && a.DataType == "部门");
				//db.PermissionData.Update(vo.Data, data, a => a.DataId);

				var users = db.PermissionRoleUser.Where(a => a.RoleId == vo.RoleId);
				db.PermissionRoleUser.Update(vo.Users, users, a => a.Id);
			}
		}

		public bool HasPermission(Guid userId, string rootModule, string module, string action)
		{
			action = rootModule + "." + module + "." + action;
			string where = string.Format("and exists(select 1 from PermissionRoleUser pru where  pru.RoleId = pr.RoleId and pru.UserId = '{0}') and pa.Action = '{1}'",
				userId, action);
			PermissionAction pa = db.Get<PermissionAction>(where);
			return pa != null;

		}

		public List<PermissionDataNode> GetPermissionDataTree()
		{
			List<PermissionDataNode> nodeList = new List<PermissionDataNode>();

			foreach (Dept dept in db.Dept)
			{
				PermissionDataNode node = new PermissionDataNode()
				{
					Id = dept.DeptId.ToString(),
					DataType = "部门",
					ParentId = dept.ParentId == null ? "部门" : dept.ParentId.ToString()
				};
				nodeList.Add(node);
			}

			return nodeList;
		}

		public List<Dept> GetPermissionData(IUser user)
		{
			string sql = @"select d.*
from Dept d
join PermissionData pd on d.DeptId = pd.Data
join PermissionRole pr on pd.RoleId = pr.RoleId
join PermissionRoleUser ru on pr.RoleId = ru.RoleId
where ru.UserId = '{0}'";

			List<Dept> deptList = db.SqlQuery<Dept>(sql, user.Id);
			return deptList;
		}

		public DataPermissionTypes GetDataPermissionType(Guid userId)
		{
			List<PermissionRole> roleList = (from a in db.PermissionRole
											 join b in db.PermissionRoleUser on a.RoleId equals b.RoleId
											 where b.UserId == userId
											 select a).ToList();

			if (roleList.Any(a => a.DataPermissionType == DataPermissionTypes.无))
			{
				return DataPermissionTypes.无;
			}

			if (roleList.Any(a => a.DataPermissionType == DataPermissionTypes.自己))
			{
				return DataPermissionTypes.自己;
			}

			if (roleList.Any(a => a.DataPermissionType == DataPermissionTypes.部门))
			{

				List<Guid> deptList = (from a in db.PermissionData
									   join b in db.PermissionRoleUser on a.RoleId equals b.RoleId
									   where b.UserId == userId && a.DataType == "部门"
									   select a.DataId).ToList();
				return DataPermissionTypes.部门;
			}

			return DataPermissionTypes.全部;
		}
	}
}
