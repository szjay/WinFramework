//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionActionNode.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	public class PermissionActionNode
	{
		public enum NodeType
		{
			Root,
			Module,
			Action
		}

		public NodeType NodeTypeId
		{
			get;
			set;
		}

		public string Id
		{
			get;
			set;
		}

		public string ParentId
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public bool IsSelected
		{
			get;
			set;
		}

		public PermissionAction Action
		{
			get;
			set;
		}
	}
}
