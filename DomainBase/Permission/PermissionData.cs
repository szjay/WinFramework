//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionData.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;
	using Framework.Common;

	[Serializable]
	[Table("PermissionData")]
	public partial class PermissionData : IPermissionData
	{
		public const string SQL = @"
select  PermissionData.*
from    PermissionData
join    PermissionRole on PermissionData.RoleId = PermissionRole.RoleId
where   1=1 {0}";

		[Key]
		public Guid DataId
		{
			get;
			set;
		}

		public Guid RoleId
		{
			get;
			set;
		}

		public String Data
		{
			get;
			set;
		}

		public String DataType
		{
			get;
			set;
		}

		public virtual PermissionRole Role
		{
			get;
			set;
		}

		public const string F_DataId = "PermissionData.DataId";
		public const string F_RoleId = "PermissionData.RoleId";
		public const string F_DataType = "PermissionData.DataType";
	}
}
