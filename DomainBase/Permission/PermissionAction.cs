//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionAction.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

	[Serializable]
    [Table("PermissionAction")]
    public partial class PermissionAction
    {
		public const string SQL = @"
select  pa.*
from    PermissionAction pa
join    PermissionRole pr on pr.RoleId = pa.RoleId
where   1=1 {0}";

		[Key]
		public Guid ActionId
		{
			get;
			set;
		}

		public Guid RoleId
		{
			get;
			set;
		}

		public String Action {get;set;}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version {get;set;}

		public virtual PermissionRole Role
		{
			get;
			set;
		}
	
	}
}	
