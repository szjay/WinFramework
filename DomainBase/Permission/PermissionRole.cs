//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionRole.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;
	using Framework.Common;

	[Serializable]
	[Table("PermissionRole")]
	public partial class PermissionRole
	{
		public const string SQL = @"
select PermissionRole.* from PermissionRole where 1=1 {0}";

		[Key]
		public Guid RoleId
		{
			get;
			set;
		}

		public String RoleName
		{
			get;
			set;
		}

		public string RoleType
		{
			get;
			set;
		}

		public int DataPermissionTypeId
		{
			get;
			set;
		}

		public String Remark
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}

		public virtual List<PermissionRoleUser> Users
		{
			get;
			set;
		}

		public virtual List<PermissionAction> Actions
		{
			get;
			set;
		}

		public virtual List<PermissionData> Data
		{
			get;
			set;
		}

		[NotMapped]
		public DataPermissionTypes DataPermissionType
		{
			get
			{
				return (DataPermissionTypes)this.DataPermissionTypeId;
			}
			set
			{
				this.DataPermissionTypeId = (int)value;
			}
		}

		[NotMapped]
		public string DataPermissionTypeName
		{
			get
			{
				return this.DataPermissionType.ToString();
			}
		}

		public const string F_RoleId = "PermissionRole.RoleId";
		public const string F_RoleName = "PermissionRole.RoleName";
		public const string F_DataPermissionTypeId = "PermissionRole.DataPermissionTypeId";
		public const string F_Remark = "PermissionRole.Remark";
		public const string F_Version = "PermissionRole.Version";

	}
}
