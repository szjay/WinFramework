//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionRoleUser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

	[Serializable]
    [Table("PermissionRoleUser")]
    public partial class PermissionRoleUser
    {
		public const string SQL = @"
select  ru.*,
        e.EmpNo,
        e.EmpName
from    PermissionRoleUser ru
join    SysUser u on u.UserId = ru.UserId
join    Employee e on e.EmpId = ru.UserId
where   1=1 {0}";

		[Key]
		public Guid Id
		{
			get;
			set;
		}

		public Guid RoleId
		{
			get;
			set;
		}

		public Guid UserId
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version {get;set;}

		[NotMapped]
		public String EmpNo {get;set;}

		[NotMapped]
		public String EmpName {get;set;}

		public const string F_Id = "Id";
		public const string F_RoleId = "RoleId";
		public const string F_UserId = "UserId";
		public const string F_Version = "Version";
	
	}
}	
