//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DB.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Configuration;
using System.Data.Entity;
using Framework.Common;
using Infrastructure.Utilities;

namespace Framework.DomainBase
{
	public partial class BaseDB : DbContext, IDB
	{
		public static string ConnectionName = "name=DB";

		static BaseDB()
		{
			Database.SetInitializer<BaseDB>(null); //关闭数据库初始化策略。
		}

		public BaseDB() //: base("name=DB")
			: base(GetConnectionString(ConnectionName))
		{
			DbContextExt.WriteSqlLog = true;
			//base.Database.Log = log => WriteSqlLog(log);
		}

		public static string GetConnectionString(string connectionName)
		{
			if (connectionName.StartsWith("name=")) //name=DB模式就认为连接字符串不是加密模式
			{
				return connectionName;
			}

			string connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
			if (!connectionString.StartsWith("data source")) //如果不是data source开关，那么就认为是加密字符串
			{
				connectionString = Encrypt.DecryptText(connectionString, Encrypt.CommonPassword);
			}
			return connectionString;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
			base.OnModelCreating(modelBuilder);
		}

		private void WriteSqlLog(string log)
		{
			if (string.IsNullOrWhiteSpace(log))
			{
				return;
			}

			//屏蔽打开、关闭数据库，以及执行时间的日志。
			if (log.StartsWith("已于") || log.StartsWith("-- 正在 ") || log.StartsWith("-- 已在 "))
			{
				return;
			}

			log = log.TrimEnd('\n').TrimEnd('\r');

			if (CurrentUser == null)
			{
				SysLogger.WriteSql(log);
			}
			else
			{
				SysLogger.WriteSql(this.CurrentUser.Account, log);
			}
		}

		public IUser CurrentUser
		{
			get;
			set;
		}

		public IDbServiceContainer ServiceContainer
		{
			get;
			set;
		}

		public T CreateService<T>() where T : new()
		{
			return ServiceContainer.Create<T>();
		}

		public long ID { get; set; } = 0;

		public int Save()
		{
			return DbContextExt.Save(this);
		}

		public void Rollback()
		{
			Database.CurrentTransaction.Rollback();
		}

		public void Close()
		{
			if (this.Database != null && this.Database.Connection != null)
			{
				this.Database.Connection.Close();
			}
		}

		public virtual DbSet<AuditedLog> AuditedLog
		{
			get;
			set;
		}

		public virtual DbSet<Company> Company
		{
			get;
			set;
		}

		public virtual DbSet<Employee> Employee
		{
			get;
			set;
		}

		public virtual DbSet<EmployeePosition> EmployeePosition
		{
			get;
			set;
		}

		public virtual DbSet<Position> Position
		{
			get;
			set;
		}

		public virtual DbSet<Dept> Dept
		{
			get;
			set;
		}

		public virtual DbSet<DeptGroup> DeptGroup
		{
			get;
			set;
		}

		public virtual DbSet<SysUser> SysUser
		{
			get;
			set;
		}

		public virtual DbSet<DeptGroupEmployee> DeptGroupEmployee
		{
			get;
			set;
		}

		public virtual DbSet<PermissionRole> PermissionRole
		{
			get;
			set;
		}

		public virtual DbSet<PermissionAction> PermissionAction
		{
			get;
			set;
		}

		public virtual DbSet<PermissionData> PermissionData
		{
			get;
			set;
		}

		public virtual DbSet<PermissionRoleUser> PermissionRoleUser
		{
			get;
			set;
		}

		public virtual DbSet<PubCode> PubCode
		{
			get;
			set;
		}

		public virtual DbSet<ShortcutToolbar> ShortcutToolbar
		{
			get;
			set;
		}

		public virtual DbSet<SkinStyle> SkinStyle
		{
			get;
			set;
		}

		public virtual DbSet<SysNumber> SysNumber
		{
			get;
			set;
		}

	}
}