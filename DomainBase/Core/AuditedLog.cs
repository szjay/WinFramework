//==============================================================
//  版权所有：深圳杰文科技
//  文件名：AuditedLog.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	[Serializable]
	public class AuditedLog
	{
		public const string SQL = @"select * from AuditedLog where 1=1 {0}";

		[Key]
		public Guid LogId
		{
			get;
			set;
		}

		public string TableName
		{
			get;
			set;
		}

		public int StateId
		{
			get;
			set;
		}

		[NotMapped]
		public EntityState State
		{
			get
			{
				return (EntityState)this.StateId;
			}
			set
			{
				this.StateId = (int)value;
			}
		}

		public string Caption
		{
			get;
			set;
		}

		public string FieldName
		{
			get;
			set;
		}

		public string OldValue
		{
			get;
			set;
		}

		public string NewValue
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public DateTime CreatedOn
		{
			get;
			set;
		}

		public string HostName
		{
			get;
			set;
		}

		[NotMapped]
		public string StateName
		{
			get
			{
				switch (this.State)
				{
					case EntityState.Added:
						return "新增";
					case EntityState.Deleted:
						return "删除";
					case EntityState.Modified:
						return "修改";
				}
				return "未知";
			}
		}

		public const string F_CreatedOn = "CreatedOn";
		public const string F_CreatedBy = "CreatedBy";
		public const string F_Caption = "Caption";
		public const string F_FieldName = "FieldName";
		public const string F_TableName = "TableName";
		public const string F_HostName = "HostName";
	}
}
