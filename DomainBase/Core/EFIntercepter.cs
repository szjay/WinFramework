//==============================================================
//  版权所有：深圳杰文科技
//  文件名：EFIntercepter.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using Framework.Common;
using Infrastructure.Utilities;

namespace Framework.DomainBase
{
	//使用：DbInterception.Add(new EFIntercepter());
	public class EFIntercepter : DbCommandInterceptor
	{
		public override void ScalarExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
		{
			base.ScalarExecuting(command, interceptionContext);
		}

		private string GetAccount(DbCommandInterceptionContext interceptionContext)
		{
			DbContext db = interceptionContext.DbContexts.First();
			if (db is IDB)
			{
				IDB idb = db as IDB;
				if (idb.CurrentUser != null)
				{
					return idb.CurrentUser.Account;
				}
			}
			return "";
		}

		public override void ScalarExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
		{
			if (interceptionContext.Exception != null)
			{
				SysLogger.Write(GetAccount(interceptionContext), "SQL Exception 1：{0}\r\n{1}".Fmt(interceptionContext.Exception.Message, command.CommandText));
			}
			//else
			//{
			//	if (!command.CommandText.Contains("EngineEdition"))
			//	{
			//		SystemLogger.Instance.Write("\r\n" + command.CommandText);
			//	}
			//}
			base.ScalarExecuted(command, interceptionContext);
		}

		public override void NonQueryExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
		{
			base.NonQueryExecuting(command, interceptionContext);
		}

		public override void NonQueryExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
		{
			if (interceptionContext.Exception != null)
			{
				SysLogger.Write(GetAccount(interceptionContext), "SQL Exception 2：{0}\r\n{1}".Fmt(interceptionContext.Exception.Message, command.CommandText));
			}
			//else
			//{
			//	SystemLogger.Instance.Write("\r\n" + command.CommandText);
			//}
			base.NonQueryExecuted(command, interceptionContext);
		}

		public override void ReaderExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
		{
			//if (!command.CommandText.Contains("EngineEdition"))
			//{
			//	base.ReaderExecuting(command, interceptionContext);
			//}
		}

		public override void ReaderExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
		{
			if (interceptionContext.Exception != null)
			{
				SysLogger.Write(GetAccount(interceptionContext), "SQL Exception 3：{0}\r\n{1}".Fmt(interceptionContext.Exception.Message, command.CommandText));
			}
			//else
			//{
			//	if (!command.CommandText.Contains("EngineEdition"))
			//	{
			//		SystemLogger.Instance.Write("\r\n" + command.CommandText);
			//	}
			//}
			base.ReaderExecuted(command, interceptionContext);
		}
	}
}
