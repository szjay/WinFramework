//==============================================================
//  版权所有：深圳杰文科技
//  文件名：TableInfo.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	[Serializable]
	public class TableInfo
	{
		public const string SQL = @"select t.name as TableName, C.name as FieldName, isnull(g.Value, t.name) TableDesc, isnull(PFD.[value], c.name) as FieldDesc
from sys.columns C
join sys.tables t on t.object_id = c.object_id
join sys.objects O on C.[object_id] = O.[object_id] and O.type = 'U' and O.is_ms_shipped = 0
left join sys.extended_properties g on t.object_id = g.major_id AND g.minor_id = 0
left join sys.extended_properties PFD on PFD.class = 1 and C.[object_id] = PFD.major_id and C.column_id = PFD.minor_id
order by t.object_id, C.column_id";

		public string TableName
		{
			get;
			set;
		}

		public string FieldName
		{
			get;
			set;
		}

		public string TableDesc
		{
			get;
			set;
		}

		public string FieldDesc
		{
			get;
			set;
		}

		public string Desc
		{
			get
			{
				return TableDesc + "." + FieldDesc;
			}
		}
	}
}
