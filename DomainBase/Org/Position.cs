//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Position.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	/// <summary>
	/// 岗位。
	/// </summary>
	[Serializable]
	[Table("Position")]
	public partial class Position
	{
		public const string SQL = "select * from Position where 1=1 {0} order by PositionNo";

		[Key]
		public Guid PositionId
		{
			get;
			set;
		}

		public int PositionNo
		{
			get;
			set;
		}

		public string PositionName
		{
			get;
			set;
		}

		public bool Enable
		{
			get;
			set;
		}

		public string Remark
		{
			get;
			set;
		}

	}
}
