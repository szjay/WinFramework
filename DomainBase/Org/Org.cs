//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Org.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Framework.DomainBase
{
	[Serializable]
	[Table("Org"), DisplayName("组织架构")]
	public class Org
	{
		public enum NodeTypes
		{
			Company = 1,	//总部/总公司。
			Dept = 2,		//部门/分公司，允许分公司下面有部门。
			Group = 3,		//小组，小组的成员可以跨部门，但不允许小组下面还有小组。
			Employee = 4,	//员工，只允许属于一个部门，但可以属于多个小组。
		}

		public NodeTypes NodeType
		{
			get;
			set;
		}

		[DisplayName("ID")]
		public Guid Id
		{
			get;
			set;
		}

		[DisplayName("上级ID")]
		public Guid? ParentId
		{
			get;
			set;
		}

		[DisplayName("编号")]
		public string No
		{
			get;
			set;
		}

		[DisplayName("名称")]
		public string Name
		{
			get;
			set;
		}
	}
}
