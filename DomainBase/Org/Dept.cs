//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Dept.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	public partial class Dept
	{
		public const string SQL = @"
select Dept.*, c.CompanyName, case when p.DeptName is null or p.DeptName = '' then c.CompanyName else p.DeptName end ParentName
from Dept
left join Dept p on p.DeptId = Dept.ParentId
left join Company c on c.CompanyId = Dept.CompanyId
where 1=1 {0}";

		public enum DeptTypes
		{
			无 = 0,
			总部 = 1,
			分部 = 2
		}

		[Key]
		public Guid DeptId
		{
			get;
			set;
		}

		public Guid CompanyId
		{
			get;
			set;
		}

		public String DeptNo
		{
			get;
			set;
		}

		public String DeptName
		{
			get;
			set;
		}

		public Guid? ParentId
		{
			get;
			set;
		}

		public Int32? DeptTypeId
		{
			get;
			set;
		}

		public String Tel
		{
			get;
			set;
		}

		public String Fax
		{
			get;
			set;
		}

		public String Remark
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}

		[NotMapped]
		public DeptTypes DeptType
		{
			get
			{
				if (DeptTypeId == null)
				{
					return DeptTypes.无;
				}
				return (DeptTypes)this.DeptTypeId;
			}
			set
			{
				this.DeptTypeId = (int)value;
			}
		}

		[NotMapped]
		public string DeptTypeName
		{
			get
			{
				if (DeptTypeId == null)
				{
					return DeptTypes.无.ToString();
				}
				return ((DeptTypes)this.DeptTypeId).ToString();
			}
		}

		[NotMapped]
		public bool IsSelected
		{
			get;
			set;
		}

		[NotMapped]
		public string ParentName
		{
			get;
			set;
		}
	}
}
