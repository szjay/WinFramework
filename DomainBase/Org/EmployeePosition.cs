//==============================================================
//  版权所有：深圳杰文科技
//  文件名：EmployeePosition.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	[Table("EmployeePosition")]
	public partial class EmployeePosition
	{
		public const string SQL = @"
select  ep.*,
        e.EmpName,
		p.PositionNo,
		p.PositionName
from    EmployeePosition ep
join    Employee e on e.EmpId = ep.EmpId
join	Position p on p.PositionId = ep.PositionId
where 1=1 {0}";

		[Key]
		public Guid Id
		{
			get;
			set;
		}

		public Guid PositionId
		{
			get;
			set;
		}

		public Guid EmpId
		{
			get;
			set;
		}

		[NotMapped]
		public string EmpName
		{
			get;
			set;
		}

		[NotMapped]
		public int PositionNo
		{
			get;
			set;
		}

		[NotMapped]
		public string PositionName
		{
			get;
			set;
		}

		//[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}

	}
}
