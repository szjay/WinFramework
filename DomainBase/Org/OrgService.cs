//==============================================================
//  版权所有：深圳杰文科技
//  文件名：OrgService.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Common;
using Infrastructure.Utilities;

namespace Framework.DomainBase
{
	public class OrgService
	{
		private BaseDB db = null;

		public List<Org> GetOrgTree(bool includeGroup)
		{
			List<Org> list = new List<Org>();

			Company company = db.Company.First();
			list.Add(new Org()
			{
				Id = company.CompanyId,
				ParentId = null,
				No = null,
				Name = company.CompanyName,
				NodeType = Org.NodeTypes.Company
			});

			foreach (Dept dept in db.Dept)
			{
				list.Add(new Org()
				{
					Id = dept.DeptId,
					ParentId = dept.ParentId,
					No = dept.DeptNo,
					Name = dept.DeptName,
					NodeType = Org.NodeTypes.Dept
				});
			}

			if (includeGroup)
			{
				foreach (DeptGroup group in db.DeptGroup)
				{
					list.Add(new Org()
					{
						Id = group.GroupId,
						ParentId = group.DeptId,
						No = null,
						Name = group.GroupName,
						NodeType = Org.NodeTypes.Group
					});
				}
			}

			return list;
		}

		public List<Org> GetOrgTreeWithEmployee()
		{
			List<Org> list = new List<Org>();

			Company company = db.Company.First();
			list.Add(new Org()
			{
				Id = company.CompanyId,
				ParentId = null,
				No = null,
				Name = company.CompanyName,
				NodeType = Org.NodeTypes.Company
			});

			foreach (Dept dept in db.Dept)
			{
				list.Add(new Org()
				{
					Id = dept.DeptId,
					ParentId = dept.ParentId,
					No = dept.DeptNo,
					Name = dept.DeptName,
					NodeType = Org.NodeTypes.Dept
				});
			}

			foreach (Employee emp in db.Employee)
			{
				list.Add(new Org()
				{
					Id = emp.EmpId,
					ParentId = emp.DeptId,
					No = emp.EmpNo,
					Name = emp.EmpName,
					NodeType = Org.NodeTypes.Employee
				});
			}

			return list;
		}

		public void Save(Company company)
		{
			Company po = db.Company.First();
			ObjectMapper.Map<Company>(company, po);
		}

		public void Save(Dept dept)
		{
			if (db.Dept.Any(a => a.DeptId != dept.DeptId && a.DeptNo == dept.DeptNo))
			{
				throw new ServiceException("部门编号重复：{0}", dept.DeptNo);
			}

			Dept po = db.Dept.FirstOrDefault(a => a.DeptId == dept.DeptId);
			if (po == null)
			{
				po = new Dept();
				db.Dept.Add(po);
			}

			ObjectMapper.Map<Dept>(dept, po);
		}

		public void Save(DeptGroup group, IEnumerable<DeptGroupEmployee> employees)
		{
			DeptGroup po = db.DeptGroup.FirstOrDefault(a => a.GroupId == group.GroupId);
			if (po == null)
			{
				po = new DeptGroup();
				db.DeptGroup.Add(po);
			}
			ObjectMapper.Map<DeptGroup>(group, po);

			var emps = db.DeptGroupEmployee.Where(a => a.GroupId == group.GroupId);
			db.DeptGroupEmployee.Update(employees, emps, a => a.Id);
		}

		public void Save(Employee vo)
		{
			if (db.Employee.Any(a => a.EmpId != vo.EmpId && a.EmpNo == vo.EmpNo))
			{
				throw new ServiceException("工号重复：{0}", vo.EmpNo);
			}

			var po = db.Employee.Update(vo, a => a.EmpId);
			db.EmployeePosition.Update(vo.Positions, db.EmployeePosition.Where(a => a.EmpId == vo.EmpId), a => a.Id);
			UpdateUser(po);
		}

		private void UpdateUser(Employee emp)
		{
			SysUser user = db.SysUser.Find(emp.EmpId);
			if (user == null)
			{
				user = new SysUser()
				{
					UserId = emp.EmpId,
					Password = Encrypt.MD5("123"),
				};
				db.SysUser.Add(user);
			}
			user.Account = emp.EmpNo;
			user.Name = emp.EmpName;
			user.DeptId = emp.DeptId;
			user.DeptName = emp.DeptName;
			user.Enable = emp.AllowLogin;
			user.Mobile = emp.Tel.IsNullOrWhiteSpace() ? "" : ((emp.Tel.Length == 11 && emp.Tel.StartsWith("1")) ? emp.Tel : "");
		}

		public List<Employee> QueryEmployeeByGroup(string groupId)
		{
			throw new NotImplementedException();
		}

		public void Save(DeptGroup group, IEnumerable<Employee> employees)
		{
			DeptGroup po = db.DeptGroup.FirstOrDefault(a => a.GroupId == group.GroupId);
			if (po == null)
			{
				po = new DeptGroup();
				db.DeptGroup.Add(po);
			}
			ObjectMapper.Map<DeptGroup>(group, po);

			IQueryable<DeptGroupEmployee> emps = db.DeptGroupEmployee.Where(a => a.GroupId == group.GroupId);
			//db.DeptGroupEmployee.Update(employees, emps, a => a.Id);

			throw new NotImplementedException();
		}

		public void DeleteDept(Guid deptId)
		{
			var po = db.Dept.FirstOrDefault(a => a.DeptId == deptId);
			if (po == null)
			{
				return;
			}

			if (db.DeptGroup.Count(a => a.DeptId == deptId) > 0)
			{
				throw new ServiceException("部门中存在小组");
			}

			if (db.Employee.Count(a => a.DeptId == deptId) > 0)
			{
				throw new ServiceException("部门中存在员工");
			}

			db.Dept.Remove(po);
		}

		public void DeleteGroup(Guid groupId)
		{
			var po = db.DeptGroup.FirstOrDefault(a => a.GroupId == groupId);
			if (po == null)
			{
				return;
			}

			if (db.DeptGroupEmployee.Count(a => a.GroupId == groupId) > 0)
			{
				throw new ServiceException("小组中存在员工");
			}

			db.DeptGroup.Remove(po);
		}

		public void DeleteEmployee(Guid empId)
		{
			var po = db.Employee.FirstOrDefault(a => a.EmpId == empId);
			if (po == null)
			{
				return;
			}

			db.Employee.Remove(po);
		}

		public void ImportEmployee(List<Employee> voList)
		{
			foreach (Employee vo in voList)
			{
				if (string.IsNullOrEmpty(vo.EmpNo) || string.IsNullOrEmpty(vo.EmpName) || string.IsNullOrEmpty(vo.DeptName))
				{
					throw new ServiceException("员工工号、姓名、部门不能为空");
				}

				Dept dept = db.Dept.SingleOrDefault(a => a.DeptName == vo.DeptName);
				if (dept == null)
				{
					throw new ServiceException("找不到部门：{0}", vo.DeptName);
				}

				if (db.Employee.Count(a => a.EmpNo == vo.EmpNo) > 0)
				{
					throw new ServiceException("工号重复：{0}", vo.EmpNo);
				}

				Employee po = new Employee()
				{
					EmpId = Guid.NewGuid(),
					EmpNo = vo.EmpNo,
					EmpName = vo.EmpName,
					AllowLogin = true,
					OnJob = true,
					DeptId = dept.DeptId
				};
				db.Employee.Add(po);
			}
		}

		public List<Employee> QueryEmployeeByPosition(int positionNo)
		{
			string where = " and exists(select 1 from EmployeePosition ep join Position p on p.PositionId = ep.PositionId where ep.EmpId = e.EmpId and p.PositionNo = {0})".Fmt((int)positionNo);
			List<Employee> list = db.Query<Employee>(where);
			return list;
		}

	}
}
