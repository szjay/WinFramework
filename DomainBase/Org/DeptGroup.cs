//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DeptGroup.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	public partial class DeptGroup
	{
		public const string SQL = @"
select  dg.*,
        d.DeptName
from    DeptGroup dg
join    Dept d on d.DeptId = dg.DeptId
where   1=1 {0}";

		[Key]
		public Guid GroupId
		{
			get;
			set;
		}

		public Guid DeptId
		{
			get;
			set;
		}

		public String GroupName
		{
			get;
			set;
		}

		public String Remark
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}

		[NotMapped]
		public String DeptName
		{
			get;
			set;
		}

		public DeptGroup()
		{
			this.Employees = new List<DeptGroupEmployee>();
		}

		public virtual List<DeptGroupEmployee> Employees
		{
			get;
			set;
		}

		public const string F_GroupId = "dg.GroupId";
		public const string F_DeptId = "DeptId";
		public const string F_GroupName = "GroupName";
		public const string F_Remark = "Remark";
		public const string F_Version = "Version";

	}
}
