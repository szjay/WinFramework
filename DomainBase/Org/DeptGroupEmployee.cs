//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DeptGroupEmployee.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

	[Serializable]
    [Table("DeptGroupEmployee")]
    public partial class DeptGroupEmployee
    {
		public const string SQL = @"
select  dge.*,
        dg.GroupName,
        e.EmpNo,
        e.EmpName
from    DeptGroupEmployee dge
join    DeptGroup dg on dg.GroupId = dge.GroupId
join    Employee e on e.EmpId = dge.EmpId
where   1=1 {0}";

		[Key]
		public Guid Id
		{
			get;
			set;
		}

		public Guid GroupId
		{
			get;
			set;
		}

		public Guid EmpId
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version {get;set;}

		[NotMapped]
		public String GroupName {get;set;}

		[NotMapped]
		public String EmpNo {get;set;}

		[NotMapped]
		public String EmpName {get;set;}

	
	}
}	
