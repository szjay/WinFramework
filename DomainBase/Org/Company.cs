//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Company.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable, Table("Company")]
	public partial class Company
	{
		public const string SQL = @"select * from Company";

		[Key]
		public Guid CompanyId
		{
			get;
			set;
		}

		public String CompanyName
		{
			get;
			set;
		}

		public String Tel1
		{
			get;
			set;
		}

		public String Tel2
		{
			get;
			set;
		}

		public String Tel3
		{
			get;
			set;
		}

		public String Fax
		{
			get;
			set;
		}

		public String Address
		{
			get;
			set;
		}

		public String PostCode
		{
			get;
			set;
		}

		public String Remark
		{
			get;
			set;
		}

		[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Version
		{
			get;
			set;
		}


	}
}
