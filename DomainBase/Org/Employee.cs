﻿//==============================================================
//  版权所有：深圳杰文科技
//  文件名：HandlerHost.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================


namespace Framework.DomainBase
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Serializable]
	[Table("Employee")]
	public partial class Employee
	{
		public const string SQL = @"
select  e.*, d.DeptNo, d.DeptName
from    Employee e
join    dept d on d.DeptId = e.DeptId
where   1=1 {0}";

		public Employee()
		{
			this.Positions = new List<EmployeePosition>();
		}

		[Key]
		public Guid EmpId
		{
			get;
			set;
		}

		public String EmpNo
		{
			get;
			set;
		}

		public String EmpName
		{
			get;
			set;
		}

		public String Sex
		{
			get;
			set;
		}

		public Guid DeptId
		{
			get;
			set;
		}

		public String Tel
		{
			get;
			set;
		}

		public String Address
		{
			get;
			set;
		}

		public String IdCard
		{
			get;
			set;
		}

		public String Headship
		{
			get;
			set;
		}

		public Boolean AllowLogin
		{
			get;
			set;
		}

		public Boolean OnJob
		{
			get;
			set;
		}

		public String Remark
		{
			get;
			set;
		}

		//[ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public DateTime? Version
		{
			get;
			set;
		}

		[NotMapped]
		public String DeptNo
		{
			get;
			set;
		}

		[NotMapped]
		public String DeptName
		{
			get;
			set;
		}

		[NotMapped]
		public bool IsSelected
		{
			get;
			set;
		}

		public virtual List<EmployeePosition> Positions
		{
			get;
			set;
		}

	}
}
