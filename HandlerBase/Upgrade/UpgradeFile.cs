//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeFile.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Framework.HandlerBase
{
	[Serializable]
	public class UpgradeFile
	{
		public UpgradeFile()
		{
			this.IsLoaded = false;
		}

		public UpgradeFile(long versionNo, string path, string fileName)
			: this()
		{
			this.VersionNo = versionNo;
			this.FullPath = path;
			this.FileName = fileName;
		}

		public long VersionNo
		{
			get;
			set;
		}

		public string FullPath
		{
			get;
			set;
		}

		public string FileName
		{
			get;
			set;
		}

		public byte[] Content
		{
			get;
			set;
		}

		public bool IsLoaded
		{
			get;
			set;
		}

	}
}
