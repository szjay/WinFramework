//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;

namespace Framework.HandlerBase
{
	public class UpgradeHandler : BaseHandler
	{
		private UpgradeFileManager manager = new UpgradeFileManager();

		//返回升级文件的最大版本号。
		public virtual long GetMaxVersionNo()
		{
			return manager.MaxVersionNo;
		}

		//升级。versionNo是一个"yyyyMMddHHmm"格式的长整型。
		public virtual UpgradeData Upgrade(long versionNo)
		{
			UpgradeData data = new UpgradeData()
			{
				VersionNo = manager.MaxVersionNo,
				UpgradeLog = manager.GetUpgradeLog(),
				UpgradeFileList = manager.Get(versionNo)
			};
			return data;
		}

		//升级服务器。
		public virtual void UploadServer(UpgradeData upgradeData)
		{
			manager.UpgradeServer(upgradeData);
		}

		//升级客户端。
		public virtual void UploadClient(UpgradeData upgradeData)
		{
			manager.UploadClient(upgradeData);
		}

		public virtual string GetUpgradeLog()
		{
			return manager.GetUpgradeLog();
		}

	}
}
