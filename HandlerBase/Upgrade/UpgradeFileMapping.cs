//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UpgradeFileMapping.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Framework.HandlerBase
{
	internal class UpgradeFileMapping
	{
		//升级的目录。
		private string rootDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UpgradeFileManager._ClientUpgradePath);

		public UpgradeFileMapping(long versionNo, string dir)
		{
			this.VersionNo = versionNo;
			Parse(dir);
		}

		public long VersionNo
		{
			get;
			set;
		}

		private List<UpgradeFile> fileList = new List<UpgradeFile>();
		public IList<UpgradeFile> FileList
		{
			get
			{
				return fileList;
			}
		}

		public void Parse(string dir)
		{
			string[] files = Directory.GetFiles(dir);
			foreach (string filePath in files)
			{
				string fileName = filePath.Replace(rootDir + "\\" + VersionNo.ToString() + "\\", "");
				UpgradeFile upgradeFile = new UpgradeFile(this.VersionNo, filePath, fileName);
				this.FileList.Add(upgradeFile);
			}
		}

	}
}
