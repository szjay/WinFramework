//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DbHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.DomainBase;

namespace Framework.HandlerBase
{
	public class DbHandler : BaseHandler, IDisposable
	{
		//注意！！！ DbHandler不能嵌套访问，即不能在一个DbHandler里直接new一个DbHandler进行访问，否则db会出问题。
		protected BaseDB db = new BaseDB();

		public DbHandler()
		{
			SetDB(db);
		}

		protected T Invoke<T>() where T : class, new()
		{
			using (DbServiceContainer sc = new DbServiceContainer(db))
			{
				T obj = sc.Create<T>();
				return obj;
			}
		}

		public void Dispose()
		{
			//if (db != null)
			//{
			//	db.Save();
			//	db.Dispose();
			//	db = null;
			//}
			//CallContext.SetData("db", null);
		}
	}
}
