//==============================================================
//  版权所有：深圳杰文科技
//  文件名：BaseHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using Framework.Common;

namespace Framework.HandlerBase
{
	public abstract class BaseHandler : MarshalByRefObject
	{
		protected EntityObject CloneEntityObject(EntityObject source)
		{
			var dcs = new DataContractSerializer(source.GetType());
			using (var ms = new System.IO.MemoryStream())
			{
				dcs.WriteObject(ms, source);
				ms.Seek(0, System.IO.SeekOrigin.Begin);
				return (EntityObject)dcs.ReadObject(ms);
			}
		}

		protected void SetDB(IDB db)
		{
			if (db.CurrentUser == null)
			{
				db.CurrentUser = ServiceContext.User;
				if (db.CurrentUser == null)
				{
					//ServiceContext.User是通过CallContext.GetData("SessionData")获取Session，但在构造函数中无法获得，因此改用CustomServerSink放进当前线程的下上文中获得
					db.CurrentUser = (IUser)CallContext.GetData("user");
				}
			}
			CallContext.SetData("db", db);
		}
	}
}