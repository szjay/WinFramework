//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DbHandler2.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Data.Entity;
using Framework.Common;

namespace Framework.HandlerBase
{
	public class DbHandler<T> : BaseHandler, IDisposable where T : DbContext, IDB, new()
	{
		//注意！！！ DbHandler不能嵌套访问，即不能在一个DbHandler里直接new一个DbHandler进行访问，否则db会出问题。
		protected T db = new T();

		public DbHandler()
		{
			SetDB(db);
		}

		protected Service Invoke<Service>() where Service : new()
		{
			using (DbServiceContainer sc = new DbServiceContainer(db))
			{
				Service serivce = sc.Create<Service>();
				return serivce;
			}
		}

		public void Dispose()
		{
			//if (db != null)
			//{
			//	db.Save();
			//	db.Dispose();
			//	db = null;
			//}
			//CallContext.SetData("db", null);
		}
	}
}
