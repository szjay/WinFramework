//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SkinStyleHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.DomainBase;
using Infrastructure.Utilities;

namespace Framework.HandlerBase
{
	public class SkinStyleHandler : DbHandler
	{
		public SkinStyle Get()
		{
			Guid userId = ServiceContext.GetCurrentUser().Id;
			SkinStyle style = db.Get<SkinStyle>("and UserId = '{0}'".Fmt(userId));
			if (style == null)
			{
				style = new SkinStyle();
			}
			if (string.IsNullOrEmpty(style.StyleName))
			{
				style.StyleName = SkinStyle.SKIN_NAME;
			}
			if (string.IsNullOrEmpty(style.FontName))
			{
				style.FontName = SkinStyle.FONT_NAME;
			}
			if (style.FontSize == null || style.FontSize == 0)
			{
				style.FontSize = SkinStyle.FONT_SIZE;
			}
			return style;
		}

		public void ChangeSkin(string styleName)
		{
			new DbServiceContainer(db).Create<SkinStyleService>().ChangeSkin(styleName);
		}

		public void ChangeFont(string fontName, int fontSize)
		{
			new DbServiceContainer(db).Create<SkinStyleService>().ChangeFont(fontName, fontSize);
		}
	}
}
