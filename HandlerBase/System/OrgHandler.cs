//==============================================================
//  版权所有：深圳杰文科技
//  文件名：OrgHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Framework.DomainBase;
using Infrastructure.Utilities;

namespace Framework.HandlerBase
{
	public class OrgHandler : DbHandler
	{
		#region - Org -

		public List<Org> GetOrgTreeWithoutGroup()
		{
			return new DbServiceContainer(db).Create<OrgService>().GetOrgTree(false);
		}

		public List<Org> GetOrgTree()
		{
			return new DbServiceContainer(db).Create<OrgService>().GetOrgTree(true);
		}

		public List<Org> GetOrgTreeWithEmployee()
		{
			return new DbServiceContainer(db).Create<OrgService>().GetOrgTreeWithEmployee();
		}

		#endregion

		#region - Company -

		public Company GetCompany()
		{
			return db.Company.FirstOrDefault();
		}

		public void Save(Company company)
		{
			new DbServiceContainer(db).Create<OrgService>().Save(company);
		}

		#endregion

		#region - Dept -

		public List<Dept> QueryDeptWithRoot()
		{
			List<Dept> deptList = db.Query<Dept>("");
			Dept root = new Dept()
			{
				DeptId = Guid.Empty,
				DeptName = "所有部门"
			};
			deptList.ForEach(dept => dept.ParentId = root.DeptId);
			deptList.Insert(0, root);
			return deptList;
		}

		public List<Dept> QueryBranch()
		{
			int deptTypeId = (int)Dept.DeptTypes.分部;
			return db.Dept.Where(a => a.DeptTypeId == deptTypeId).ToList();
		}

		public List<Dept> QueryDept()
		{
			return db.Dept.ToList();
		}

		public Dept GetDept(Guid deptId)
		{
			Dept dept = db.Get<Dept>(" and Dept.DeptId = '{0}'".Fmt(deptId));
			if (dept.ParentId == dept.CompanyId)
			{
				dept.ParentName = db.Get<Company>(dept.CompanyId).CompanyName;
			}
			return dept;
		}

		public void Save(Dept dept)
		{
			new DbServiceContainer(db).Create<OrgService>().Save(dept);
		}

		public void DeleteDept(Guid deptId)
		{
			new DbServiceContainer(db).Create<OrgService>().DeleteDept(deptId);
		}

		#endregion

		#region - Group -

		public List<DeptGroup> QueryGroup()
		{
			return db.Query<DeptGroup>("");
		}

		public DeptGroup GetGroup(Guid groupId)
		{
			DeptGroup group = db.GetById<DeptGroup>(groupId);
			return group;
		}

		public DeptGroup GetGroupWithEmployee(Guid groupId)
		{
			string where = new QueryBuilder().Equal(DeptGroup.F_GroupId, groupId).ToString();
			DeptGroup group = db.Query<DeptGroup>(where).FirstOrDefault();
			group.Employees = db.Query<DeptGroupEmployee>(where);
			return group;
		}

		public void Save(DeptGroup group, IEnumerable<DeptGroupEmployee> employees)
		{
			new DbServiceContainer(db).Create<OrgService>().Save(group, employees);
		}

		public void Save(DeptGroup group, IEnumerable<Employee> employees)
		{
			new DbServiceContainer(db).Create<OrgService>().Save(group, employees);
		}

		public void DeleteGroup(Guid groupId)
		{
			new DbServiceContainer(db).Create<OrgService>().DeleteGroup(groupId);
		}

		#endregion

		#region - Employee -

		public Employee GetEmployee(Guid empId)
		{
			Employee emp = db.Get<Employee>(string.Format("and e.EmpId = '{0}'", empId));
			return emp;
		}

		public Employee GetEmployeeWithPosition(Guid empId)
		{
			Employee emp = db.Get<Employee>(string.Format("and e.EmpId = '{0}'", empId));
			emp.Positions = db.Query<EmployeePosition>(string.Format("and ep.EmpId = '{0}'", empId));
			return emp;
		}

		public Employee GetEmployeeByNo(string empNo)
		{
			Employee emp = db.Get<Employee>(string.Format("and e.EmpNo = '{0}'", empNo));
			return emp;
		}

		public List<Employee> QueryEmployee()
		{
			var list = db.Query<Employee>();
			return list;
		}

		public List<Employee> QueryEmployeeForUser()
		{
			var list = db.Query<Employee>(" and exists(select 1 from SysUser where e.EmpId = SysUser.UserId)");
			return list;
		}

		public void Save(Employee employee, SysUser user)
		{
			using (DbServiceContainer sc = new DbServiceContainer(db))
			{
				sc.Create<OrgService>().Save(employee);
			}
		}

		public List<Employee> QueryEmployeeByDept(Guid deptId)
		{
			var list = db.Query<Employee>("and e.DeptId = '{0}'".Fmt(deptId));
			return list;
		}

		public List<Employee> QueryEmployeeByDeptForUser(Guid deptId)
		{
			var list = db.Query<Employee>("and e.DeptId = '{0}' and exists(select 1 from SysUser where e.EmpId =  SysUser.UserId)".Fmt(deptId));
			return list;
		}

		public List<Employee> QueryEmployeeByGroup(Guid groupId)
		{
			var list = db.Query<Employee>(" and exists(select 1 from DeptGroupEmployee dge where dge.EmpId = e.EmpId and dge.GroupId = '{0}')".Fmt(groupId));
			return list.ToList();
		}

		public List<Employee> QueryEmployeeByGroupForUser(Guid groupId)
		{
			var list = db.Query<Employee>(" and exists(select 1 from DeptGroupEmployee dge where dge.EmpId = e.EmpId and dge.GroupId = '{0}') and exists(select 1 from SysUser where e.EmpId =  SysUser.UserId)".Fmt(groupId));
			return list.ToList();
		}

		public void DeleteEmployee(Employee emp)
		{
			new DbServiceContainer(db).Create<OrgService>().DeleteEmployee(emp.EmpId);
		}

		public void ImportEmployee(List<Employee> empList)
		{
			new DbServiceContainer(db).Create<OrgService>().ImportEmployee(empList);
		}

		public Employee Validate(string empNo, string password)
		{
			Employee emp = db.Employee.FirstOrDefault(a => a.EmpNo == empNo);
			if (emp == null)
			{
				return null;
			}

			SysUser user = db.SysUser.FirstOrDefault(a => a.Account == empNo);
			if (user == null)
			{
				return null;
			}

			if (user.Password != Encrypt.MD5(password))
			{
				return null;
			}

			return ObjectMapper.Map<Employee>(emp);
		}

		public List<Position> QueryPosition()
		{
			return db.Query<Position>();
		}

		#endregion

		public SysUser GetUser(Guid userId)
		{
			return db.GetById<SysUser>(userId);
		}
	}
}
