//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ShortcutToolbarHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.DomainBase;

namespace Framework.HandlerBase
{
	public class ShortcutToolbarHandler : DbHandler
	{
		public List<ShortcutToolbar> QueryByUser(Guid userId)
		{
			if (ServiceContext.Test)
			{
				return new List<ShortcutToolbar>();
			}
			return db.Query<ShortcutToolbar>(string.Format(" and UserId = '{0}'", userId));
		}

		public void Add(string module)
		{
			new DbServiceContainer(db).Create<ShortcutToolbarService>().Add(module);
		}

		public void Remove(string module)
		{
			new DbServiceContainer(db).Create<ShortcutToolbarService>().Remove(module);
		}

		public void Save(List<ShortcutToolbar> toolbar)
		{
			new DbServiceContainer(db).Create<ShortcutToolbarService>().Save(toolbar);
		}
	}
}
