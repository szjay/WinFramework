//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Common;
using Framework.DomainBase;
using Infrastructure.Utilities;

namespace Framework.HandlerBase
{
	public class PermissionHandler : DbHandler
	{
		public DataPermissionTypes GetDataPermissionType(Guid userId)
		{
			return db.CreateService<PermissionService>().GetDataPermissionType(userId);
		}

		public List<PermissionAction> QueryActionByUser(Guid userId, string roleType)
		{
			string where = string.Format("and pr.RoleType = '{0}' and exists(select 1 from PermissionRoleUser pru where pru.RoleId = pr.RoleId and pru.UserId = '{1}')", roleType, userId);
			List<PermissionAction> list = db.Query<PermissionAction>(where);
			return list;
		}

		public List<PermissionAction> QueryActionByUser(Guid userId)
		{
			string where = string.Format("and exists(select 1 from PermissionRoleUser pru where pru.RoleId = pr.RoleId and pru.UserId = '{0}' )", userId);
			List<PermissionAction> list = db.Query<PermissionAction>(where);
			return list;
		}

		public List<Guid> QueryDeptByUser(Guid userId)
		{
			List<Guid> deptList = new List<Guid>();

			string where = "and exists(select 1 from PermissionRoleUser pru where pru.RoleId = pr.RoleId and pru.UserId = '{0}' and DataType = '部门')".Fmt(userId);
			List<PermissionData> dataList = db.Query<PermissionData>(where);

			foreach (PermissionData data in dataList)
			{
				if (deptList.Count(a => a == data.DataId) == 0)
				{
					deptList.Add(data.DataId);
				}
			}

			return deptList;
		}

		public List<PermissionData> QueryDataByRole(Guid roleId, string dataType)
		{
			string where = "and PermissionData.RoleId = '{0}' and PermissionData.DataType = '{1}'".Fmt(roleId, dataType);
			List<PermissionData> list = db.Query<PermissionData>(where);
			return list;
		}

		public List<PermissionRole> QueryRole()
		{
			return db.Query<PermissionRole>("");
		}

		public List<PermissionRole> QueryRoleByUser(Guid userId)
		{
			string where = "and exists(select 1 from PermissionRoleUser pru where pru.RoleId = PermissionRole.RoleId and pru.UserId = '{0}')".Fmt(userId);
			return db.Query<PermissionRole>(where);
		}

		public PermissionRole GetRole(Guid roleId)
		{
			return db.Get<PermissionRole>("and RoleId = '{0}'".Fmt(roleId));
		}

		public PermissionRole GetRoleWithItem(Guid roleId, string dataType)
		{
			PermissionRole role = db.GetById<PermissionRole>(roleId);

			string where1 = new QueryBuilder().Equal("pa.RoleId", roleId).ToString();
			role.Actions = db.Query<PermissionAction>(where1);

			string where2 = new QueryBuilder().Equal("PermissionData.RoleId", roleId).Equal("PermissionData.DataType", dataType).ToString();
			role.Data = db.Query<PermissionData>(where2);

			string where3 = new QueryBuilder().Equal("ru.RoleId", roleId).ToString();
			role.Users = db.Query<PermissionRoleUser>(where3);

			return role;
		}

		/**********************************************************/

		public void SaveWithItem(PermissionRole role)
		{
			new DbServiceContainer(db).Create<PermissionService>().SaveRole(role, true);
		}

		public void SaveRole(PermissionRole role)
		{
			new DbServiceContainer(db).Create<PermissionService>().SaveRole(role, false);
		}

		public void Save(PermissionRole role, List<PermissionData> dataList)
		{
			var tempList = db.PermissionData.Where(a => a.RoleId == role.RoleId);
			db.PermissionData.RemoveRange(tempList);

			foreach (PermissionData data in dataList)
			{
				db.PermissionData.Update(data);
			}

			db.Save();
		}

	}
}
