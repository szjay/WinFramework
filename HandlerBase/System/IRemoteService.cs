﻿using System;

namespace Framework.HandlerBase
{
	public interface IRemoteService
    {
        event EventHandler<MessageEventArgs> MessageReceived;
        void SendMessage(string message);
    }

    public class RemoteService : MarshalByRefObject, IRemoteService
    {
        public event EventHandler<MessageEventArgs> MessageReceived;

        public void SendMessage(string message)
        {
            MessageReceived?.Invoke(this, new MessageEventArgs(message));
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public string Message
        {
            get;
        }

        public MessageEventArgs(string message)
        {
            Message = message;
        }
    }
}
