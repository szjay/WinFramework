//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PubCodeHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Framework.DomainBase;
using Infrastructure.Utilities;

namespace Framework.HandlerBase
{
	public class PubCodeHandler : DbHandler
	{
		protected static ConcurrentDictionary<string, List<string>> cacheDict = new ConcurrentDictionary<string, List<string>>();

		public void ClearCahce(string codeType)
		{
			if (cacheDict.ContainsKey(codeType))
			{
				cacheDict.TryRemove(codeType, out _);
			}
		}

		public string[] GetNames()
		{
			//优先以文件列表为主。
			string path = Path.Combine(ServiceContext.CurrentPath, "PubCode.lst");
			if (File.Exists(path))
			{
				return File.ReadAllLines(path);
			}

			//如果没有文件列表，那么取PubCodeType。
			if (ServiceContext.PubCodeType != null)
			{
				List<string> list = new List<string>();
				foreach (FieldInfo fi in ServiceContext.PubCodeType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy))
				{
					string name = (string)fi.GetValue(null);
					list.Add(name);
				}
				return list.ToArray();
			}

			var r = db.PubCode.Select(a => a.CodeType).Distinct().ToArray();
			return r;
		}

		public string Get(string codeType)
		{
			string where = new QueryBuilder().Equal("CodeType", codeType).ToString();
			PubCode code = db.Query<PubCode>(where).FirstOrDefault();
			return code == null ? "" : code.CodeName;
		}

		public PubCode Get(string codeType, string codeName)
		{
			string where = new QueryBuilder()
				.Equal("CodeType", codeType)
				.Equal("CodeName", codeName)
				.ToString();
			return db.Get<PubCode>(where);
		}

		public List<PubCode> QueryByType(string codeType)
		{
			string where = new QueryBuilder().Equal("CodeType", codeType).ToString();
			return db.Query<PubCode>(where);
		}

		public List<string> QueryNameByType(string codeType)
		{
			if (!cacheDict.ContainsKey(codeType))
			{
				List<PubCode> list = db.Query<PubCode>(QueryBuilder.Eq("CodeType", codeType));
				cacheDict.TryAdd(codeType, list.Select(a => a.CodeName).ToList());
			}

			if (cacheDict.TryGetValue(codeType, out List<string> list2))
			{
				return list2;
			}
			return null;
		}

		public List<string> QueryNameByTypeOrderByRemark(string codeType)
		{
			if (!cacheDict.ContainsKey(codeType))
			{
				List<PubCode> list = db.Query<PubCode>(QueryBuilder.Eq("CodeType", codeType));
				cacheDict.TryAdd(codeType, list.OrderBy(a => a.Remark).Select(a => a.CodeName).ToList());
			}

			if (cacheDict.TryGetValue(codeType, out List<string> list2))
			{
				return list2;
			}
			return null;
		}

		public List<string> QueryNameByType(string codeType, int rowNum)
		{
			if (!cacheDict.ContainsKey(codeType))
			{
				string sql = $"select top {rowNum} * from PubCode where CodeType = '{codeType}' order by SeqNo";
				List<PubCode> list = db.SqlQuery<PubCode>(sql);
				cacheDict.TryAdd(codeType, list.Select(a => a.CodeName).ToList());
			}

			if (cacheDict.TryGetValue(codeType, out List<string> list2))
			{
				return list2;
			}
			return null;
		}

		public void Save(PubCode code)
		{
			if (cacheDict.ContainsKey(code.CodeType))
			{
				cacheDict.TryRemove(code.CodeType, out _);
			}

			new DbServiceContainer(db).Create<PubCodeService>().Save(code);
		}

		public void Delete(PubCode code)
		{
			if (cacheDict.ContainsKey(code.CodeType))
			{
				cacheDict.TryRemove(code.CodeType, out _);
			}

			new DbServiceContainer(db).Create<PubCodeService>().Delete(code);
		}

	}
}
