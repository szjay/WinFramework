//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ServiceContext.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Framework.Common;

namespace Framework.HandlerBase
{
	public static class ServiceContext
	{
		//注意，类的构造函数中调用CallContext.GetData只能返回null，所以必须在非构造函数中调用。
		public static bool ValidateCurrentUser()
		{
			SessionData sessionData = CallContext.GetData("SessionData") as SessionData;
			return sessionData != null && SessionCache.Exists(sessionData.SessionId);
		}

		public static bool ValidateCurrentUser(string sessionId)
		{
			UserSession session = SessionCache.Get(sessionId);
			if (session == null)
			{
				return false;
			}
			session.LastActiveTime = DateTime.Now;
			return true;
		}

		public static IUser GetUser(string sessionId)
		{
			UserSession session = SessionCache.Get(sessionId);
			if (session == null)
			{
				return null;
			}
			session.LastActiveTime = DateTime.Now;
			return session.User;
		}

		public static IUser ActiveUser(string sessionId, string method)
		{
			UserSession session = SessionCache.Get(sessionId);
			if (session == null)
			{
				return null;
			}
			session.LastActiveTime = DateTime.Now;

			if (!method.StartsWith("SessionHandler"))
			{
				session.LastCallTime = DateTime.Now;
				session.LastCallMethod = method;
			}

			return session.User;
		}

		public static bool ActiveCurrentSession()
		{
			UserSession session = GetCurrentSession();
			if (session == null)
			{
				//throw new ServiceException("Session异常：找不到当前Session");
				return false;
			}
			if (!session.Active())
			{
				RemoveSession(session.SessionId);
				return false;
			}

			return true;
		}

		//定时核查Session，移除超时的Session。
		public static void CheckSession()
		{
			SessionCache.Check();
		}

		private static UserSession GetCurrentSession()
		{
			SessionData sessionData = (SessionData)CallContext.GetData("SessionData");
			if (sessionData != null)
			{
				UserSession session = SessionCache.Get(sessionData.SessionId);
				return session;
			}
			return null;
		}

		public static IUser User
		{
			get
			{
				return GetCurrentUser();
			}
		}

		public static IUser GetCurrentUser()
		{
			UserSession session = GetCurrentSession();
			if (session != null)
			{
				return session.User;
			}
			return null;
		}

		public static Guid CurrentUserId
		{
			get
			{
				return GetCurrentUser().Id;
			}
		}

		public static string CurrentUserName
		{
			get
			{
				return GetCurrentUser().Name;
			}
		}

		public static UserSession CreateSession(IUser user, bool compulsory)
		{
			foreach (UserSession session in SessionCache.GetAllUser())
			{
				if (session.User.Id == user.Id)
				{
					if (compulsory) //如果是强制登录，那么删除重复的ID。
					{
						SessionCache.Remove(session.SessionId);
						break;
					}
					else
					{
						user.IP = session.IP;
						user.Name = "DuplicateId"; //标记为重复ID。
						return new UserSession()
						{
							SessionId = "DuplicateId",
							User = user
						};
					}
				}
			}

			UserSession newSession = new UserSession()
			{
				SessionId = Guid.NewGuid().ToString(),
				FirstActiveTime = DateTime.Now,
				IP = user.IP,
				User = user
			};

			user.SessionId = newSession.SessionId;

			SessionCache.Add(newSession);
			return newSession;
		}

		public static void RemoveSession(IUser user)
		{
			RemoveSession(user.SessionId);
		}

		public static void RemoveSession(string sessionId)
		{
			SessionCache.Remove(sessionId);
		}

		public static void MergeSession(List<UserSession> sessionList)
		{
			foreach (UserSession session in sessionList)
			{
				SessionCache.Add(session);
			}
		}

		public static string CurrentPath
		{
			get
			{
				return AppDomain.CurrentDomain.BaseDirectory;
				//return System.Environment.CurrentDirectory;
			}
		}

		public static string ServerVersion = "20190101.0000";

		public static bool Test = false; //服务端是否是测试模式。

		public static bool AuditedLog = false; //是否记录数据更新日志。

		public static string UsbKeyPassword = "";

		public static Action TimedTask; //定时任务，系统服务器每隔一分钟调用一次。

		public static Type PubCodeType = null;

	}
}
