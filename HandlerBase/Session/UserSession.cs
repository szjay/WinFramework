//==============================================================
//  版权所有：深圳杰文科技
//  文件名：UserSession.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using Framework.Common;

namespace Framework.HandlerBase
{
	[Serializable]
	public class UserSession
	{
		public const int TIMEOUT_THRESHOLD = 90; //Session超时阀值，默认为90秒，Client每隔60秒激活一次心跳

		public string SessionId;

		public IUser User = null;

		public DateTime FirstActiveTime; //第一次心跳时间。

		public DateTime LastActiveTime; //最后一次心跳时间。

		public DateTime LastCallTime; //最后一次调用（非心跳）时间。

		public string LastCallMethod; //最后一次调用（非心跳）方法。

		public string IP; //客户端的IP地址。

		public string OS; //客户端设备。

		public int AccessCount = 0; //访问计数，客户端每调用一次服务器，都加1。

		public bool Active()
		{
			if (FirstActiveTime == DateTime.MinValue || FirstActiveTime > DateTime.Now)
			{
				//throw new ServiceException("Session异常：FirstActiveTime错误");
				return false;
			}

			if (IsTimeout)
			{
				//throw new ServiceException("Session异常：Session超时");
				return false;
			}

			LastActiveTime = DateTime.Now;

			return true;
		}

		public bool IsTimeout
		{
			get
			{
				return DateTime.Now.Subtract(LastActiveTime).TotalSeconds > TIMEOUT_THRESHOLD;
			}
		}
	}
}
