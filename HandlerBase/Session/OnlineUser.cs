//==============================================================
//  版权所有：深圳杰文科技
//  文件名：OnlineUser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.HandlerBase
{
	[Serializable]
	public class OnlineUser
	{
		public OnlineUser()
		{
			this.TokenId = Guid.Empty;
			this.FirstActiveTime = DateTime.Now;
			this.LastActiveTime = DateTime.Now;
		}

		public Guid TokenId
		{
			get;
			set;
		}

		public string LoginAccount
		{
			get;
			set;
		}

		public string UserName
		{
			get;
			set;
		}

		public bool IsAdmin
		{
			get;
			set;
		}

		public DateTime? FirstActiveTime
		{
			get;
			set;
		}

		public DateTime? LastActiveTime
		{
			get;
			set;
		}

		public DateTime? LastCallTime
		{
			get;
			set;
		}

		public string LastCallMethod
		{
			get;
			set;
		}

		public string IP
		{
			get;
			set;
		}

		public DateTime LoginTime
		{
			get;
			set;
		}

		public int LoginCount
		{
			get;
			set;
		}
	}
}
