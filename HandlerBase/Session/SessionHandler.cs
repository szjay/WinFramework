//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SessionHandler.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using Framework.Common;

namespace Framework.HandlerBase
{
	public class SessionHandler : BaseHandler
	{
		//此接口已废弃，改用Heartbeat2。
		public DateTime Heartbeat()
		{
			if (!ServiceContext.ActiveCurrentSession())
			{
				throw new ServiceException("您已离线，请重新登录系统");
			}
			return DateTime.Now;
		}

		public HeartbeatResult Heartbeat2()
		{
			if (!ServiceContext.ActiveCurrentSession())
			{
				throw new ServiceException("您已离线，请重新登录系统");
			}

			HeartbeatResult hr = new HeartbeatResult()
			{
				Now = DateTime.Now,
				Code = 0
			};

			return hr;
		}

		public List<UserSession> Query()
		{
			return SessionCache.GetAllUser();
		}

		public List<OnlineUser> GetOnlineUserList()
		{
			List<OnlineUser> list = new List<OnlineUser>();
			foreach (UserSession session in SessionCache.GetAllUser())
			{
				OnlineUser user = new OnlineUser()
				{
					LoginAccount = session.User.Account,
					UserName = session.User.Name,
					FirstActiveTime = session.FirstActiveTime,
					LastActiveTime = session.LastActiveTime,
					LastCallTime = session.LastCallTime,
					LastCallMethod = session.LastCallMethod,
					IP = session.IP,
					IsAdmin = session.User.IsAdmin,
					LoginCount = session.User.LoginCount,
					LoginTime = session.User.LastLoginTime.GetValueOrDefault()
				};
				list.Add(user);
			}

			return list;
		}

		public void KickoutOnlineUser(string loginAccount)
		{
			foreach (UserSession session in SessionCache.GetAllUser())
			{
				if (session.User.Account == loginAccount)
				{
					SessionCache.Remove(session.SessionId);
					break;
				}
			}
		}

		public int Test()
		{
			string path = SessionCache.Save();
			Console.WriteLine($"Session Save：{path}");

			int cnt = SessionCache.Load();
			Console.WriteLine($"Session Load：{cnt}");
			return cnt;
		}

	}
}
