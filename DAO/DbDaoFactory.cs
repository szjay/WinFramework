//==============================================================
//  版权所有：深圳杰文科技
//  文件名：DbDaoFactory.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Utilities;

namespace Infrastructure.DAO
{
	public static class DbDaoFactory
	{
		public static IDbDao Create(DataSourceSetting.DbTypes dbType)
		{
			switch (dbType)
			{
				case DataSourceSetting.DbTypes.None:
					return null;

				case DataSourceSetting.DbTypes.SqlServer:
					return new SqlServerDao();

				case DataSourceSetting.DbTypes.Oracle:
					return new OracleDao();

				case DataSourceSetting.DbTypes.MySql:
					throw new NotImplementedException();
			}
			throw new NotImplementedException();
		}
	}
}
