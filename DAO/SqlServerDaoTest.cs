﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Utilities;

namespace Infrastructure.DAO
{
	public class SqlServerDaoTest
	{
		public void Test()
		{
			DataSourceSetting setting = new DataSourceSetting()
			{
				DbType = DataSourceSetting.DbTypes.SqlServer,
				Server = "localhost",
				User = "sa",
				Password = "123",
				DbName = "Platform"
			};
			IDbDao dao = DbDaoFactory.Create(DataSourceSetting.DbTypes.SqlServer);

			dao.BeginTransaction(setting.ConnectionString);

			Guid codeId = Guid.NewGuid();

			dao.Insert<PubCode>(setting.ConnectionString, new PubCode()
			{
				CodeId = codeId,
				CodeType = "TEST",
				SeqNo = 1,
				CodeName = "测试",
				Remark = "备注"
			});

			dao.Update<PubCode>(setting.ConnectionString, new PubCode()
			{
				CodeId = codeId,
				CodeType = "TEST",
				SeqNo = 2,
				CodeName = "测试",
				Remark = "备注2"
			});

			dao.CommitTransaction(setting.ConnectionString);
		}
	}

	public class PubCode
	{
		public Guid CodeId
		{
			get;
			set;
		}
		public string CodeType
		{
			get;
			set;
		}
		public int SeqNo
		{
			get;
			set;
		}
		public string CodeName
		{
			get;
			set;
		}
		public string Remark
		{
			get;
			set;
		}
	}
}
