//==============================================================
//  版权所有：深圳杰文科技
//  文件名：IDbDao.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Infrastructure.DAO
{
	public interface IDbDao : IDisposable
	{
		List<T> Select<T>(string connectionString, string where) where T : new();
		T FirstOrDefault<T>(string connectionString, string where) where T : new();
		int Insert<T>(string connectionString, T entity);
		int Update<T>(string connectionString, T entity);
		int Delete<T>(string connectionString, object id);
		void Proc(string connectionString, string procName, IEnumerable<DbParameter> parameters);
		bool Open(string connectionString, out string exception);
		bool BeginTransaction(string connectionString);
		bool CommitTransaction(string connectionString);
		string GetTimestampWhere(Type type, DateTime startTimestamp, DateTime endTimestamp);
	}
}
