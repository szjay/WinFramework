//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ClientConfig.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Infrastructure.Utilities;

namespace Framework.BaseUI
{
	[Serializable]
	public class ClientConfig
	{
		private static readonly string path = Path.Combine(System.Windows.Forms.Application.StartupPath, "ClientConfig.xml");

		public string LoginAccount
		{
			get;
			set;
		}

		public string Server
		{
			get;
			set;
		}

		#region - Load & Save -

		public static ClientConfig Load()
		{
			if (File.Exists(path))
			{
				return ObjectSerializer.XmlDeserialize<ClientConfig>(path);
			}
			return null;
		}

		public void Save()
		{
			ObjectSerializer.XmlSerialize(this, path);
		}

		public static Dictionary<string, string> LoadServers()
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();
			for (int i = 0; i < 5; i++)
			{
				string server = ConfigurationManager.AppSettings["server" + (i + 1).ToString()];
				if (string.IsNullOrEmpty(server))
				{
					continue;
				}

				string[] nameAndUrl = server.SplitArray('@');
				if (nameAndUrl.Length > 1)
				{
					dict.Add(nameAndUrl[0], nameAndUrl[1]);
				}
			}
			return dict;
		}

		#endregion
	}
}
