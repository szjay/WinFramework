//==============================================================
//  版权所有：深圳杰文科技
//  文件名：IBaseForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Framework.BaseUI
{
	public interface IBaseForm
	{
		bool ReceiveBroadcast(string message, object param);

		bool ManualCall
		{
			get;
			set;
		}

		string ModuleName
		{
			get;
			set;
		}

		//void ShowMsg(string msg, params object[] args);

		//void ShowMsg(object msg);

		#region - Form固有的事件、属性和方法 -

		event EventHandler Load;

		event EventHandler Shown;

		event EventHandler Activated;

		event FormClosedEventHandler FormClosed;

		event EventHandler Disposed;

		void Dispose();

		string Text
		{
			get;
			set;
		}

		FormBorderStyle FormBorderStyle
		{
			get;
			set;
		}

		Form MdiParent
		{
			get;
			set;
		}

		FormWindowState WindowState
		{
			get;
			set;
		}

		Size Size
		{
			get;
			set;
		}

		bool IsDisposed
		{
			get;
		}

		bool Disposing
		{
			get;
		}

		bool IsAccessible
		{
			get;
		}

		void Show();

		void Show(IWin32Window owner);

		void Hide();

		void BringToFront();

		DialogResult ShowDialog();

		void Close();

		DialogResult DialogResult
		{
			get;
			set;
		}

		#endregion

	}
}
