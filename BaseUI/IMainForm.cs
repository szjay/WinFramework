//==============================================================
//  版权所有：深圳杰文科技
//  文件名：IMainForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Framework.Common;

namespace Framework.BaseUI
{
	//MDI窗体的主窗体接口。
	public interface IMainForm
	{
		/// <summary>
		/// 弹出对话框
		/// </summary>
		void ShowPopForm(Form destForm);

		/// <summary>
		/// 最小化
		/// </summary>
		void MinimizeForm();

		Type InstanceType
		{
			get;
		}

		IList<IBaseForm> FormList
		{
			get;
		}

		int OpenFormsCount
		{
			get;
		}

		void Close();

		void AddShortcutButton(ModuleInfo module);

		void RemoveShortcutButton(ModuleInfo module);

		event EventHandler Shown;
	}
}
