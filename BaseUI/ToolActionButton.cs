//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ToolActionButton.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using DevExpress.XtraBars;
using DevExpress.Utils;

namespace Framework.BaseUI
{
	public class ToolActionButton : ToolStripButton
	{
		public string MethodName; //按钮所对应的方法名称
		public bool IsPrivilege = true; //是否受权限管理
		public MethodInvoker MethodDelegate; //需要调用的方法
		public MethodInfo Method;
		public bool HasPrivilege = false;
		public string ModuleName; //模块名称用节点分隔
	}

	public class BarActionButton : BarButtonItem
	{
		public string MethodName; //按钮所对应的方法名称
		public bool IsPrivilege = true; //是否受权限管理
		public MethodInvoker MethodDelegate; //需要调用的方法
		public MethodInfo Method;
		public bool HasPrivilege = false; //是否有权限（运行时使用）
		public string ModuleName; //模块名称用节点分隔
		public bool IsSystem = false; //是否是系统按钮（例如关闭按钮）

		private string tooltip; //提示
		public string Tooltip
		{
			get
			{
				return tooltip;
			}
			set
			{
				tooltip = value;

				ToolTipItem toolTipItem1 = new ToolTipItem();
				toolTipItem1.Text = tooltip;

				SuperToolTip superToolTip1 = new SuperToolTip();
				superToolTip1.Items.Add(toolTipItem1);

				this.SuperTip = superToolTip1;
			}
		}

	}
}
