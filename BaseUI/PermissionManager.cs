//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PermissionManager.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework.BaseUI
{
	public class PermissionManager
	{
		public string RoleType = "";

		private Dictionary<string, string> _PermissionList = new Dictionary<string, string>();

		private PermissionManager()
		{
		}

		private static readonly object objectLocker = new object();

		private static PermissionManager instance = null;

		public static PermissionManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (objectLocker)
					{
						if (instance == null)
						{
							instance = new PermissionManager();
						}
					}
				}
				return instance;
			}
		}

		public void Clear()
		{
			_PermissionList.Clear();
		}

		public void Add(IEnumerable<string> permissionList)
		{
			foreach (string permission in permissionList)
			{
				Add(permission);
			}
		}

		public void Add(string permission)
		{
			if (!_PermissionList.ContainsKey(permission))
			{
				_PermissionList.Add(permission, "");
			}
		}

		public bool HasPermission(string module, string action)
		{
			string s = module + "." + action;
			ClientContext.LastestAction = s;
			if (ClientContext.User.IsAdmin)
			{
				return true;
			}
			bool r = _PermissionList.ContainsKey(s);
			return r;
		}

		public bool HasPermission(ModuleInfo moduleInfo, string action)
		{
			//如果模块是系统模块或固定模块，那么它的按钮不受权限管理
			if (moduleInfo.Attribute.Fix || moduleInfo.Attribute.SystemModule)
			{
				return true;
			}

			return this.HasPermission(moduleInfo.Attribute.Name, action);
		}

	}
}
