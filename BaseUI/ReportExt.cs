//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ReportExt.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：2021-9-26 11:04
//  修改说明：增加获得报表配置的打印机名称 
//==============================================================

using System;
using System.Linq;
using DevExpress.XtraReports.UI;
using Infrastructure.Utilities;

namespace Framework.BaseUI
{
	public static class ReportExt
	{
		//直接打印，适用于强制不预览的批量打印。
		public static void DirectPrint(this XtraReport report)
		{
			ReportAttribute attr = report.GetType().GetAttribute<ReportAttribute>();
			if (attr == null) //如果没有指定打印配置，那么输出到默认打印机。
			{
				report.Print();
				return;
			}

			InitPrinterSetting(report, attr.Name);
			report.Print();
		}

		public static void Show(this XtraReport report)
		{
			ReportAttribute attr = report.GetType().GetAttribute<ReportAttribute>();
			if (attr == null) //如果没有指定打印配置，那么输出到默认打印机，并弹出预览。
			{
				report.ShowPreviewDialog();
				return;
			}

			PrinterSetting printerSetting = InitPrinterSetting(report, attr.Name);
			if (printerSetting == null || printerSetting.Preview)
			{
				report.ShowPreviewDialog();
			}
			else
			{
				report.Print();
			}
		}

		private static PrinterSetting InitPrinterSetting(XtraReport report, string reportName)
		{
			PrinterSetting printerSetting = ClientContext.PrinterSettingList.FirstOrDefault(a => a.ReportName == reportName);
			if (printerSetting == null)
			{
				return null;
			}

			report.PrinterName = printerSetting.Printer;

			//优先使用用户设置的打印边距
			report.Margins = new System.Drawing.Printing.Margins(
				printerSetting.MarginsLeft > 0 ? printerSetting.MarginsLeft : report.Margins.Left,
				printerSetting.MarginsRight > 0 ? printerSetting.MarginsRight : report.Margins.Right,
				printerSetting.MarginsTop > 0 ? printerSetting.MarginsTop : report.Margins.Top,
				printerSetting.MarginsBottom > 0 ? printerSetting.MarginsBottom : report.Margins.Bottom);

			return printerSetting;
		}

		//获得报表的配置打印机
		public static string GetPrinterName(Type reportType)
		{
			ReportAttribute attr = reportType.GetAttribute<ReportAttribute>();
			if (attr == null)
			{
				return null;
			}

			PrinterSetting printerSetting = ClientContext.PrinterSettingList.FirstOrDefault(a => a.ReportName == attr.Name);
			if (printerSetting == null)
			{
				return null;
			}

			return printerSetting.Printer;
		}

	}
}
