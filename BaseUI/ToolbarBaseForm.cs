//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ToolbarBaseForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Infrastructure.Components;

namespace Framework.BaseUI
{
	public partial class ToolbarBaseForm : BaseForm
	{
		private static Dictionary<string, Image> _ImageList = null;

		protected Toolbar Toolbar = null;

		public static bool AutoSaveLayout = false;

		public ToolbarBaseForm()
		{
			InitializeComponent();
			this.Load += new EventHandler(ToolbarBaseForm_Load);
		}

		void ToolbarBaseForm_Load(object sender, EventArgs e)
		{
			if (!this.DesignMode)
			{
				LoadImages();
				BuildToolbar();

				this.SetStyle();

				this.Shown += ToolbarBaseForm_Shown;
				this.KeyDown += ToolbarBaseForm_KeyDown;
				this.FormClosing += ToolbarBaseForm_FormClosing;
			}
		}

		void ToolbarBaseForm_Shown(object sender, EventArgs e)
		{
			if (AutoSaveLayout)
			{
				this.LoadStyle();
			}
		}

		void ToolbarBaseForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (AutoSaveLayout)
			{
				this.SaveStyle();
			}
		}

		protected Toolbar ToolbarManager
		{
			get
			{
				return this.Toolbar;
			}
		}

		#region - 生成窗体上的工具栏 -

		protected virtual void BuildToolbar()
		{
			if (this.Toolbar == null)
			{
				this.Toolbar = new Toolbar(this.MainToolbar);
				this.barManager.Images = this.ToolbarImageList;
				this.Toolbar.ImageList = this.ToolbarImageList;
				this.Toolbar.BarManager = this.barManager;
				this.Toolbar.Form = this;

				Font font = new Font(ClientContext.FontName, (float)ClientContext.FontSize);
				BarAndDockingController barAndDockingController = new BarAndDockingController();
				barAndDockingController.AppearancesBar.ItemsFont = font;
				this.Toolbar.BarManager.Controller = barAndDockingController;

				this.Toolbar.BuildToolbar();
			}
		}

		[Action(Name = "默认布局", Image = Toolbar.wizard, Permission = false, BeginGroup = true, Order = 1001)]
		protected virtual void RestoreCustomStyle()
		{
			if (!MsgBox.Confirm("您确定要恢复默认布局吗？"))
			{
				return;
			}

			this.ClearStyle();
			MsgBox.Show("默认布局恢复成功，请重新打开当前窗体");
		}

		[Action(Name = "关闭", Permission = false, Order = 1002)]
		protected virtual void CloseForm() //工具栏上的关闭窗体是无条件必须有的
		{
			this.Close();
		}

		#endregion

		protected virtual void EndEdit()
		{
			lblTitle.Focus();
		}

		private void LoadImages()
		{
			if (_ImageList == null)
			{
				_ImageList = new Dictionary<string, Image>();

				string path = Path.Combine(ClientContext.ApplicationPath, "Images");
				if (!Directory.Exists(path))
				{
					return;
				}

				foreach (string file in Directory.GetFiles(path, "*.png"))
				{
					_ImageList.Add(Path.GetFileNameWithoutExtension(file), Bitmap.FromFile(file));
				}
			}

			foreach (KeyValuePair<string, Image> kvp in _ImageList)
			{
				ToolbarImageList.AddImage(kvp.Value, kvp.Key);
			}

		}

		void ToolbarBaseForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				if (this.Modal)
				{
					this.DialogResult = DialogResult.Cancel;
				}
			}
		}

	}
}
