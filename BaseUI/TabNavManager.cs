//==============================================================
//  版权所有：深圳杰文科技
//  文件名：TabNavManager.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Windows.Forms;
using DevExpress.XtraTab;

namespace Framework.BaseUI
{
	public class TabNavManager
	{
		public TabNavManager()
		{
		}

		private XtraTabControl tabNav;

		public XtraTabControl TabNav
		{
			get
			{
				return tabNav;
			}
			set
			{
				tabNav = value;
				ContextMenuStrip menu = CreatContextMenuStrip();
				tabNav.ContextMenuStrip = menu;
			}
		}

		//初始化导航栏，navForm是导航窗体
		public void InitTabNav(Form navForm)
		{
			tabNav.TabPages.Clear();
			tabNav.SelectedPageChanged += new TabPageChangedEventHandler(tabNav_SelectedPageChanged);

			if (navForm != null)
			{
				XtraTabPage page = new XtraTabPage();
				page.Text = "模块导航";
				page.Tag = navForm;
				navForm.Show();
				navForm.WindowState = FormWindowState.Maximized;
				tabNav.TabPages.Add(page);
				page.Tooltip = string.Format("用鼠标“左键”点击这里，可以打开“{0}”窗口；\r\n“右键”点击这里，可以选择关闭该标签页。", page.Text);
			}
		}

		private void tabNav_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
		{
			if (tabNav.SelectedTabPage == null || tabNav.SelectedTabPage.Tag == null)
			{
				return;
			}
			Form f = (Form)tabNav.SelectedTabPage.Tag;
			f.BringToFront();
			//f.Visible = true;
			f.Activate();
		}

		public void Add(IBaseForm view)
		{
			if (tabNav == null)
			{
				return;
			}
			XtraTabPage page = new XtraTabPage();
			page.Text = view.Text;
			page.Tag = view;

			tabNav.TabPages.Add(page);
			tabNav.SelectedTabPage = page;
			page.Tooltip = string.Format("用鼠标“左键”点击这里，可以打开“{0}”窗口；\r\n“右键”点击这里，可以选择关闭该标签页。", view.Text);
		}

		private ContextMenuStrip CreatContextMenuStrip()
		{
			ContextMenuStrip menu = new ContextMenuStrip();
			menu.Items.AddRange(new ToolStripItem[] { 
				new ToolStripMenuItem("增加到快捷工具栏"), 
				new ToolStripMenuItem("从快捷工具栏移除"), 
				new ToolStripSeparator(),
				new ToolStripMenuItem("关闭标签页"), 
				new ToolStripMenuItem("关闭其他标签页"),
				new ToolStripMenuItem("关闭全部标签页")
			});

			foreach (ToolStripItem item in menu.Items)
			{
				item.Click += new EventHandler(item_Click);
			}
			menu.Opening += new System.ComponentModel.CancelEventHandler(menu_Opening);
			return menu;
		}

		void menu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (tabNav.SelectedTabPage.Text == "模块导航")
			{
				ContextMenuStrip menu = sender as ContextMenuStrip;
				foreach (ToolStripItem item in menu.Items)
				{
					if (item.Text == "关闭标签页")
					{
						item.Visible = false;
					}
				}
			}
			else
			{
				ContextMenuStrip menu = sender as ContextMenuStrip;
				foreach (ToolStripItem item in menu.Items)
				{
					if (item.Text == "关闭标签页")
					{
						item.Visible = true;
					}
				}
			}
		}

		void item_Click(object sender, EventArgs e)
		{
			ToolStripItem item = sender as ToolStripItem;
			IBaseForm view = tabNav.SelectedTabPage.Tag as IBaseForm;

			switch (item.Text)
			{
				case "增加到快捷工具栏":
					{
						ModuleInfo moduleInfo = ModuleParser.Instance.GetModuleInfo(view.GetType().FullName);
						ModuleLoader.Instance.MainForm.AddShortcutButton(moduleInfo);
					}
					break;

				case "从快捷工具栏移除":
					{
						ModuleInfo moduleInfo = ModuleParser.Instance.GetModuleInfo(view.GetType().FullName);
						ModuleLoader.Instance.MainForm.RemoveShortcutButton(moduleInfo);
					}
					break;

				case "关闭标签页":
					CloseMdiForm(view);
					break;

				case "关闭其他标签页":
					for (int i = tabNav.TabPages.Count - 1; i > -1; i--)
					{
						XtraTabPage page = tabNav.TabPages[i];
						if (page.Text == "模块导航")
						{
							continue;
						}

						IBaseForm f1 = page.Tag as IBaseForm;
						if (f1.GetType() != view.GetType())
						{
							f1.Close();
						}
					}
					ActivateMdiForm(view);
					break;

				case "关闭全部标签页":
					for (int i = tabNav.TabPages.Count - 1; i > -1; i--)
					{
						XtraTabPage page = tabNav.TabPages[i];
						if (page.Text == "模块导航")
						{
							continue;
						}

						Form f1 = page.Tag as Form;
						f1.Close();
					}
					break;
			}
		}

		public void ActivateMdiForm(IBaseForm mdiForm)
		{
			if (tabNav == null)
			{
				return;
			}
			if (tabNav.SelectedTabPage != null && tabNav.SelectedTabPage.Tag != mdiForm)
			{
				foreach (XtraTabPage page in tabNav.TabPages)
				{
					if (page.Tag == mdiForm)
					{
						tabNav.SelectedTabPage = page;
						break;
					}
				}
			}
		}

		public void CloseMdiForm(IBaseForm mdiForm)
		{
			XtraTabPage leftPage = null;
			if (tabNav == null)
			{
				return;
			}
			foreach (XtraTabPage page in tabNav.TabPages)
			{
				if (page.Tag == mdiForm)
				{
					if (mdiForm != null && !mdiForm.IsDisposed && mdiForm.IsAccessible)
					{
						mdiForm.Close();  //先关闭窗口，后切换页面
					}

					if (leftPage != null)
					{
						tabNav.SelectedTabPage = leftPage;
					}
					tabNav.TabPages.Remove(page);

					break;
				}
				leftPage = page;
			}
		}

		public bool ExistsForm(IBaseForm form)
		{
			if (tabNav == null)
			{
				return false;
			}
			foreach (XtraTabPage page in tabNav.TabPages)
			{
				if (page.Tag == form)
				{
					return true;
				}
			}
			return false;
		}

	}
}
