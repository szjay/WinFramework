//==============================================================
//  版权所有：深圳杰文科技
//  文件名：RootModuleAttribute.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseUI
{
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
	public class RootModuleAttribute : Attribute
	{
		//根模块名称，它作为当前DLL中所有模块(Form)的根模块
		public string Name;

		//是否分析此DLL文件中的模块
		public bool AnalyseModule = true;

		//序号，从小到大排序
		public float Order = 100f;

		//是否显示在菜单中
		public bool ShowInMenu = true;
	}
}