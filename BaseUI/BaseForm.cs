//==============================================================
//  版权所有：深圳杰文科技
//  文件名：BaseForm.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Framework.BaseUI
{
	public partial class BaseForm : XtraForm, IBaseForm
	{
		public BaseForm()
		{
			InitializeComponent();
			this.Load += BaseForm_Load;
		}

		void BaseForm_Load(object sender, EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.StartPosition = FormStartPosition.CenterScreen;
			}
		}

		public bool ReceiveBroadcast(string message, object param)
		{
			return true;
		}

		//是否是手工调用。用户从系统菜单或者导航按钮调用模块，称为“自动调用”，而从别的入口调用，则为“手工调用”。
		public bool ManualCall
		{
			get;
			set;
		}

		public string ModuleName
		{
			get;
			set;
		}

	}
}
