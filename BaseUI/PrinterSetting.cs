//==============================================================
//  版权所有：深圳杰文科技
//  文件名：PrinterSetting.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevExpress.XtraReports.UI;
using Infrastructure.Utilities;

namespace Framework.BaseUI
{
	[Serializable]
	public class PrinterSetting
	{
		private const string FileName = "PrinterSetting.xml";

		public static void Save(List<PrinterSetting> settingList)
		{
			string path = Path.Combine(ClientContext.LocalPath, FileName);
			ObjectSerializer.XmlSerializeList<PrinterSetting>(settingList, FileName);
			ClientContext.PrinterSettingList = settingList;
		}

		public static void Load()
		{
			string path = Path.Combine(ClientContext.LocalPath, FileName);
			if (File.Exists(path))
			{
				ClientContext.PrinterSettingList = ObjectSerializer.XmlDeserializeList<PrinterSetting>(FileName);
			}
			else
			{
				ClientContext.PrinterSettingList = new List<PrinterSetting>();
			}
		}

		/*****************************************************************/

		public string ReportName
		{
			get;
			set;
		}

		public string Printer
		{
			get;
			set;
		}

		public bool Preview
		{
			get;
			set;
		}

		/// <summary>
		/// 左边距，单位十分之一毫米。
		/// </summary>
		public int MarginsLeft
		{
			get;
			set;
		}

		public int MarginsRight
		{
			get;
			set;
		}

		public int MarginsTop
		{
			get;
			set;
		}

		public int MarginsBottom
		{
			get;
			set;
		}
	}
}
