//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ModuleAttribute.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Framework.BaseUI
{
	[AttributeUsage(AttributeTargets.Class)]
	public class ModuleAttribute : Attribute
	{
		public ModuleAttribute()
		{
		}

		public ModuleAttribute(string name)
		{
			this.Name = name;
		}

		public ModuleAttribute(string name, double order)
		{
			this.Name = name;
			this.Order = order;
		}

		public ModuleAttribute(string name, string rootModule)
		{
			this.Name = name;
			this.RootModule = rootModule;
		}

		public ModuleAttribute(string name, string rootModule, double order)
		{
			this.Name = name;
			this.RootModule = rootModule;
			this.Order = order;
		}

		/// <summary>
		/// 模块名称，上级模块名称与下级模块名称之间用“.”分隔
		/// </summary>
		public string Name = "";

		/// <summary>
		/// 根模块名称，如果为空，那么取当前DLL的模块为根模块
		/// </summary>
		public string RootModule = "";

		/// <summary>
		/// 是否在菜单中显示
		/// </summary>
		public bool ShowInMenu = true;

		/// <summary>
		/// 菜单快捷键
		/// </summary>
		public Keys ShortcutKey = Keys.None;

		/// <summary>
		/// 排序，小号排在前面
		/// </summary>
		public double Order = 100;

		/// <summary>
		/// 是否是弹出窗体
		/// </summary>
		public bool Pop = false;

		/// <summary>
		/// 图片
		/// </summary>
		public string Image = null;

		/// <summary>
		/// 是否是系统模块
		/// </summary>
		public bool SystemModule = false;

		/// <summary>
		/// 模块的权限，权限名称使用中文，多个名称之间用逗号分隔
		/// </summary>
		public string ModulePermission = "";

		/// <summary>
		/// 是否属于权限管理。
		/// </summary>
		public bool Permission = true;

		/// <summary>
		/// 固定显示的模块，任何人不需要分配权限即可使用。
		/// 此属性常用于about窗体、关闭窗体菜单。
		/// </summary>
		public bool Fix = false;

		/// <summary>
		/// 模块前面是否有水平分隔条
		/// </summary>
		public bool HR = false;

		/// <summary>
		/// 此模块是否有UI
		/// </summary>
		public bool UI = true;

	}
}
