//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ActionAttribute.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;

namespace Framework.BaseUI
{
	[AttributeUsage(AttributeTargets.Method)]
	public class ActionAttribute : Attribute
	{
		public ActionAttribute()
		{
		}

		public ActionAttribute(string name)
		{
			this.Name = name;
		}

		public ActionAttribute(string name, string image)
		{
			this.Name = name;
			this.Image = image;
		}

		/// <summary>
		/// 方法本身的信息
		/// </summary>
		public MethodInfo Method;

		/// <summary>
		/// 方法的名称，用于显示在权限分配模块中，如果未赋值，默认为方法本身的名称
		/// </summary>
		public string Name;

		/// <summary>
		/// 标题
		/// </summary>
		public string Caption;

		/// <summary>
		/// 是否属于权限管理，默认为接受管理
		/// </summary>
		public bool Permission = true;

		/// <summary>
		/// 方法所对应的按钮的快捷键，默认为没有快捷键
		/// </summary>
		public Keys ShortcutKey = Keys.None;

		/// <summary>
		/// 方法所对应的按钮的图片，默认取方法名为图片名，如果需要不显示图片，那么请赋值为空字符串
		/// </summary>
		public string Image = null;

		/// <summary>
		/// 方法出现在工具栏上的顺序，默认为-1，表示放在有Sequence值的按钮之后
		/// </summary>
		public double Order = -1;

		/// <summary>
		/// 是否开始分组
		/// </summary>
		public bool BeginGroup = false;

		/// <summary>
		/// 图片在按钮的位置，默认为文本在图片的下面
		/// </summary>
		public TextImageRelation TextImageRelation = TextImageRelation.ImageAboveText;

		//方法所对应的按钮，是否接受窗体状态管理，默认不接受管理
		//如果为Edit，表示此按钮只有在窗体是编辑状态时，才有效，如Save按钮；
		//如果为View，表示此按钮在窗体是浏览状态时，才有效，如Add按钮
		//public FormStateType ViewState = FormStateType.None;

		/// <summary>
		/// 是否是下拉选择的按钮
		/// </summary>
		public bool SubItem = false;

		/// <summary>
		/// 父下拉按钮名称，暂时最多只支持两级
		/// </summary>
		public string ParentItem = "";

		//按钮的尺寸，默认为64 * 32
		//public Size Size = new Size(64, 32);

		/// <summary>
		/// 按钮的宽度，默认为-1，表示让系统根据情况调控；如果为0，表示自动宽度
		/// </summary>
		public int Width = -1;

		/// <summary>
		/// 是否显示在工具栏中，默认显示；不显示在工具栏中，多数为需要接受权限分配，但无法显示在界面上的功能
		/// </summary>
		public bool ShowInToolbar = true;
	}

}
