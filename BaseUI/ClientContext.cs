//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ClientContext.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using Framework.Common;

namespace Framework.BaseUI
{
	public static class ClientContext
	{
		//前台与后台的会话ID。
		public static string SessionId
		{
			get;
			set;
		}

		public static bool Testing = false; //是否是测试环境。

		public static Action TimedTask; //定时任务，系统服务器每发送一次心跳包后调用一次。

		public static string ApplicationPath = Application.StartupPath;

		public static string ApplicationCode = ConfigurationManager.AppSettings["ApplicationCode"];

		public static string ApplicationName = "管理信息系统";

		public static string ApplicationVersion = "V1.9";

		public static string CompanyName = "技术支持：深圳杰文科技";

		public static string CompanyTip = "QQ：85363208";

		public static INavigationForm NavigationForm
		{
			get;
			set;
		}

		public static ISystemMessageHint SystemMessageHint
		{
			get;
			set;
		}

		public static string Server = ""; //服务器URL地址。

		public static IUser User = null; //当前登录用户。

		public static int UdpPort = -1; //UDP广播消息的接收端口。

		public static readonly string TempPath = Environment.GetFolderPath(Environment.SpecialFolder.Templates);

		public static readonly string LocalPath = Application.StartupPath;

		#region - 时间服务 -

		/// <summary>
		/// 上次设置的服务器时间
		/// </summary>
		private static DateTime lastServerTime;

		/// <summary>
		/// 本地时间
		/// </summary>
		private static DateTime lastLocalTime;

		/// <summary>
		/// 获取或设置当前时间(与服务器时间同步)，精确到毫秒。
		/// </summary>
		public static DateTime Now
		{
			get
			{
				if (lastServerTime == DateTime.MinValue)
				{
					return DateTime.Now;
				}
				else
				{
					//服务器当前时间 = 上一次服务器返回的时间 + 返回之后的时间
					return lastServerTime.AddMilliseconds(DateTime.Now.Subtract(lastLocalTime).TotalMilliseconds);
				}
			}
			set
			{
				lastLocalTime = DateTime.Now;
				lastServerTime = value;
			}
		}

		/// <summary>
		/// 获得与服务器同步的当前时间，精确到分钟（秒和毫秒为0）。
		/// </summary>
		public static DateTime Now2
		{
			get
			{
				DateTime now = ClientContext.Now;
				return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, 0);
			}
		}

		#endregion

		//预备登录，通过CallContext把登录信息传递给Remoting。
		public static void SetSessionData()
		{
			if (ClientContext.User == null)
			{
				return;
			}

			CallContext.SetData("SessionData", new SessionData()
			{
				SessionId = ClientContext.SessionId,
				Account = ClientContext.User.Account
			});
		}

		public static string SkinName = "Office 2013 Dark Gray";

		public static string FontName = "Tahoma";

		public static int FontSize = 9;

		private static List<PrinterSetting> _PrinterSettingList = null;

		public static List<PrinterSetting> PrinterSettingList
		{
			get
			{
				if (_PrinterSettingList == null)
				{
					PrinterSetting.Load();
				}

				return _PrinterSettingList;
			}
			set
			{
				_PrinterSettingList = value;
			}
		}

		public static string UsbkeyAccount = ""; //UsbKey登录账号。

		public static string UsbkeyPassword = ""; //UsbKey登录密码。

		public static string ClientUpgradeHintMsg = "发现新版本，您确定从当前版本{0}升级到新版本{1}吗？\r\n点击[是]升级系统，点击[否]不升级并退出系统。";

		public static string LastestAction = ""; //最后一次调用的权限方法。

		public static event Action MainFormClosed;

		public static void RaiseMainFormClosed()
		{
			if (MainFormClosed != null)
			{
				MainFormClosed();
			}
		}
	}
}
