﻿namespace Framework.BaseUI
{
	partial class ToolbarBaseForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolbarBaseForm));
			this.barManager = new DevExpress.XtraBars.BarManager(this.components);
			this.MainToolbar = new DevExpress.XtraBars.Bar();
			this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.lblTitle = new System.Windows.Forms.Label();
			this.ToolbarImageList = new DevExpress.Utils.ImageCollection(this.components);
			((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ToolbarImageList)).BeginInit();
			this.SuspendLayout();
			// 
			// barManager
			// 
			this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.MainToolbar});
			this.barManager.DockControls.Add(this.barDockControlTop);
			this.barManager.DockControls.Add(this.barDockControlBottom);
			this.barManager.DockControls.Add(this.barDockControlLeft);
			this.barManager.DockControls.Add(this.barDockControlRight);
			this.barManager.Form = this;
			this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
			this.barManager.MaxItemId = 1;
			// 
			// MainToolbar
			// 
			this.MainToolbar.BarName = "Tools";
			this.MainToolbar.DockCol = 0;
			this.MainToolbar.DockRow = 0;
			this.MainToolbar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.MainToolbar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
			this.MainToolbar.Text = "Tools";
			// 
			// barButtonItem1
			// 
			this.barButtonItem1.Caption = "测试(T)";
			this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
			this.barButtonItem1.Id = 0;
			this.barButtonItem1.Name = "barButtonItem1";
			// 
			// barDockControlTop
			// 
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.barDockControlTop.Size = new System.Drawing.Size(1206, 63);
			// 
			// barDockControlBottom
			// 
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 663);
			this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.barDockControlBottom.Size = new System.Drawing.Size(1206, 0);
			// 
			// barDockControlLeft
			// 
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 63);
			this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 600);
			// 
			// barDockControlRight
			// 
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(1206, 63);
			this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 600);
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTitle.AutoSize = true;
			this.lblTitle.Location = new System.Drawing.Point(1206, 0);
			this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(0, 22);
			this.lblTitle.TabIndex = 4;
			// 
			// ToolbarImageList
			// 
			this.ToolbarImageList.ImageSize = new System.Drawing.Size(32, 32);
			this.ToolbarImageList.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ToolbarImageList.ImageStream")));
			this.ToolbarImageList.TransparentColor = System.Drawing.Color.Transparent;
			this.ToolbarImageList.InsertGalleryImage("add", "images/actions/addfile_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/addfile_32x32.png"), 0);
			this.ToolbarImageList.Images.SetKeyName(0, "add");
			this.ToolbarImageList.InsertGalleryImage("ok", "images/actions/apply_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_32x32.png"), 1);
			this.ToolbarImageList.Images.SetKeyName(1, "ok");
			this.ToolbarImageList.InsertGalleryImage("cancel", "images/actions/cancel_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_32x32.png"), 2);
			this.ToolbarImageList.Images.SetKeyName(2, "cancel");
			this.ToolbarImageList.InsertGalleryImage("closeform", "images/actions/close_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/close_32x32.png"), 3);
			this.ToolbarImageList.Images.SetKeyName(3, "closeform");
			this.ToolbarImageList.InsertGalleryImage("refreshdata", "images/actions/convert_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/convert_32x32.png"), 4);
			this.ToolbarImageList.Images.SetKeyName(4, "refreshdata");
			this.ToolbarImageList.InsertGalleryImage("delete", "images/actions/deletelist2_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/deletelist2_32x32.png"), 5);
			this.ToolbarImageList.Images.SetKeyName(5, "delete");
			this.ToolbarImageList.InsertGalleryImage("open", "images/actions/loadfrom_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/loadfrom_32x32.png"), 6);
			this.ToolbarImageList.Images.SetKeyName(6, "open");
			this.ToolbarImageList.InsertGalleryImage("redo", "images/actions/refresh2_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh2_32x32.png"), 7);
			this.ToolbarImageList.Images.SetKeyName(7, "redo");
			this.ToolbarImageList.InsertGalleryImage("remove", "images/actions/removeitem_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/removeitem_32x32.png"), 8);
			this.ToolbarImageList.Images.SetKeyName(8, "remove");
			this.ToolbarImageList.InsertGalleryImage("barcode", "images/content/barcode_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/barcode_32x32.png"), 9);
			this.ToolbarImageList.Images.SetKeyName(9, "barcode");
			this.ToolbarImageList.InsertGalleryImage("audit", "images/content/checkbox_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/checkbox_32x32.png"), 10);
			this.ToolbarImageList.Images.SetKeyName(10, "audit");
			this.ToolbarImageList.InsertGalleryImage("image", "images/content/image_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/image_32x32.png"), 11);
			this.ToolbarImageList.Images.SetKeyName(11, "image");
			this.ToolbarImageList.InsertGalleryImage("remark", "images/content/notes_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/notes_32x32.png"), 12);
			this.ToolbarImageList.Images.SetKeyName(12, "remark");
			this.ToolbarImageList.InsertGalleryImage("summary", "images/data/summary_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/data/summary_32x32.png"), 13);
			this.ToolbarImageList.Images.SetKeyName(13, "summary");
			this.ToolbarImageList.InsertGalleryImage("modify", "images/edit/edit_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_32x32.png"), 14);
			this.ToolbarImageList.Images.SetKeyName(14, "modify");
			this.ToolbarImageList.InsertGalleryImage("export", "images/export/exporttoxls_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/export/exporttoxls_32x32.png"), 15);
			this.ToolbarImageList.Images.SetKeyName(15, "export");
			this.ToolbarImageList.InsertGalleryImage("find", "images/find/find_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/find/find_32x32.png"), 16);
			this.ToolbarImageList.Images.SetKeyName(16, "find");
			this.ToolbarImageList.InsertGalleryImage("upload", "images/navigation/up_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/up_32x32.png"), 17);
			this.ToolbarImageList.Images.SetKeyName(17, "upload");
			this.ToolbarImageList.InsertGalleryImage("user", "images/people/customer_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/customer_32x32.png"), 18);
			this.ToolbarImageList.Images.SetKeyName(18, "user");
			this.ToolbarImageList.InsertGalleryImage("usergroup", "images/people/team_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/team_32x32.png"), 19);
			this.ToolbarImageList.Images.SetKeyName(19, "usergroup");
			this.ToolbarImageList.InsertGalleryImage("preview", "images/print/preview_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_32x32.png"), 20);
			this.ToolbarImageList.Images.SetKeyName(20, "preview");
			this.ToolbarImageList.InsertGalleryImage("print", "images/print/print_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/print_32x32.png"), 21);
			this.ToolbarImageList.Images.SetKeyName(21, "print");
			this.ToolbarImageList.InsertGalleryImage("config", "images/programming/ide_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/ide_32x32.png"), 22);
			this.ToolbarImageList.Images.SetKeyName(22, "config");
			this.ToolbarImageList.InsertGalleryImage("save", "images/save/save_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_32x32.png"), 23);
			this.ToolbarImageList.Images.SetKeyName(23, "save");
			this.ToolbarImageList.InsertGalleryImage("setting", "images/setup/properties_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/setup/properties_32x32.png"), 24);
			this.ToolbarImageList.Images.SetKeyName(24, "setting");
			this.ToolbarImageList.InsertGalleryImage("query", "images/zoom/zoom_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/zoom/zoom_32x32.png"), 25);
			this.ToolbarImageList.Images.SetKeyName(25, "query");
			this.ToolbarImageList.InsertGalleryImage("idcard", "images/mail/contact_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/contact_32x32.png"), 26);
			this.ToolbarImageList.Images.SetKeyName(26, "idcard");
			this.ToolbarImageList.InsertGalleryImage("browse", "images/support/template_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/template_32x32.png"), 27);
			this.ToolbarImageList.Images.SetKeyName(27, "browse");
			this.ToolbarImageList.InsertGalleryImage("import", "images/data/addnewdatasource_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/data/addnewdatasource_32x32.png"), 28);
			this.ToolbarImageList.Images.SetKeyName(28, "import");
			this.ToolbarImageList.InsertGalleryImage("wizard", "images/miscellaneous/wizard_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/wizard_32x32.png"), 29);
			this.ToolbarImageList.Images.SetKeyName(29, "wizard");
			this.ToolbarImageList.InsertGalleryImage("attach", "images/mail/attach_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/attach_32x32.png"), 30);
			this.ToolbarImageList.Images.SetKeyName(30, "attach");
			this.ToolbarImageList.InsertGalleryImage("disable", "images/actions/remove_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/remove_32x32.png"), 31);
			this.ToolbarImageList.Images.SetKeyName(31, "disable");
			this.ToolbarImageList.InsertGalleryImage("tag", "images/programming/tag_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/programming/tag_32x32.png"), 32);
			this.ToolbarImageList.Images.SetKeyName(32, "tag");
			this.ToolbarImageList.InsertGalleryImage("history", "images/history/historyitem_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/history/historyitem_32x32.png"), 33);
			this.ToolbarImageList.Images.SetKeyName(33, "history");
			this.ToolbarImageList.InsertGalleryImage("gauges", "images/gauges/gaugestylefullcircular_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/gauges/gaugestylefullcircular_32x32.png"), 34);
			this.ToolbarImageList.Images.SetKeyName(34, "gauges");
			this.ToolbarImageList.InsertGalleryImage("insert", "images/actions/insert_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_32x32.png"), 35);
			this.ToolbarImageList.Images.SetKeyName(35, "insert");
			this.ToolbarImageList.InsertGalleryImage("paste", "images/edit/paste_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/paste_32x32.png"), 36);
			this.ToolbarImageList.Images.SetKeyName(36, "paste");
			this.ToolbarImageList.InsertGalleryImage("sendmsg", "images/mail/send_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/send_32x32.png"), 37);
			this.ToolbarImageList.Images.SetKeyName(37, "sendmsg");
			this.ToolbarImageList.InsertGalleryImage("feed", "images/mail/editfeed_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/editfeed_32x32.png"), 38);
			this.ToolbarImageList.Images.SetKeyName(38, "feed");
			this.ToolbarImageList.InsertGalleryImage("image", "images/content/image_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/image_32x32.png"), 39);
			this.ToolbarImageList.Images.SetKeyName(39, "image");
			this.ToolbarImageList.InsertImage(global::Framework.BaseUI.Properties.Resources.car, "car", typeof(global::Framework.BaseUI.Properties.Resources), 40);
			this.ToolbarImageList.Images.SetKeyName(40, "car");
			this.ToolbarImageList.InsertGalleryImage("item", "images/format/listbullets_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/format/listbullets_32x32.png"), 41);
			this.ToolbarImageList.Images.SetKeyName(41, "item");
			this.ToolbarImageList.InsertGalleryImage("chart", "images/chart/stackedbar_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/chart/stackedbar_32x32.png"), 42);
			this.ToolbarImageList.Images.SetKeyName(42, "chart");
			this.ToolbarImageList.InsertGalleryImage("prev", "images/navigation/backward_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/backward_32x32.png"), 43);
			this.ToolbarImageList.Images.SetKeyName(43, "prev");
			this.ToolbarImageList.InsertGalleryImage("next", "images/navigation/forward_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/forward_32x32.png"), 44);
			this.ToolbarImageList.Images.SetKeyName(44, "next");
			this.ToolbarImageList.InsertGalleryImage("down", "images/navigation/next_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/next_32x32.png"), 45);
			this.ToolbarImageList.Images.SetKeyName(45, "down");
			this.ToolbarImageList.InsertGalleryImage("up", "images/navigation/previous_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/previous_32x32.png"), 46);
			this.ToolbarImageList.Images.SetKeyName(46, "up");
			this.ToolbarImageList.InsertGalleryImage("undo", "images/history/undo_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/history/undo_32x32.png"), 47);
			this.ToolbarImageList.Images.SetKeyName(47, "undo");
			// 
			// ToolbarBaseForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1206, 663);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
			this.Name = "ToolbarBaseForm";
			this.Text = "ToolbarBaseForm";
			((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ToolbarImageList)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private DevExpress.XtraBars.BarManager barManager;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarButtonItem barButtonItem1;
		private System.Windows.Forms.Label lblTitle;
		protected DevExpress.XtraBars.Bar MainToolbar;
		private DevExpress.Utils.ImageCollection ToolbarImageList;
	}
}