﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Common
{
	public enum DataPermissionTypes
	{
		无 = 0,
		自己 = 1,
		小组 = 2,
		部门 = 3, //部门和分公司。
		全部 = 10 //总部、总公司。
	}

}
