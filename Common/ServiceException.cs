//==============================================================
//  版权所有：深圳杰文科技
//  文件名：ServiceException.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Infrastructure.Utilities;

namespace Framework.Common
{
	[Serializable]
	public class ServiceException : Exception, ISerializable
	{
		public ServiceException()
			: base()
		{
			SysLogger.WriteError(this);
		}

		public ServiceException(Exception e)
			: base(e.Message)
		{
			SysLogger.WriteError(this);
		}

		public ServiceException(string msg, params object[] args)
			: base(string.Format(msg, args))
		{
			SysLogger.WriteError(this);
		}

		public ServiceException(int code)
			: base()
		{
			this.Code = code;
			SysLogger.WriteError(this);
		}

		protected ServiceException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			SysLogger.WriteError(this);
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		public int Code
		{
			get;
			set;
		}

	}
}
