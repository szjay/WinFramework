//==============================================================
//  版权所有：深圳杰文科技
//  文件名：IDB.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace Framework.Common
{
	public interface IDB : ILogicalThreadAffinative, IDisposable
	{
		int Save();

		void Rollback();

		void Close();

		IUser CurrentUser
		{
			get;
			set;
		}

		IDbServiceContainer ServiceContainer
		{
			get;
			set;
		}

		long ID
		{
			get; set;
		}

	}
}
