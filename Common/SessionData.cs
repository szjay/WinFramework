//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SessionData.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Remoting.Messaging;

namespace Framework.Common
{
	//SessionData是服务器与客户端之间的共享对象，被放入CallContext中，因此要实现ILogicalThreadAffinative接口。
	[Serializable]
	public class SessionData : ILogicalThreadAffinative
	{
		public string SessionId;
		public string Account;
	}
}
