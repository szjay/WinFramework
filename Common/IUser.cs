//==============================================================
//  版权所有：深圳杰文科技
//  文件名：IUser.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace Framework.Common
{
	public interface IUser
	{
		string SessionId
		{
			get;
			set;
		}

		Guid Id
		{
			get;
			set;
		}

		string Account
		{
			get;
			set;
		}

		string Name
		{
			get;
			set;
		}

		bool IsAdmin
		{
			get;
			set;
		}

		Guid DeptId
		{
			get;
			set;
		}

		string DeptName
		{
			get;
			set;
		}

		string IP
		{
			get;
			set;
		}

		int LoginCount
		{
			get;
			set;
		}

		DateTime? LastLoginTime
		{
			get;
			set;
		}

		DataPermissionTypes DataPermissionType
		{
			get;
			set;
		}

	}
}
