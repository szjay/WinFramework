using System;
using System.Runtime.Remoting.Channels;
using System.Collections;
using System.Diagnostics;
using Framework.Common;

namespace Framework.Server
{
	//[DebuggerStepThrough]
	public class CustomClientSinkProvider : IClientChannelSinkProvider
	{
		private IClientChannelSinkProvider _nextProvider;

		public CustomClientSinkProvider()
		{
		}

		public CustomClientSinkProvider(IDictionary properties, ICollection providerData)
		{
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return _nextProvider;
			}
			set
			{
				_nextProvider = value;
			}
		}

		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = _nextProvider.CreateSink(channel, url, remoteChannelData);
			return new CustomClientSink(next);
		}
	}
}
