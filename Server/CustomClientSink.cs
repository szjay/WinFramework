using System;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace Framework.Common
{
	public class CustomClientSink : BaseChannelSinkWithProperties, IClientChannelSink
	{
		private IClientChannelSink _nextSink;

		public CustomClientSink(IClientChannelSink next)
		{
			_nextSink = next;
		}

		public IClientChannelSink NextChannelSink
		{
			get
			{
				return _nextSink;
			}
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, IMessage msg, ITransportHeaders headers, Stream stream)
		{
			//headers["Compress"] = "True";
			//stream = CompressionHelper.GetCompressedStream(stream);
			//sinkStack.Push(this, null);
			_nextSink.AsyncProcessRequest(sinkStack, msg, headers, stream);
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, Stream stream)
		{
			//stream = CompressionHelper.GetUncompressedStream(stream);
			sinkStack.AsyncProcessResponse(headers, stream);
		}

		public Stream GetRequestStream(IMessage msg, ITransportHeaders headers)
		{
			return _nextSink.GetRequestStream(msg, headers);
		}

		public void ProcessMessage(IMessage msg, ITransportHeaders requestHeaders, Stream requestStream, out ITransportHeaders responseHeaders, out Stream responseStream)
		{
			_nextSink.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);

			//requestHeaders["Compress"] = "True";
			//Stream localrequestStream = CompressionHelper.GetCompressedStream(requestStream);
			//_nextSink.ProcessMessage(msg, requestHeaders, localrequestStream, out responseHeaders, out responseStream);
			//responseStream = CompressionHelper.GetUncompressedStream(responseStream);
		}
	}
}
