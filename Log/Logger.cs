//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Logger.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System.IO;
using System.Text;
using Infrastructure.TaskQueue;
using System;

namespace Infrastructure.Log
{
	class TestLogger
	{
		public void Test()
		{
			Logger logger = new Logger("TestLog");
			logger.LogPath = @"f:\temp\a.log";
			logger.WriteLine("【{0}】这是第一个测试", DateTime.Now);
			logger.WriteLine("【{0}】这是第二个测试", DateTime.Now);
			logger.WriteLine("【{0}】这是第三个测试", DateTime.Now);
		}
	}

	public class Logger
	{
		protected TaskManager _TaskManager;
		protected FileStream _FS = null; 

		public Logger(string name)
		{
			_TaskManager = new TaskManager((obj) =>
			{
				if (_FS == null)
				{
					return;
				}
				byte[] bytes = Encoding.UTF8.GetBytes(obj.ToString() + "\r\n");
				_FS.Write(bytes, 0, bytes.Length);
				_FS.Flush();
			}, name);
		}

		protected string _LogPath = "";
		public virtual string LogPath
		{
			get
			{
				return _LogPath;
			}
			set
			{
				if (_LogPath == value)
				{
					return;
				}

				_LogPath = value;

				if (_FS != null)
				{
					_FS.Close();
					_FS.Dispose();
					_FS = null;
				}

				if (string.IsNullOrEmpty(_LogPath))
				{
					return;
				}

				_FS = new FileStream(_LogPath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
			}
		}

		public void WriteLine(string msg, params object[] args)
		{
			msg += "\r\n";
			Write(msg, args);
		}

		public void Write(string msg, params object[] args)
		{
			if (args == null || args.Length == 0)
			{
				RealWrite(msg);
			}
			else
			{
				RealWrite(string.Format(msg, args));
			}
		}

		protected virtual void RealWrite(string msg)
		{
			_TaskManager.Append(msg);
		}

		public void Close()
		{
			if (_FS != null)
			{
				_FS.Close();
				_FS.Dispose();
				_FS = null;
			}

			if (_TaskManager != null)
			{
				_TaskManager.Dispose();
				_TaskManager = null;
			}
		}

	}
}
