//==============================================================
//  版权所有：深圳杰文科技
//  文件名：HourlyLogger.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Infrastructure.Log
{
	/// <summary>
	/// 每小时一个日志文件。
	/// </summary>
	public class HourlyLogger : Logger
	{
		private string _RootPath = "";

		public HourlyLogger(string rootPath, string name)
			: base(name)
		{
			_RootPath = rootPath;
		}

		protected override void RealWrite(string msg)
		{
			if (string.IsNullOrEmpty(_RootPath))
			{
				return;
			}

			string fileName = DateTime.Now.ToString("yyyy-MM-dd_HH") + ".log";
			try
			{
				this.LogPath = Path.Combine(_RootPath, fileName);
				_TaskManager.Append(msg);
			}
			catch
			{

			}
		}
	}
}
