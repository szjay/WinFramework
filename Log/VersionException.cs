//==============================================================
//  版权所有：深圳杰文科技
//  文件名：VersionException.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Infrastructure.Log
{
	[Serializable]
	public class VersionException : Exception, ISerializable
	{
		public VersionException()
			: base()
		{
			SystemLogger.Instance.WriteError(this);
		}

		public VersionException(Exception e)
			: base(e.Message)
		{
			SystemLogger.Instance.WriteError(this);
		}

		public VersionException(string msg, params object[] args)
			: base(string.Format(msg, args))
		{
			SystemLogger.Instance.WriteError(this);
		}

		public VersionException(int code)
			: base()
		{
			this.Code = code;
			SystemLogger.Instance.WriteError(this);
		}

		protected VersionException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			SystemLogger.Instance.WriteError(this);
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		public int Code
		{
			get;
			set;
		}

	}
}
