//==============================================================
//  版权所有：深圳杰文科技
//  文件名：SystemLogger.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Infrastructure.Log
{
	public class SysLogger
	{
		//public static readonly SystemLogger Instance = new SystemLogger();

		private static readonly Logger _Logger = null;

		public static string LogPath = "";

		public static bool IsDebug = false;

		public static bool IsDaily = false;

		static SysLogger()
		{
			if (!Directory.Exists(LogPath))
			{
				LogPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
				if (!Directory.Exists(LogPath))
				{
					Directory.CreateDirectory(LogPath);
				}
			}

			if (IsDaily)
			{
				_Logger = new DailyLogger(LogPath, "DailyLogger");
			}
			else
			{
				_Logger = new HourlyLogger(LogPath, "HourlyLog");
			}
		}

		public static void WriteAction(string id, string action)
		{
			RealWrite(id, action);
		}

		public static void WriteSql(string id, string sql)
		{
			RealWrite(id, sql);
		}

		public static void WriteError(Exception error)
		{
			WriteError("ERROR", error);
		}

		public static void WriteError(string id, Exception error)
		{
			Exception ex = error.InnerException ?? error;
			string content = string.Format("{0}：{1}\r\n{2}", ex.GetType(), ex.Message, ex.StackTrace);

			RealWrite(id, content);
		}

		public static void Write(string msg)
		{
			RealWrite("", msg);
		}

		public static void Write(string id, string msg)
		{
			RealWrite(id, msg);
		}

		private static void RealWrite(string id, string msg)
		{
			msg = string.Format("【{0}({1})】{2}", DateTime.Now.ToString("HH:mm:ss.fff"), id, msg);

			if (IsDebug)
			{
				Debug.WriteLine(msg);
			}
			else
			{
				Console.WriteLine(msg);
			}

			_Logger.Write(msg);
		}

		public void Close()
		{
			if (_Logger != null)
			{
				_Logger.Close();
			}
		}

	}
}
