//==============================================================
//  版权所有：深圳杰文科技
//  文件名：Test.cs
//  版本：V1.0  
//  创建者：Jay  ( QQ： 85363208 )
//  创建时间：2017-11-28 16:18
//  创建描述：
//  修改者：
//  修改时间：
//  修改说明： 
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Infrastructure.Log
{
	internal class Test
	{
		void 简单测试()
		{
			Logger logger = new Logger("test");
			logger.LogPath = @"e:\temp\LogTest.log";
			for (int i = 0; i < 100; i++)
			{
				logger.Write("{0} test{1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), i);
				Thread.Sleep(10);
			}
		}

		void test(params object[] args)
		{

			string s = string.Format(@"Executing 3: SELECT [Extent1].[UserActionPermissionId] AS [UserActionPermissionId], [Extent1].[User] AS [User], [Extent1].[Module] AS [Module], [Extent1].[Action] AS [Action] FROM [dbo].[UserActionPermission] AS [Extent1] WHERE [Extent1].[User] = @p__linq__0 { p__linq__0=[String,-1,Input]康宽林 }", args);
			Console.WriteLine(s);
		}
	}
}
